#!/usr/bin/env python
# -*- coding: utf-8 -*-

# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "Package implementing data structures and algorithms for the manipulation of cellular complexes."
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='cellcomplex',
    version="1.1.0",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Guillaume Cerutti",
    author_email="guillaume.cerutti@inria.fr",
    url='https://gitlab.com/mosaic/cellcomplex',
    license='LGPL-3.0-or-later',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    setup_requires=[
        ],
    install_requires=[
        ],
    tests_require=[
        "coverage",
        "mock",
        "nose",
        ],
    entry_points={},
    keywords='',
    
    test_suite='nose.collector',
    )
# #}
# change setup_kwds below before the next pkglts tag

# setup_kwds['entry_points']['wralea'] = ['mesh = openalea.mesh_oalab_wralea']
# setup_kwds['entry_points']['oalab.applet'] = ['oalab.applet/mesh = openalea.cellcomplex.mesh_oalab.plugin.applet']
# setup_kwds['entry_points']['oalab.plugin'] = ['oalab/oalab.core = openalea.cellcomplex.mesh_oalab.plugin.mimedata']
# setup_kwds['entry_points']['oalab.world'] = ['oalab.world/mesh = openalea.cellcomplex.mesh_oalab.plugin.world']
setup_kwds['entry_points']['console_scripts'] = []
setup_kwds['entry_points']['console_scripts'] += ["cellcomplex_editor = cellcomplex.property_topomesh.app.cellcomplex_editor:main"]

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
