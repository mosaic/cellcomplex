=======
Credits
=======

Coordination
----------------

* Guillaume Cerutti, <guillaume.cerutti@inria.fr>
* Christophe Godin, <christophe.godin@inria.fr>

Development
----------------

.. {# pkglts, doc.authors

* Guillaume Cerutti, <guillaume.cerutti@inria.fr>
* Frederic Boudon, <frederic.boudon@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Guillaume Cerutti <guillaume.cerutti@inria.fr>
* Hadrien Oliveri <hadrien.oliveri@inria.fr>
* revesansparole <revesansparole@gmail.com>
* Florian <florian.gacon@inria.fr>
* Frederic Boudon <frederic.boudon@cirad.fr>
* CERUTTI Guillaume <guillaume.cerutti@inria.fr>
* jonlegra <jonathan.legrand@inria.fr>
* Olli <olivier.ali@inria.fr>
* jlegrand62 <jlegrand62@gmail.com>
* Thibaud Kloczko <thibaud.kloczko@inria.fr>
* LEGRAND Jonathan <jonathan.legrand@inria.fr>
* Jerome Chopard <revesansparole@gmail.com>
* GACON Florian <florian.gacon@inria.fr>
* Christophe Pradal <christophe.pradal@inria.fr>

.. #}
