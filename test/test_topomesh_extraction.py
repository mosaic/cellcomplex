# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################


import unittest
from copy import deepcopy

import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_triangular
from cellcomplex.property_topomesh.extraction import cut_surface_topomesh, surface_dual_topomesh, contiguous_wisps_topomesh
from cellcomplex.property_topomesh.extraction import cell_topomesh, epidermis_topomesh, property_filtering_sub_topomesh
from cellcomplex.property_topomesh.extraction import delaunay_interface_topomesh, star_interface_topomesh
from cellcomplex.property_topomesh.extraction import extruded_2d_topomesh, topomesh_connected_components
from cellcomplex.property_topomesh.extraction import triangulation_add_exterior, triangulation_remove_exterior
from cellcomplex.property_topomesh.optimization import topomesh_triangle_split
from cellcomplex.property_topomesh.example_topomesh import icosahedron_topomesh, truncated_octahedra_layered_grid_topomesh
from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh


class TestExtractPropertyTopomesh(unittest.TestCase):
    '''
    Tests the extraction functionalities.
    '''

    def setUp(self):
        self.topomesh = topomesh_triangle_split(icosahedron_topomesh())
        self.cell_topomesh = truncated_octahedra_layered_grid_topomesh(size=2)
        self.flat_topomesh = hexagonal_grid_topomesh(size=1)

    def tearDown(self):
        pass

    def test_cut_topomesh(self):
        cut_topomesh = cut_surface_topomesh(self.topomesh, z_cut=0)

        compute_topomesh_property(self.topomesh,'vertices',1)
        compute_topomesh_property(self.topomesh,'length',1)
        z_offset = np.percentile(self.topomesh.wisp_property('length', 1).values(), 10)

        assert cut_topomesh.is_valid()
        assert np.isclose(np.min(cut_topomesh.wisp_property('barycenter',0).values()[:,2]),-z_offset,atol=1e-5)

        cut_topomesh = cut_surface_topomesh(self.topomesh, z_cut=0, below=False)

        assert cut_topomesh.is_valid()
        assert np.isclose(np.max(cut_topomesh.wisp_property('barycenter',0).values()[:,2]),z_offset,atol=1e-5)

    def test_dual_topomesh(self):
        dual_topomesh = surface_dual_topomesh(self.topomesh, vertex_placement='voronoi')

        for degree in range(3):
            assert dual_topomesh.nb_wisps(degree) == self.topomesh.nb_wisps(2-degree)
        assert np.all([dual_topomesh.nb_regions(0,v) == 3 for v in dual_topomesh.wisps(0)])

        new_topomesh = surface_dual_topomesh(dual_topomesh)

        for degree in range(3):
            assert new_topomesh.nb_wisps(degree) == self.topomesh.nb_wisps(degree)
            assert list(new_topomesh.wisps(degree)) == list(self.topomesh.wisps(degree))
            if degree>0:
                new_borders = np.sort([list(new_topomesh.borders(degree, w)) for w in self.topomesh.wisps(degree)])
                borders = np.sort([list(self.topomesh.borders(degree, w)) for w in self.topomesh.wisps(degree)])
                assert np.all(new_borders == borders)

    def test_contiguous_wisp_topomesh(self):
        cut_topomesh = cut_surface_topomesh(self.topomesh, z_cut=0)

        assert not np.all(np.sort(list(cut_topomesh.wisps(0)))==np.arange(cut_topomesh.nb_wisps(0)))

        contiguous_topomesh = contiguous_wisps_topomesh(cut_topomesh)

        assert np.all(np.sort(list(contiguous_topomesh.wisps(0)))==np.arange(contiguous_topomesh.nb_wisps(0)))

    def test_cell_topomesh(self):
        compute_topomesh_property(self.cell_topomesh,'volume',3)
        for c in self.cell_topomesh.wisps(3):
            t = cell_topomesh(self.cell_topomesh,[c],preserve_ids=False)
            assert t.nb_borders(3,list(t.wisps(3))[0]) == self.cell_topomesh.nb_borders(3,c)
            t = cell_topomesh(self.cell_topomesh,[c],preserve_ids=True)
            assert np.all(list(t.borders(3,c))==list(self.cell_topomesh.borders(3,c)))
            t = cell_topomesh(self.cell_topomesh,[c],preserve_ids=True, copy_properties=True)
            assert t.wisp_property('volume',3)[c] == self.cell_topomesh.wisp_property('volume',3)[c]

    def test_epidermis_topomesh(self):
        topomesh = epidermis_topomesh(self.cell_topomesh)
        assert np.all([topomesh.nb_regions(2,f)==1 for f in topomesh.wisps(2)])
        topomesh = epidermis_topomesh(self.cell_topomesh,cells=[0])
        assert np.all([topomesh.nb_regions(2,f)==1 for f in topomesh.wisps(2)])

    def test_sub_topomesh(self):
        compute_topomesh_property(self.cell_topomesh,'epidermis',2)

        topomesh = property_filtering_sub_topomesh(self.cell_topomesh,'epidermis',2,filter_range=(1,1))
        e = epidermis_topomesh(self.cell_topomesh)

        for d in range(4):
            assert np.all(np.sort(list(topomesh.wisps(d))) == np.sort(list(e.wisps(d))))

    def test_extrusion(self):
        topomesh = extruded_2d_topomesh(self.flat_topomesh, thickness=1.)
        assert topomesh.nb_wisps(3) == self.flat_topomesh.nb_wisps(3)
        for c in topomesh.wisps(3):
            f = list(self.flat_topomesh.borders(3,c))[0]
            assert topomesh.nb_borders(3,c) == (self.flat_topomesh.nb_borders(2,f)+2)

    def test_star(self):
        assert is_triangular(self.cell_topomesh)
        topomesh = star_interface_topomesh(self.cell_topomesh)
        assert topomesh.nb_wisps(0) == self.cell_topomesh.nb_wisps(0)
        assert is_triangular(topomesh)

    def test_delaunay(self):
        assert not is_triangular(self.flat_topomesh)
        topomesh = delaunay_interface_topomesh(self.flat_topomesh)
        assert topomesh.nb_wisps(0) == self.flat_topomesh.nb_wisps(0)
        assert is_triangular(topomesh)

    def test_connected_components(self):
        compute_topomesh_property(self.flat_topomesh,'barycenter',3)
        for c in [2,3,4]:
            for f in self.flat_topomesh.borders(3,c):
                self.flat_topomesh.remove_wisp(2,f)
            self.flat_topomesh.remove_wisp(3,c)
        components = topomesh_connected_components(self.flat_topomesh, degree=2)
        assert len(components) == 2
        for t in components:
            assert t.nb_wisps(2) == 2

    def test_triangulation_exterior(self):

        triangulation_topomesh = star_interface_topomesh(self.flat_topomesh)
        compute_topomesh_property(triangulation_topomesh,'contour',0)
        contour_vertices = [v for v in triangulation_topomesh.wisps(0)
                              if triangulation_topomesh.wisp_property('contour',0)[v]]
        faces = list(triangulation_topomesh.wisps(2))
        ext = triangulation_add_exterior(triangulation_topomesh)
        for v in contour_vertices:
            assert ext in triangulation_topomesh.region_neighbors(0,v)
        triangulation_remove_exterior(triangulation_topomesh,exterior_vertex=ext)
        assert not ext in triangulation_topomesh.wisps(0)
        assert np.all(list(triangulation_topomesh.wisps(2))==faces)