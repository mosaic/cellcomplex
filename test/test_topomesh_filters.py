# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.filters import topomesh_property_filtering
from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh


class TestPropertyTopomeshFilters(unittest.TestCase):

    def setUp(self):
        self.size = 2
        self.cell_size = 1
        self.topomesh = hexagonal_grid_topomesh(size=self.size, cell_size=self.cell_size)

    def tearDown(self):
        pass

    def test_filter(self):
        displacement = 0.1 * (0.5 - np.random.rand((self.topomesh.nb_wisps(0)), 3))
        self.topomesh.update_wisp_property('barycenter', 0, dict(zip(self.topomesh.wisps(0), self.topomesh.wisp_property('barycenter', 0).values(list(self.topomesh.wisps(0))) + displacement)))

        compute_topomesh_property(self.topomesh, 'length', 1)

        assert not np.all(np.isclose(self.topomesh.wisp_property('length', 1).values(), 1, 1e-2))

        for iteration in range(5):
            topomesh_property_filtering(self.topomesh, 'length', 1)

        assert np.all(np.isclose(self.topomesh.wisp_property('length', 1).values(), 1, 1e-2))



