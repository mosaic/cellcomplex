# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.utils.implicit_surfaces import implicit_surface_topomesh


class TestPropertyTopomeshImplicitSurface(unittest.TestCase):

    def setUp(self):
        self.array = np.zeros((100,100,100),np.uint8)
        self.radius = 25
        coords = np.mgrid[0:100,0:100,0:100]
        distance = np.linalg.norm(np.transpose(coords,(1,2,3,0)) - np.array([50,50,50]),axis=-1)
        self.array[distance < self.radius] = 1

    def tearDown(self):
        pass

    def test_implicit_surface_vtk(self):
        surface_topomesh = implicit_surface_topomesh(self.array,size=(100,100,100),voxelsize=(1,1,1),iso=0.5,smoothing=0,decimation=0,center=True,use_vtk=True)
        assert np.all(np.isclose(np.linalg.norm(surface_topomesh.wisp_property("barycenter",0).values(),axis=1),self.radius,atol=1))

    def test_implicit_surface_skimage(self):
        surface_topomesh = implicit_surface_topomesh(self.array,size=(100,100,100),voxelsize=(1,1,1),iso=0.5,smoothing=0,decimation=0,center=True,use_vtk=False)
        assert np.all(np.isclose(np.linalg.norm(surface_topomesh.wisp_property("barycenter",0).values(),axis=1),self.radius,atol=1))
