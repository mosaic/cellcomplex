# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import os

from cellcomplex.property_topomesh.io import read_ply_property_topomesh
from cellcomplex.property_topomesh.io import read_obj_property_topomesh
from cellcomplex.property_topomesh.io import read_msh_property_topomesh
from cellcomplex.property_topomesh.io import meshread


def test_cellcomplex_ply_reading():
    dirname = os.path.join(os.path.dirname(__file__),"../share/data/")
    filename = os.path.join(dirname, "p194-t4_L1_topomesh.ply")
    topomesh = read_ply_property_topomesh(filename)

    assert topomesh.nb_wisps(0) == 2148


def test_cellcomplex_ply_reading_no_fuse():
    dirname = os.path.join(os.path.dirname(__file__),"../share/data/")
    filename = os.path.join(dirname, "hexagonal_prisms.ply")
    topomesh = read_ply_property_topomesh(filename, fuse_elements=False, fuse_vertices=False)

    assert topomesh.nb_wisps(0) == 92
    assert topomesh.nb_wisps(1) == 3*topomesh.nb_wisps(2)


def test_ply_reading():
    dirname = os.path.join(os.path.dirname(__file__),"../share/data/")
    filename = os.path.join(dirname, "cube.ply")
    topomesh = read_ply_property_topomesh(filename)

    assert topomesh.nb_wisps(0) == 8
    assert topomesh.nb_wisps(2) == 6


def test_obj_reading():
    dirname = os.path.join(os.path.dirname(__file__),"../share/data/")
    filename = os.path.join(dirname, "icosahedron.obj")
    topomesh = read_obj_property_topomesh(filename)

    assert topomesh.nb_wisps(0) == 12
    assert topomesh.nb_wisps(2) == 20


def test_msh_reading():
    dirname = os.path.join(os.path.dirname(__file__),"../share/data/")
    filename = os.path.join(dirname, "rectangle.msh")
    topomesh = read_msh_property_topomesh(filename)

    assert topomesh.nb_wisps(0) == 20
    assert topomesh.nb_wisps(2) == 24


def test_generic_reading():
    dirname = os.path.join(os.path.dirname(__file__),"../share/data/")

    filename = os.path.join(dirname, "cube.ply")
    topomesh = read_ply_property_topomesh(filename)

    assert topomesh.nb_wisps(0) == 8

    filename = os.path.join(dirname, "icosahedron.obj")
    topomesh = meshread(filename)

    assert topomesh.nb_wisps(0) == 12

    filename = os.path.join(dirname, "rectangle.msh")
    topomesh = meshread(filename)

    assert topomesh.nb_wisps(0) == 20

