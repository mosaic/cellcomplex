# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.morphology import topomesh_binary_property_fill_holes, topomesh_binary_property_morphological_operation, topomesh_property_median_filtering
from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property


class TestPropertyTopomeshMorphology(unittest.TestCase):

    def setUp(self):
        self.topomesh = hexagonal_grid_topomesh(size=1)
        compute_topomesh_property(self.topomesh,'barycenter',2)
        self.topomesh.update_wisp_property('binary',2,{f:1 for f in self.topomesh.wisps(2)})
        self.topomesh.update_wisp_property('scalar',2,{f:1. for f in self.topomesh.wisps(2)})

    def tearDown(self):
        pass

    def test_fill_holes(self):
        self.topomesh.wisp_property('binary',2)[3] = 0
        assert np.sum(self.topomesh.wisp_property('binary',2).values()==1) == 6
        topomesh_binary_property_fill_holes(self.topomesh,'binary',2)
        assert np.sum(self.topomesh.wisp_property('binary',2).values()==1) == 7

    def test_erosion(self):
        self.topomesh.wisp_property('binary', 2)[3] = 0
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 6
        topomesh_binary_property_morphological_operation(self.topomesh, 'binary', 2, method='erosion')
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 0

    def test_dilation(self):
        self.topomesh.update_wisp_property('binary',2,{f:0 for f in self.topomesh.wisps(2)})
        self.topomesh.wisp_property('binary', 2)[3] = 1
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 1
        topomesh_binary_property_morphological_operation(self.topomesh, 'binary', 2, method='dilation')
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 7

    def test_closing(self):
        self.topomesh.wisp_property('binary', 2)[3] = 0
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 6
        topomesh_binary_property_morphological_operation(self.topomesh, 'binary', 2, method='closing')
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 7

    def test_opening(self):
        self.topomesh.wisp_property('binary', 2)[3] = 0
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 6
        topomesh_binary_property_morphological_operation(self.topomesh, 'binary', 2, method='opening')
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 0

    def test_median_label(self):
        self.topomesh.wisp_property('binary', 2)[3] = 0
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 6
        topomesh_property_median_filtering(self.topomesh, 'binary', 2, mode='label')
        assert np.sum(self.topomesh.wisp_property('binary', 2).values() == 1) == 7

    def test_median_scalar(self):
        self.topomesh.wisp_property('scalar', 2)[3] = 0.
        assert np.sum(self.topomesh.wisp_property('scalar', 2).values() == 1) == 6
        topomesh_property_median_filtering(self.topomesh, 'scalar', 2, mode='scalar')
        assert np.sum(self.topomesh.wisp_property('scalar', 2).values() == 1) == 7
