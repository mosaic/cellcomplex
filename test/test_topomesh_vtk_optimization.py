# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

try:
    import vtk
except:
    pass
else:
    from cellcomplex.property_topomesh.example_topomesh import sphere_topomesh
    from cellcomplex.property_topomesh.optimization import topomesh_triangle_split

    from cellcomplex.property_topomesh.utils.vtk_optimization_tools import property_topomesh_vtk_smoothing_decimation


    class TestPropertyTopomeshImplicitSurface(unittest.TestCase):

        def setUp(self):
            self.topomesh = sphere_topomesh()
            self.initial_faces = self.topomesh.nb_wisps(2)
            self.topomesh = topomesh_triangle_split(self.topomesh)

        def tearDown(self):
            pass

        def test_vtk_decimation(self):
                decimated_topomesh = property_topomesh_vtk_smoothing_decimation(self.topomesh,smoothing=0,decimation=4)
                assert decimated_topomesh.nb_wisps(2) == self.initial_faces
