# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.optimization import property_topomesh_isotropic_remeshing, topomesh_face_star_split
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import star_interface_topomesh
from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh


class TestPropertyTopomeshOptimization(unittest.TestCase):

    def setUp(self):
        self.topomesh = star_interface_topomesh(hexagonal_grid_topomesh(size=2))
        compute_topomesh_property(self.topomesh,'length',1)

    def tearDown(self):
        pass

    def test_isotropic_remeshing(self):
        optimized_topomesh = property_topomesh_isotropic_remeshing(self.topomesh,maximal_length=1.,iterations=5)
        compute_topomesh_property(optimized_topomesh,'vertices',1)
        compute_topomesh_property(optimized_topomesh,'length',1)

        assert optimized_topomesh.wisp_property('length',1).values().max() < 1.

    def test_star_split(self):
        split1_topomesh = topomesh_face_star_split(self.topomesh,split_edges=False)
        assert split1_topomesh.nb_wisps(2) == 3*self.topomesh.nb_wisps(2)
        split2_topomesh = topomesh_face_star_split(self.topomesh,split_edges=True)
        assert split2_topomesh.nb_wisps(2) == 6*self.topomesh.nb_wisps(2)
