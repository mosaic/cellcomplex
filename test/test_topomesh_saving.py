# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################


import unittest
import os

import numpy as np

from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh, hexagon_topomesh, circle_voronoi_topomesh

from cellcomplex.property_topomesh.io import read_ply_property_topomesh, save_ply_property_topomesh
from cellcomplex.property_topomesh.io import save_ply_cellcomplex_topomesh

from cellcomplex.property_topomesh.io import save_obj_property_topomesh
from cellcomplex.property_topomesh.io import read_obj_property_topomesh


class TestSavePropertyTopomesh(unittest.TestCase):
    '''
    Tests the save ply + obj functionalities.
    '''

    def setUp(self):
        self.saved_ply_filename = "tmp/hexagons.ply"
        self.saved_obj_filename = "tmp/hexagons.obj"
        self.saved_mtl_filename = "tmp/hexagons.mtl"
        if not os.path.exists("tmp/"):
            os.makedirs("tmp/")

        self.topomesh = hexagonal_grid_topomesh(3)
        self.triangular_topomesh = hexagon_topomesh()
        self.polygonal_topomesh = circle_voronoi_topomesh(size=3)

    def tearDown(self):
        if os.path.exists(self.saved_ply_filename):
            os.remove(self.saved_ply_filename)
        if os.path.exists(self.saved_obj_filename):
            os.remove(self.saved_obj_filename)
        if os.path.exists(self.saved_mtl_filename):
            os.remove(self.saved_mtl_filename)

    def test_ply_saving(self):
        save_ply_property_topomesh(self.topomesh,self.saved_ply_filename)
        assert os.path.exists(self.saved_ply_filename)

    def test_ply_saving_wisps(self):
        save_ply_property_topomesh(self.topomesh, self.saved_ply_filename)

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.nb_wisps(degree) == self.topomesh.nb_wisps(degree)

    def test_ply_saving_barycenter(self):
        save_ply_property_topomesh(self.topomesh, self.saved_ply_filename)

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        assert np.all(read_topomesh.wisp_property('barycenter',0).values() == self.topomesh.wisp_property('barycenter',0).values())

    def test_ply_saving_scalar_property(self):
        for degree in range(4):
            scalar_property = dict(zip(self.topomesh.wisps(degree),np.arange(self.topomesh.nb_wisps(degree))))
            self.topomesh.update_wisp_property('scalar',degree,scalar_property)

        save_ply_property_topomesh(self.topomesh, self.saved_ply_filename, properties_to_save=dict(zip(range(4),[['scalar'] for i in range(4)])))

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.has_wisp_property('scalar',degree)
            assert read_topomesh.wisp_property('scalar',degree).values().ndim == 1
            assert np.all(read_topomesh.wisp_property('scalar',degree).values() == self.topomesh.wisp_property('scalar',degree).values())

    def test_ply_saving_boolean_property(self):
        for degree in range(4):
            boolean_property = dict(zip(self.topomesh.wisps(degree),np.random.rand(self.topomesh.nb_wisps(degree))>0.5))
            self.topomesh.update_wisp_property('boolean',degree,boolean_property)

        save_ply_property_topomesh(self.topomesh, self.saved_ply_filename, properties_to_save=dict(zip(range(4),[['boolean'] for i in range(4)])))

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.has_wisp_property('boolean',degree)
            assert read_topomesh.wisp_property('boolean',degree).values().ndim == 1
            assert np.all(read_topomesh.wisp_property('boolean',degree).values() == self.topomesh.wisp_property('boolean',degree).values())

    def test_ply_saving_vector_property(self):
        for degree in range(4):
            vector_property = dict(zip(self.topomesh.wisps(degree), np.tile(np.arange(self.topomesh.nb_wisps(degree))[:,np.newaxis],(1,3))))
            self.topomesh.update_wisp_property('vector',degree,vector_property)

        save_ply_property_topomesh(self.topomesh, self.saved_ply_filename, properties_to_save=dict(zip(range(4),[['vector'] for i in range(4)])))

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.has_wisp_property('vector',degree)
            assert read_topomesh.wisp_property('vector',degree).values().ndim == 2
            assert np.all(read_topomesh.wisp_property('vector',degree).values() == self.topomesh.wisp_property('vector',degree).values())

    def test_ply_saving_tensor_property(self):
        for degree in range(4):
            tensor_property = dict(zip(self.topomesh.wisps(degree), np.tile(np.arange(self.topomesh.nb_wisps(degree))[:,np.newaxis,np.newaxis],(1,2,3))))
            self.topomesh.update_wisp_property('tensor',degree,tensor_property)

        save_ply_property_topomesh(self.topomesh, self.saved_ply_filename, properties_to_save=dict(zip(range(4),[['tensor'] for i in range(4)])))

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.has_wisp_property('tensor',degree)
            assert read_topomesh.wisp_property('tensor',degree).values().ndim == 3
            assert np.all(read_topomesh.wisp_property('tensor',degree).values() == self.topomesh.wisp_property('tensor',degree).values())

    def test_ply_saving_polygonal(self):
        save_ply_property_topomesh(self.polygonal_topomesh, self.saved_ply_filename)

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.nb_wisps(degree) == self.polygonal_topomesh.nb_wisps(degree)
        assert np.all(read_topomesh.wisp_property('barycenter', 0).values() == self.polygonal_topomesh.wisp_property('barycenter', 0).values())

    def test_ply_saving_polygonal_no_fuse(self):
        save_ply_property_topomesh(self.polygonal_topomesh, self.saved_ply_filename)

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename,fuse_elements=False)

        for degree in [0,2]:
            assert read_topomesh.nb_wisps(degree) == self.polygonal_topomesh.nb_wisps(degree)
        assert np.all(read_topomesh.wisp_property('barycenter', 0).values() == self.polygonal_topomesh.wisp_property('barycenter', 0).values())

    def test_ply_saving_cellcomplex(self):
        save_ply_cellcomplex_topomesh(self.topomesh, self.saved_ply_filename, color_faces=True)
        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        assert np.all(read_topomesh.wisp_property('barycenter',0).values() == self.topomesh.wisp_property('barycenter',0).values())

    def test_ply_saving_cellcomplex_non_oriented(self):
        save_ply_cellcomplex_topomesh(self.topomesh, self.saved_ply_filename, oriented=False)
        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        assert np.all(read_topomesh.wisp_property('barycenter',0).values() == self.topomesh.wisp_property('barycenter',0).values())

    def test_ply_saving_cellcomplex_property(self):
        for degree in range(4):
            boolean_property = dict(zip(self.topomesh.wisps(degree),np.random.rand(self.topomesh.nb_wisps(degree))>0.5))
            self.topomesh.update_wisp_property('boolean',degree,boolean_property)

        save_ply_cellcomplex_topomesh(self.topomesh, self.saved_ply_filename, properties_to_save=dict(zip(range(4),[['boolean'] for i in range(4)])))

        read_topomesh = read_ply_property_topomesh(self.saved_ply_filename)

        for degree in range(4):
            assert read_topomesh.has_wisp_property('boolean',degree)
            assert read_topomesh.wisp_property('boolean',degree).values().ndim == 1
            assert np.all(read_topomesh.wisp_property('boolean',degree).values() == self.topomesh.wisp_property('boolean',degree).values())


    def test_obj_saving_wisps(self):
        save_obj_property_topomesh(self.triangular_topomesh, self.saved_obj_filename)

        read_topomesh = read_obj_property_topomesh(self.saved_obj_filename)

        for degree in range(4):
            assert read_topomesh.nb_wisps(degree) == self.triangular_topomesh.nb_wisps(degree)
        assert np.all(read_topomesh.wisp_property('barycenter', 0).values() == self.triangular_topomesh.wisp_property('barycenter', 0).values())
