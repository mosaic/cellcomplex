# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.analysis import compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.analysis import compute_topomesh_vertex_property_from_cells
from cellcomplex.property_topomesh.analysis import compute_topomesh_face_property_from_cells
from cellcomplex.property_topomesh.analysis import compute_topomesh_cell_property_from_faces

from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh


def test_vertex_face_cell_property():
    topomesh = hexagonal_grid_topomesh(size=2,cell_size=1)
    compute_topomesh_property(topomesh, 'area', 2)

    compute_topomesh_vertex_property_from_faces(topomesh,'area',weighting='uniform')
    assert np.all(np.isclose(topomesh.wisp_property('area', 0).values(), 6.*np.sqrt(3) / 4., 1e-3))

    for method in ['mean','sum']:
        for weighting in ['area','uniform']:
            compute_topomesh_cell_property_from_faces(topomesh,'area',reduce=method,weighting=weighting)
    compute_topomesh_vertex_property_from_cells(topomesh,'area',weighting='uniform')
    assert np.all(np.isclose(topomesh.wisp_property('area', 0).values(), 6.* np.sqrt(3) / 4., 1e-3))

    compute_topomesh_face_property_from_cells(topomesh,'area',weighting='uniform')
    assert np.all(np.isclose(topomesh.wisp_property('area', 2).values(), 6.* np.sqrt(3) / 4., 1e-3))
