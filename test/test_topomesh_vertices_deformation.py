# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np
import scipy.ndimage as nd

from cellcomplex.property_topomesh.optimization import property_topomesh_vertices_deformation, topomesh_triangle_split
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_triangle_properties
from cellcomplex.property_topomesh.extraction import star_interface_topomesh
from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh, hexagon_topomesh, hexagonal_prism_layered_grid_topomesh

np.random.seed(1986)


class TestPropertyTopomeshVerticesDeformation(unittest.TestCase):

    def setUp(self):
        self.hexagon = hexagon_topomesh()

        self.topomesh = star_interface_topomesh(hexagonal_grid_topomesh(size=2, center=np.array([5, 5, 5])))
        compute_topomesh_property(self.topomesh, 'vertices', degree=2)
        compute_topomesh_property(self.topomesh, 'vertices', degree=1)
        points = self.topomesh.wisp_property("barycenter", 0).values()
        self.topomesh.update_wisp_property("barycenter", 0, points + 0.25 * np.random.rand(3 * len(points)).reshape((len(points), 3)))

        # border_edges = np.array(list(self.topomesh.wisps(1)))[np.array([self.topomesh.nb_regions(1,e) == 1 for e in self.topomesh.wisps(1)])]
        border_vertices = np.array(list(self.topomesh.wisps(0)))[np.array([np.any([self.topomesh.nb_regions(1, e) == 1 for e in self.topomesh.regions(0,v)]) for v in self.topomesh.wisps(0)])]

        # is_face_border = [np.any([e in border_edges for e in self.topomesh.borders(2,f)]) for f in self.topomesh.wisps(2)]
        is_face_border = [np.any([v in border_vertices for v in self.topomesh.borders(2,f,2)]) for f in self.topomesh.wisps(2)]
        self.non_border_faces = np.array(list(self.topomesh.wisps(2)))[np.logical_not(is_face_border)]

        compute_topomesh_triangle_properties(self.topomesh)
        assert not np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(), 0, atol=0.01))

        self.gradient_img = np.zeros((10, 10, 10)).astype(np.uint8)
        self.gradient_img[:, :, 4:6] = 255
        self.gradient_derivatives = [nd.gaussian_filter1d(self.gradient_img.astype(float), sigma=1, axis=k, order=1) for k in range(3)]

        self.target_area = 0.5

    def tearDown(self):
        pass

    def test_simple_taubin_smoothing(self):
        self.hexagon.wisp_property("barycenter", 0)[0] += np.array([-0.2, 0.1, 0])
        property_topomesh_vertices_deformation(self.hexagon, omega_forces={'taubin_smoothing': 0.65}, fix_borders=True, iterations=1000, sigma_deformation=1e-2)

        assert np.all(np.isclose(self.hexagon.wisp_property("barycenter", 0)[0], 0, atol=1e-14))

    def test_laplacian_smoothing(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'laplacian_smoothing': 0.33}, iterations=10, sigma_deformation=1e-2)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(self.non_border_faces), 0, atol=0.01))

    def test_gaussian_smoothing(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'gaussian_smoothing': 0.33}, iterations=10, sigma_deformation=1e-2)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(self.non_border_faces), 0, atol=0.01))

    def test_taubin_smoothing(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'taubin_smoothing': 0.33}, iterations=100, sigma_deformation=1e-3)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(self.non_border_faces), 0, atol=0.02))

    def test_global_taubin_smoothing(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'global_taubin_smoothing': 0.33}, iterations=100, sigma_deformation=1e-3)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(self.non_border_faces), 0, atol=0.02))

    def test_curvature_flow_smoothing(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'curvature_flow_smoothing': 0.33}, iterations=100, sigma_deformation=1e-2)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(), 0, atol=1))

    def test_mean_curvature_smoothing(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'mean_curvature_smoothing': 0.33}, iterations=100, sigma_deformation=1e-2)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(), 0, atol=1))

    def test_regularization(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'regularization': 0.1}, iterations=500, sigma_deformation=1e-2)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(), 0, atol=1e-3))

    def test_gradient(self):
        points = self.topomesh.wisp_property("barycenter", 0).values()
        self.topomesh.update_wisp_property("barycenter", 0, points + np.array([0, 0, 2])[np.newaxis] * np.random.rand(3 * len(points)).reshape((len(points), 3)))

        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'gradient': 0.1}, iterations=100, sigma_deformation=1e-1, gradient_derivatives=self.gradient_derivatives)

        assert np.all(np.isclose(self.topomesh.wisp_property("barycenter", 0).values()[:, 2], 5, atol=1e-1))

    def test_area(self):
        compute_topomesh_triangle_properties(self.topomesh)

        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'area': 0.1}, iterations=1000, sigma_deformation=1e-2, target_areas=[self.target_area for f in self.topomesh.wisps(2)])

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("area", 2).values(), self.target_area, atol=1e-3))

class TestPropertyTopomeshVerticesDeformation3D(unittest.TestCase):

    def setUp(self):
        self.topomesh = hexagonal_prism_layered_grid_topomesh(n_layers=2, size=1, triangulate=True)
        self.topomesh = topomesh_triangle_split(self.topomesh)
        points = self.topomesh.wisp_property("barycenter", 0).values()
        self.topomesh.update_wisp_property("barycenter", 0, points + (1 - 2 * np.random.rand(3 * len(points)).reshape((len(points), 3))))
    
        compute_topomesh_triangle_properties(self.topomesh)
        assert not np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(), 0, atol=0.05))

    def tearDown(self):
        pass
    
    def test_regularization(self):
        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'regularization': 0.1}, iterations=500, sigma_deformation=1e-1)

        compute_topomesh_triangle_properties(self.topomesh)
        assert np.all(np.isclose(self.topomesh.wisp_property("eccentricity", 2).values(), 0, atol=0.02))

    def test_interface_planarization(self):
        for cid in self.topomesh.wisps(3):
            for n_cid in self.topomesh.border_neighbors(3, cid):
                if (n_cid < cid) and (not (n_cid, cid) in self.topomesh._interface[3].values()):
                    iid = self.topomesh._interface[3].add((n_cid, cid), None)

        compute_topomesh_property(self.topomesh, 'cell_interface', 2)
        interface_faces = [f for f in self.topomesh.wisps(2)
                           if f in self.topomesh.wisp_property('cell_interface', 2).keys()]
        interface_face_interface = self.topomesh.wisp_property('cell_interface', 2).values(interface_faces)
        interface_face_vertices = self.topomesh.wisp_property('vertices', 2).values(interface_faces)
        interface_vertices = {}
        for i in self.topomesh._interface[3].keys():
            interface_vertices[i] = np.unique(interface_face_vertices[interface_face_interface == i])

        initial_interface_covariance = [np.cov(self.topomesh.wisp_property('barycenter', 0).values(interface_vertices[i]), rowvar=False) for i in self.topomesh._interface[3].keys()]
        initial_eval, _ = np.linalg.eig(initial_interface_covariance)
        initial_eval = np.sort(initial_eval)

        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'planarization': 0.1}, iterations=500, sigma_deformation=1e-1)

        final_interface_covariance = [np.cov(self.topomesh.wisp_property('barycenter', 0).values(interface_vertices[i]), rowvar=False) for i in self.topomesh._interface[3].keys()]
        final_eval, _ = np.linalg.eig(final_interface_covariance)
        final_eval = np.sort(final_eval)

        assert np.all(final_eval[:,0] < 1e-1)

    def test_epidermis_planarization(self):
        for cid in self.topomesh.wisps(3):
            for n_cid in self.topomesh.border_neighbors(3, cid):
                if (n_cid < cid) and (not (n_cid, cid) in self.topomesh._interface[3].values()):
                    iid = self.topomesh._interface[3].add((n_cid, cid), None)

        compute_topomesh_property(self.topomesh,'epidermis',0)
        compute_topomesh_property(self.topomesh,'vertices',3)
        epidermis_vertices = [v for v in self.topomesh.wisp_property('vertices',3)[0]
                                if self.topomesh.wisp_property('epidermis',0)[v]
                                and self.topomesh.wisp_property('barycenter',0)[v][2]>0]

        initial_epidermis_variance = np.var(self.topomesh.wisp_property('barycenter', 0).values(epidermis_vertices)[:,2])

        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'epidermis_planarization': 0.1}, iterations=500, sigma_deformation=1e-1)

        final_epidermis_variance = np.var(self.topomesh.wisp_property('barycenter', 0).values(epidermis_vertices)[:,2])

        assert final_epidermis_variance < initial_epidermis_variance

    def test_convexity(self):
        compute_topomesh_property(self.topomesh, 'faces', 3)
        compute_topomesh_property(self.topomesh, 'volume', 3)
        compute_topomesh_property(self.topomesh, 'area', 2)
        initial_volumes = self.topomesh.wisp_property('volume', 3).values()
        initial_areas = self.topomesh.wisp_property('area', 2).values(self.topomesh.wisp_property('faces', 3).values()).sum(axis=1)
        initial_sphericities = np.power(np.pi, 1 / 3.) * np.power(6 * initial_volumes, 2 / 3.) / initial_areas

        property_topomesh_vertices_deformation(self.topomesh, omega_forces={'convexity': 0.1}, iterations=100, sigma_deformation=1e-1)

        compute_topomesh_property(self.topomesh, 'volume', 3)
        compute_topomesh_property(self.topomesh, 'area', 2)
        final_volumes = self.topomesh.wisp_property('volume', 3).values()
        final_areas = self.topomesh.wisp_property('area', 2).values(self.topomesh.wisp_property('faces', 3).values()).sum(axis=1)
        final_sphericities = np.power(np.pi, 1 / 3.) * np.power(6 * final_volumes, 2 / 3.) / final_areas

        assert final_sphericities[0] > initial_sphericities[0]
