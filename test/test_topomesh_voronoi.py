# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.optimization import property_topomesh_isotropic_remeshing
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import star_interface_topomesh
from cellcomplex.property_topomesh.example_topomesh import circle_voronoi_topomesh, periodic_square_voronoi_topomesh


class TestPropertyTopomeshVoronoi(unittest.TestCase):

    def setUp(self):
        self.size = 3
        self.n_cells = 3*self.size*(self.size-1)+1

    def tearDown(self):
        pass

    def test_voronoi_circle(self):
        topomesh = circle_voronoi_topomesh(size=self.size)
        assert topomesh.nb_wisps(2) == self.n_cells

    def test_voronoi_square(self):
        topomesh = periodic_square_voronoi_topomesh(n_points=self.n_cells)
        assert len(np.unique(topomesh.wisp_property('label',2).values())) == self.n_cells


