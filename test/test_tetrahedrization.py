# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################


import unittest

import numpy as np

from cellcomplex.property_topomesh.creation import tetrahedra_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_tetrahedral

from cellcomplex.property_topomesh.utils.geometry_tools import tetra_geometric_features, triangle_geometric_features

class TestTetrahedrizationTopomesh(unittest.TestCase):
    '''
    Tests the tetrahedra-related functionalities.
    '''

    def setUp(self):
        self.side_length = 1.
        points = {}
        points[0] = self.side_length*np.array([-np.sqrt(3)/2, -1/2., 0.])/np.sqrt(3)
        points[1] = self.side_length*np.array([np.sqrt(3)/2, -1/2., 0.])/np.sqrt(3)
        points[2] = self.side_length*np.array([0., 1., 0.])/np.sqrt(3)
        points[3] = self.side_length*np.array([0., 0., np.sqrt(2)])/np.sqrt(3)
        points[4] = self.side_length*np.array([0., 0., -np.sqrt(2)])/np.sqrt(3)
        self.topomesh = tetrahedra_topomesh([[0,1,2,3],[0,1,2,4]],points)

    def tearDown(self):
        pass

    def test_tetrahedra_topomesh(self):
        assert is_tetrahedral(self.topomesh)

        compute_topomesh_property(self.topomesh,'length',1)
        assert np.all(np.isclose(self.topomesh.wisp_property('length',1).values(), 1., 1e-7))

    def test_tetrahedra_geometrical_properties(self):
        compute_topomesh_property(self.topomesh,'vertices',3)
        tetras = self.topomesh.wisp_property('vertices',3).values()
        positions = self.topomesh.wisp_property('barycenter',0)

        features_to_compute = ['volume','eccentricity','circumscribed_sphere_radius','min_dihedral_angle','max_dihedral_angle']

        features = tetra_geometric_features(tetras, positions, features=features_to_compute)
        features_dict = dict(zip(features_to_compute,features.T))

        assert np.all(np.isclose(features_dict['volume'],
                                 np.power(self.side_length,3) / (6*np.sqrt(2)), 1e-7))
        assert np.all(np.isclose(features_dict['eccentricity'],
                                 0, 1e-7))
        assert np.all(np.isclose(features_dict['circumscribed_sphere_radius'],
                                 self.side_length * np.sqrt(3/8), 1e-7))
        assert np.all(np.isclose(features_dict['min_dihedral_angle'],
                                 np.degrees(np.arccos(1/3)), 1e-7))
        assert np.all(np.isclose(features_dict['max_dihedral_angle'],
                                 np.degrees(np.arccos(1/3)), 1e-7))


    def test_triangle_geometrical_properties(self):
        compute_topomesh_property(self.topomesh,'vertices',2)
        triangles = self.topomesh.wisp_property('vertices',2).values()
        positions = self.topomesh.wisp_property('barycenter',0)

        features_to_compute = ['area', 'perimeter', 'eccentricity']

        features = triangle_geometric_features(triangles, positions, features=features_to_compute)
        features_dict = dict(zip(features_to_compute,features.T))

        assert np.all(np.isclose(features_dict['area'],
                                 np.power(self.side_length,2) * np.sqrt(3)/4, 1e-7))
        assert np.all(np.isclose(features_dict['perimeter'],
                                 self.side_length * 3, 1e-7))
        assert np.all(np.isclose(features_dict['eccentricity'],
                                 0, 1e-7))
