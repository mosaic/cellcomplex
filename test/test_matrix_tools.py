# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################


import unittest

import numpy as np

from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh
from cellcomplex.property_topomesh.extraction import star_interface_topomesh
from cellcomplex.property_topomesh.utils.matrix_tools import laplacian_matrix, vertex_distance_matrix


class TestMatrixToolsTopomesh(unittest.TestCase):
    '''
    Tests the matrix tools functionalities.
    '''

    def setUp(self):
        self.topomesh = hexagonal_grid_topomesh(1)

    def tearDown(self):
        pass

    def test_laplacian_matrix(self):
        lap = laplacian_matrix(self.topomesh, 2)
        expected_result = np.array([[3., -1., -1., -1.,  0.,  0.,  0.],
                                    [-1.,  3.,  0., -1., -1.,  0.,  0.],
                                    [-1.,  0.,  3., -1.,  0., -1.,  0.],
                                    [-1., -1., -1.,  6., -1., -1., -1.],
                                    [0., -1.,  0., -1.,  3.,  0., -1.],
                                    [0.,  0., -1., -1.,  0.,  3., -1.],
                                    [0.,  0.,  0., -1., -1., -1.,  3.]])
        assert np.array_equal(lap, expected_result)

    def test_distance_matrix(self):
        dist = vertex_distance_matrix(self.topomesh,split_faces=False)
        assert dist.max() == 7.
        split_dist = vertex_distance_matrix(self.topomesh,split_faces=True)
        assert np.isclose(split_dist.max(), 2.+2.*np.sqrt(3.), atol=1e-5)