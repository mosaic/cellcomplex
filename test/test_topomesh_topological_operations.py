# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################


import unittest

import numpy as np

from cellcomplex.property_topomesh.topological_operations import topomesh_remove_vertex
from cellcomplex.property_topomesh.topological_operations import topomesh_remove_interface_vertex, topomesh_remove_interface_edge
from cellcomplex.property_topomesh.topological_operations import topomesh_remove_boundary_vertex
from cellcomplex.property_topomesh.topological_operations import topomesh_collapse_edge
from cellcomplex.property_topomesh.topological_operations import topomesh_flip_edge
from cellcomplex.property_topomesh.topological_operations import topomesh_split_vertex, topomesh_split_edge, topomesh_split_triangle
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.example_topomesh import square_topomesh


class TestTopomeshTopologicalOperations(unittest.TestCase):
    """
    Tests the topological operations functionalities.
    """
    
    def setUp(self):
        self.side_length = 1.
        self.topomesh = square_topomesh(self.side_length, triangular=True)

    def tearDown(self):
        pass

    def test_topomesh_vertex_removal(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        assert self.topomesh.nb_wisps(0) == 5
        assert self.topomesh.nb_wisps(2) == 4

        for fid in self.topomesh.wisps(2):
            assert self.topomesh.nb_borders(2, fid) == 3

        topomesh_remove_vertex(self.topomesh, 4)

        assert self.topomesh.nb_wisps(2) == 1

    def test_topomesh_boundary_vertex_removal(self):
        topomesh_split_edge(self.topomesh, 0, split_adjacent_faces=False)
        assert self.topomesh.nb_wisps(0) == 5
        assert self.topomesh.nb_wisps(2) == 2

        topomesh_remove_boundary_vertex(self.topomesh, 4)

        assert self.topomesh.nb_wisps(2) == 2
        for fid in self.topomesh.wisps(2):
            assert self.topomesh.nb_borders(2, fid) == 3

    def test_topomesh_interface_vertex_removal(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        assert self.topomesh.nb_wisps(0) == 5
        assert self.topomesh.nb_wisps(2) == 4

        for fid in self.topomesh.wisps(2):
            assert self.topomesh.nb_borders(2, fid) == 3

        topomesh_remove_interface_vertex(self.topomesh, 4)

        assert self.topomesh.nb_wisps(2) == 1

    def test_topomesh_interface_edge_removal(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        assert self.topomesh.nb_wisps(0) == 5
        assert self.topomesh.nb_wisps(2) == 4

        topomesh_remove_interface_edge(self.topomesh, 6)
        topomesh_remove_interface_edge(self.topomesh, 7)

        assert self.topomesh.nb_wisps(2) == 2

        for fid in self.topomesh.wisps(2):
            assert self.topomesh.nb_borders(2, fid) == 4

        compute_topomesh_property(self.topomesh, 'area', 2)
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 2., 1e-7))

    def test_topomesh_edge_collapse(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        self.topomesh.wisp_property('barycenter',0)[4] = np.array([-0.25, 0.25, 0])

        assert self.topomesh.nb_wisps(2) == 6

        status = topomesh_collapse_edge(self.topomesh, 8)

        assert status
        assert self.topomesh.nb_wisps(2) == 4
        assert np.all(np.isclose(self.topomesh.wisp_property('barycenter',0)[4],
                                 np.array([0, 0, 0]), 1e-7))

        compute_topomesh_property(self.topomesh, 'area', 2)
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 4., 1e-7))

    def test_topomesh_edge_tricky_collapse(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        self.topomesh.wisp_property('barycenter',0)[4] = np.array([-0.25, 0.25, 0])
        topomesh_split_triangle(self.topomesh, 5)

        status = topomesh_collapse_edge(self.topomesh, 8)

        assert status
        assert self.topomesh.nb_wisps(2) == 4
        assert np.all(np.isclose(self.topomesh.wisp_property('barycenter',0)[4],
                                 np.array([0, 0, 0]), 1e-7))

    def test_topomesh_edge_wrong_collapse(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)
        self.topomesh.wisp_property('barycenter',0)[4] = np.array([-0.25, 0.25, 0])

        status = topomesh_collapse_edge(self.topomesh,5)
        assert not status

    def test_topomesh_edge_flip(self):
    
        for fid in self.topomesh.wisps(2):
            assert set(self.topomesh.borders(2, fid, 2)) == set([0, 1, 2]) or set(
                self.topomesh.borders(2, fid, 2)) == set([1, 2, 3])
        compute_topomesh_property(self.topomesh, 'area', 2)
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 2., 1e-7))
    
        topomesh_flip_edge(self.topomesh, 2)
    
        for fid in self.topomesh.wisps(2):
            assert set(self.topomesh.borders(2, fid, 2)) == set([0, 2, 3]) or set(
                self.topomesh.borders(2, fid, 2)) == set([0, 1, 3])
    
        compute_topomesh_property(self.topomesh, 'area', 2)
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 2., 1e-7))

    def test_topomesh_edge_split(self):
        topomesh_split_edge(self.topomesh, 2)
        compute_topomesh_property(self.topomesh, 'area', 2)
    
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 4., 1e-7))

    def test_topomesh_triangle_split(self):
        topomesh_split_triangle(self.topomesh, 0)
        topomesh_split_triangle(self.topomesh, 1)

        assert self.topomesh.nb_wisps(2) == 6

        compute_topomesh_property(self.topomesh, 'area', 2)
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 6., 1e-7))

    def test_topomesh_vertex_split(self):
        topomesh_split_edge(self.topomesh, 2, split_adjacent_faces=True)

        assert self.topomesh.nb_wisps(2) == 4

        topomesh_split_vertex(self.topomesh, 4,
                              splitting_distance=self.side_length*np.sqrt(2)/3,
                              splitting_axis=np.array([1.,1.,0.]))

        assert self.topomesh.nb_wisps(2) == 6

        compute_topomesh_property(self.topomesh, 'area', 2)
        assert np.all(np.isclose(self.topomesh.wisp_property('area', 2).values(),
                                 np.power(self.side_length, 2.) / 6., 1e-7))