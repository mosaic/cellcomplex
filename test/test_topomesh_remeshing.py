# -*- coding: utf-8 -*-
# -*- python -*-

import unittest

import numpy as np

from cellcomplex.property_topomesh.example_topomesh import sphere_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.utils.gmsh_tools import gmsh_remeshing


class TestPropertyTopomeshRemeshing(unittest.TestCase):
    '''
    Tests gmsh remeshing functionalities.
    '''

    def setUp(self):
        self.topomesh = sphere_topomesh()
        self.target_length = 0.1

    def tearDown(self):
        pass

    def test_gmsh_remeshing(self):
        compute_topomesh_property(self.topomesh, 'length', 1)
        compute_topomesh_property(self.topomesh, 'oriented_vertices', 2)
        compute_topomesh_property(self.topomesh, 'oriented_borders', 3)

        points = self.topomesh.wisp_property('barycenter', 0).values(list(self.topomesh.wisps(0)))
        triangle_ids, triangle_orientations = self.topomesh.wisp_property('oriented_borders', 3).values()[0]
        triangles = self.topomesh.wisp_property('oriented_vertices', 2).values(triangle_ids)
        triangles = np.array([t[::o] for t, o in zip(triangles, triangle_orientations)])

        points, triangles = gmsh_remeshing(
            points, triangles, target_length=self.target_length, boundary=False
        )
        remeshed_topomesh = triangle_topomesh(triangles, points)

        compute_topomesh_property(remeshed_topomesh, 'length', 1)
        edge_lengths = remeshed_topomesh.wisp_property('length', 1).values()
        assert np.allclose(
            [np.percentile(edge_lengths, p) for p in [10, 90]],
            self.target_length, rtol=0.5
        )
