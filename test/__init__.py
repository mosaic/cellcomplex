import logging


def setup_package():
    """Some code executed once when test are loaded.
    """
    logging.info("setup package")


def teardown_package():
    """Some code executed once after tests have been played.
    """
    logging.info("teardown")
