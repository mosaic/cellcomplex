# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.example_topomesh import hexagonal_prism_topomesh
from cellcomplex.property_topomesh.composition import append_topomesh

from cellcomplex.utils import array_dict


def test_element_properties():
    topomesh = hexagonal_prism_topomesh()

    for d in range(4):
        if d>0:
            compute_topomesh_property(topomesh,'borders',d)
        if d<3:
            compute_topomesh_property(topomesh,'regions',d)
        compute_topomesh_property(topomesh,'vertices',d)
        compute_topomesh_property(topomesh,'edges',d)
        compute_topomesh_property(topomesh,'faces',d)
        compute_topomesh_property(topomesh,'cells',d)

def test_oriented_properties():
    topomesh = hexagonal_prism_topomesh()
    compute_topomesh_property(topomesh, 'oriented_borders', 2)
    compute_topomesh_property(topomesh, 'oriented_borders', 3)

def test_disconnected_oriented_properties():
    topomesh,_ = append_topomesh(hexagonal_prism_topomesh(),hexagonal_prism_topomesh(center=[20,20,0]),properties_to_append={0:[],1:[],2:[],3:[]})
    cids_to_remove = set()
    for f in topomesh.wisps(2):
        for c in topomesh.regions(2,f):
            if c != 0:
                topomesh.unlink(3,c,f)
                cids_to_remove |= {c}
        if not 0 in topomesh.regions(2,f):
            topomesh.link(3,0,f)
    for c in cids_to_remove:
        topomesh.remove_wisp(3,c)

    compute_topomesh_property(topomesh, 'oriented_borders', 2)
    compute_topomesh_property(topomesh, 'oriented_borders', 3)

