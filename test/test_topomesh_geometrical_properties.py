# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_triangle_properties
from cellcomplex.property_topomesh.analysis import compute_topomesh_vertex_property_from_faces

from cellcomplex.property_topomesh.example_topomesh import square_topomesh, hexagon_topomesh, sphere_topomesh

from cellcomplex.utils import array_dict


def test_length_property():
    side_length = 1.

    topomesh = square_topomesh(side_length)
    compute_topomesh_property(topomesh, 'length', 1)
    for eid in topomesh.wisps(1):
        if topomesh.nb_regions(1, eid) == 1:
            assert np.isclose(topomesh.wisp_property('length', 1)[eid],
                              side_length, 1e-7)
        else:
            assert np.isclose(topomesh.wisp_property('length', 1)[eid],
                              np.sqrt(2) * side_length, 1e-7)

    topomesh = hexagon_topomesh(side_length)
    compute_topomesh_property(topomesh, 'length', 1)
    for eid in topomesh.wisps(1):
        assert np.isclose(topomesh.wisp_property('length', 1)[eid], side_length,
                          1e-7)


def test_length_scaling():
    side_length = 1.
    scale_factor = 2.

    topomesh = hexagon_topomesh(side_length)
    compute_topomesh_property(topomesh, 'length', 1)

    topomesh.update_wisp_property('barycenter', 0, array_dict(
        topomesh.wisp_property('barycenter', 0).values() * scale_factor,
        list(topomesh.wisps(0))))
    side_length *= scale_factor

    compute_topomesh_property(topomesh, 'length', 1)
    for eid in topomesh.wisps(1):
        assert np.isclose(topomesh.wisp_property('length', 1)[eid], side_length, 1e-7)


def test_area_property():
    side_length = 1.

    topomesh = square_topomesh(side_length)
    compute_topomesh_property(topomesh, 'area', 2)
    for fid in topomesh.wisps(2):
        assert np.isclose(topomesh.wisp_property('area', 2)[fid],
                          np.power(side_length, 2.) / 2., 1e-7)

    topomesh = hexagon_topomesh(side_length)
    compute_topomesh_property(topomesh, 'area', 2)
    for fid in topomesh.wisps(2):
        assert np.isclose(topomesh.wisp_property('area', 2)[fid], np.power(side_length, 2.) * np.sqrt(3) / 4., 1e-7)


def test_angle_property():
    side_length = 1.

    topomesh = square_topomesh(side_length)
    compute_topomesh_property(topomesh, 'angles', 2)
    for fid in topomesh.wisps(2):
        angles = topomesh.wisp_property('angles', 2)[fid]
        assert np.isclose(angles,np.pi/2.,1e-5).sum() == 1
        assert np.isclose(angles,np.pi/4.,1e-5).sum() == 2

    topomesh = hexagon_topomesh(side_length)
    compute_topomesh_property(topomesh, 'angles', 2)
    for fid in topomesh.wisps(2):
        assert np.all(np.isclose(topomesh.wisp_property('angles', 2)[fid], np.pi / 3.,1e-5))


def test_eccentricity_property():
    side_length = 1.

    topomesh = hexagon_topomesh(side_length)
    compute_topomesh_property(topomesh, 'eccentricity', 2)
    assert np.all(np.isclose(1 - topomesh.wisp_property('eccentricity', 2).values(), 1.,1e-3))

def test_triangle_property():
    side_length = 1.

    topomesh = hexagon_topomesh(side_length)
    compute_topomesh_triangle_properties(topomesh)
    assert np.all(np.isclose(topomesh.wisp_property('perimeter', 2).values(), 3*side_length,1e-3))


def test_curvature_property():
    radius = 10.

    topomesh = sphere_topomesh(radius)
    compute_topomesh_property(topomesh, 'normal', 2, normal_method='orientation')
    compute_topomesh_vertex_property_from_faces(topomesh, 'normal')
    compute_topomesh_property(topomesh, 'mean_curvature', 2)

    assert np.all(np.isclose(topomesh.wisp_property('mean_curvature', 2).values(),1. / radius, 1e-1))

def test_normal_property():
    radius = 10.

    topomesh = sphere_topomesh(radius)

    for method in ['barycenter','density','orientation']:
        compute_topomesh_property(topomesh, 'normal', 2, normal_method=method)
        normal_dot_products = np.einsum("...ij,...ij->...i", topomesh.wisp_property('normal', 2).values(), topomesh.wisp_property('barycenter', 2).values())
        assert np.all(np.isclose(normal_dot_products/radius,1, 1e-1))

    for weighting in ['uniform','area','angle','cotangent','angular sector']:
        compute_topomesh_vertex_property_from_faces(topomesh, 'normal', weighting=weighting)

    for weighting in ['uniform', 'area']:
        compute_topomesh_vertex_property_from_faces(topomesh, 'normal', weighting=weighting, neighborhood=2)

    normal_dot_products = np.einsum("...ij,...ij->...i", topomesh.wisp_property('normal', 0).values(), topomesh.wisp_property('barycenter', 0).values())
    assert np.all(np.isclose(normal_dot_products/radius,1, 1e-3))


def test_vertex_curvature_property():
    radius = 10.

    topomesh = sphere_topomesh(radius)
    compute_topomesh_property(topomesh, 'normal', 0, normal_method='density')
    compute_topomesh_property(topomesh, 'mean_curvature', 0)
    compute_topomesh_property(topomesh, 'mean_curvature', 3)

    assert np.all(np.isclose(topomesh.wisp_property('mean_curvature', 0).values(),1. / radius, 1e-1))


def test_convexity_property():
    radius = 10.

    topomesh = sphere_topomesh(radius)
    compute_topomesh_property(topomesh, 'convexity', 3)

    assert np.all(np.isclose(topomesh.wisp_property('convexity', 3).values(),1., 1e-5))

