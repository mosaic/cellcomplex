# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################

import unittest

import numpy as np

from cellcomplex.property_topomesh.transformation import topomesh_transformation, spherical_topomesh_2d_projection
from cellcomplex.property_topomesh.example_topomesh import sphere_topomesh


class TestPropertyTopomeshTransformation(unittest.TestCase):

    def setUp(self):
        self.topomesh = sphere_topomesh()

    def tearDown(self):
        pass

    def test_azimuthal_projection(self):
        projected_topomesh = spherical_topomesh_2d_projection(self.topomesh, projection_type='azimuthal')

        assert np.all(projected_topomesh.wisp_property('barycenter',0).values()[:,2]==0)
