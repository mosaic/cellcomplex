import logging
from time import time

import numpy as np
import scipy.ndimage as nd

from multiprocessing import Pool

from cellcomplex.utils import array_dict

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import topomesh_connected_wisps, property_filtering_sub_topomesh


def _max_count(all_values, weights=None):
    values = np.unique(all_values)
    if weights is None:
        weights = np.ones_like(all_values)
    count = nd.sum(weights, all_values, index=values)
    if (count == np.max(count)).sum() == 1:
        return values[np.argmax(count)]
    else:
        max_values = values[count == np.max(count)]
        if all_values[0] in max_values:
            return all_values[0]
        else:
            return max_values[0]


def topomesh_property_median_filtering(topomesh, property_name, degree, mode='label', iterations=1):
    """Filter an existing property by a median-like operator.

    A new value of an existing property is computed for each element of a given
    degree based of the property values of the 1-ring of neighboring elements.
    If the mode is set to scalar the median value is retained, and if the mode
    is set to label, them most represented value is retained.

    Parameters
    --------
        topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
            The structure on which to compute the property.
        property_name : str
            The name of the property to compute (must be already computed on
            the elements).
        degree : int
            The degree of the elements on which to compute the property.
        mode : str
            The method used to compute the median value:
                * 'scalar' : retain the median value of the neighborhood.
                * 'label' : retain the most represnted value of the neighborhood.
        iterations : int
            The number of repeated applications of the median filter.

    Returns
    --------
        None

    Note
    --------
        The PropertyTopomesh passed as argument is updated.

    """
    assert topomesh.has_wisp_property(property_name,degree=degree,is_computed=True)

    start_time = time()
    logging.debug(f"--> Computing median property ({property_name}, {degree})")
    compute_topomesh_property(topomesh,'neighbors',degree)

    wisp_neighbors = topomesh.wisp_property('neighbors',degree).values(list(topomesh.wisps(degree)))

    for iteration in range(iterations):
        wisp_neighbor_properties = topomesh.wisp_property(property_name, degree).values(wisp_neighbors)
        wisp_properties = topomesh.wisp_property(property_name, degree).values(list(topomesh.wisps(degree)))
        wisp_neighborhood_properties = [[p] + list(n_p) for p, n_p in zip(wisp_properties, wisp_neighbor_properties)]

        pool = Pool()
        if mode == 'label':
            median_neighborhood_properties = pool.map(_max_count, wisp_neighborhood_properties)
        elif mode == 'scalar':
            median_neighborhood_properties = pool.map(np.median, wisp_neighborhood_properties)
        else:
            median_neighborhood_properties = wisp_neighbor_properties

        topomesh.update_wisp_property(property_name, degree, dict(zip(topomesh.wisps(degree), median_neighborhood_properties)))
    logging.debug(f"--> Computing median property ({property_name}, {degree}) [{time()-start_time}]")


def topomesh_binary_property_morphological_operation(topomesh, property_name, degree, method='erosion', iterations=1, contour_value=None):
    """

    Parameters
    ----------
        topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
            The structure on which to filter the property.
        property_name : str
            The name of the binary property to filter (must be already computed
            on the elements).
        degree : int
            The degree of the elements on which to filter the property.
        method : str
            The type of morphological operation to perform (erosion, dilation,
            opening or closing).
        iterations : int
            The number of repeated applications of the morphological filter.
        contour_value : bool
            The binary value that is considered to be at the boundary of the
            structure.

    Returns
    --------
        None

    Note
    --------
        The PropertyTopomesh passed as argument is updated.

    """

    assert topomesh.has_wisp_property(property_name, degree, is_computed=True)

    if contour_value is None:
        contour_value = 0 if method in ['erosion', 'opening'] else 1 if method in ['dilation', 'closing'] else 0

    compute_topomesh_property(topomesh, 'contour', degree)
    compute_topomesh_property(topomesh, 'neighbors', degree)

    wisp_ids = list(topomesh.wisps(degree))

    contour_wisp_ids = [w for w in topomesh.wisps(degree) if topomesh.wisp_property('contour', degree)[w]]

    wisp_neighbor_ids = [n for nn in topomesh.wisp_property('neighbors', degree).values(wisp_ids) for n in nn]
    wisp_neighbor_wisp_ids = [w for w in topomesh.wisps(degree) for _ in topomesh.wisp_property('neighbors', degree)[w]]

    for iteration in range(iterations):
        wisp_neighbor_properties = topomesh.wisp_property(property_name, degree).values(wisp_ids + wisp_neighbor_ids)
        wisp_neighbor_properties = np.array(list(wisp_neighbor_properties) + [contour_value for w in contour_wisp_ids])
        if method == 'erosion':
            morpho_property = nd.minimum(wisp_neighbor_properties != 0, wisp_ids + wisp_neighbor_wisp_ids + contour_wisp_ids, index=wisp_ids)
        elif method == 'dilation':
            morpho_property = nd.maximum(wisp_neighbor_properties != 0, wisp_ids + wisp_neighbor_wisp_ids + contour_wisp_ids, index=wisp_ids)
        elif method == 'opening':
            morpho_property = nd.minimum(wisp_neighbor_properties != 0, wisp_ids + wisp_neighbor_wisp_ids + contour_wisp_ids, index=wisp_ids)
            wisp_neighbor_properties = array_dict(morpho_property, keys=wisp_ids).values(wisp_ids + wisp_neighbor_ids)
            wisp_neighbor_properties = np.array(list(wisp_neighbor_properties) + [contour_value for w in contour_wisp_ids])
            morpho_property = nd.maximum(wisp_neighbor_properties != 0, wisp_ids + wisp_neighbor_wisp_ids + contour_wisp_ids, index=wisp_ids)
        elif method == 'closing':
            morpho_property = nd.maximum(wisp_neighbor_properties != 0, wisp_ids + wisp_neighbor_wisp_ids + contour_wisp_ids, index=wisp_ids)
            wisp_neighbor_properties = array_dict(morpho_property, keys=wisp_ids).values(wisp_ids + wisp_neighbor_ids)
            wisp_neighbor_properties = np.array(list(wisp_neighbor_properties) + [contour_value for w in contour_wisp_ids])
            morpho_property = nd.minimum(wisp_neighbor_properties != 0, wisp_ids + wisp_neighbor_wisp_ids + contour_wisp_ids, index=wisp_ids)
        else:
            morpho_property = topomesh.wisp_property(property_name, degree).values(wisp_ids)
        topomesh.update_wisp_property(property_name, degree, dict(zip(wisp_ids, morpho_property)))


def topomesh_binary_property_fill_holes(topomesh, property_name, degree):
    """

    Parameters
    ----------
        topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
            The structure on which to filter the property.
        property_name : str
            The name of the binary property to filter (must be already computed
            on the elements).
        degree : int
            The degree of the elements on which to filter the property.
    Returns
    -------
        None

    Note
    --------
        The PropertyTopomesh passed as argument is updated.

    """

    assert topomesh.has_wisp_property(property_name, degree, is_computed=True)

    compute_topomesh_property(topomesh, 'faces', 1)
    compute_topomesh_property(topomesh, 'contour', 1)
    compute_topomesh_property(topomesh, 'contour', degree)

    hole_topomesh = property_filtering_sub_topomesh(topomesh, property_name, degree, (0, 0))
    hole_wisps = topomesh_connected_wisps(hole_topomesh)
    hole_contour = [topomesh.wisp_property('contour', degree).values(h).any() for h in hole_wisps]
    if np.logical_not(hole_contour).sum() > 0:
        non_contour_hole_wisps = np.concatenate([list(h) for h, c in zip(hole_wisps, hole_contour) if not c])

        binary_property = topomesh.wisp_property(property_name, degree)
        hole_filled_binary_property = {w: p or w in non_contour_hole_wisps for w, p in binary_property.items()}
        topomesh.update_wisp_property(property_name, degree, hole_filled_binary_property)

