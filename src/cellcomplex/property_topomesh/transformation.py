import logging

import numpy as np

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.creation import triangle_topomesh, poly_topomesh
# from cellcomplex.property_topomesh.extraction import clean_topomesh_properties
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

# from cellcomplex.property_topomesh.utils.matching_tools import kd_tree_match as match
# from cellcomplex.property_topomesh.utils.matching_tools import vector_quantization_match as match

from cellcomplex.utils import array_dict

from copy import deepcopy
from time import time as current_time


def transform_points(points, transformation_matrix):
    """

    Parameters
    ----------
    points
    transformation_matrix

    Returns
    -------

    """
    homogeneous_points = np.concatenate([points, np.ones_like(points[:, :1])], axis=1)
    transformed_points = np.einsum("...ij,...j->...i", transformation_matrix, homogeneous_points)
    return transformed_points[:,:3]


def topomesh_transformation(input_topomesh, transformation_matrix=None, center=np.zeros(3), axes=np.eye(3)):
    """

    Parameters
    ----------
    input_topomesh
    transformation_matrix
    center
    axes

    Returns
    -------

    """
    if transformation_matrix is None:
        rigid_transform = np.eye(4, 4)
        rigid_transform[:3, :3] = axes
        rigid_transform[:3, 3] = center
        transformation_matrix = np.linalg.inv(rigid_transform)

    points = input_topomesh.wisp_property('barycenter', 0).values(list(input_topomesh.wisps(0)))
    aligned_points = transform_points(points,transformation_matrix)

    topomesh = deepcopy(input_topomesh)
    topomesh.update_wisp_property('barycenter',0,dict(zip(topomesh.wisps(0), aligned_points)))

    return topomesh


def topomesh_apply_vectorfield_transformation(input_topomesh, vector_field_transform, interpolation='nearest'):
    """

    Parameters
    ----------
    input_topomesh
    vector_field_transform
    interpolation

    Returns
    -------

    """

    topomesh = deepcopy(input_topomesh)

    positions = topomesh.wisp_property('barycenter',0)
    points = positions.values(list(topomesh.wisps(0)))

    vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(),
                                 vector_field_transform.vy.to_spatial_image(),
                                 vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
    voxelsize = np.array(vector_field_transform.vx.to_spatial_image().voxelsize)
    shape = np.array(vector_field_transform.vx.to_spatial_image().shape)

    coords = np.round(points/voxelsize).astype(int)
    coords = np.minimum(shape-1,np.maximum(coords,0))
    coords = tuple(np.transpose(coords))

    if interpolation == 'nearest':
        point_displacement = vector_field[coords]
    else:
        point_displacement = np.zeros_like(points)
        logging.error("Not implemented yet! (interpolation = "+str(interpolation)+")")

    topomesh.update_wisp_property('barycenter',0,dict(zip(topomesh.wisps(0),points+point_displacement)))

    return topomesh


def topomesh_2d_projection(input_topomesh, projection_type="XY"):
    """

    Parameters
    ----------
    input_topomesh
    projection_type

    Returns
    -------

    """
    points = input_topomesh.wisp_property('barycenter', 0).values(list(input_topomesh.wisps(0)))

    topomesh = deepcopy(input_topomesh)
    coords = ['XYZ'.index(dim) for dim in projection_type]
    topomesh.update_wisp_property('barycenter',0,dict(zip(topomesh.wisps(0), np.transpose([points[:, coords[0]], points[:, coords[1]], np.zeros_like(points[:, 0])]))))

    return topomesh


def spherical_topomesh_2d_projection(input_topomesh, center=np.zeros(3), axes=np.eye(3), projection_type='cylindrical'):
    """

    Parameters
    ----------
    input_topomesh
    center
    axes
    projection_type : str ['cylindrical','cassini','azimuthal']

    Returns
    -------

    """
    points = input_topomesh.wisp_property('barycenter', 0).values(list(input_topomesh.wisps(0)))
    homogeneous_points = np.concatenate([points, np.ones_like(points[:, :1])], axis=1)

    rigid_transform = np.eye(4, 4)
    rigid_transform[:3, :3] = axes
    rigid_transform[:3, 3] = center
    alignment_transform = np.linalg.inv(rigid_transform)

    aligned_points = np.einsum("...ij,...j->...i", alignment_transform, homogeneous_points)[:, :3]

    spherical_topomesh = deepcopy(input_topomesh)

    spherical_distances = np.linalg.norm(aligned_points, axis=1)
    spherical_thetas = np.arctan2(aligned_points[:, 1], aligned_points[:, 0])
    spherical_phis = np.arcsin(aligned_points[:, 2] / spherical_distances)

    if projection_type == 'cylindrical':
        spherical_topomesh.update_wisp_property('barycenter', 0, dict(zip(spherical_topomesh.wisps(0), np.transpose([spherical_thetas, spherical_phis, np.zeros_like(spherical_thetas)]))))
    elif projection_type == 'cassini':
        cassini_thetas = np.arcsin(np.cos(spherical_phis) * np.sin(spherical_thetas))
        cassini_phis = np.arctan2(np.tan(spherical_phis), np.cos(spherical_thetas))
        spherical_topomesh.update_wisp_property('barycenter', 0, dict(zip(spherical_topomesh.wisps(0), np.transpose([cassini_thetas, cassini_phis, np.zeros_like(cassini_thetas)]))))
    elif projection_type == 'azimuthal':
        azimuthal_phi_0 = 0
        azimuthal_theta_0 = 0
        azimuthal_alphas = np.sqrt(1. / (1 + np.cos(spherical_phis - azimuthal_phi_0) + np.cos(azimuthal_phi_0) * np.cos(spherical_phis) * (np.cos(spherical_thetas - azimuthal_theta_0) - 1)))
        azimuthal_x = azimuthal_alphas * np.cos(spherical_phis) * np.sin(spherical_thetas - azimuthal_theta_0)
        azimuthal_y = azimuthal_alphas * (np.sin(spherical_phis - azimuthal_phi_0) - np.sin(azimuthal_phi_0) * np.cos(spherical_phis) * (np.cos(spherical_thetas - azimuthal_theta_0) - 1))
        spherical_topomesh.update_wisp_property('barycenter', 0, dict(zip(spherical_topomesh.wisps(0), np.transpose([azimuthal_x, azimuthal_y, np.zeros_like(azimuthal_x)]))))

    return spherical_topomesh

