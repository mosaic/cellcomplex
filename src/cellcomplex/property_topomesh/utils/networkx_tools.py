import logging
import json

import numpy as np
import networkx as nx

from cellcomplex.property_topomesh.analysis import compute_topomesh_property

topological_properties = ['vertices','edges','faces','cells',
                          'borders','regions',
                          'oriented_borders','oriented_vertices',
                          'neighbors','border_neighbors','region_neighbors']


def topomesh_to_graph(topomesh, vertex_properties=None, edge_properties=None):
    G = nx.Graph()

    G.add_nodes_from(list(topomesh.wisps(0)))

    nx.set_node_attributes(G, topomesh.wisp_property('barycenter',0).to_dict(), 'pos')
    if vertex_properties is None:
        vertex_properties = [property_name
                             for property_name in topomesh.wisp_property_names(0)
                             if topomesh.has_wisp_property(property_name, 0, is_computed=True)
                             and not property_name in ['barycenter']+topological_properties]
    for property_name in vertex_properties:
        nx.set_node_attributes(G, topomesh.wisp_property(property_name,0).to_dict(), property_name)

    compute_topomesh_property(topomesh,'vertices',1)
    edge_vertices = topomesh.wisp_property('vertices',1).values(list(topomesh.wisps(1)))
    G.add_edges_from(edge_vertices)

    compute_topomesh_property(topomesh,'length',1)
    if edge_properties is None:
        edge_properties = [property_name
                           for property_name in topomesh.wisp_property_names(1)
                           if topomesh.has_wisp_property(property_name, 1, is_computed=True)
                           and not property_name in ['barycenter']+topological_properties]
    for property_name in edge_properties:
        property_dict = dict(zip([tuple(v) for v in edge_vertices],
                                 topomesh.wisp_property(property_name,1).values(list(topomesh.wisps(1)))))
        nx.set_edge_attributes(G, property_dict, property_name)

    return G


def _json_safe(val):
    if np.ndim(val) == 0 or isinstance(val, str):
        if isinstance(val,bool):
            return int(val)
        elif isinstance(val, np.bool_):
            return int(val)
        elif isinstance(val, np.integer):
            return int(val)
        elif isinstance(val, np.floating):
            return float(val)
        else:
            return val
    elif np.ndim(val) == 1:
        return list(val)
    elif np.ndim(val) == 2:
        return [list(v) for v in val]


def topomesh_to_element_graph(topomesh, degree=0, position_property_name='barycenter', direction='region', node_properties=None, edge_properties=None):
    """

    Parameters
    ----------
    topomesh
    degree
    position_attribute
    direction
    node_properties
    edge_properties

    Returns
    -------

    """

    assert direction in ['region', 'border']
    assert not (direction == 'region' and degree == topomesh.degree())
    assert not (direction == 'border' and degree == 0)

    G = nx.Graph()
    G.add_nodes_from(list(topomesh.wisps(degree)))

    if not topomesh.has_wisp_property(position_property_name, degree, is_computed=True):
        compute_topomesh_property(topomesh, position_property_name, degree)

    position_dict = dict(zip(topomesh.wisps(degree),
                             [_json_safe(topomesh.wisp_property(position_property_name, degree)[w])
                              for w in topomesh.wisps(degree)]))
    nx.set_node_attributes(G, position_dict, 'pos')
    if node_properties is None:
        node_properties = [property_name
                           for property_name in topomesh.wisp_property_names(degree)
                           if topomesh.has_wisp_property(property_name, degree, is_computed=True)
                           and not property_name in ['barycenter']+topological_properties]
    for property_name in node_properties:
        property_dict = dict(zip(topomesh.wisps(degree),
                                 [_json_safe(topomesh.wisp_property(property_name, degree)[w])
                                  for w in topomesh.wisps(degree)]))
        nx.set_node_attributes(G, property_dict, property_name)

    if direction == 'region':
        edge_degree = degree+1
        edge_nodes = [list(topomesh.borders(edge_degree, w)) for w in topomesh.wisps(edge_degree)]
    elif direction == 'border':
        edge_degree = degree-1
        edge_nodes = [list(topomesh.regions(edge_degree, w)) for w in topomesh.wisps(edge_degree)]
    G.add_edges_from(edge_nodes)

    if edge_properties is None:
        edge_properties = [property_name
                           for property_name in topomesh.wisp_property_names(edge_degree)
                           if topomesh.has_wisp_property(property_name, edge_degree, is_computed=True)
                           and not property_name in ['barycenter']+topological_properties]
    for property_name in edge_properties:
        property_dict = dict(zip([tuple(v) for v in edge_nodes],
                                 [_json_safe(topomesh.wisp_property(property_name, edge_degree)[w])
                                  for w in topomesh.wisps(edge_degree)]))
        nx.set_edge_attributes(G, property_dict, property_name)

    return G


def save_json_networkx_graph(G, json_filename):
    data = nx.node_link_data(G)
    json.dump(data, open(json_filename, "w+"), indent=4)


def read_json_networkx_graph(json_filename):
    data = json.load(open(json_filename,"r"))
    return nx.node_link_graph(data)

