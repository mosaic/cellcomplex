import json
import numpy as np


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def is_iterable(s):
    try:
        l = eval(s)
        return isinstance(l,list) or isinstance(l,tuple)
    except:
        return False


def keys_to_number(d):
    if isinstance(d, dict):
        new_d = {}
        for k,v in d.items():
            if is_int(k):
                new_k = int(k)
            elif is_float(k):
                new_k = float(k)
            elif is_iterable(k):
                new_k = eval(k)
            else:
                new_k = k
            new_d[new_k] = keys_to_number(v)
        return new_d
    elif isinstance(d, list) and len(d)==2:
        if np.any([isinstance(v, dict) for v in d]):
            return [keys_to_number(v) for v in d]
        else:
            return d
    else:
        return d


def keys_to_string(d):
    if isinstance(d, dict):
        new_d = {}
        for k,v in d.items():
            new_d[str(k)] = keys_to_string(v)
        return new_d
    elif isinstance(d, list) and len(d)==2:
        if np.any([isinstance(v, dict) for v in d]):
            return [keys_to_string(v) for v in d]
        else:
            return d
    else:
        return d


def values_array_to_list(d):
    if isinstance(d, dict):
        new_d = {}
        for k,v in d.items():
            new_d[k] = values_array_to_list(v)
        return new_d
    elif isinstance(d, list):
        if np.any([isinstance(v, (dict,np.ndarray)) for v in d]):
            return [values_array_to_list(v) for v in d]
        else:
            return d
    else:
        if isinstance(d,np.ndarray):
            if d.ndim == 1:
                return list(d)
            else:
                return values_array_to_list(list(d))
        else:
            return d


def save_json_dict(dictionary, json_filename):
    json_file = open(json_filename, 'w+')
    dictionary_to_write = keys_to_string(values_array_to_list(dictionary))
    json.dump(dictionary_to_write, json_file, sort_keys=True, indent=4)
    json_file.close()


def read_json_dict(json_filename):
    json_file = open(json_filename, 'r')
    read_dictionary = json.load(json_file)
    return keys_to_number(read_dictionary)
