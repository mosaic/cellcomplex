import logging

import numpy as np
import pandas as pd


def topomesh_to_dataframe(topomesh, degree=3, scalar_only=True, properties=None):
    """

    Parameters
    ----------
    topomesh
    degree
    scalar_only
    properties

    Returns
    -------

    """

    topological_properties = []
    topological_properties += ['borders','regions','oriented_borders','oriented_border_components']
    topological_properties += ['vertices','edges','faces','cells','oriented_vertices']

    if properties is None:
        properties = [p for p in topomesh.wisp_properties(degree).keys() if not p in topological_properties]

    dataframe = pd.DataFrame()
    dataframe['id'] = np.array(list(topomesh.wisps(degree)))

    for property_name in properties:
        if (not scalar_only) or np.array(topomesh.wisp_property(property_name,degree).values()[0]).ndim == 0:
            logging.debug("  --> Adding column "+str(property_name))
            property_dict = topomesh.wisp_property(property_name,degree)
            dataframe[property_name] = [property_dict[w] if w in property_dict.keys()
                                        else np.nan*np.ones_like(list(property_dict.values())[0])
                                        for w in dataframe['id'].values]
    
    if topomesh.has_wisp_property('barycenter',degree):
        for i,k in enumerate(['x','y','z']):
             dataframe['center_'+k] = topomesh.wisp_property('barycenter',degree).values(dataframe['id'].values)[:,i]

    dataframe = dataframe.set_index('id')
    dataframe.index.name = None
        
    return dataframe


def normalized_data_range(data_range, dataframe, property_name):
    return tuple([100.*(s-dataframe[property_name].min())/(dataframe[property_name].max()-dataframe[property_name].min()) for s in data_range])
        


