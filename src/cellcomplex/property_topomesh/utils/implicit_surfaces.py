# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2015-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import logging

import numpy as np
from scipy.cluster.vq import kmeans, vq

from cellcomplex.property_topomesh.creation import triangle_topomesh

from cellcomplex.utils import array_dict


def implicit_surface(density_field,size,voxelsize,iso=0.5):
    try:
        from skimage.measure import marching_cubes as skimage_marching_cubes
    except:
        from skimage.measure import marching_cubes_lewiner as skimage_marching_cubes
    surface_points, surface_triangles, _, _ = skimage_marching_cubes(density_field,iso)
    
    surface_points = (np.array(surface_points))*(size*voxelsize/np.array(density_field.shape)) - size*voxelsize/2.

    points_ids = np.arange(len(surface_points))
    points_to_delete = []
    for p,point in enumerate(surface_points):
        matching_points = np.sort(np.where(vq(surface_points,np.array([point]))[1] == 0)[0])
        if len(matching_points) > 1:
            points_to_fuse = matching_points[1:]
            for m_p in points_to_fuse:
                surface_triangles[np.where(surface_triangles==m_p)] = matching_points[0]
                points_to_delete.append(m_p)

    points_to_delete = np.unique(points_to_delete)
    if len(points_to_delete) > 0:
        print(len(points_to_delete), "points deleted")
        surface_points = np.delete(surface_points, points_to_delete,0)
        points_ids = np.delete(points_ids,points_to_delete,0)
        surface_triangles = array_dict(np.arange(len(surface_points)),points_ids).values(surface_triangles)

    for p,point in enumerate(surface_points):
        matching_points = np.where(vq(surface_points,np.array([point]))[1] == 0)[0]
        if len(matching_points) > 1:
            print(p,point)
            input()

    triangles_to_delete = []
    for t,triangle in enumerate(surface_triangles):
        if len(np.unique(triangle)) < 3:
            triangles_to_delete.append(t)
        # elif triangle.max() >= len(surface_points):
        #     triangles_to_delete.append(t)

    if len(triangles_to_delete) > 0:
        surface_triangles = np.delete(surface_triangles,triangles_to_delete,0)

    return surface_points, surface_triangles


def marching_cubes(field,iso=0.5):
    try:
        from skimage.measure import marching_cubes as skimage_marching_cubes
    except:
        from skimage.measure import marching_cubes_lewiner as skimage_marching_cubes
    surface_points, surface_triangles, _, _ = skimage_marching_cubes(field, iso)

    return surface_points, surface_triangles


def vtk_marching_cubes(field, iso=0.5, smoothing=2, decimation=2):

    import vtk
    from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type
    from visu_core.vtk.utils.image_tools import vtk_image_data_from_image

    def SetInput(obj, _input):
        if vtk.VTK_MAJOR_VERSION <= 5:
            obj.SetInput(_input)
        else:
            obj.SetInputData(_input)

    int_field = (np.minimum(np.array(field)*255,255)).astype(np.uint8)
    field_data = vtk.vtkImageData()
    field_array = numpy_to_vtk(int_field.transpose(2,1,0).ravel(), deep=True, array_type=get_vtk_array_type(int_field.dtype))
    field_data.SetDimensions(int_field.shape)
    field_data.GetPointData().SetScalars(field_array)

    contour = vtk.vtkImageMarchingCubes()
    SetInput(contour, field_data)
    contour.ComputeNormalsOn()
    contour.ComputeGradientsOn()
    contour.SetValue(0,int(iso*255))
    contour.Update()

    print("Marching cubes :",contour.GetOutput().GetNumberOfPoints())

    if smoothing>0:
        smoother = vtk.vtkWindowedSincPolyDataFilter()
        SetInput(smoother,contour.GetOutput())
        smoother.BoundarySmoothingOn()
        smoother.FeatureEdgeSmoothingOn()
        smoother.SetFeatureAngle(120.0)
        smoother.SetPassBand(1)
        smoother.SetNumberOfIterations(smoothing)
        # smoother.NonManifoldSmoothingOn()
        smoother.NormalizeCoordinatesOn()
        smoother.Update()

        print("Smoothing :",smoother.GetOutput().GetNumberOfPoints())

    if decimation>0:
        # decimate = vtk.vtkQuadricClustering()
        decimate = vtk.vtkQuadricDecimation()
        # decimate = vtk.vtkDecimatePro()

        if smoothing>0:
            SetInput(decimate,smoother.GetOutput())
        else:
            SetInput(decimate,contour.GetOutput())
        decimate.SetTargetReduction(1. - 1/float(decimation))
        # decimate.SetNumberOfDivisions(int(decimation),int(decimation),int(decimation))
        # decimate.SetFeaturePointsAngle(60.0)
        decimate.Update()

        print("Decimation :",decimate.GetOutput().GetNumberOfPoints())

    if decimation>0:
        field_polydata = decimate.GetOutput()
    elif smoothing>0:
        field_polydata = smoother.GetOutput()
    else:
        field_polydata = contour.GetOutput()

    polydata_points = vtk_to_numpy(field_polydata.GetPoints().GetData())
    polydata_triangles = vtk_to_numpy(field_polydata.GetPolys().GetData())
    polydata_triangles = polydata_triangles.reshape((field_polydata.GetNumberOfCells(), 4))[:, 1:]

    return polydata_points, polydata_triangles


def implicit_surface_topomesh(density_field,size,voxelsize,smoothing=1,decimation=2,iso=0.5,center=True,use_vtk=True):
    """

    Parameters
    ----------
    density_field
    size
    voxelsize
    smoothing
    decimation
    iso
    center

    Returns
    -------

    """

    if use_vtk:
        try:
            import vtk
        except:
            logging.error("Can not use VTK to perform Marching Cubes!")
            use_vtk = False

    if use_vtk:
        surface_points, surface_triangles = vtk_marching_cubes(density_field, iso, smoothing=smoothing, decimation=decimation)
    else:
        try:
            from skimage.measure import marching_cubes as skimage_marching_cubes
        except:
            from skimage.measure import marching_cubes_lewiner as skimage_marching_cubes
        surface_points, surface_triangles, _, _ = skimage_marching_cubes(density_field, iso)

    surface_points = (np.array(surface_points))*(np.array(size)*np.array(voxelsize)/np.array(density_field.shape))
    if center:
        surface_points -= np.array(density_field.shape)*np.array(voxelsize)/2.

    surface_topomesh = triangle_topomesh(surface_triangles,surface_points)

    return surface_topomesh


def spherical_density_function(positions,sphere_radius,k=0.1):
    
    def density_func(x,y,z):
        density = np.zeros_like(x+y+z,float)
        max_radius = sphere_radius
        # max_radius = 0.

        for p in positions.keys():
            cell_distances = np.power(np.power(x-positions[p][0],2) + np.power(y-positions[p][1],2) + np.power(z-positions[p][2],2),0.5)
            density += 1./2. * (1. - np.tanh(k*(cell_distances - (sphere_radius+max_radius)/2.)))
        return density
    return density_func


def point_spherical_density(positions,points,sphere_radius=5,k=1.0):
    return spherical_density_function(positions,sphere_radius=sphere_radius,k=k)(points[:,0],points[:,1],points[:,2])

