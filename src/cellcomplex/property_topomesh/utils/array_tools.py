# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2015-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import numpy as np
from scipy import ndimage as nd

from cellcomplex.utils.array_dict import isiterable

# def array_unique(array,return_index=False):
#   _,unique_rows = np.unique(np.ascontiguousarray(array).view(np.dtype((np.void,array.dtype.itemsize * array.shape[1]))),return_index=True)
#   if return_index:
#     return array[unique_rows],unique_rows
#   else:
#     return array[unique_rows]


def where_list(array, values):
    mask = nd.sum(np.ones_like(values),values,index=array)
    return np.where(mask>0)


def array_difference(array, subarray):
    return np.array(list(set(array).difference(set(subarray))))


def weighted_percentile(values, percentiles, sample_weight=None, values_sorted=False):
    values = np.array(values)
    percentiles = np.array(percentiles)
    if sample_weight is None:
        sample_weight = np.ones(len(values))
    sample_weight = np.array(sample_weight)
    assert np.all(percentiles >= 0) and np.all(percentiles <= 100), 'percentiles should be in [0, 1]'

    if not values_sorted:
        sorter = np.argsort(values)
        values = values[sorter]
        sample_weight = sample_weight[sorter]

    weighted_percentiles = 100.*(np.cumsum(sample_weight) - 0.5*sample_weight)/np.sum(sample_weight)
    return np.interp(percentiles, weighted_percentiles, values)


def make_array(a):
    is_iterable = isiterable(a)
    if is_iterable and len(a) == 0:
        array = np.array(a)
        is_ragged = False
    else:
        is_nested = is_iterable and isiterable(a[0])
        is_ragged = is_nested and len(set(map(len, a))) > 1
        data_type = object if is_ragged else None
        if is_iterable:
            array = np.array(list(a), dtype=data_type)
        else:
            array = np.array([a])
    return array, is_ragged
