import numpy as np

from scipy.cluster.vq import vq


tetra_triangle_edge_list = np.array([[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]])
tetra_triangle_list = np.array([[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]])
triangle_edge_list = np.array([[1, 2], [0, 2], [0, 1]])


def triangles_from_adjacency_edges(edges, positions=None):
    edges = np.sort(edges)

    edge_triangles = np.concatenate([[tuple(e) + (n,) for n in edges[:, 1][np.where(edges[:, 0] == e[0])]] for e in edges])

    edge_triangles = edge_triangles[np.where((edge_triangles[:, 1] > edge_triangles[:, 0]) & (edge_triangles[:, 2] > edge_triangles[:, 1]))]
    corresponding_triangles_1 = vq(edge_triangles[:, 1:], edges)
    corresponding_triangles_2 = vq(np.concatenate([edge_triangles[:, :1], edge_triangles[:, 2:]], axis=1), edges)
    edge_triangles = edge_triangles[np.where((corresponding_triangles_1[1] == 0) & (corresponding_triangles_2[1] == 0))]

    return edge_triangles


def tetrahedra_from_triangulation(triangulation_triangles, positions, exterior=True):
    """
    Builds a list of tetrahedra from a list of triangles describing a valid tetrahedrization
    Handles the case of 5-cliques by removing appearing tetrahedra that are composed of smaller ones
    """
    triangulation_tetrahedra = np.concatenate(
        [[tuple(c) + (n,) for n in triangulation_triangles[:, 2][np.where((triangulation_triangles[:, 0] == c[0]) & (triangulation_triangles[:, 1] == c[1]))]] for c in triangulation_triangles],
        axis=0)
    triangulation_tetrahedra = triangulation_tetrahedra[np.where(
        (triangulation_tetrahedra[:, 1] > triangulation_tetrahedra[:, 0]) & (triangulation_tetrahedra[:, 2] > triangulation_tetrahedra[:, 1]) & (
                    triangulation_tetrahedra[:, 3] > triangulation_tetrahedra[:, 2]))]
    corresponding_tetrahedra_1 = vq(triangulation_tetrahedra[:, 1:], triangulation_triangles)
    corresponding_tetrahedra_2 = vq(np.concatenate([triangulation_tetrahedra[:, :1], triangulation_tetrahedra[:, 2:]], axis=1), triangulation_triangles)
    corresponding_tetrahedra_3 = vq(np.concatenate([triangulation_tetrahedra[:, :2], triangulation_tetrahedra[:, 3:]], axis=1), triangulation_triangles)
    triangulation_tetrahedra = triangulation_tetrahedra[np.where((corresponding_tetrahedra_1[1] == 0) & (corresponding_tetrahedra_2[1] == 0) & (corresponding_tetrahedra_3[1] == 0))]
    # print triangulation_tetrahedra.shape[0]," cell vertices"

    if triangulation_tetrahedra.shape[0] > 0:
        triangulation_5_cliques = np.concatenate([[tuple(c) + (n,) for n in triangulation_tetrahedra[:, 3][
            np.where((triangulation_tetrahedra[:, 0] == c[0]) & (triangulation_tetrahedra[:, 1] == c[1]) & (triangulation_tetrahedra[:, 2] == c[2]))]] for c in triangulation_tetrahedra], axis=0)
        corresponding_5_cliques = vq(triangulation_5_cliques[:, 1:], triangulation_tetrahedra)
        triangulation_5_cliques = triangulation_5_cliques[np.where(corresponding_5_cliques[1] == 0)]

        tetrahedra_triangle_matching = np.array(np.concatenate([vq(triangulation_tetrahedra[:, 1:], triangulation_triangles),
                                                                vq(np.concatenate([triangulation_tetrahedra[:, 0][:, np.newaxis], triangulation_tetrahedra[:, 2:]], axis=1), triangulation_triangles),
                                                                vq(np.concatenate([triangulation_tetrahedra[:, :2], triangulation_tetrahedra[:, -1][:, np.newaxis]], axis=1), triangulation_triangles),
                                                                vq(triangulation_tetrahedra[:, :-1], triangulation_triangles)], axis=1), int)
        n_5_cliques = 0
        for q in triangulation_5_cliques:
            clique_tetrahedra_vertices = np.array([q[:4], np.concatenate([q[:3], q[4:]]), np.concatenate([q[:2], q[3:]]), np.concatenate([q[:1], q[2:]]), q[1:]])

            tetra_matching = vq(clique_tetrahedra_vertices, triangulation_tetrahedra)
            clique_tetrahedra_vertices = clique_tetrahedra_vertices[np.where(tetra_matching[1] == 0)]
            clique_tetrahedra_indices = tetra_matching[0][np.where(tetra_matching[1] == 0)]
            clique_tetrahedra = positions.values(clique_tetrahedra_vertices)
            clique_tetra_matrix = np.transpose(np.array([clique_tetrahedra[:, 1], clique_tetrahedra[:, 2], clique_tetrahedra[:, 3]]) - clique_tetrahedra[:, 0], axes=(1, 2, 0))
            clique_tetra_volume = abs(np.linalg.det(clique_tetra_matrix)) / 6.0

            if len(clique_tetra_volume) > 3:
                n_5_cliques += 1
                max_tetra = clique_tetrahedra_indices[np.argmax(clique_tetra_volume)]
                t0 = triangulation_tetrahedra[max_tetra]
                triangulation_tetrahedra = np.delete(triangulation_tetrahedra, max_tetra, 0)
                triangulation_5_cliques = np.concatenate([[tuple(c) + (n,) for n in triangulation_tetrahedra[:, 3][
                    np.where((triangulation_tetrahedra[:, 0] == c[0]) & (triangulation_tetrahedra[:, 1] == c[1]) & (triangulation_tetrahedra[:, 2] == c[2]))]] for c in triangulation_tetrahedra],
                                                         axis=0)
                corresponding_5_cliques = vq(triangulation_5_cliques[:, 1:], triangulation_tetrahedra)
                triangulation_5_cliques = triangulation_5_cliques[np.where(corresponding_5_cliques[1] == 0)]
        triangulation_5_cliques = np.concatenate([[tuple(c) + (n,) for n in triangulation_tetrahedra[:, 3][
            np.where((triangulation_tetrahedra[:, 0] == c[0]) & (triangulation_tetrahedra[:, 1] == c[1]) & (triangulation_tetrahedra[:, 2] == c[2]))]] for c in triangulation_tetrahedra], axis=0)
        corresponding_5_cliques = vq(triangulation_5_cliques[:, 1:], triangulation_tetrahedra)
        triangulation_5_cliques = triangulation_5_cliques[np.where(corresponding_5_cliques[1] == 0)]
        # print n_5_cliques," 5-Cliques remaining"

        if exterior:
            tetrahedra_triangle_matching = np.array(np.concatenate([vq(triangulation_tetrahedra[:, 1:], triangulation_triangles),
                                                                    vq(np.concatenate([triangulation_tetrahedra[:, 0][:, np.newaxis], triangulation_tetrahedra[:, 2:]], axis=1),
                                                                       triangulation_triangles),
                                                                    vq(np.concatenate([triangulation_tetrahedra[:, :2], triangulation_tetrahedra[:, -1][:, np.newaxis]], axis=1),
                                                                       triangulation_triangles),
                                                                    vq(triangulation_tetrahedra[:, :-1], triangulation_triangles)], axis=1), int)

            matched_triangles = tetrahedra_triangle_matching[0][np.where(tetrahedra_triangle_matching[1] == 0)]
            triangle_tetrahedra = nd.sum(np.ones_like(matched_triangles), matched_triangles, index=np.arange(triangulation_triangles.shape[0]))
            exterior_triangles = triangulation_triangles[np.where(triangle_tetrahedra == 1)]
            triangulation_tetrahedra = np.concatenate([triangulation_tetrahedra, np.sort([np.concatenate([[1], t]) for t in exterior_triangles])])

    # print triangulation_tetrahedra.shape[0]," cell vertices (+exterior)"

    return triangulation_tetrahedra