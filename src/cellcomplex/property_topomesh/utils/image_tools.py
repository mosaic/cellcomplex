# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################


from time import time

import numpy as np
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from scipy.cluster.vq import vq

try:
    import vtk
except ImportError:
    print("VTK needs to be installed to use these functionalities")
    raise

try:
    from tissuelab.gui.vtkviewer.vtk_utils import matrix_to_image_reader
except ImportError:
    print("TissueLab should be installed to use these functionalities")

from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

from cellcomplex.utils import array_dict

from timagetk import SpatialImage

from cellcomplex.property_topomesh.extraction import cell_topomesh


triangle_edge_list  = np.array([[1, 2],[0, 2],[0, 1]])

def SetInput(obj, _input):
    if vtk.VTK_MAJOR_VERSION <= 5:
        obj.SetInput(_input)
    else:
        obj.SetInputData(_input)

def vtk_image_data_from_image(image):
    image_data = vtk.vtkImageData()
    # image_array = numpy_to_vtk(image.transpose(1,0,2)[::-1,::-1].transpose(2,0,1).ravel(), deep=True, array_type=get_vtk_array_type(image.dtype))
    image_array = numpy_to_vtk(image.get_array().transpose(2,1,0).ravel(), deep=True, array_type=get_vtk_array_type(image.dtype))
    # .transpose(2, 0, 1) may be required depending on numpy array order see - https://github.com/quentan/Test_ImageData/blob/master/TestImageData.py

    image_data.SetDimensions(image.shape)
    image_data.SetSpacing(image.voxelsize)
    image_data.SetOrigin(image.origin)
    image_data.GetPointData().SetScalars(image_array)

    print("--> Image data : "+str(image_data.GetDimensions()))

    return image_data


def compute_topomesh_image(topomesh, template_img):

    image_start_time = time()
    print("<-- Computing topomesh image")

    polydata_img = np.ones_like(template_img)
        
    for c in list(topomesh.wisps(3)):
        if len(list(topomesh.borders(3,c))) > 0:
            polydata_start_time = time()
            sub_topomesh = cell_topomesh(topomesh,cells=[c]) 
            
            start_time = time()
            bounding_box = np.array([[0,polydata_img.shape[0]],[0,polydata_img.shape[1]],[0,polydata_img.shape[2]]])
            bounding_box[:,0] = np.floor(sub_topomesh.wisp_property('barycenter',0).values().min(axis=0)/np.array(template_img.voxelsize)).astype(int)-1
            bounding_box[:,0] = np.maximum(bounding_box[:,0],0)
            bounding_box[:,1] = np.ceil(sub_topomesh.wisp_property('barycenter',0).values().max(axis=0)/np.array(template_img.voxelsize)).astype(int)+1
            bounding_box[:,1] = np.minimum(bounding_box[:,1],np.array(template_img.shape)-1)
            
            sub_polydata_img = polydata_img[bounding_box[0,0]:bounding_box[0,1],bounding_box[1,0]:bounding_box[1,1],bounding_box[2,0]:bounding_box[2,1]]
            #world.add(sub_polydata_img,"topomesh_image",colormap='glasbey',alphamap='constant',bg_id=1,intensity_range=(0,1))

            image_data = vtk_image_data_from_image(sub_polydata_img)
            #reader = matrix_to_image_reader('polydata_image',polydata_img,polydata_img.dtype)

            end_time = time()
            print("  --> Extracting cell sub-image      [",end_time-start_time,"s]")

            start_time = time()
            topomesh_center = bounding_box[:,0]*np.array(template_img.voxelsize)
            positions = sub_topomesh.wisp_property('barycenter',0)
            compute_topomesh_property(sub_topomesh,"oriented_vertices",2)
            face_vertices = sub_topomesh.wisp_property("oriented_vertices", 2).values(list(sub_topomesh.wisps(2)))

            polydata = vtk.vtkPolyData()
            vtk_points = vtk.vtkPoints()
            vtk_faces = vtk.vtkCellArray()

            double_array = numpy_to_vtk(positions.values(list(sub_topomesh.wisps(0)))-topomesh_center, deep=True, array_type=vtk.VTK_DOUBLE)
            vtk_points.SetData(double_array)
            point_mapping = array_dict(dict(zip(list(sub_topomesh.wisps(0)), range(sub_topomesh.nb_wisps(0)))))

            for face_vids in face_vertices:
                vtk_face_point_ids = point_mapping.values(face_vids)
                vtk_face_id = vtk_faces.InsertNextCell(len(vtk_face_point_ids))
                for vtk_point_id in vtk_face_point_ids:
                    vtk_faces.InsertCellPoint(vtk_point_id)

            polydata.SetPoints(vtk_points)
            polydata.SetPolys(vtk_faces)
            
            end_time = time()
            print("  --> Creating VTK PolyData      [",end_time-start_time,"s]")
            
            start_time = time()
            pol2stenc = vtk.vtkPolyDataToImageStencil()
            pol2stenc.SetTolerance(0)
            pol2stenc.SetOutputOrigin((0,0,0))
            #pol2stenc.SetOutputOrigin(tuple(-bounding_box[:,0]))
            pol2stenc.SetOutputSpacing(template_img.voxelsize)
            SetInput(pol2stenc,polydata)
            pol2stenc.Update()
            end_time = time()
            print("  --> Cell ",c," polydata stencil   [",end_time-start_time,"s]")

            start_time = time()
            imgstenc = vtk.vtkImageStencil()
            imgstenc.SetInputData(image_data)
            imgstenc.SetStencilConnection(pol2stenc.GetOutputPort())
            imgstenc.ReverseStencilOn()
            imgstenc.SetBackgroundValue(c)
            imgstenc.Update()
            end_time = time()
            print("  --> Cell ",c," image stencil   [",end_time-start_time,"s]")

            start_time = time()
            dim = tuple((bounding_box[:,1]-bounding_box[:,0])[::-1])
            array = np.ones(dim, template_img.dtype)
            export = vtk.vtkImageExport()
            export.SetInputConnection(imgstenc.GetOutputPort())
            export.Export(array)
            end_time = time()
            print("  --> Exporting image       [",end_time-start_time,"s]")
            
            start_time = time()
            array = np.transpose(array,(2,1,0))
            polydata_img[bounding_box[0,0]:bounding_box[0,1],bounding_box[1,0]:bounding_box[1,1],bounding_box[2,0]:bounding_box[2,1]] = array
            end_time = time()
            print("  --> Inserting cell sub-image       [",end_time-start_time,"s]")
            
            polydata_end_time = time()
            print("--> Inserting topomesh cell ",c,"   [",polydata_end_time-polydata_start_time,"s]")
        
    image_end_time = time()
    print("<-- Computing topomesh image   [",image_end_time-image_start_time,"s]")

    return polydata_img


def link_polydata_triangle_cells(polydata,img,img_graph=None):
    if img_graph is None:
        from tissue_analysis.temporal_graph_from_image import graph_from_image
        img_graph = graph_from_image(img,spatio_temporal_properties=['barycenter','volume'],ignore_cells_at_stack_margins = False,property_as_real=True)

    polydata_cell_data = polydata.GetCellData().GetArray(0)

    start_time = time()
    print("    --> Listing points")
    polydata_points = np.array([polydata.GetPoints().GetPoint(i) for i in range(polydata.GetPoints().GetNumberOfPoints())])
    end_time = time()
    print("    <-- Listing points               [",end_time - start_time,"s]")

    start_time = time()
    print("    --> Merging points")
    point_ids = {}
    for p in range(polydata.GetPoints().GetNumberOfPoints()):
        point_ids[tuple(polydata_points[p])] = []
    for p in range(polydata.GetPoints().GetNumberOfPoints()):
        point_ids[tuple(polydata_points[p])] += [p]

    unique_points = np.unique(polydata_points,axis=0)
    n_points = unique_points.shape[0]

    point_unique_id = {}
    for p in range(n_points):
        for i in point_ids[tuple(unique_points[p])]:
            point_unique_id[i] = p
    end_time = time()
    print("    <-- Merging points               [",end_time - start_time,"s]")

    triangle_cell_start_time = time()
    print("    --> Listing triangles")
    print("      - ",polydata.GetNumberOfCells()," triangles")
    polydata_triangles = np.sort([[polydata.GetCell(t).GetPointIds().GetId(i) for i in range(3)] for t in range(polydata.GetNumberOfCells())])   
    print("      - ",np.unique(polydata_triangles,axis=0).shape[0]," unique triangles")
    # polydata_triangle_points = [polydata.GetCell(t).GetPointIds() for t in range(polydata.GetNumberOfCells())]
    # polydata_triangles = np.sort([[triangle_points.GetId(i) for i in range(3)] for triangle_points in polydata_triangle_points])
    polydata_triangles = np.sort(array_dict(point_unique_id).values(polydata_triangles)) 
    print("      - ",np.unique(polydata_triangles,axis=0).shape[0]," unique triangles (merged vertices)")
    triangle_cell_end_time = time()
    print("    <-- Listing triangles            [",triangle_cell_end_time - triangle_cell_start_time,"s]")
    input()

    triangle_cell_start_time = time()
    print("    --> Initializing triangle cells")
    triangle_cells = {}
    for t in range(polydata.GetNumberOfCells()):
        triangle_cells[tuple(polydata_triangles[t])] = []  
        for i in range(10):
            if t == (i*polydata.GetNumberOfCells())/10:
                print("     --> Initializing triangle cells (",10*i,"%)")
    triangle_cell_end_time = time()
    print("    <-- Initializing triangle cells  [",triangle_cell_end_time - triangle_cell_start_time,"s]")

    triangle_cell_start_time = time()
    for t in range(polydata.GetNumberOfCells()):
        triangle_cells[tuple(polydata_triangles[t])] += list(polydata_cell_data.GetTuple(t))
        for i in range(100):
            if t == (i*polydata.GetNumberOfCells())/100:
                triangle_cell_end_time = time()
                print("     --> Listing triangle cells (",i,"%) [",(triangle_cell_end_time-triangle_cell_start_time)/(polydata.GetNumberOfCells()/100.),"s]")
                triangle_cell_start_time = time()

    triangle_cell_start_time = time()
    print("    --> Cleaning triangle cells")
    for t in range(polydata.GetNumberOfCells()):
        triangle_cells[tuple(polydata_triangles[t])] = np.unique(triangle_cells[tuple(polydata_triangles[t])])  
        for i in range(10):
            if t == (i*polydata.GetNumberOfCells())/10:
                print("     --> Cleaning triangle cells (",10*i,"%)")
    triangle_cell_end_time = time()
    print("    <-- Cleaning triangle cells      [",triangle_cell_end_time - triangle_cell_start_time,"s]")


    # triangle_cell_start_time = time()
    # print "    --> Listing triangle cells"
    # triangle_cell = np.array([polydata_cell_data.GetTuple(t)[0] for t in range(polydata.GetNumberOfCells())],np.uint16)
    # triangle_cell_end_time = time()
    # print "    <-- Listing triangle cells     [",triangle_cell_end_time - triangle_cell_start_time,"s]"

    # triangle_cells = {}
    # for t in range(polydata.GetNumberOfCells()):
    #     triangle_cells[t] = []

    # for c in considered_cells:
    #     cell_triangles = np.where(triangle_cell == c)[0]
    #     for t in cell_triangles:
    #         triangle_cells[t] += [c]
    #     print "    Cell ",c," : ",cell_triangles.shape[0]," triangles"
    #     neighbor_triangles = where_list(triangle_cell,list(img_graph.neighbors(c)))[0]
    #     neighbor_triangle_matching = vq(polydata_triangles[cell_triangles],polydata_triangles[neighbor_triangles])
    #     cell_double_triangles = neighbor_triangles[neighbor_triangle_matching[0][np.where(neighbor_triangle_matching[1]==0)[0]]]
    #     for t in cell_double_triangles:
    #         triangle_cells[t] += [c]

    # print triangle_cells.values()
    # raw_input()

    # unique_triangles,unique_triangle_rows = array_unique(polydata_triangles,return_index=True)
    # n_triangles = unique_triangles.shape[0]

    # triangle_unique = array_dict(np.arange(n_triangles),unique_triangle_rows)

    # triangle_id = (polydata_triangles * np.array([contour.GetOutput().GetPoints().GetNumberOfPoints(),1,1./contour.GetOutput().GetPoints().GetNumberOfPoints()])).sum(axis=1)
    # unique_triangle_id = (unique_triangles * np.array([contour.GetOutput().GetPoints().GetNumberOfPoints(),1,1./contour.GetOutput().GetPoints().GetNumberOfPoints()])).sum(axis=1)

    # triangle_number = nd.sum(np.ones_like(triangle_id),triangle_id,index=triangle_id)
    # double_triangle_rows = np.where(triangle_number==2)[0]
    # print "    ",double_triangle_rows.shape[0]," double triangles / ",contour.GetOutput().GetNumberOfCells()
    # double_triangles = polydata_triangles[double_triangle_rows]

    # unique_triangle_number = nd.sum(np.ones_like(triangle_id),triangle_id,index=unique_triangle_id)
    # double_unique_triangle_rows = np.where(unique_triangle_number==2)[0]
    # double_unique_triangles = unique_triangles[double_unique_triangle_rows]

    # triangle_triangle_matching = (vq(double_triangles,double_unique_triangles)[0])
    # triangle_triangle_matching = array_dict(double_unique_triangle_rows[triangle_triangle_matching],double_triangle_rows)

    # unique_triangle_cells = {}
    # for t in range(n_triangles):
    #     unique_triangle_cells[t] = []
    # for t in range(polydata.GetNumberOfCells()):
    #     if triangle_number[t] == 1:
    #         unique_triangle_cells[triangle_unique[t]] += list(polydata_cell_data.GetTuple(t))
    #     elif triangle_number[t] == 2:
    #         unique_triangle_cells[triangle_triangle_matching[t]] += list(polydata_cell_data.GetTuple(t))
    # triangle_cells = {}
    # for t in range(polydata.GetNumberOfCells()):
    #     if triangle_number[t] == 1:
    #         triangle_cells[t] = np.array(unique_triangle_cells[triangle_unique[t]],np.uint16)
    #     elif triangle_number[t] == 2:
    #         triangle_cells[t] = np.array(unique_triangle_cells[triangle_triangle_matching[t]],np.uint16)

    cell_data = vtk.vtkFloatArray()
    cell_data.SetNumberOfComponents(2)
    cell_data.SetNumberOfTuples(polydata.GetNumberOfCells())
    for t in range(polydata.GetNumberOfCells()):
        triangle_key = tuple(polydata_triangles[t])
        if len(triangle_cells[triangle_key]) == 2:
            cell_data.SetTuple(t,np.sort(triangle_cells[triangle_key]))
        else:
            cell_data.SetTuple(t,np.concatenate([triangle_cells[triangle_key],[1]]))
    polydata.GetCellData().SetScalars(cell_data)

    return triangle_cells


def image_to_vtk_polydata(img,considered_cells=None,mesh_center=None,coef=1.0,mesh_fineness=1.0):
    start_time = time()
    print("--> Generating vtk mesh from image")

    vtk_mesh = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()
    vtk_triangles = vtk.vtkCellArray()
    vtk_cells = vtk.vtkLongArray()
    
    nx, ny, nz = img.shape
    data_string = img.tostring('F')

    reader = vtk.vtkImageImport()
    reader.CopyImportVoidPointer(data_string, len(data_string))
    if img.dtype == np.uint8:
        reader.SetDataScalarTypeToUnsignedChar()
    else:
        reader.SetDataScalarTypeToUnsignedShort()
    reader.SetNumberOfScalarComponents(1)
    reader.SetDataExtent(0, nx - 1, 0, ny - 1, 0, nz - 1)
    reader.SetWholeExtent(0, nx - 1, 0, ny - 1, 0, nz - 1)
    reader.SetDataSpacing(*img.voxelsize)

    if considered_cells is None:
        considered_cells = np.unique(img)[1:]

    if mesh_center is None:
        mesh_center = np.array(img.voxelsize)*np.array(img.shape)/2.

    marching_cube_start_time = time()
    print("  --> Marching Cubes")
    contour = vtk.vtkDiscreteMarchingCubes()
    SetInput(contour,reader.GetOutput())
    contour.ComputeNormalsOn()
    contour.ComputeGradientsOn()
    contour.ComputeScalarsOn()
    for i,label in enumerate(considered_cells):
        contour.SetValue(i,label)
    contour.Update()
    marching_cube_end_time = time()
    print("  <-- Marching Cubes : ",contour.GetOutput().GetPoints().GetNumberOfPoints()," Points,",contour.GetOutput().GetNumberOfCells()," Triangles, ",len(np.unique(img)[1:])," Cells [",marching_cube_end_time - marching_cube_start_time,"s]")

    marching_cubes = contour.GetOutput()

    marching_cubes_cell_data = marching_cubes.GetCellData().GetArray(0)

    triangle_cell_start_time = time()
    print("    --> Listing triangles")
    print("      - ",marching_cubes.GetNumberOfCells()," triangles")
    marching_cubes_triangles = np.sort([[marching_cubes.GetCell(t).GetPointIds().GetId(i) for i in range(3)] for t in range(marching_cubes.GetNumberOfCells())])   
    triangle_cell_end_time = time()
    print("    <-- Listing triangles            [",triangle_cell_end_time - triangle_cell_start_time,"s]")

    triangle_cell_start_time = time()
    print("    --> Listing triangle cells")
    triangle_cell = np.array([marching_cubes_cell_data.GetTuple(t)[0] for t in range(marching_cubes.GetNumberOfCells())],np.uint16)
    triangle_cell_end_time = time()
    print("    <-- Listing triangle cells     [",triangle_cell_end_time - triangle_cell_start_time,"s]")

    triangle_cell_start_time = time()
    print("    --> Updating marching cubes mesh")
    vtk_mesh = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()
    vtk_triangles = vtk.vtkCellArray()
    vtk_cells = vtk.vtkLongArray()

    for label in considered_cells:

        # cell_start_time = time()

        cell_marching_cubes_triangles = marching_cubes_triangles[np.where(triangle_cell == label)]

        marching_cubes_point_ids = np.unique(cell_marching_cubes_triangles)

        marching_cubes_points = np.array([marching_cubes.GetPoints().GetPoint(p) for p in marching_cubes_point_ids])
        marching_cubes_center = marching_cubes_points.mean(axis=0)
        marching_cubes_points = marching_cubes_center + coef*(marching_cubes_points-marching_cubes_center) - mesh_center

        cell_points = []
        for p in range(marching_cubes_points.shape[0]):
            pid = vtk_points.InsertNextPoint(marching_cubes_points[p])
            cell_points.append(pid)
        cell_points = array_dict(cell_points,marching_cubes_point_ids)

        for t in range(cell_marching_cubes_triangles.shape[0]):
            poly = vtk_triangles.InsertNextCell(3)
            for i in range(3):
                pid = cell_marching_cubes_triangles[t][i]
                vtk_triangles.InsertCellPoint(cell_points[pid])
            vtk_cells.InsertValue(poly,label)

        # cell_end_time = time()
        # print "  --> Cell",label,":",cell_marching_cubes_triangles.shape[0],"triangles [",cell_end_time-cell_start_time,"s]"

    vtk_mesh.SetPoints(vtk_points)
    vtk_mesh.SetPolys(vtk_triangles)
    vtk_mesh.GetCellData().SetScalars(vtk_cells)

    triangle_cell_end_time = time()
    print("    <-- Updating marching cubes mesh [",triangle_cell_end_time - triangle_cell_start_time,"s]")

    decimation_start_time = time()
    print("  --> Decimation")
    smoother = vtk.vtkWindowedSincPolyDataFilter()
    SetInput(smoother,vtk_mesh)
    smoother.SetFeatureAngle(30.0)
    smoother.SetPassBand(0.05)
    smoother.SetNumberOfIterations(25)
    smoother.NonManifoldSmoothingOn()
    smoother.NormalizeCoordinatesOn()
    smoother.Update()

    decimate = vtk.vtkQuadricClustering()
    SetInput(decimate,smoother.GetOutput())
    decimate.SetNumberOfDivisions(*tuple(mesh_fineness*np.array(np.array(img.shape)*np.array(img.voxelsize)/2.,np.uint16)))
    decimate.SetFeaturePointsAngle(30.0)
    decimate.CopyCellDataOn()
    decimate.Update()

    decimation_end_time = time()
    print("  <-- Decimation     : ",decimate.GetOutput().GetPoints().GetNumberOfPoints()," Points,",decimate.GetOutput().GetNumberOfCells()," Triangles, ",len(considered_cells)," Cells [",decimation_end_time - decimation_start_time,"s]")

    end_time = time()
    print("<-- Generating vtk mesh from image      [",end_time-start_time,"s]")

    return decimate.GetOutput()


def image_to_vtk_cell_polydata(img,considered_cells=None,mesh_center=None,coef=1.0,mesh_fineness=1.0,smooth_factor=1.0):

    start_time = time()
    print("--> Generating vtk mesh from image")

    vtk_mesh = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()
    vtk_triangles = vtk.vtkCellArray()
    vtk_cells = vtk.vtkLongArray()
    
    nx, ny, nz = img.shape
    data_string = img.tostring('F')

    reader = vtk.vtkImageImport()
    reader.CopyImportVoidPointer(data_string, len(data_string))
    if img.dtype == np.uint8:
        reader.SetDataScalarTypeToUnsignedChar()
    else:
        reader.SetDataScalarTypeToUnsignedShort()
    reader.SetNumberOfScalarComponents(1)
    reader.SetDataExtent(0, nx - 1, 0, ny - 1, 0, nz - 1)
    reader.SetWholeExtent(0, nx - 1, 0, ny - 1, 0, nz - 1)
    reader.SetDataSpacing(*img.voxelsize)
    reader.Update()

    if considered_cells is None:
        considered_cells = np.unique(img)[1:]

    if mesh_center is None:
        #mesh_center = np.array(img.voxelsize)*np.array(img.shape)/2.
        mesh_center = np.array([0,0,0])

    for label in considered_cells:

        cell_start_time = time()

        cell_volume = (img.get_array()==label).sum()*np.array(img.voxelsize).prod()

        # mask_data = vtk.vtkImageThreshold()
        # mask_data.SetInputConnection(reader.GetOutputPort())
        # mask_data.ThresholdBetween(label, label)
        # mask_data.ReplaceInOn()
        # mask_data.SetInValue(label)
        # mask_data.SetOutValue(0)
        contour = vtk.vtkDiscreteMarchingCubes()
        # contour.SetInput(mask_data.GetOutput())
        SetInput(contour,reader.GetOutput())
        contour.ComputeNormalsOn()
        contour.ComputeGradientsOn()
        contour.SetValue(0,label)
        contour.Update()

        # print "    --> Marching Cubes : ",contour.GetOutput().GetPoints().GetNumberOfPoints()," Points,",contour.GetOutput().GetNumberOfCells()," Triangles,  1 Cell"

        # decimate = vtk.vtkDecimatePro()
        # decimate.SetInputConnection(contour.GetOutputPort())
        # # decimate.SetTargetReduction(0.75)
        # decimate.SetTargetReduction(0.66)
        # # decimate.SetTargetReduction(0.5)
        # # decimate.SetMaximumError(2*np.sqrt(3))
        # decimate.Update()

        smooth_iterations = int(np.ceil(smooth_factor*8.))

        smoother = vtk.vtkWindowedSincPolyDataFilter()
        SetInput(smoother,contour.GetOutput())
        smoother.BoundarySmoothingOn()
        # smoother.BoundarySmoothingOff()
        smoother.FeatureEdgeSmoothingOn()
        # smoother.FeatureEdgeSmoothingOff()
        smoother.SetFeatureAngle(120.0)
        # smoother.SetPassBand(1)
        smoother.SetPassBand(0.01)
        smoother.SetNumberOfIterations(smooth_iterations)
        smoother.NonManifoldSmoothingOn()
        smoother.NormalizeCoordinatesOn()
        smoother.Update()

        divisions = int(np.ceil(np.power(cell_volume,1/3.)*mesh_fineness))

        decimate = vtk.vtkQuadricClustering()
        # decimate = vtk.vtkQuadricDecimation()
        # decimate = vtk.vtkDecimatePro()
        # decimate.SetInput(contour.GetOutput())
        SetInput(decimate,smoother.GetOutput())
        # decimate.SetTargetReduction(0.95)
        # decimate.AutoAdjustNumberOfDivisionsOff()
        decimate.SetNumberOfDivisions(divisions,divisions,divisions)
        decimate.SetFeaturePointsAngle(120.0)
        # decimate.AttributeErrorMetricOn()
        # decimate.ScalarsAttributeOn()
        # decimate.PreserveTopologyOn()
        # decimate.CopyCellDataOn()
        # decimate.SetMaximumCost(1.0)
        # decimate.SetMaximumCollapsedEdges(10000.0)
        decimate.Update()

        # print "    --> Decimation     : ",decimate.GetOutput().GetPoints().GetNumberOfPoints()," Points,",decimate.GetOutput().GetNumberOfCells()," Triangles,  1 Cell"

        cell_polydata = decimate.GetOutput()
        # cell_polydata = smoother.GetOutput()

        polydata_points = np.array([cell_polydata.GetPoints().GetPoint(p) for p in range(cell_polydata.GetPoints().GetNumberOfPoints())])
        polydata_center = polydata_points.mean(axis=0)
        polydata_points = polydata_center + coef*(polydata_points-polydata_center) - mesh_center

        cell_points = []
        for p in range(cell_polydata.GetPoints().GetNumberOfPoints()):
            pid = vtk_points.InsertNextPoint(polydata_points[p])
            cell_points.append(pid)
        cell_points = array_dict(cell_points,np.arange(cell_polydata.GetPoints().GetNumberOfPoints()))

        for t in range(cell_polydata.GetNumberOfCells()):
            poly = vtk_triangles.InsertNextCell(3)
            for i in range(3):
                pid = cell_polydata.GetCell(t).GetPointIds().GetId(i)
                vtk_triangles.InsertCellPoint(cell_points[pid])
                vtk_cells.InsertValue(poly,label)

        cell_end_time = time()
        print("  --> Cell",label,":",decimate.GetOutput().GetNumberOfCells(),"triangles (",cell_volume," microm3 ) [",cell_end_time-cell_start_time,"s]")

    vtk_mesh.SetPoints(vtk_points)
    vtk_mesh.SetPolys(vtk_triangles)
    vtk_mesh.GetCellData().SetScalars(vtk_cells)

    print("  <-- Cell Mesh      : ",vtk_mesh.GetPoints().GetNumberOfPoints()," Points,",vtk_mesh.GetNumberOfCells()," Triangles, ",len(considered_cells)," Cells")

    end_time = time()
    print("<-- Generating vtk mesh from image      [",end_time-start_time,"s]")

    return vtk_mesh


def draw_triangular_mesh(mesh, mesh_id=None, colormap=None):
    import openalea.plantgl.all as pgl
    import openalea.plantgl.ext.color as color

    triangle_points = array_dict(mesh.points).values(np.array(mesh.triangles.values()))
    triangle_normals = np.cross(triangle_points[:,1]-triangle_points[:,0],triangle_points[:,2]-triangle_points[:,0])
    mesh_center = np.mean(mesh.points.values(),axis=0)
    reversed_normals = np.array(mesh.triangles.keys())[np.where(np.einsum('ij,ij->i',triangle_normals,triangle_points[:,0]-mesh_center) < 0)[0]]
    for t in reversed_normals:
        mesh.triangles[t] = list(reversed(mesh.triangles[t]))

    if colormap is None:
        colormap = color.GlasbeyMap(0,255)

    points_index = array_dict(np.arange(len(mesh.points)),mesh.points.keys())

    if isinstance(colormap,color.GlasbeyMap):
        if isiterable(mesh.triangle_data.values()[0]):
            colors = [pgl.Color4(colormap(mesh.triangle_data[t][0]%256).i3tuple()) if t in mesh.triangle_data else pgl.Color4(colormap(0).i3tuple()) for t in mesh.triangles.keys()]
        else:
            colors = [pgl.Color4(colormap(mesh.triangle_data[t]%256).i3tuple()) if t in mesh.triangle_data else pgl.Color4(colormap(0).i3tuple()) for t in mesh.triangles.keys()]
    else:
        if isiterable(mesh.triangle_data.values()[0]):
            colors = [pgl.Color4(colormap(mesh.triangle_data[t][0]).i3tuple()) if t in mesh.triangle_data else pgl.Color4(colormap(0).i3tuple()) for t in mesh.triangles.keys()]
        else:
            colors = [pgl.Color4(colormap(mesh.triangle_data[t]).i3tuple()) if t in mesh.triangle_data else pgl.Color4(colormap(0).i3tuple()) for t in mesh.triangles.keys()]

    scene = pgl.Scene()
    if mesh_id is not None:
        scene += pgl.Shape(pgl.FaceSet(mesh.points.values(),list(points_index.values(np.array(mesh.triangles.values()))),colorList=colors,colorPerVertex=False),id=int(mesh_id))
    else:
        scene += pgl.Shape(pgl.FaceSet(mesh.points.values(),list(points_index.values(np.array(mesh.triangles.values()))),colorList=colors,colorPerVertex=False))
    return scene

'''
def image_to_pgl_mesh(img,sampling=4,cell_coef=1.0,mesh_fineness=1.0,smooth=1.0,colormap=None):
    try:
        import openalea.plantgl.all as pgl
    except ImportError:
        print("PlantGL needs to be installed to use this functionality")
        raise

    try:
        voxelsize = img.voxelsize
    except:
        voxelsize = (1.0,1.0,1.0)

    img = SpatialImage(img[0:-1:sampling,0:-1:sampling,0:-1:sampling],voxelsize=tuple(np.array(voxelsize)*sampling))

    img_polydata = image_to_vtk_cell_polydata(img,coef=cell_coef,mesh_fineness=mesh_fineness,smooth_factor=smooth)
    # img_mesh = vtk_polydata_to_triangular_mesh(img_polydata)
    # return img_mesh._repr_geom_()
    img_mesh = vtk_polydata_to_cell_triangular_meshes(img_polydata)
    start_time = time()
    print("--> Constructing geometry (pGL)")
    scene = pgl.Scene()
    for c in img_mesh.keys():
        scene += draw_triangular_mesh(img_mesh[c],mesh_id=c,colormap=colormap)
    end_time = time()
    print("<-- Constructing geometry (pGL) [",end_time - start_time,"s]")
    return scene


def image_to_triangular_mesh(img,sampling=4,cell_coef=1.0,mesh_fineness=1.0,smooth=1.0,voxelsize=None):

    if voxelsize is None:
        try:
            voxelsize = img.voxelsize
        except:
            voxelsize = (1.0,1.0,1.0)
    img = SpatialImage(img[0:-1:sampling,0:-1:sampling,0:-1:sampling],voxelsize=tuple(np.array(voxelsize)*sampling))

    img_polydata = image_to_vtk_cell_polydata(img,coef=cell_coef,mesh_fineness=mesh_fineness,smooth_factor=smooth)
    img_mesh = vtk_polydata_to_triangular_mesh(img_polydata)
    return img_mesh
'''

def img_extent(sp_img):
    extent = []
    for ind in range(0,len(sp_img.shape)):
        extent.append(sp_img.shape[ind]*sp_img.voxelsize[ind])
    return extent


def img_resize(sp_img, sub_factor=2, option=None):
    """
    Down interpolation of image 'sp_img' by a factor 'sub_factor'. 
    For intensity image use 'option="gray"', for segmented images use 'option="label"'.
    """
    from timagetk.algorithms.trsf import BalTransformation, create_trsf, apply_trsf

    vx = sp_img.voxelsize
    poss_opt = ['gray', 'label']
    if option is None:
        option='label'
    else:
        if option not in poss_opt:
            option='label'
    
    extent = img_extent(sp_img)
    tmp_voxelsize = np.array(sp_img.voxelsize) *sub_factor
    print(tmp_voxelsize)
    new_shape, new_voxelsize = [], []
    for ind in range(0,len(sp_img.shape)):
        new_shape.append(int(np.ceil(extent[ind]/tmp_voxelsize[ind])))
        new_voxelsize.append(extent[ind]/new_shape[ind])
    identity_trsf = create_trsf(param_str_2='-identity', trsf_type=BalTransformation.RIGID_3D, trsf_unit=BalTransformation.REAL_UNIT)
    template_img = np.zeros((new_shape[0],new_shape[1],new_shape[2]), dtype=sp_img.dtype)
    template_img = SpatialImage(template_img, voxelsize=new_voxelsize)
    if option=='gray':
        param_str_2 = '-interpolation linear'
    elif option=='label':
        param_str_2 = '-interpolation nearest'
    out_img = apply_trsf(sp_img, identity_trsf,template_img=template_img, param_str_2=param_str_2)

    return out_img






