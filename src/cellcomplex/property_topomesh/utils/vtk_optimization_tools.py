# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       vplantsLab Website : http://virtualplants.github.io/
#
###############################################################################


import numpy as np
from scipy.cluster.vq import vq

from cellcomplex.utils import array_dict

try:
    import vtk

    def SetInput(obj, _input):
        if vtk.VTK_MAJOR_VERSION <= 5:
            obj.SetInput(_input)
        else:
            obj.SetInputData(_input)

except ImportError:
    print("VTK needs to be installed to use these functionalities")
    raise

from cellcomplex.property_topomesh.analysis import is_triangular, compute_topomesh_property
from cellcomplex.property_topomesh.creation import triangle_topomesh

from visu_core.vtk.polydata import face_scalar_property_polydata

def property_topomesh_vtk_smoothing_decimation(input_topomesh, smoothing=2, decimation=2):
    if not is_triangular(input_topomesh):
        raise "The input mesh should be triangular!"

    compute_topomesh_property(input_topomesh,'vertices',2)

    #input_mesh = TriangularMesh()
    points = input_topomesh.wisp_property('barycenter',0).to_dict()
    triangles = input_topomesh.wisp_property('vertices',2).values()

    input_polydata = face_scalar_property_polydata(points,triangles,np.ones(len(triangles)))

    if smoothing>0:
        smoother = vtk.vtkWindowedSincPolyDataFilter()
        SetInput(smoother,input_polydata)
        smoother.BoundarySmoothingOn()
        smoother.FeatureEdgeSmoothingOn()
        smoother.SetFeatureAngle(120.0)
        smoother.SetPassBand(1)
        smoother.SetNumberOfIterations(smoothing)
        # smoother.NonManifoldSmoothingOn()
        # smoother.NormalizeCoordinatesOn()
        smoother.Update()

        print("Smoothing :",smoother.GetOutput().GetNumberOfPoints())

    if decimation>0:
        # decimate = vtk.vtkQuadricClustering()
        decimate = vtk.vtkQuadricDecimation()
        # decimate = vtk.vtkDecimatePro()

        if smoothing>0:
            SetInput(decimate,smoother.GetOutput())
        else:
            SetInput(decimate,input_polydata)
        decimate.SetTargetReduction(1-1./float(decimation))
        # decimate.SetNumberOfDivisions(int(decimation),int(decimation),int(decimation))
        # decimate.SetFeaturePointsAngle(60.0)
        decimate.Update()

        print("Decimation :",decimate.GetOutput().GetNumberOfPoints())

    if decimation>0:
        polydata = decimate.GetOutput()
    elif smoothing>0:
        polydata = smoother.GetOutput()
    else:
        polydata = input_polydata

    # input_polydata_points = np.array([input_polydata.GetPoints().GetPoint(p) for p in range(input_polydata.GetPoints().GetNumberOfPoints())])
    polydata_points = np.array([polydata.GetPoints().GetPoint(p) for p in range(polydata.GetPoints().GetNumberOfPoints())])

    polydata_triangles =  np.array([[polydata.GetCell(t).GetPointIds().GetId(i) for i in range(3)] for t in range(polydata.GetNumberOfCells())])

    if len(polydata_triangles)>0:
        return triangle_topomesh(polydata_triangles,array_dict(polydata_points,np.arange(len(polydata_points))))
    else:
        return None




