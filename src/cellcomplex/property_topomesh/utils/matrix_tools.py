import numpy as np
import scipy.ndimage as nd
import networkx as nx

from cellcomplex.utils import array_dict

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_border_property, is_region_property
from cellcomplex.property_topomesh.optimization import topomesh_face_star_split

from cellcomplex.property_topomesh.utils.networkx_tools import topomesh_to_element_graph


def adjacency_matrix(topomesh, degree):
    if degree > 0:
        adj = np.array([[n in topomesh.border_neighbors(degree, w) for n in topomesh.wisps(degree)]
                        for w in topomesh.wisps(degree)]).astype(float)
    else:
        adj = np.array([[n in topomesh.region_neighbors(degree, w) for n in topomesh.wisps(degree)]
                        for w in topomesh.wisps(degree)]).astype(float)

    return adj


def incidence_matrix(topomesh, degree=0, direction='region'):
    assert direction in ['region', 'border']
    assert not (direction == 'region' and degree == topomesh.degree())
    assert not (direction == 'border' and degree == 0)

    if direction == 'region':
        inc = np.array([[r in topomesh.regions(degree, w) for r in topomesh.wisps(degree+1)]
                        for w in topomesh.wisps(degree)]).astype(float)
    elif direction == 'border':
        inc = np.array([[b in topomesh.borders(degree, w) for b in topomesh.wisps(degree-1)]
                        for w in topomesh.wisps(degree)]).astype(float)

    return inc


def property_incidence_matrix(topomesh, property_name, degree=0, direction='region', missing_value=np.nan):
    assert direction in ['region', 'border']
    assert not (direction == 'region' and degree == topomesh.degree())
    assert not (direction == 'border' and degree == 0)
    assert topomesh.has_wisp_property(property_name, degree, is_computed=True)

    if direction == 'region':
        if is_region_property(topomesh, property_name, degree):
            inc = np.array([[topomesh.wisp_property(property_name, degree)[w][list(topomesh.regions(degree, w)).index(r)]
                             if r in topomesh.regions(degree, w)
                             else missing_value
                             for r in topomesh.wisps(degree+1)]
                            for w in topomesh.wisps(degree)])
        else:
            property_values = topomesh.wisp_property(property_name, degree).values(list(topomesh.wisps(degree)))
            inc = property_values[:, np.newaxis] * incidence_matrix(topomesh, degree, direction)

    elif direction == 'border':
        if is_border_property(topomesh, property_name, degree):
            inc = np.array([[topomesh.wisp_property(property_name, degree)[w][list(topomesh.borders(degree, w)).index(r)]
                             if r in topomesh.borders(degree, w)
                             else np.nan
                             for r in topomesh.wisps(degree+1)]
                            for w in topomesh.wisps(degree)])
        else:
            property_values = topomesh.wisp_property(property_name, degree).values(list(topomesh.wisps(degree)))
            inc = property_values[:, np.newaxis] * incidence_matrix(topomesh, degree, direction)

    return inc


def degree_matrix(topomesh, degree):
    if degree > 0:
        deg = np.diag([topomesh.nb_border_neighbors(degree, w) for w in topomesh.wisps(degree)]).astype(float)
    else:
        deg = np.diag([topomesh.nb_region_neighbors(degree, w) for w in topomesh.wisps(degree)]).astype(float)
    return deg


def add_connection(laplacian, ids_list):
    for ids in ids_list:
        id1 = ids[0]
        id2 = ids[1]
        assert id1 != id2
        laplacian[id1, id2] += -1
        laplacian[id2, id1] += -1
        laplacian[id1, id1] += 1
        laplacian[id2, id2] += 1


def laplacian_matrix(topomesh, degree, connection=[]):
    adj = adjacency_matrix(topomesh, degree)
    deg = degree_matrix(topomesh, degree)
    lap = deg - adj
    add_connection(lap, connection)
    return lap


def topological_distance_matrix(topomesh, degree=0, cutoff=None):
    if degree == 0:
        G = topomesh_to_element_graph(topomesh, degree=0, direction='region')
    else:
        G = topomesh_to_element_graph(topomesh, degree=degree, direction='border')

    dijkstra = dict(nx.all_pairs_dijkstra(G, weight=None, cutoff=cutoff))
    distance_matrix = np.array([[dijkstra[i][0][j] if j in dijkstra[i][0] else np.nan for j in G.nodes] for i in G.nodes])

    return distance_matrix


def vertex_distance_matrix(input_topomesh, split_faces=False, cutoff=None):
    if split_faces:
        topomesh = topomesh_face_star_split(input_topomesh, split_edges=True)
    else:
        topomesh = input_topomesh

    compute_topomesh_property(topomesh, 'vertices', 1)
    compute_topomesh_property(topomesh, 'length', 1)

    G = topomesh_to_element_graph(topomesh, degree=0, direction='region')
    dijkstra = dict(nx.all_pairs_dijkstra(G, weight='length', cutoff=cutoff))

    # edge_vertices = topomesh.wisp_property('vertices', 1).values(list(topomesh.wisps(1)))
    # edge_vertices = np.concatenate([edge_vertices, edge_vertices[:, ::-1]])
    #
    # edge_lengths = np.tile(topomesh.wisp_property('length', 1).values(list(topomesh.wisps(1))), (2,))
    #
    # vertex_vertex_distances = np.nan * np.ones((topomesh.nb_wisps(0), topomesh.nb_wisps(0)), float)
    # vertex_vertex_distances[np.eye(topomesh.nb_wisps(0)).astype(bool)] = 0
    #
    # vertex_vertex_distances = array_dict(dict(zip(topomesh.wisps(0), vertex_vertex_distances)))
    #
    # change = topomesh.nb_wisps(0)
    # total_change = change
    # while change > 0:
    #     edge_vertex_vertex_distances = np.nanmin([vertex_vertex_distances.values(edge_vertices[:, 0]) + edge_lengths[:, np.newaxis],
    #                                               vertex_vertex_distances.values(edge_vertices[:, 1])], axis=0)
    #     vertex_vertex_min_distances = np.transpose([nd.minimum(edge_vertex_vertex_distances[:, i_v],
    #                                                            edge_vertices[:, 1],
    #                                                            index=list(topomesh.wisps(0)))
    #                                                 for i_v, v in enumerate(topomesh.wisps(0))])
    #     change = (vertex_vertex_distances.values(list(topomesh.wisps(0))) != vertex_vertex_min_distances).sum()
    #     change -= np.isnan(vertex_vertex_min_distances).sum()
    #     total_change += change
    #     #print(total_change / np.power(topomesh.nb_wisps(0), 2))
    #     vertex_vertex_distances = array_dict(dict(zip(topomesh.wisps(0), vertex_vertex_min_distances)))
    #
    # distance_matrix = vertex_vertex_distances.values(list(topomesh.wisps(0)))

    distance_matrix = np.array([[dijkstra[i][0][j] if j in dijkstra[i][0] else np.nan for j in G.nodes] for i in G.nodes])

    if split_faces:
        vertex_non_split = np.array([v in input_topomesh.wisps(0) for v in topomesh.wisps(0)])
        distance_matrix = distance_matrix[vertex_non_split][:, vertex_non_split]

    return distance_matrix
