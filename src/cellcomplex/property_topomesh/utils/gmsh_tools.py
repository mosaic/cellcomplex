import logging
import tempfile
from time import time as current_time

import numpy as np
from cellcomplex.property_topomesh.utils.matching_tools import kd_tree_match

try:
    import gmsh
except:
    logging.error("GMSH needs to be installed to use these functionalities!")
    logging.error("Try `conda install -c conda-forge python-gmsh` for instance")

from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property


def _gmsh_add_geo_from_topomesh(polygonal_topomesh):
    compute_topomesh_property(polygonal_topomesh, 'oriented_vertices', 2)
    compute_topomesh_property(polygonal_topomesh, 'oriented_borders', 3)

    for v, p in polygonal_topomesh.wisp_property('barycenter', 0).items():
        gmsh.model.geo.addPoint(p[0], p[1], p[2], v+1)

    for e in polygonal_topomesh.wisps(1):
        v1, v2 = polygonal_topomesh.borders(1, e)
        gmsh.model.geo.addLine(v1+1, v2+1, e+1)

    for c in polygonal_topomesh.wisps(3):
        for f, f_o in zip(*polygonal_topomesh.wisp_property('oriented_borders', 3)[c]):
            e, o = polygonal_topomesh.wisp_property('oriented_borders', 2)[f]
            gmsh.model.geo.addCurveLoop(list((e+1)*o*f_o), f+1)
            gmsh.model.geo.addPlaneSurface([f+1], f+1)


def _add_gmsh_geo_from_mesh(points, triangles):
    triangle_edge_points = np.array([[0, 1], [1, 2], [2, 0]])

    triangle_edges = triangles[:, triangle_edge_points]
    sorted_edges = np.sort(np.concatenate(triangle_edges))
    unique_edges = np.unique(sorted_edges, axis=0)

    edge_matching = kd_tree_match(sorted_edges, unique_edges)
    triangle_unique_edges = edge_matching.reshape((-1, 3))

    triangle_edge_orientation = 2*np.all(triangle_edges == sorted_edges.reshape((-1, 3, 2)), -1) - 1

    for v, p in enumerate(points):
        gmsh.model.geo.addPoint(p[0], p[1], p[2], v+1)

    for e, (v1, v2) in enumerate(unique_edges):
        gmsh.model.geo.addLine(v1+1, v2+1, e+1)

    for f, (e, o) in enumerate(zip(triangle_unique_edges, triangle_edge_orientation)):
        gmsh.model.geo.addCurveLoop(list((e+1)*o), f+1)
        gmsh.model.geo.addPlaneSurface([f+1], f+1)
    gmsh.model.geo.synchronize()


def _mesh_from_gmsh():
    vertices, vertex_points, _ = gmsh.model.mesh.getNodes()
    vertices = vertices.astype(int)
    vertex_points = vertex_points.reshape((len(vertices), -1)).astype(float)

    all_triangle_vertices = []
    for f in gmsh.model.getEntities(2):
        _, (triangles,), (triangle_vertices,) = gmsh.model.mesh.getElements(*f)
        triangle_vertices = triangle_vertices.reshape((len(triangles), -1)).astype(int)
        all_triangle_vertices += list(triangle_vertices)

    return vertices, vertex_points, all_triangle_vertices


def gmsh_remeshing(points, triangles, target_length=1., feature_angle=180., boundary=True, verbose=False):
    """Remesh a surface defined by well-oriented triangles.

    Examples
    --------
    >>> import numpy as np

    >>> from cellcomplex.property_topomesh.example_topomesh import sphere_topomesh
    >>> from cellcomplex.property_topomesh.analysis import compute_topomesh_property
    >>> from cellcomplex.property_topomesh.creation import triangle_topomesh
    >>> from cellcomplex.property_topomesh.utils.gmsh_tools import gmsh_remeshing

    >>> topomesh = sphere_topomesh()
    >>> compute_topomesh_property(topomesh, 'oriented_vertices', 2)
    >>> compute_topomesh_property(topomesh, 'oriented_borders', 3)

    >>> points = topomesh.wisp_property('barycenter', 0).values(list(topomesh.wisps(0)))
    >>> triangle_ids, triangle_orientations = topomesh.wisp_property('oriented_borders', 3).values()[0]
    >>> triangles = topomesh.wisp_property('oriented_vertices', 2).values(triangle_ids)
    >>> triangles = np.array([t[::o] for t, o in zip(triangles, triangle_orientations)])

    >>> points, triangles = gmsh_remeshing(points, triangles, target_length=0.2)
    >>> remeshed_topomesh = triangle_topomesh(triangles, points)

    """
    gmsh.initialize()
    if not verbose:
        gmsh.option.setNumber("General.Verbosity", 2)

    start_time = current_time()
    logging.info(f"    --> Building original mesh")
    gmsh.clear()

    _add_gmsh_geo_from_mesh(points, triangles)
    end_time = current_time()
    logging.info(f"    <-- Building original mesh [{end_time-start_time}s]")

    start_time = current_time()
    logging.info(f"    --> Generating original mesh")
    gmsh.model.mesh.generate(2)
    end_time = current_time()
    logging.info(f"    <-- Generating original mesh [{end_time-start_time}s]")

    start_time = current_time()
    f = tempfile.NamedTemporaryFile(suffix=".stl")
    gmsh.write(f.name)
    gmsh.clear()
    gmsh.merge(f.name)
    gmsh.model.mesh.remove_duplicate_nodes()
    gmsh.model.mesh.classify_surfaces(np.radians(feature_angle), boundary=boundary, forReparametrization=True)
    end_time = current_time()
    logging.info(f"    --> Classifying surfaces [{end_time-start_time}s]")

    start_time = current_time()
    gmsh.model.mesh.create_topology()
    gmsh.model.mesh.create_geometry()
    gmsh.model.geo.synchronize()
    end_time = current_time()
    logging.info(f"    --> Re-parameterizing mesh surface [{end_time-start_time}s]")

    start_time = current_time()
    gmsh.option.setNumber("Mesh.CharacteristicLengthMin", target_length);
    gmsh.option.setNumber("Mesh.CharacteristicLengthMax", target_length);

    gmsh.model.mesh.generate(2)
    remeshed_vertices, remeshed_points, remeshed_triangles = _mesh_from_gmsh()
    end_time = current_time()
    logging.info(f"    --> Remeshing parameterized surface [{end_time-start_time}s]")

    try:
        gmsh.finalize()
    except:
        logging.error("GMSH couldn't finalize properly!")

    remeshed_positions = dict(zip(remeshed_vertices, remeshed_points))

    return remeshed_positions, remeshed_triangles


def gmsh_triangular_topomesh(polygonal_topomesh, edge_length=1.):
    gmsh.initialize()

    gmsh.clear()
    gmsh.model.add("mesh")
    _gmsh_add_geo_from_topomesh(polygonal_topomesh)

    gmsh.model.geo.synchronize()

    gmsh.option.setNumber("Mesh.CharacteristicLengthMin", edge_length);
    gmsh.option.setNumber("Mesh.CharacteristicLengthMax", edge_length);
    gmsh.model.mesh.generate(2)

    vertices, vertex_points, _ = gmsh.model.mesh.getNodes()
    vertices = vertices.astype(int)
    vertex_points = vertex_points.reshape((len(vertices), -1)).astype(float)
    vertex_positions = dict(zip(vertices, vertex_points))

    all_triangle_vertices = []
    all_triangle_faces = []
    for f in gmsh.model.getEntities(2):
        _, (triangles,), (triangle_vertices,) = gmsh.model.mesh.getElements(*f)
        all_triangle_faces += [f[1] for _ in triangles]
        triangle_vertices = triangle_vertices.reshape((len(triangles), -1)).astype(int)
        all_triangle_vertices += list(triangle_vertices)
    try:
        gmsh.finalize()
    except:
        logging.error("GMSH couldn't finalize properly!")

    topomesh = triangle_topomesh(all_triangle_vertices, vertex_positions)
    topomesh.update_wisp_property('face', 2, dict(zip(range(len(all_triangle_vertices)), all_triangle_faces)))
    topomesh.update_wisp_property('label', 2, dict(zip(range(len(all_triangle_vertices)), [f%256 for f in all_triangle_faces])))

    return topomesh