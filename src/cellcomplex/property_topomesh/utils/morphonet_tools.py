import logging
from time import time as current_time

import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_triangular
from cellcomplex.property_topomesh.extraction import star_interface_topomesh, cell_topomesh

def save_obj_topomesh(obj_filename, topomesh, coef=0.97):
    """

    Parameters
    ----------
    obj_filename
    topomesh
    coef

    Returns
    -------

    """

    obj_file = open(obj_filename,"w+")
    offset = 1
    for c in topomesh.wisps(3):
        start_time = current_time()
        obj_file.write("g cell_"+str(c)+"\n")
        mesh = cell_topomesh(topomesh,[c])
        logging.debug(f"    --> Extract cell [{mesh.nb_wisps(3)}] {c} [{current_time()-start_time}s]")


        prop_start_time = current_time()
        compute_topomesh_property(mesh, 'barycenter', 3)
        logging.debug(f"    --> barycenter 3 [{mesh.nb_wisps(3)}] {c} [{current_time()-prop_start_time}s]")

        prop_start_time = current_time()
        vertex_dict = {}
        for v,p in enumerate(mesh.wisps(0)):
            point = mesh.wisp_property('barycenter',3).values()[0] + coef*(mesh.wisp_property('barycenter',0)[p]-mesh.wisp_property('barycenter',3).values()[0])
            obj_file.write("v "+str(point[0])+" "+str(point[1])+" "+str(point[2])+"\n")
            vertex_dict[p]=v+offset
        offset += mesh.nb_wisps(0)
        logging.debug(f"    --> vertices [{mesh.nb_wisps(0)}] {c} [{current_time()-prop_start_time}s]")

        if (mesh.nb_wisps(2)>1) and (is_triangular(mesh)):
            compute_topomesh_property(mesh,'barycenter',2)
            compute_topomesh_property(mesh,'normal',2,normal_method='orientation')
            cell_orientation = np.nanmedian(np.sign(np.einsum('...ij,...ij->...i',mesh.wisp_property('barycenter',2).values()-mesh.wisp_property('barycenter',3).values()[0][np.newaxis,:],mesh.wisp_property('normal',2).values())))
            cell_orientation += int(cell_orientation == 0)

            for f in mesh.wisps(2):
                face_orientation = mesh.wisp_property('orientation',2)[f]
                face_orientation += int(face_orientation == 0)

                # t = list(mesh.borders(2,f,2))[::mesh.wisp_property('orientation',2)[f]]
                t = mesh.wisp_property('oriented_vertices',2)[f][::int(cell_orientation*face_orientation)]
                #obj_file.write("f "+str(vertex_dict[t[0]])+" "+str(vertex_dict[t[1]])+" "+str(vertex_dict[t[2]])+"\n")
                face_string = "f"
                for v in t:
                    face_string += " "+str(vertex_dict[v])
                face_string += "\n"
                obj_file.write(face_string)
        else:
            prop_start_time = current_time()
            compute_topomesh_property(mesh,'oriented_vertices',2)
            logging.debug(f"    --> oriented_vertices 2 [{mesh.nb_wisps(2)}] {c} [{current_time()-prop_start_time}s]")

            prop_start_time = current_time()
            for f in mesh.wisps(2):
                t = mesh.wisp_property('oriented_vertices',2)[f]
                #obj_file.write("f "+str(vertex_dict[t[0]])+" "+str(vertex_dict[t[1]])+" "+str(vertex_dict[t[2]])+"\n")
                face_string = "f"
                for v in t:
                    face_string += " "+str(vertex_dict[v])
                face_string += "\n"
                obj_file.write(face_string)

            logging.debug(f"    --> faces [{mesh.nb_wisps(2)}] {c} [{current_time()-prop_start_time}s]")
        logging.debug("  --> Write cell "+str(c)+" ["+str(current_time()-start_time)+" s]")
    obj_file.flush()
    obj_file.close()


def save_obj_time_topomesh(obj_filename, topomesh_list, coef=0.97):
    """

    Parameters
    ----------
    obj_filename
    topomesh_list
    coef

    Returns
    -------

    """

    obj_file = open(obj_filename,"w+")
    offset = 1

    for time, topomesh in enumerate(topomesh_list):
        for c in topomesh.wisps(3):
            obj_file.write("g "+str(time)+", cell_"+str(c)+"\n")
            # mesh = star_interface_topomesh(cell_topomesh(topomesh,[c]))
            mesh = cell_topomesh(topomesh,[c])
            compute_topomesh_property(mesh,'normal',2,normal_method='orientation')
            compute_topomesh_property(mesh,'barycenter',2)
            compute_topomesh_property(mesh,'barycenter',3)
            cell_orientation = np.median(np.sign(np.einsum('...ij,...ij->...i',mesh.wisp_property('barycenter',2).values()-mesh.wisp_property('barycenter',3).values()[0][np.newaxis,:],mesh.wisp_property('normal',2).values())))
            vertex_dict = {}
            for v,p in enumerate(mesh.wisps(0)):
                point = mesh.wisp_property('barycenter',3).values()[0] + coef*(mesh.wisp_property('barycenter',0)[p]-mesh.wisp_property('barycenter',3).values()[0])
                obj_file.write("v "+str(point[0])+" "+str(point[1])+" "+str(point[2])+"\n")
                vertex_dict[p]=v+offset
            offset += mesh.nb_wisps(0)
            for f in mesh.wisps(2):
                t = mesh.wisp_property('oriented_vertices',2)[f][::int(cell_orientation*mesh.wisp_property('orientation',2)[f])]
                obj_file.write("f "+str(vertex_dict[t[0]])+" "+str(vertex_dict[t[1]])+" "+str(vertex_dict[t[2]])+"\n")

        logging.getLogger().setLevel(logging.INFO)
        logging.info("Wrote mesh "+str(time)+" / "+str(len(topomesh_list)-1))
    obj_file.flush()
    obj_file.close()


def time_topomesh_info_dicts(topomesh_list, property_names=None):
    """

    Parameters
    ----------
    topomesh_list
    property_names

    Returns
    -------

    """

    if property_names is None:
        property_names = np.unique([list(topomesh.wisp_property_names(3)) for topomesh in topomesh_list])
    property_names = [p for p in property_names if np.all([p in topomesh.wisp_property_names(3) for topomesh in topomesh_list])]

    infos = {}
    for f in property_names:
        infos[f] = {}
    for f in ['space', 'time']:
        infos[f] = {}

    for i_time, topomesh in enumerate(topomesh_list):
        for field in property_names:
            infos[field].update(dict(zip([(i_time, c) for c in topomesh.wisps(3)], [0 for c in topomesh.wisps(3)])))

        infos['space'].update(dict(zip([(i_time, c) for c in topomesh.wisps(3)], [[] for c in topomesh.wisps(3)])))
        infos['time'].update(dict(zip([(i_time, c) for c in topomesh.wisps(3)], [[] for c in topomesh.wisps(3)])))

        for field in property_names:
            field_values = topomesh.wisp_property(field,3).values()
            if field_values.ndim == 1:
                for c in topomesh.wisps(3):
                    field_value = topomesh.wisp_property(field, 3)[c]
                    if field_value is not None:
                        infos[field][(i_time, c)] = field_value
            elif field_values.ndim == 2:
                if (field_values.shape[1] == 3)&(field != 'barycenter'):
                    if not topomesh.has_wisp_property('barycenter',3,is_computed=True):
                        compute_topomesh_property(topomesh,'barycenter',3)
                    for c in topomesh.wisps(3):
                        center = topomesh.wisp_property('barycenter',3)[c]
                        field_value = topomesh.wisp_property(field, 3)[c]
                        centered_field_value = np.array(list(center-field_value) + list(center+field_value))
                        if centered_field_value is not None:
                            infos[field][(i_time, c)] = centered_field_value
                else:
                    for c in topomesh.wisps(3):
                        field_value = topomesh.wisp_property(field, 3)[c]
                        if field_value is not None:
                            infos[field][(i_time, c)] = field_value


        for c in topomesh.wisps(3):
            for neighbor in topomesh.border_neighbors(3, c):
                infos['space'][(i_time, c)] += [(i_time, neighbor)]

        if i_time < (len(topomesh_list) - 1):
            for c in topomesh.wisps(3):
                infos['time'][(i_time, c)] += [(i_time + 1, c)]

    return infos


def save_info_dicts(infos, dirname, filename):
    """

    Parameters
    ----------
    infos
    dirname
    filename

    Returns
    -------

    """

    for field in infos.keys():
        infos_file = open(dirname + "/" + filename + "_" + field + "_infos.txt", 'w+')
        if field == 'layer':
            infos_file.write('type:group\n')
            for (i_time, c), layer in infos['layer'].items():
                infos_file.write(str(i_time) + "," + str(c) + ":\"" + "L" + str(layer) + "\"\n")

        elif field == 'space':
            infos_file.write('type:space\n')
            for (i_time, c), neighbors in infos['space'].items():
                for _, neighbor in neighbors:
                    infos_file.write(str(i_time) + "," + str(c) + ":" + str(i_time) + "," + str(neighbor) + "\n")

        elif field == 'time':
            infos_file.write('type:time\n')
            for (i_time, c), lineage in infos['time'].items():
                for i_next_time, next_c in lineage:
                    infos_file.write(str(i_time) + "," + str(c) + ":" + str(i_next_time) + "," + str(next_c) + "\n")

        else:
            dtype = np.array(infos[field].values()).dtype.name
            ndim = np.array(infos[field].values()).ndim

            if ndim == 1:
                if ('int' in dtype) or ('char' in dtype) or ('long' in dtype):
                    infos_file.write('type:selection\n')
                    for (i_time, c), value in infos[field].items():
                        infos_file.write(str(i_time) + "," + str(c) + ":" + str(int(value)) + "\n")
                elif ('float' in dtype) or ('double' in dtype):
                    infos_file.write('type:float\n')
                    for (i_time, c), value in infos[field].items():
                        infos_file.write(str(i_time) + "," + str(c) + ":" + str(float(value)) + "\n")
            elif ndim == 2:
                if (np.array(infos[field].values()).shape[1] == 6):
                    if ('float' in dtype) or ('double' in dtype):
                        infos_file.write('type:vector\n')
                        for (i_time, c), value in infos[field].items():
                            infos_file.write(str(i_time) + "," + str(c) + ":")
                            for i in range(0,3):
                                infos_file.write(str(float(value[i])) + ",")
                            infos_file.write("100.,")
                            for i in range(3,6):
                                infos_file.write(str(float(value[i])) + ",")
                            infos_file.write("100.\n")
                elif (np.array(infos[field].values()).shape[1] == 3):
                    if ('float' in dtype) or ('double' in dtype):
                        infos_file.write('type:sphere\n')
                        for (i_time, c), value in infos[field].items():
                            infos_file.write(str(i_time) + "," + str(c) + ":")
                            for i in range(0,3):
                                infos_file.write(str(float(value[i])) + ",")
                            infos_file.write("2.\n")

        infos_file.write("\n")

        infos_file.flush()
        infos_file.close()