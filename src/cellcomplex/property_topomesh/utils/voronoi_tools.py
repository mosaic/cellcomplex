import logging

from copy import deepcopy

from tqdm import tqdm

import numpy as np
import scipy.ndimage as nd

from scipy.spatial import Voronoi
from scipy.cluster.vq import vq

import matplotlib.pyplot as plt
import matplotlib as mpl

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.creation import edge_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.optimization import topomesh_face_star_split
from cellcomplex.property_topomesh.topological_operations import topomesh_remove_boundary_vertex

from cellcomplex.property_topomesh.utils.intersection_tools import get_segment_intersection, intersecting_segment


def binary_image_contour_topomesh(img, contour_size=None):
    """

    Parameters
    ----------
    img
    contour_size

    Returns
    -------

    """

    img = 255 * nd.binary_opening(img > 128).astype(img.dtype)
    if img.ndim == 3:
        img = np.mean(img, axis=2).astype(img.dtype)
    img = 255 * nd.binary_fill_holes(img).astype(img.dtype)

    figure = plt.figure()
    contour = figure.gca().contour(img,[128],alpha=0.5,colors=['k'])
    contour_points = np.concatenate([path.to_polygons()[0] for path in contour.collections[0].get_paths()])
    contour_ids = np.concatenate([i_p*np.ones_like(path.to_polygons()[0][:, 0]) for i_p, path in enumerate(contour.collections[0].get_paths())])
    plt.close(figure)

    if contour_size is not None:
        contour_indices = np.linspace(0,len(contour_points-1),contour_size+1)[:-1].astype(int)
        contour_points = contour_points[contour_indices]
        contour_ids = contour_ids[contour_indices]
    else:
        contour_size = len(contour_points)

    contour_edges = []
    for i_c in np.unique(contour_ids):
        component_vertices = np.where(contour_ids==i_c)[0]
        component_edges = [
            component_vertices,
            list(component_vertices[1:])+[component_vertices[0]]
        ]
        contour_edges += list(np.transpose(component_edges))
    contour_topomesh = edge_topomesh(contour_edges, contour_points)

    return contour_topomesh


def is_closed(contour_topomesh):
    return np.all([contour_topomesh.nb_regions(0,v) == 2 for v in contour_topomesh.wisps(0)])


def random_points_within_contour(contour_topomesh, n_points=20):
    assert is_closed(contour_topomesh)

    contour_points = contour_topomesh.wisp_property('barycenter', 0).values(list(contour_topomesh.wisps(0)))
    contour_path = mpl.path.Path(contour_points[:, :2])

    points = []
    while len(points) < n_points:
        point = contour_points.min(axis=0) + np.random.rand(contour_points.shape[1])*(contour_points.max(axis=0) - contour_points.min(axis=0))
        if contour_path.contains_point(point[:2]):
            points += [point]
    return np.array(points)


def voronoi_topomesh_2d(cell_points, contour_topomesh, return_voronoi=False):

    n_cells = len(cell_points)
    center = cell_points.mean(axis=0)
    extent = np.max(cell_points.max(axis=0) - cell_points.min(axis=0))

    contour_points = contour_topomesh.wisp_property('barycenter',0).values(list(contour_topomesh.wisps(0)))
    contour_path = mpl.path.Path(contour_points[:, :2])

    compute_topomesh_property(contour_topomesh, 'vertices', 1)
    contour_edge_vertices = contour_topomesh.wisp_property('vertices',1).values(list(contour_topomesh.wisps(1)))
    contour_edge_points = contour_topomesh.wisp_property('barycenter',0).values(contour_edge_vertices)

    vor = Voronoi(cell_points[:, :2])

    voronoi_points = list(vor.vertices)

    logging.debug(str(vor.ridge_vertices))

    voronoi_edges = []
    voronoi_edge_cells = []

    contour_edges_to_split = []
    contour_edge_split_vertices = []
    contour_edge_split_vertex_cells = []

    for edge_cells, voronoi_edge_vertices in zip(vor.ridge_points, vor.ridge_vertices):
        voronoi_edge_vertices = np.array(voronoi_edge_vertices)
        if np.all(voronoi_edge_vertices != -1):
            voronoi_edge_points = vor.vertices[voronoi_edge_vertices]

            voronoi_edge_points_inside = [contour_path.contains_point(p) for p in voronoi_edge_points]
            if np.all(voronoi_edge_points_inside):
                voronoi_edges += [list(voronoi_edge_vertices)]
                voronoi_edge_cells += [list(edge_cells)]
            elif np.any(voronoi_edge_points_inside):
                edge_vertex = voronoi_edge_vertices[voronoi_edge_points_inside][0]
                logging.debug(str(vor.vertices[edge_vertex]))

                contour_intersection = intersecting_segment(voronoi_edge_points, contour_edge_points[..., :2])
                if np.any(contour_intersection):
                    voronoi_edges += [[edge_vertex, len(voronoi_points)]]
                    voronoi_edge_cells += [list(edge_cells)]

                    intersecting_contour_edge = np.array(list(contour_topomesh.wisps(1)))[contour_intersection][0]
                    intersecting_contour_edge_points = contour_edge_points[contour_intersection][0]
                    intersection_point = get_segment_intersection(voronoi_edge_points, intersecting_contour_edge_points[..., :2])

                    contour_edges_to_split += [intersecting_contour_edge]
                    contour_edge_split_vertices += [len(voronoi_points)]
                    contour_edge_split_vertex_cells += [list(edge_cells)]

                    voronoi_points += [intersection_point]
            else:
                contour_intersection = intersecting_segment(voronoi_edge_points, contour_edge_points[..., :2])
                if (contour_intersection.sum()==2):
                    intersecting_contour_edges = np.array(list(contour_topomesh.wisps(1)))[contour_intersection]

                    voronoi_edges += [[len(voronoi_points)+1, len(voronoi_points)]]
                    voronoi_edge_cells += [list(edge_cells)]

                    for e in intersecting_contour_edges:
                        intersecting_contour_edge_vertices = contour_topomesh.wisp_property('vertices',1)[e]
                        intersecting_contour_edge_points = contour_topomesh.wisp_property('barycenter',0).values(intersecting_contour_edge_vertices)[:, :2]
                        intersection_point = get_segment_intersection(voronoi_edge_points, intersecting_contour_edge_points[..., :2])

                        contour_edges_to_split += [e]
                        contour_edge_split_vertices += [len(voronoi_points)]
                        contour_edge_split_vertex_cells += [list(edge_cells)]

                        voronoi_points += [intersection_point]
                else:
                    logging.info("Outside edge!")

        else:
            edge_vertex = voronoi_edge_vertices[voronoi_edge_vertices != -1][0]  # finite end Voronoi vertex

            if contour_path.contains_point(vor.vertices[edge_vertex]):
                cell_vector = cell_points[edge_cells[1]] - cell_points[edge_cells[0]]  # tangent
                cell_vector = cell_vector / np.linalg.norm(cell_vector)
                edge_vector = np.array([-cell_vector[1], cell_vector[0]])  # normal
                cell_midpoint = cell_points[edge_cells].mean(axis=0)[:2]
                edge_direction = np.sign(np.dot(cell_midpoint - center[:2], edge_vector))
                edge_far_point = vor.vertices[edge_vertex] + edge_direction * edge_vector * extent

                voronoi_edge_points = [vor.vertices[edge_vertex], edge_far_point]

                contour_intersection = intersecting_segment(voronoi_edge_points, contour_edge_points[..., :2])

                if np.any(contour_intersection):
                    voronoi_edges += [[edge_vertex, len(voronoi_points)]]
                    voronoi_edge_cells += [list(edge_cells)]

                    intersecting_contour_edge = np.array(list(contour_topomesh.wisps(1)))[contour_intersection][0]
                    intersecting_contour_edge_points = contour_edge_points[contour_intersection][0]
                    intersection_point = get_segment_intersection(voronoi_edge_points, intersecting_contour_edge_points[..., :2])

                    contour_edges_to_split += [intersecting_contour_edge]
                    contour_edge_split_vertices += [len(voronoi_points)]
                    contour_edge_split_vertex_cells += [list(edge_cells)]

                    voronoi_points += [intersection_point]

    voronoi_edges = np.array(voronoi_edges)
    voronoi_edge_cells = np.array(voronoi_edge_cells)

    voronoi_points = np.array(voronoi_points)

    contour_edges_to_split = np.array(contour_edges_to_split)
    contour_edge_split_vertices = np.array(contour_edge_split_vertices)
    contour_edge_split_vertex_cells = np.array(contour_edge_split_vertex_cells)

    voronoi_topomesh = PropertyTopomesh(3)

    for c in range(n_cells):
        voronoi_topomesh.add_wisp(3,c)
        voronoi_topomesh.add_wisp(2,c)
        voronoi_topomesh.link(3,c,c)
    voronoi_topomesh.update_wisp_property('voronoi_point', 3, dict(zip(range(n_cells), cell_points)))

    voronoi_kept_vertices = np.unique(voronoi_edges)
    positions = {}
    contour_vertex_index = {}
    for v, p in enumerate(voronoi_points):
        if v in voronoi_kept_vertices:
            voronoi_topomesh.add_wisp(0,v)
            positions[v] = list(p) + [np.mean(contour_points[:,2+k]) for k in range(contour_points.shape[1]-2)]
    for v, p in enumerate(contour_points):
        vid = voronoi_topomesh.add_wisp(0)
        positions[vid] = p
        contour_vertex_index[v] = vid
    voronoi_topomesh.update_wisp_property('barycenter', 0, positions)

    for edge_vertices, edge_cells in zip(voronoi_edges, voronoi_edge_cells):
        e = voronoi_topomesh.add_wisp(1)
        for v in edge_vertices:
            voronoi_topomesh.link(1, e, v)
        for c in edge_cells:
            voronoi_topomesh.link(2, c, e)

    for e, edge_vertices in enumerate(contour_edge_vertices):
        if not e in contour_edges_to_split:
            eid = voronoi_topomesh.add_wisp(1)
            for v in edge_vertices:
                voronoi_topomesh.link(1, eid, contour_vertex_index[v])
            edge_cells = vq(contour_points[edge_vertices], cell_points)[0]
            if len(np.unique(edge_cells)) != 1:
                logging.error("Contour <-> Cell connection error!")
            voronoi_topomesh.link(2, edge_cells[0], eid)
        else:
            if (contour_edges_to_split==e).sum() == 1:
                split_vertex = contour_edge_split_vertices[contour_edges_to_split==e][0]
                logging.debug(str(split_vertex))
                for v in edge_vertices:
                    eid = voronoi_topomesh.add_wisp(1)
                    voronoi_topomesh.link(1, eid, contour_vertex_index[v])
                    voronoi_topomesh.link(1, eid, split_vertex)
                    edge_cell = vq([contour_points[v]], cell_points)[0][0]
                    voronoi_topomesh.link(2, edge_cell, eid)
            else:
                logging.debug("Contour edge multi-split!")
                split_vertices = contour_edge_split_vertices[contour_edges_to_split==e]
                split_vertex_cells = contour_edge_split_vertex_cells[contour_edges_to_split==e]

                split_points = voronoi_points[split_vertices]
                edge_points = contour_points[edge_vertices][..., :2]

                edge_vector = edge_points[1] - edge_points[0]
                split_vectors = [p - edge_points[0] for p in split_points]
                split_abscissa = [np.dot(v, edge_vector) for v in split_vectors]

                sorted_split_vertices = split_vertices[np.argsort(split_abscissa)]
                sorted_split_vertex_cells = split_vertex_cells[np.argsort(split_abscissa)]

                eid = voronoi_topomesh.add_wisp(1)
                voronoi_topomesh.link(1, eid, contour_vertex_index[edge_vertices[0]])

                edge_cell = vq([edge_points[0]], cell_points[:, :2])[0][0]
                voronoi_topomesh.link(2, edge_cell, eid)
                logging.debug("  --> edge {} : cell {}".format(eid, edge_cell))

                for v, edge_cells in zip(sorted_split_vertices, sorted_split_vertex_cells):
                    voronoi_topomesh.link(1, eid, v)

                    logging.debug("  -> split vertex {} : cells {}".format(v, edge_cells))

                    eid = voronoi_topomesh.add_wisp(1)
                    voronoi_topomesh.link(1, eid, v)

                    edge_cell = [c for c in edge_cells if c != edge_cell][0]
                    voronoi_topomesh.link(2, edge_cell, eid)

                    logging.debug("  --> edge {} : cell {}".format(eid, edge_cell))

                voronoi_topomesh.link(1, eid, contour_vertex_index[edge_vertices[1]])

    if return_voronoi:
        return voronoi_topomesh, voronoi_points, voronoi_edges
    else:
        return voronoi_topomesh


def compute_topomesh_cell_centers(topomesh):
    triangulated_topomesh = topomesh_face_star_split(topomesh, split_edges=False)

    compute_topomesh_property(triangulated_topomesh, 'area', degree=2)
    compute_topomesh_property(triangulated_topomesh, 'vertices', degree=2)
    compute_topomesh_property(triangulated_topomesh, 'cells', degree=2)

    points = triangulated_topomesh.wisp_property('barycenter', 0).values()

    face_vertices = triangulated_topomesh.wisp_property('vertices', 2).values(list(triangulated_topomesh.wisps(2)))
    face_points = triangulated_topomesh.wisp_property('barycenter',0).values(face_vertices)
    face_centers = np.mean(face_points, axis=1)

    face_areas = triangulated_topomesh.wisp_property('area', 2).values(list(triangulated_topomesh.wisps(2)))
    face_cells = triangulated_topomesh.wisp_property('cells', 2).values(list(triangulated_topomesh.wisps(2)))[:,0]
    cell_centers = np.transpose([nd.sum(face_areas*face_centers[:,k], face_cells, list(topomesh.wisps(3))) /
                                 nd.sum(face_areas, face_cells, list(topomesh.wisps(3)))
                                 for k in range(points.shape[1])])
    topomesh.update_wisp_property('center',3,dict(zip(topomesh.wisps(3), cell_centers)))


def centroidal_voronoi_topomesh_2d(contour_topomesh, n_cells, iterations=100, lloyd_coef=0.33, remove_contour_vertices=False, return_trajectory=False):

    cell_points = random_points_within_contour(contour_topomesh, n_points=n_cells)

    if return_trajectory:
        trajectory = []

    for iteration in tqdm(range(iterations+1), total=iterations, unit=' iterations'):
        voronoi_topomesh = voronoi_topomesh_2d(cell_points, contour_topomesh)
        compute_topomesh_cell_centers(voronoi_topomesh)

        if return_trajectory:
            trajectory.append(deepcopy(voronoi_topomesh))

        cell_centers = voronoi_topomesh.wisp_property('center', 3).values(list(voronoi_topomesh.wisps(3)))
        cell_points = cell_points + lloyd_coef*(cell_centers - cell_points)

    if remove_contour_vertices:
        compute_topomesh_property(voronoi_topomesh, 'contour', 1)
        compute_topomesh_property(voronoi_topomesh, 'vertices', 1)
        compute_topomesh_property(voronoi_topomesh, 'contour', 0)

        vertices_to_remove = [v for v in voronoi_topomesh.wisps(0)
                              if voronoi_topomesh.wisp_property('contour', 0)[v]
                              and voronoi_topomesh.nb_regions(0, v) == 2]

        for v in vertices_to_remove:
            topomesh_remove_boundary_vertex(voronoi_topomesh, v)

        compute_topomesh_property(voronoi_topomesh, 'contour', 1)
        compute_topomesh_property(voronoi_topomesh, 'vertices', 1)
        compute_topomesh_property(voronoi_topomesh, 'contour', 0)

    if return_trajectory:
        return voronoi_topomesh, trajectory
    else:
        return voronoi_topomesh