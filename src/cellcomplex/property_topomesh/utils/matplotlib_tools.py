import logging

from cellcomplex.property_topomesh.visualization.matplotlib import plot_tensor_data, mpl_draw_topomesh, mpl_draw_incidence_graph

logging.warning("The module cellcomplex.property_topomesh.utils.matplotlib_tools is DEPRECATED!")
logging.warning("Imports should be made from cellcomplex.property_topomesh.visualization.matplotlib")