# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import numpy as np
from scipy.spatial.qhull import Delaunay

from cellcomplex.property_topomesh.creation import tetrahedra_topomesh, triangle_topomesh, edge_topomesh

from cellcomplex.property_topomesh.utils.triangulation_tools import tetrahedra_from_triangulation

from cellcomplex.utils import array_dict

tetra_triangle_edge_list = np.array([[0,1],[0,2],[0,3],[1,2],[1,3],[2,3]])
tetra_triangle_list = np.array([[0,1,2],[0,1,3],[0,2,3],[1,2,3]])
triangle_edge_list = np.array([[1, 2],[0, 2],[0, 1]])


def delaunay_triangulation(points):
    if np.any(np.isnan(points)):
        triangles = np.array([])
    elif len(np.unique(points[:,2])) == 1:
        if np.all(points==0):
            triangles = np.array([])
        else:
            # try:
            triangles = Delaunay(np.array(points)[:,:2]).simplices
            # except QhullError:
            #    triangles = np.array([])
    else:
        if np.all(points==0):
            tetras = [[0,1,2,3]]
        else:
            # try:
            tetras = Delaunay(np.array(points)).simplices
            # except QhullError:
            #     tetras = [[0,1,2,3]]
        
        triangles = np.unique(np.sort(np.concatenate(tetras[:,tetra_triangle_list])),axis=0)

    return triangles


def delaunay_triangulation_topomesh(positions,delaunay_positions=None):
    if delaunay_positions is None:
        delaunay_positions = positions

    triangles = np.array(delaunay_positions.keys())[delaunay_triangulation(np.array(delaunay_positions.values()))]

    return triangle_topomesh(triangles,positions)


def convex_hull_topomesh(positions, degree=3):
    if degree == 2:
        topomesh = delaunay_triangulation_topomesh(positions)

        convex_hull_edges = [e for e in topomesh.wisps(1) if topomesh.nb_regions(1,e) == 1]
        convex_hull_edge_vertices = [list(topomesh.borders(1,e)) for e in convex_hull_edges]

        convex_hull_positions = {v:topomesh.wisp_property('barycenter',0)[v] for v in np.unique(convex_hull_edge_vertices)}
        convex_hull_topomesh = edge_topomesh(convex_hull_edge_vertices, convex_hull_positions)

        fid = convex_hull_topomesh.add_wisp(2)
        for eid in convex_hull_topomesh.wisps(1):
            convex_hull_topomesh.link(2,fid,eid)
        convex_hull_topomesh.add_wisp(3, 0)

        for fid in convex_hull_topomesh.wisps(2):
            convex_hull_topomesh.link(3, 0, fid)
            
    elif degree == 3:
        triangles = np.array(list(positions.keys()))[delaunay_triangulation(np.array(list(positions.values())))]
        tetrahedra = tetrahedra_from_triangulation(np.sort(triangles),array_dict(positions),exterior=False)

        topomesh = tetrahedra_topomesh(tetrahedra, positions)

        convex_hull_triangles = [f for f in topomesh.wisps(2) if topomesh.nb_regions(2,f) == 1]
        convex_hull_triangle_vertices = [list(topomesh.borders(2,f,2)) for f in convex_hull_triangles]
        convex_hull_positions = {v:topomesh.wisp_property('barycenter',0)[v] for v in np.unique(convex_hull_triangle_vertices)}
        convex_hull_topomesh = triangle_topomesh(convex_hull_triangle_vertices, convex_hull_positions)
            
    return convex_hull_topomesh