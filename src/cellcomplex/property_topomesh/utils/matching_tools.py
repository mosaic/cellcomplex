# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import numpy as np
from scipy.spatial import cKDTree
from scipy.cluster.vq import vq

def brute_force_match(obs, codebook):
    distance_matrix = np.linalg.norm(obs[:,np.newaxis] - codebook[np.newaxis,:],axis=2)
    return (np.argmin(distance_matrix,axis=1),np.min(distance_matrix,axis=1))

def kd_tree_match(obs, codebook, radius=1e-5):
    code = cKDTree(codebook)
    res1 = code.query(obs, k=1, distance_upper_bound=radius)[1]
    return res1

def vector_quantization_match(obs, codebook):
    return vq(obs, codebook)[0]