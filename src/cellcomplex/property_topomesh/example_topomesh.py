# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import logging

import numpy as np

import scipy.ndimage as nd
from scipy.cluster.vq import vq

from cellcomplex.utils import array_dict

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.creation import triangle_topomesh, quad_topomesh, poly_topomesh, dual_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_triangle_properties
from cellcomplex.property_topomesh.extraction import star_interface_topomesh, clean_topomesh
from cellcomplex.property_topomesh.optimization import topomesh_triangle_split, property_topomesh_vertices_deformation
from cellcomplex.property_topomesh.composition import append_topomesh, fuse_topomesh_identical_vertices

from cellcomplex.property_topomesh.utils.delaunay_tools import delaunay_triangulation
from cellcomplex.property_topomesh.utils.intersection_tools import intersecting_segment, inside_triangle, get_segment_intersection


def square_topomesh(side_length=1, center=np.array([0,0,0],float), triangular=True):
    points = {}
    points[0] = center + np.array([-side_length/2, -side_length/2, 0])
    points[1] = center + np.array([ side_length/2, -side_length/2, 0])
    points[2] = center + np.array([-side_length/2,  side_length/2, 0])
    points[3] = center + np.array([side_length/2,  side_length/2, 0])

    if triangular:
        return triangle_topomesh([[0,1,2],[3,2,1]], points)
    else:
        return poly_topomesh([[0,1,3,2]], points)


def cube_topomesh(side_length=1, center=np.array([0,0,0],float), triangular=True):
    points = {}
    points[0] = center + np.array([-side_length/2, -side_length/2, -side_length/2])
    points[1] = center + np.array([ side_length/2, -side_length/2, -side_length/2])
    points[2] = center + np.array([ side_length/2,  side_length/2, -side_length/2])
    points[3] = center + np.array([-side_length/2,  side_length/2, -side_length/2])
    points[4] = center + np.array([-side_length/2, -side_length/2,  side_length/2])
    points[5] = center + np.array([ side_length/2, -side_length/2,  side_length/2])
    points[6] = center + np.array([ side_length/2,  side_length/2,  side_length/2])
    points[7] = center + np.array([-side_length/2,  side_length/2,  side_length/2])

    faces = []
    faces += [[0,1,2,3]]
    faces += [[0,1,5,4]]
    faces += [[1,2,6,5]]
    faces += [[2,3,7,6]]
    faces += [[3,0,4,7]]
    faces += [[7,6,5,4]]
    faces = np.array(faces)

    if triangular:
        triangles = [f[[0,1,3]] for f in faces] + [f[[2,3,1]] for f in faces]
        return triangle_topomesh(triangles, points)
    else:
        return poly_topomesh(faces, points)


def hexagon_topomesh(side_length = 1, angle=0.):
    points = {}
    points[0] = [0,0,0]
    for p in range(6):
        points[p+1] = [side_length*np.cos(p*np.pi/3.+angle),side_length*np.sin(p*np.pi/3.+angle),0]

    triangles = []
    for p in range(6):
        triangles += [[0,p + 1,(p+1)%6 + 1]]

    return triangle_topomesh(triangles, points)


def icosahedron_topomesh(center=np.array([0,0,0],float), radius=10.):
    ico_points = {}
    ico_points[0] = center + radius*np.array([ 0.      , -1.      ,  0.      ])
    ico_points[1] = center + radius*np.array([ 0.7236  , -0.447215,  0.52572 ])
    ico_points[2] = center + radius*np.array([-0.276385, -0.447215,  0.85064 ])
    ico_points[3] = center + radius*np.array([-0.894425, -0.447215,  0.      ])
    ico_points[4] = center + radius*np.array([-0.276385, -0.447215, -0.85064 ])
    ico_points[5] = center + radius*np.array([ 0.7236  , -0.447215, -0.52572 ])
    ico_points[6] = center + radius*np.array([ 0.276385,  0.447215,  0.85064 ])
    ico_points[7] = center + radius*np.array([-0.7236  ,  0.447215,  0.52572 ])
    ico_points[8] = center + radius*np.array([-0.7236  ,  0.447215, -0.52572 ])
    ico_points[9] = center + radius*np.array([ 0.276385,  0.447215, -0.85064 ])
    ico_points[10] = center + radius*np.array([ 0.894425,  0.447215,  0.      ])
    ico_points[11] = center + radius*np.array([ 0.      ,  1.      ,  0.      ])

    ico_triangles = [[ 0,  1,  2],
        [ 0,  1,  5],
        [ 0,  2,  3],
        [ 0,  3,  4],
        [ 0,  4,  5],
        [ 1,  5, 10],
        [ 1,  2,  6],
        [ 2,  3,  7],
        [ 3,  4,  8],
        [ 4,  5,  9],
        [ 1,  6, 10],
        [ 2,  6,  7],
        [ 3,  7,  8],
        [ 4,  8,  9],
        [ 5,  9, 10],
        [ 6, 10, 11],
        [ 6,  7, 11],
        [ 7,  8, 11],
        [ 8,  9, 11],
        [ 9, 10, 11]]

    return triangle_topomesh(ico_triangles, ico_points)


def truncated_octahedron_topomesh(center=np.array([0,0,0],float),radius=10.,ratio=1.):

    points = []
    points.append(np.array([0,-1,-1]))
    points.append(np.array([0,-1,1]))
    points.append(np.array([0,1,1]))
    points.append(np.array([0,1,-1]))
    points.append(np.array([np.sqrt(2),0,0]))
    points.append(np.array([-np.sqrt(2),0,0]))

    triangles = []
    triangles.append(np.array([0,1,4]))
    triangles.append(np.array([1,2,4]))
    triangles.append(np.array([2,3,4]))
    triangles.append(np.array([3,0,4]))
    triangles.append(np.array([1,0,5]))
    triangles.append(np.array([2,1,5]))
    triangles.append(np.array([3,2,5]))
    triangles.append(np.array([0,3,5]))
    triangles = np.array(triangles)

    for t in triangles:
        points.append(((ratio+1)*points[t[0]]+ratio*points[t[1]])/(2*ratio+1.))
        points.append(((ratio+1)*points[t[1]]+ratio*points[t[2]])/(2*ratio+1.))
        points.append(((ratio+1)*points[t[2]]+ratio*points[t[0]])/(2*ratio+1.))

    for t in triangles:
        points.append(np.mean([points[p] for p in t],axis=1))

    points = np.array(points)
    points = center + radius*points

    points = dict(zip(np.arange(len(points)), points))

    faces = []
    faces.append([6,16,27,19])
    faces.append([9,7,18,22])
    faces.append([12,10,21,25])
    faces.append([15,13,24,28])

    faces.append([8,11,14,17])
    faces.append([20,23,26,29])

    faces.append([6,16,8,11,7,18])
    faces.append([9,7,11,14,10,21])
    faces.append([12,10,14,17,13,24])
    faces.append([15,13,17,8,16,27])
    #faces.append([9,21,25,23,20,22])

    faces.append([15,28,26,29,19,27])
    faces.append([12,25,23,26,28,24])
    faces.append([9,22,20,23,25,21])
    faces.append([6,19,29,20,22,18])
    # faces.append([9,7,11,14,10,21])
    # faces.append([12,10,14,17,13,24])
    # faces.append([15,13,17,8,16,27])

    topomesh = poly_topomesh(faces,points)

    return topomesh


def hexagonal_prism_topomesh(center=np.array([0,0,0],float),radius=10.,height=5.,angle=np.pi/6.):
    points = {}
    for p in range(6):
        points[p] = [radius*np.cos(p*np.pi/3.+angle),radius*np.sin(p*np.pi/3.+angle),0]

    top_hexagon_topomesh = poly_topomesh([[0,1,2,3,4,5]],points)
    top_hexagon_topomesh.update_wisp_property('barycenter',0,center+top_hexagon_topomesh.wisp_property('barycenter',0).values()+np.array([0,0,height/2.]))

    bottom_hexagon_topomesh = poly_topomesh([[0,1,2,3,4,5]],points)
    bottom_hexagon_topomesh.update_wisp_property('barycenter',0,center+bottom_hexagon_topomesh.wisp_property('barycenter',0).values()-np.array([0,0,height/2.]))

    top_positions = top_hexagon_topomesh.wisp_property('barycenter',0)
    bottom_positions = bottom_hexagon_topomesh.wisp_property('barycenter',0)

    side_topomeshes = []

    for t_e, b_e in zip(top_hexagon_topomesh.wisps(1),bottom_hexagon_topomesh.wisps(1)):
        top_vertices = list(top_hexagon_topomesh.borders(1,t_e))
        bottom_vertices = list(bottom_hexagon_topomesh.borders(1,b_e))

        points = {}
        points[0] = top_positions[top_vertices[0]]
        points[1] = top_positions[top_vertices[1]]
        points[2] = bottom_positions[bottom_vertices[1]]
        points[3] = bottom_positions[bottom_vertices[0]]

        side_topomeshes.append(poly_topomesh([[0,1,2,3]], points))

    topomesh,_ = append_topomesh(top_hexagon_topomesh,bottom_hexagon_topomesh)
    for side_topomesh in side_topomeshes:
        topomesh,_ = append_topomesh(topomesh,side_topomesh)

    fuse_topomesh_identical_vertices(topomesh)

    if not 0 in topomesh.wisps(3):
        topomesh.add_wisp(3,0)
    cells_to_remove = [c for c in topomesh.wisps(3) if c!=0]
    for f in topomesh.wisps(2):
        if not 0 in topomesh.regions(2,f):
            topomesh.link(3,0,f)
    for c in cells_to_remove:
        topomesh.remove_wisp(3,c)


    return topomesh


def sphere_topomesh(radius=1.0,center=np.zeros(3)):
    """

    Parameters
    ----------
    radius
    center

    Returns
    -------

    """

    ico = icosahedron_topomesh()
    topomesh = topomesh_triangle_split(topomesh_triangle_split(ico))

    positions = topomesh.wisp_property('barycenter',0)
    new_positions = array_dict(center + radius*positions.values()/np.linalg.norm(positions.values(),axis=1)[:,np.newaxis],positions.keys())
    topomesh.update_wisp_property('barycenter',0,new_positions)

    return topomesh


def triangular_grid_topomesh(size=1, cell_size=1., center=np.zeros(3)):
    """Generate a regular 2D grid of triangular faces.

    The function creates a PropertyTopomesh formed by layers of equilateral
    triangles stacked around a center point.

    Parameters
    ----------
    size : int
        The number of triangle layers around the center point
    cell_size : float
        The length of triangle sides
    center : list, :class:`np.ndarray`
        The position of the grid center point (default is (0,0,0))

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The grid mesh
    """

    x = []
    y = []
    for n in range(size+1,2*size+2):
        x += list((np.arange(n)-((n-1)/2.))*cell_size*np.sqrt(3.))
        y += [((2*size+1 - n))*cell_size*3./2. for p in range(n)]

    for n in range(size+1,2*size+1)[::-1]:
        x += list((np.arange(n)-((n-1)/2.))*cell_size*np.sqrt(3.))
        y += [-((2*size+1 - n))*cell_size*3./2. for p in range(n)]

    x = np.array(x) + center[0]
    y = np.array(y) + center[1]
    z = np.zeros_like(x) + center[2]

    positions = array_dict(dict(list(zip(list(range(len(x))),np.transpose([x,y,z])))))

    triangles = positions.keys()[delaunay_triangulation(positions.values())]

    topomesh = triangle_topomesh(triangles,positions)

    return topomesh


def square_grid_topomesh(size=1, y_size=None, cell_size=1., center=np.zeros(3)):
    """Generate a regular 2D grid of squared faces.

    The function creates a PropertyTopomesh formed by layers of squares
    stacked around a center point.

    Parameters
    ----------
    size : int
        The number of square layers around the center point
    cell_size : float
        The length of square sides
    center : list, :class:`np.ndarray`
        The position of the grid center point (default is (0,0,0))

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The grid mesh
    """

    if y_size is None:
        y_size = size

    x = np.linspace(0, size, size+1)*cell_size
    x += center[0] - size*cell_size/2
    y = np.linspace(0, y_size, y_size+1)*cell_size
    y += center[1] - y_size*cell_size/2

    xx,yy = np.meshgrid(x, y)

    zz = np.zeros_like(xx) + center[2]

    square_bl = np.concatenate([np.arange(size+1)[:-1]+i*(size+1) for i in np.arange(y_size+1)[:-1]])
    square_br = np.concatenate([np.arange(size+1)[1:]+i*(size+1) for i in np.arange(y_size+1)[:-1]])
    square_tl = np.concatenate([np.arange(size+1)[:-1]+i*(size+1) for i in np.arange(y_size+1)[1:]])
    square_tr = np.concatenate([np.arange(size+1)[1:]+i*(size+1) for i in np.arange(y_size+1)[1:]])

    squares = np.transpose([square_bl, square_tl, square_tr, square_br])

    positions = dict(list(zip(list(range(len(np.ravel(xx)))),np.transpose([np.ravel(xx),np.ravel(yy),np.ravel(zz)]))))

    topomesh = quad_topomesh(squares, positions, faces_as_cells=True)

    return topomesh


def hexagonal_grid_topomesh(size=1, cell_size=1., center=np.zeros(3), angle=0):
    """Generate a regular 2D grid of hexagonal faces.

    The function creates a PropertyTopomesh formed by layers of hexagons
    stacked around a center point and rotated by a given angle.

    Parameters
    ----------
    size : int
        The number of hexagon layers around the center point
    cell_size : float
        The length of hexagon sides
    center : list, :class:`np.ndarray`
        The position of the grid center point (default is (0,0,0))
    angle: float
        Tre rotation angle around the center point, in degrees

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The grid mesh
    """

    x = []
    y = []
    p = []
    for n in range(size+1,2*size+2):
        for r in [0,1]:
            p += list(len(p)+np.arange(n+r))
            x += list((np.arange(n+r)-((n+r-1)/2.))*cell_size*np.sqrt(3.))
            row_y = (((2*size+1 - n)+1)//2)*3
            row_side = (((2*size+1 - n)+1)%2)
            y += list((row_y+(2*row_side-1)-(row_side==r)*(2*row_side-1)/2.)*np.ones(n+r)*cell_size)

    for n in range(size+1,2*size+2)[::-1]:
        for r in [1,0]:
            p += list(len(p)+np.arange(n+r))
            x += list((np.arange(n+r)-((n+r-1)/2.))*cell_size*np.sqrt(3.))
            row_y = (((2*size+1 - n)+1)//2)*3
            row_side = (((2*size+1 - n)+1)%2)
            y += list(-(row_y+(2*row_side-1)-(row_side==r)*(2*row_side-1)/2.)*np.ones(n+r)*cell_size)

    rotation_matrix = np.array([[np.cos(np.radians(angle)),np.sin(np.radians(angle))],[-np.sin(np.radians(angle)),np.cos(np.radians(angle))]])
    x,y = np.transpose(np.einsum('...ij,...j->...i',rotation_matrix,np.transpose([x,y])))

    x = np.array(x) + center[0]
    y = np.array(y) + center[1]

    z = np.zeros_like(x) + center[2]

    positions = dict(list(zip(list(range(len(x))),np.transpose([x,y,z]))))

    index_list = []
    row_length_list = []
    row_gaps = []
    start = 0
    for n in range(size+1,2*size+1):
        index_list += list(np.arange(n)+start)
        start += n+n+1
        row_length_list += list(n*np.ones(n,int))
        for h in range(n):
            row_gaps += [[n+1,n+1,n+1,-(n+2),-(n+1)]]
    for n in [2*size+1]:
        index_list += list(np.arange(n)+start)
        start += n+n+2
        row_length_list += list(n*np.ones(n,int))
        for h in range(n):
            row_gaps += [[n+1,n+1,n,-(n+1),-(n+1)]]
    for n in range(2*size,size,-1):
        index_list += list(np.arange(n)+start)
        start += n+n+3
        row_length_list += list(n*np.ones(n,int))
        for h in range(n):
            row_gaps += [[n+2,n+1,n,-(n+1),-(n+1)]]

    hexagons = []
    for i,n,gaps in zip(index_list,row_length_list,row_gaps):
        hexagon = [i]
        for p, gap in zip(range(5),gaps):
            hexagon += [hexagon[-1]+gap]
        hexagons += [hexagon]

    topomesh = poly_topomesh(hexagons,positions,faces_as_cells=True)

    return topomesh

def square_hexagonal_grid_topomesh(size=1, side_length=1., center=np.zeros(3)):
    """Generate a square 2D grid of hexagonal faces.

    The function creates a PropertyTopomesh formed by a square grid of
    hexagonal faces.

    Parameters
    ----------
    size : int
        The number of hexagon rows and columns on each side of the grid.
    side_length : float
        The length of hexagon sides
    center : list, :class:`np.ndarray`
        The position of the grid center point (default is (0,0,0))

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The grid mesh
    """

    x = []
    y = []
    p = []
    for n in range(2*size+1):
        p += list(len(p)+np.arange(2*size+1))
        x += list((np.arange(2*size+1)-(n%2)/2)*np.sqrt(3.)*side_length)
        y += list(np.ones(2*size+1)*n*3/2*side_length)
        p += list(len(p)+np.arange(2*size+1))
        x += list((np.arange(2*size+1)+(n%2-1)/2)*np.sqrt(3.)*side_length)
        y += list(np.ones(2*size+1)*(n+1/3)*3/2*side_length)

    center_x = (size-1/4)*np.sqrt(3.)*side_length
    center_y = (size+1/6)*3/2*side_length

    x = np.array(x) + center[0] - center_x
    y = np.array(y) + center[1] - center_y
    z = np.zeros_like(x) + center[2]

    positions = dict(list(zip(list(range(len(x))),np.transpose([x,y,z]))))
    logging.debug(f"{positions}")

    hexagons = []
    for n in range(2 * size):
        for h in range(2 * size):
            hexagon = []
            hexagon += [h + n*2*(2*size+1) + n%2]
            hexagon += [h + n*2*(2*size+1) + 2*size+1]
            hexagon += [h + n*2*(2*size+1) + 2*(2*size+1)]
            hexagon += [h + n*2*(2*size+1) + 3*(2*size+1) + n%2]
            hexagon += [h + n*2*(2*size+1) + 2*(2*size+1)+1]
            hexagon += [h + n*2*(2*size+1) + 2*size+2]
            hexagons += [hexagon]
    topomesh = poly_topomesh(hexagons,positions,faces_as_cells=True)

    return topomesh


def circular_grid_topomesh(size=1, n_sides=6, cell_size=1., center=np.zeros(3), angle=0):
    """Generate a 2D circular grid of concentric face rings.

    The function creates a PropertyTopomesh formed of concentric rings of faces
    arranged around a central regular polygonal face with n_sides edges. Each
    ring at distance d from the central cell consists of d * n_sides faces in
    a regular arrangement.

    Parameters
    ----------
    size: int
        The number of concentric rings of faces around the central face.
    n_sides: int
        The number of sides of the central regular polygonal face.
    cell_size: float
        The radial width of each face ring.
    center : list, :class:`np.ndarray`
        The position of the grid center point (default is (0,0,0))
    angle: float
        Tre rotation angle around the center point, in degrees

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The grid mesh
    """

    circle_points = {}
    circle_lower_points = {}
    for s in range(1, size+2):
        circle_thetas = np.radians(angle-180/n_sides) + (np.arange(n_sides*s) + ((s+1)%2)/2.) * 2 * np.pi/(n_sides*s)
        if s < size+1:
            circle_points[s] = center + s*cell_size*np.transpose([np.cos(circle_thetas), np.sin(circle_thetas), np.zeros_like(circle_thetas)])
        if s-1 in circle_points:
            circle_lower_points[s] = center + (circle_points[s-1] - center) *(s/(s-1))

    topomesh = PropertyTopomesh(3)
    circle_vertices = {}
    circle_lower_vertices = {}
    vertex_positions = {}

    circle_lower_edges = {}
    circle_edges = {}
    circle_faces = {}

    for s in range(1, size+2):
        if s in circle_points:
            circle_vertices[s] = []
            for i, p in enumerate(circle_points[s]):
                v = topomesh.add_wisp(0)
                circle_vertices[s].append(v)
                vertex_positions[v] = p
        if s in circle_lower_points:
            circle_lower_vertices[s] = []
            for i, p in enumerate(circle_lower_points[s]):
                v = topomesh.add_wisp(0)
                circle_lower_vertices[s].append(v)
                vertex_positions[v] = p

            circle_lower_edges[s] = []
            for i_v in range(len(circle_lower_vertices[s])):
                l_v = circle_lower_vertices[s][i_v]
                v  = circle_vertices[s-1][i_v]

                f = circle_faces[s][(i_v-1)%len(circle_lower_vertices[s])]
                n_f = circle_faces[s][(i_v)%len(circle_lower_vertices[s])]

                e = topomesh.add_wisp(1)
                circle_lower_edges[s].append(e)
                topomesh.link(1, e, v)
                topomesh.link(1, e, l_v)

                topomesh.link(2, f, e)
                topomesh.link(2, n_f, e)

        circle_faces[s+1] = []
        circle_edges[s] = []

        if s in circle_points:
            if s not in circle_lower_points:
                l_f = topomesh.add_wisp(2)
                circle_faces[s] = [l_f]

                for i_p in range(len(circle_points[s])):
                    v = circle_vertices[s][i_p%len(circle_vertices[s])]
                    n_v = circle_vertices[s][(i_p+1)%len(circle_vertices[s])]

                    e = topomesh.add_wisp(1)
                    circle_edges[s].append(e)
                    topomesh.link(1, e, v)
                    topomesh.link(1, e, n_v)
                    topomesh.link(2, l_f, e)

                for i_p in range(len(circle_edges[s])):
                    e = circle_edges[s][(i_p)%len(circle_edges[s])]
                    f = topomesh.add_wisp(2)
                    circle_faces[s+1].append(f)
                    topomesh.link(2, f, e)

            if s in circle_lower_points:
                i_p = 0 if s%2 == 0 else 1
                i_l_p = 1 if s%2 == 0 else 0
                count = -(s-2)
                offset = 0

                if s%2 == 1:
                    f = topomesh.add_wisp(2)
                    circle_faces[s+1].append(f)

                while i_p<len(circle_points[s])+1 and i_l_p<len(circle_lower_points[s])+1:
                    v = circle_vertices[s][i_p%len(circle_vertices[s])]
                    l_v = circle_lower_vertices[s][i_l_p%len(circle_lower_vertices[s])]
                    n_v = circle_vertices[s][(i_p+1)%len(circle_vertices[s])]

                    e = topomesh.add_wisp(1)
                    circle_edges[s].append(e)

                    if (count % (2*(s-1)+1)) != 0:
                        topomesh.link(1, e, v)
                        topomesh.link(1, e, l_v)

                        if (count+offset)%2 == 0:
                            i_p += 1
                            if i_p < len(circle_points[s]) + 1:
                                f = topomesh.add_wisp(2)
                                circle_faces[s+1].append(f)
                            else:
                                f = circle_faces[s+1][0]
                        else:
                            i_l_p += 1

                        count += 1
                    else:
                        topomesh.link(1, e, v)
                        topomesh.link(1, e, n_v)

                        i_p += 1
                        if i_p < len(circle_points[s]) + 1:
                            f = topomesh.add_wisp(2)
                            circle_faces[s+1].append(f)
                        else:
                            f = circle_faces[s+1][0]
                        count += 1

                    if (count % (2*(s-1)+1)) == 1:
                        offset += 1
                    topomesh.link(2, f, e)

                    l_f = circle_faces[s][(i_l_p-1)%len(circle_lower_vertices[s])]
                    topomesh.link(2, l_f, e)
        else:
            for i_p in range(len(circle_lower_points[s])):
                v = circle_lower_vertices[s][i_p%len(circle_lower_vertices[s])]
                n_v = circle_lower_vertices[s][(i_p+1)%len(circle_lower_vertices[s])]

                f = circle_faces[s][(i_p)%len(circle_lower_vertices[s])]

                e = topomesh.add_wisp(1)
                circle_edges[s].append(e)
                topomesh.link(1, e, v)
                topomesh.link(1, e, n_v)

                topomesh.link(2, f, e)

    topomesh.update_wisp_property('barycenter', 0, vertex_positions)

    return topomesh


def circle_topomesh(radius=1., n_points=100, center=np.zeros(3,float), angle=0):
    """

    Parameters
    ----------
    radius
    n_points
    center
    angle: float
        Tre rotation angle around the center point, in degrees

    Returns
    -------

    """

    circle_thetas = np.radians(angle) + np.linspace(-np.pi,np.pi-2*np.pi/float(n_points),n_points)
    circle_points = center + np.transpose([radius*np.cos(circle_thetas),radius*np.sin(circle_thetas),np.zeros_like(circle_thetas)])
    circle_positions = dict(zip(range(n_points),circle_points))

    circle_vertices = np.arange(n_points)

    return poly_topomesh([circle_vertices],circle_positions)


def circle_voronoi_topomesh(size = 1,cell_size = 1.,circle_size = 100.,z_coef = 0.,iterations=None,return_triangulation=False):
    """
    Create a 2D cell complex as a Voronoi diagram on a circular domain.

    Generates a set of cell points homogeneously placed within a 2D circular
    domain and computes their Delaunay triangulation, in order to return the
    dual Voronoi cell complex.

    Parameters
    ----------
    size : int
        The approximative number of "layers" in the resulting circular cell
         complex.
    cell_size : float
        The average characteristic size of the cells in the complex.
    circle_size : int
        Number of points on the circle contour used for the dualization.
    z_coef
        Coefficient of a quadratic bending function for a 2.5D complex.
    iterations, optional
        Number of iterations in the cell center placement optimization process.
    return_triangulation
        If True, also return the Delaunay triangulation used to generate the
        cell complex.

    Returns
    -------
    cell_topomesh : ndarray
        The circular Voronoi cell complex.
    triangulation_topomesh : ndarray, optional
        The Delaunay triangulation used to generate the cell complex. Only
         provided if `return_triangulation` is True.

    """
    n_cells = 3*size*(size-1)+1
    radius = size*cell_size

    circle_thetas = np.linspace(-np.pi,np.pi-2*np.pi/float(circle_size),int(circle_size))
    circle_points = np.transpose([radius*np.cos(circle_thetas),radius*np.sin(circle_thetas)])

    cell_thetas = np.array([np.pi*np.random.randint(-180,180)/180. for c in range(n_cells)])
    cell_distances = 0.5*radius*np.sqrt([np.random.rand() for c in range(n_cells)])

    cell_points = np.transpose([cell_distances*np.cos(cell_thetas),cell_distances*np.sin(cell_thetas)])

    omega_forces = dict(repulsion=0.5)
    sigma_deformation = 2.*radius/float(n_cells)

    if iterations is None:
        iterations = n_cells//2

    for iteration in range(iterations):
        cell_to_cell_vectors = cell_points[:, np.newaxis] - cell_points[np.newaxis]
        cell_to_cell_distances = np.linalg.norm(cell_to_cell_vectors,axis=2)/radius
        cell_to_circle_vectors = cell_points[:, np.newaxis] - circle_points[np.newaxis]
        cell_to_circle_distances = np.linalg.norm(cell_to_circle_vectors,axis=2)/radius

        deformation_force = np.zeros_like(cell_points)

        cell_repulsion_force = np.nansum(cell_to_cell_vectors/np.power(cell_to_cell_distances,3)[:,:,np.newaxis],axis=1)
        circle_repulsion_force = np.nansum(cell_to_circle_vectors/np.power(cell_to_circle_distances,3)[:,:,np.newaxis],axis=1)

        deformation_force += omega_forces['repulsion']*cell_repulsion_force
        deformation_force += 1.5*(n_cells/float(circle_size))*omega_forces['repulsion']*circle_repulsion_force

        deformation_force_amplitude = np.linalg.norm(deformation_force,axis=1)
        deformation_force = np.minimum(1.0,sigma_deformation/deformation_force_amplitude)[:,np.newaxis] * deformation_force

        cell_points += deformation_force
        cell_points = np.minimum(1.0,radius/(np.linalg.norm(cell_points,axis=1)))[:,np.newaxis] * cell_points

    all_positions = array_dict(np.transpose([np.concatenate([cell_points[:,0],circle_points[:,0]]),np.concatenate([cell_points[:,1],circle_points[:,1]]),np.zeros(int(np.round(n_cells+circle_size)))]),keys=np.arange(int(np.round(n_cells+circle_size))).astype(int))
    triangles = all_positions.keys()[delaunay_triangulation(all_positions.values())]

    radial_distances = np.linalg.norm(all_positions.values(),axis=1)
    radial_z = z_coef*np.power(radial_distances/radius,2)
    all_positions = array_dict(np.transpose([all_positions.values()[:,0],all_positions.values()[:,1],radial_z]),keys=all_positions.keys())

    triangulation_topomesh = triangle_topomesh(triangles,all_positions)
    cell_topomesh = dual_topomesh(triangulation_topomesh,2,vertex_positions='voronoi')

    if return_triangulation:
        return cell_topomesh, triangulation_topomesh
    else:
        return cell_topomesh


def periodic_square_voronoi_topomesh(n_points=10, square_size=10., cvt_iterations=10, cell_size=None, lloyd_coef=0.33, return_edge_matching=False):
    """
    Create a 2D cell complex as a Voronoi diagram on a periodic square domain.

    Generates a 2D centroidal Voronoi tesselation (CVT) on a 3x3 reiteration
    of a square domain so that the restriction to the central square has
    connected opposite boundaries. The algorithm uses the dual of the Delaunay
    triangulation of the CVT seed points, clipped by the boundaries of the
    square domain to get the matching edges.

    Parameters
    ----------
    n_points : int
        Number of uniquely labelled cells in the Voronoi cell complex.
    square_size : float
        The dimension of the side of the square domain.
    cvt_iterations : int
        The number of iterations in the CVT Lloyd's optimization process.
    cell_size : float
        The grid step used in the CVT Lloyd's optimization process.
    lloyd_coef : float
        The relaxation coefficient used in the CVT Lloyd's optimization process.
    return_edge_matching : bool
        If True, also return the list of matching edge ids.

    Returns
    -------
    cell_topomesh : ndarray
        The periodic square Voronoi cell complex.
    edge_matching : list of tuples, optional
        The list of pairs of matching opposite edge ids in the Voronoi
        cell complex.

    """
    if cell_size is None:
        cell_size = 0.05 * np.power(square_size,2)/n_points

    size = int(np.ceil(square_size / cell_size))

    cell_xx, cell_yy = np.meshgrid((np.arange(size) - size / 2) * cell_size, (np.arange(size) - size / 2) * cell_size)
    initial_points = np.array([(cell_xx.ravel()[i], cell_yy.ravel()[i]) for i in np.random.permutation(range(len(cell_xx.ravel())))[:n_points]])

    positions = dict(zip(range(n_points), initial_points))
    points = np.array([positions[p] for p in range(n_points)])
    periodic_points = np.concatenate([points + np.array([x * square_size, y * square_size]) for x in [-1, 0, 1] for y in [-1, 0, 1]])

    for iteration in range(cvt_iterations):
        periodic_point_labels = np.concatenate([np.arange(n_points) + 2 for x in [-1, 0, 1] for y in [-1, 0, 1]])
        periodic_cell_xx = np.tile(np.concatenate([cell_xx + x * square_size for x in [-1, 0, 1]], axis=1), (3, 1))
        periodic_cell_yy = np.tile(np.concatenate([cell_yy + y * square_size for y in [-1, 0, 1]], axis=0), (1, 3))

        periodic_cell_grid = vq(np.concatenate(np.transpose([periodic_cell_xx, periodic_cell_yy])), periodic_points)[0].reshape(periodic_cell_xx.shape)
        periodic_voronoi = periodic_point_labels[periodic_cell_grid]

        periodic_cell_centers = np.array([np.array(nd.center_of_mass(periodic_cell_grid == l)) * cell_size - 3 * square_size / 2 for l in range(len(periodic_points))])

        center_in_frame = (periodic_cell_centers[:, 0] >= -square_size / 2) & (
                           periodic_cell_centers[:, 0] < square_size / 2) & (
                           periodic_cell_centers[:, 1] >= -square_size / 2) & (
                           periodic_cell_centers[:, 1] < square_size / 2)
        in_frame_centers = periodic_cell_centers[center_in_frame]
        in_frame_labels = periodic_point_labels[center_in_frame]
        in_frame_positions = dict(zip(in_frame_labels - 2, in_frame_centers))

        positions = dict(zip(range(n_points), [positions[p] + lloyd_coef * (in_frame_positions[p] - positions[p]) for p in range(n_points)]))
        points = np.array([positions[p] for p in range(n_points)])
        periodic_points = np.concatenate([points + np.array([x * square_size, y * square_size]) for x in [-1, 0, 1] for y in [-1, 0, 1]])

    square_n_points = int(np.ceil(np.pi * np.sqrt(n_points)))
    square_points = np.concatenate([3.1*square_size*np.array(side)*np.transpose([np.ones(square_n_points+1)/2, np.linspace(-1/2,1/2,square_n_points+1)])[:-1,::np.prod(side)] for side in [[1,1], [-1,1], [-1,-1], [1,-1]]])

    all_positions = array_dict(np.transpose([np.concatenate([periodic_points[:,0], square_points[:,0]]), np.concatenate([periodic_points[:,1], square_points[:,1]]), np.zeros(int(np.round(9*n_points + 4*square_n_points)))]),
                               keys=np.arange(int(np.round(9*n_points + 4*square_n_points))).astype(int))

    triangles = all_positions.keys()[delaunay_triangulation(all_positions.values())]

    triangulation_topomesh = triangle_topomesh(triangles, all_positions)
    cell_topomesh = dual_topomesh(triangulation_topomesh, 2, vertex_positions='voronoi')
    cell_topomesh.update_wisp_property('label', 2, dict([(w, w % n_points + 2) for w in cell_topomesh.wisps(2)]))

    frame_points = np.array([square_size / 2 * np.array(side) for side in [[1, 1], [-1, 1], [-1, -1], [1, -1]]])
    frame_lines = frame_points[np.array([[0, 1], [1, 2], [2, 3], [3, 0]])]

    compute_topomesh_property(cell_topomesh, 'vertices', 1)
    cell_edge_points = cell_topomesh.wisp_property('barycenter', 0).values(cell_topomesh.wisp_property('vertices', 1).values())[:, :, :2]

    edge_segment_intersection = np.array([intersecting_segment(e, frame_lines) for e in cell_edge_points])

    inserted_vertices = {}
    for e, p, i in zip(cell_topomesh.wisps(1), cell_edge_points, edge_segment_intersection):
        if np.any(i):
            intersecting_lines = frame_lines[i]
            for i_l, line in enumerate(intersecting_lines):
                intersection_point = get_segment_intersection(p, line)
                inserted_vertices[(e, i_l)] = np.array(list(intersection_point) + [0])

    inserted_vertex_ids = {}
    for (e, i_l) in inserted_vertices.keys():
        vertex_to_keep, vertex_to_clip = cell_topomesh.borders(1, e)
        cell_topomesh.unlink(1, e, vertex_to_clip)
        intersection_vertex = cell_topomesh.add_wisp(0)
        inserted_vertex_ids[(e, i_l)] = intersection_vertex
        clipped_edge = cell_topomesh.add_wisp(1)
        cell_topomesh.link(1, e, intersection_vertex)
        cell_topomesh.link(1, clipped_edge, intersection_vertex)
        cell_topomesh.link(1, clipped_edge, vertex_to_clip)
        for f in cell_topomesh.regions(1, e):
            cell_topomesh.link(2, f, clipped_edge)
        cell_topomesh.wisp_property('barycenter', 0)[intersection_vertex] = inserted_vertices[(e, i_l)]

    compute_topomesh_property(cell_topomesh, 'oriented_vertices', 2)

    frame_vertex_ids = {}
    frame_vertex_face_id = {}
    for p in frame_points:
        frame_vertex = cell_topomesh.add_wisp(0)
        cell_topomesh.wisp_property('barycenter', 0)[frame_vertex] = np.array(list(p) + [0])
        frame_vertex_ids[tuple(list(p))] = frame_vertex

        for f in cell_topomesh.wisps(2):
            face_points = cell_topomesh.wisp_property('barycenter', 0).values(cell_topomesh.wisp_property('oriented_vertices', 2)[f])[:, :2]
            face_center = np.mean(face_points, axis=0)
            face_triangles = np.array([[p1, p2, face_center] for p1, p2 in zip(face_points, list(face_points[1:]) + [face_points[0]])])
            if np.any(inside_triangle(p, face_triangles)):
                frame_vertex_face_id[frame_vertex] = f

    edge_matching = []
    for line1, line2 in frame_lines[np.array([(0, 2), (1, 3)])]:
        line2 = line2[::-1]

        line1_vertices = [frame_vertex_ids[tuple(list(line1[0]))]]
        line2_vertices = [frame_vertex_ids[tuple(list(line2[0]))]]

        all_inserted_points = np.array([inserted_vertices[k] for k in inserted_vertices.keys()])[:, :2]
        all_inserted_vertex_ids = np.array([inserted_vertex_ids[k] for k in inserted_vertices.keys()])

        line1_point_vectors = all_inserted_points - line1[0]
        line1_vector = line1[1] - line1[0]

        line1_dot_products = np.einsum("...ij,...ij->...i", line1_point_vectors, line1_vector[np.newaxis])
        line1_length_products = np.linalg.norm(line1_point_vectors, axis=1) * np.linalg.norm(line1_vector)

        line1_inserted_vertex_ids = all_inserted_vertex_ids[line1_dot_products == line1_length_products]
        line1_inserted_point_abscissas = line1_dot_products[line1_dot_products == line1_length_products] / np.power(np.linalg.norm(line1_vector), 2)
        line1_vertices += list(line1_inserted_vertex_ids[np.argsort(line1_inserted_point_abscissas)])

        line2_point_vectors = all_inserted_points - line2[0]
        line2_vector = line2[1] - line2[0]

        line2_dot_products = np.einsum("...ij,...ij->...i", line2_point_vectors, line2_vector[np.newaxis])
        line2_length_products = np.linalg.norm(line2_point_vectors, axis=1) * np.linalg.norm(line2_vector)

        line2_inserted_vertex_ids = all_inserted_vertex_ids[line2_dot_products == line2_length_products]
        line2_inserted_point_abscissas = line2_dot_products[line2_dot_products == line2_length_products] / np.power(np.linalg.norm(line2_vector), 2)
        line2_vertices += list(line2_inserted_vertex_ids[np.argsort(line2_inserted_point_abscissas)])

        line1_vertices += [frame_vertex_ids[tuple(list(line1[1]))]]
        line2_vertices += [frame_vertex_ids[tuple(list(line2[1]))]]

        assert len(line1_vertices) == len(line2_vertices)

        for l1_v1, l1_v2, l2_v1, l2_v2 in zip(line1_vertices[:-1], line1_vertices[1:], line2_vertices[:-1], line2_vertices[1:]):
            l1_e = cell_topomesh.add_wisp(1)
            cell_topomesh.link(1, l1_e, l1_v1)
            cell_topomesh.link(1, l1_e, l1_v2)

            if l1_v1 in frame_vertex_face_id:
                l1_f = frame_vertex_face_id[l1_v1]
            elif l1_v2 in frame_vertex_face_id:
                l1_f = frame_vertex_face_id[l1_v2]
            else:
                l1_f = np.intersect1d(list(cell_topomesh.regions(0, l1_v1, 2)),
                                      list(cell_topomesh.regions(0, l1_v2, 2)))[0]
            cell_topomesh.link(2, l1_f, l1_e)

            l2_e = cell_topomesh.add_wisp(1)
            cell_topomesh.link(1, l2_e, l2_v1)
            cell_topomesh.link(1, l2_e, l2_v2)

            if l2_v1 in frame_vertex_face_id:
                l2_f = frame_vertex_face_id[l2_v1]
            elif l2_v2 in frame_vertex_face_id:
                l2_f = frame_vertex_face_id[l2_v2]
            else:
                l2_f = np.intersect1d(list(cell_topomesh.regions(0, l2_v1, 2)),
                                      list(cell_topomesh.regions(0, l2_v2, 2)))[0]
            cell_topomesh.link(2, l2_f, l2_e)

            edge_matching += [(l1_e, l2_e)]

    cell_vertex_points = cell_topomesh.wisp_property('barycenter', 0).values(list(cell_topomesh.wisps(0)))
    vertex_point_in_frame = (cell_vertex_points[:, 0] >= -square_size/2 - 1e-5) & (
                             cell_vertex_points[:, 0] <= square_size/2 + 1e-5) & (
                             cell_vertex_points[:, 1] >= -square_size/2 - 1e-5) & (
                             cell_vertex_points[:, 1] <= square_size/2 + 1e-5)

    vertices_to_remove = np.array(list(cell_topomesh.wisps(0)))[np.logical_not(vertex_point_in_frame)]
    for v in vertices_to_remove:
        cell_topomesh.remove_wisp(0, v)

    edges_to_remove = [e for e in cell_topomesh.wisps(1) if cell_topomesh.nb_borders(1, e) != 2]
    for e in edges_to_remove:
        cell_topomesh.remove_wisp(1, e)

    faces_to_remove = [f for f in cell_topomesh.wisps(2) if cell_topomesh.nb_borders(2, f) < 3]
    for f in faces_to_remove:
        cell_topomesh.remove_wisp(2, f)

    cells_to_remove = [c for c in cell_topomesh.wisps(3) if cell_topomesh.nb_borders(3, c) == 0]
    for c in cells_to_remove:
        cell_topomesh.remove_wisp(3, c)

    cell_topomesh = clean_topomesh(cell_topomesh, clean_properties=True, degree=2)

    compute_topomesh_property(cell_topomesh, 'vertices', 1)
    compute_topomesh_property(cell_topomesh, 'barycenter', 1)
    compute_topomesh_property(cell_topomesh, 'vertices', 2)
    compute_topomesh_property(cell_topomesh, 'barycenter', 2)
    compute_topomesh_property(cell_topomesh, 'oriented_vertices', 2)

    if return_edge_matching:
        return cell_topomesh, edge_matching
    else:
        return cell_topomesh


def vtk_ellipsoid_topomesh(ellipsoid_radius=50.0, ellipsoid_scales=[1,1,1], ellipsoid_axes=np.diag(np.ones(3)), ellipsoid_center=np.zeros(3), subdivisions=3):
    """
    """
    import vtk
    from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy

    ico = vtk.vtkPlatonicSolidSource()
    ico.SetSolidTypeToIcosahedron()
    ico.Update()

    subdivide = vtk.vtkLoopSubdivisionFilter()
    subdivide.SetNumberOfSubdivisions(subdivisions)
    subdivide.SetInputConnection(ico.GetOutputPort())
    subdivide.Update()

    scale_transform = vtk.vtkTransform()
    scale_factor = ellipsoid_radius/(np.sqrt(2)/2.)
    scale_transform.Scale(scale_factor,scale_factor,scale_factor)

    ellipsoid_sphere = vtk.vtkTransformPolyDataFilter()
    ellipsoid_sphere.SetInputData(subdivide.GetOutput())
    ellipsoid_sphere.SetTransform(scale_transform)
    ellipsoid_sphere.Update()

    ellipsoid_transform = vtk.vtkTransform()
    axes_transform = vtk.vtkLandmarkTransform()
    source_points = vtk.vtkPoints()
    source_points.InsertNextPoint([1,0,0])
    source_points.InsertNextPoint([0,1,0])
    source_points.InsertNextPoint([0,0,1])

    target_points = vtk.vtkPoints()
    target_points.InsertNextPoint(ellipsoid_axes[0])
    target_points.InsertNextPoint(ellipsoid_axes[1])
    target_points.InsertNextPoint(ellipsoid_axes[2])

    axes_transform.SetSourceLandmarks(source_points)
    axes_transform.SetTargetLandmarks(target_points)
    axes_transform.SetModeToRigidBody()
    axes_transform.Update()

    ellipsoid_transform.SetMatrix(axes_transform.GetMatrix())
    ellipsoid_transform.Scale(ellipsoid_scales[0],ellipsoid_scales[1],ellipsoid_scales[2])

    center_transform = vtk.vtkTransform()
    center_transform.Translate(ellipsoid_center[0],ellipsoid_center[1],ellipsoid_center[2])
    center_transform.Concatenate(ellipsoid_transform)

    ellipsoid_ellipsoid = vtk.vtkTransformPolyDataFilter()
    ellipsoid_ellipsoid.SetInputData(ellipsoid_sphere.GetOutput())
    ellipsoid_ellipsoid.SetTransform(center_transform)
    ellipsoid_ellipsoid.Update()

    ellipsoid_polydata = ellipsoid_ellipsoid.GetOutput()
    ellipsoid_points = vtk_to_numpy(ellipsoid_polydata.GetPoints().GetData())
    ellipsoid_triangles = vtk_to_numpy(ellipsoid_polydata.GetPolys().GetData())
    ellipsoid_triangles = ellipsoid_triangles.reshape((ellipsoid_polydata.GetNumberOfCells(), 4))[:, 1:]

    topomesh = triangle_topomesh(ellipsoid_triangles, dict(zip(range(len(ellipsoid_points)), ellipsoid_points)))

    return topomesh


def hexagonal_prism_layered_grid_topomesh(size=1, n_layers=1,center=np.array([0,0,0]), radius=10, height_factor=2/3., triangulate=True, smoothing=False):
    topomesh = hexagonal_prism_topomesh(center=center,radius=radius,height=height_factor*radius,angle=np.pi/6.)

    for layer in range(n_layers):
        for row in np.arange(-size,size+1):
            for col in (np.arange(2*size+1 - np.abs(row) - layer) - (size-layer/2.-(np.abs(row))/2.)):
                layer_row = row-layer/3.
                if ((layer_row!=0) or (col!=0) or (layer!=0))&(np.abs(layer_row)<=(size-(layer-1)/2.)):
                    topomesh,_ = append_topomesh(topomesh, hexagonal_prism_topomesh(center=np.array([2.*col*radius*np.sqrt(3.)/2.,2.*layer_row*radius*3./4.,-layer*height_factor*radius]),radius=radius,height=height_factor*radius,angle=np.pi/6.))

    for layer in range(n_layers):
        for row in np.arange(-size,size+1):
            for col in (np.arange(2*size+1 - np.abs(row) - layer) - (size-layer/2.-(np.abs(row))/2.)):
                layer_row = row-layer/3.
                logging.debug(f"{layer} | {layer_row} | {col}")

    if triangulate:
        topomesh = star_interface_topomesh(topomesh)

    fuse_topomesh_identical_vertices(topomesh)

    if triangulate and smoothing:
            topomesh = topomesh_triangle_split(topomesh)

            for cid in topomesh.wisps(3):
                for n_cid in topomesh.border_neighbors(3,cid):
                    if (n_cid<cid) and (not (n_cid,cid) in topomesh._interface[3].values()):
                        iid = topomesh._interface[3].add((n_cid,cid),None)

    compute_topomesh_property(topomesh,'vertices',degree=1)
    compute_topomesh_property(topomesh,'vertices',degree=2)
    compute_topomesh_property(topomesh,'vertices',degree=3)

    if triangulate and smoothing:
        compute_topomesh_triangle_properties(topomesh)
        property_topomesh_vertices_deformation(topomesh,omega_forces=dict(taubin_smoothing=0.65,planarization=1),iterations=100)
        # property_topomesh_vertices_deformation(topomesh,omega_forces=dict(taubin_smoothing=0.65,regularization=0.01,planarization=1),iterations=50)
        # topomesh = property_topomesh_isotropic_remeshing(topomesh,maximal_length=3.,iterations=10)

    return topomesh


def truncated_octahedra_layered_grid_topomesh(size=1, n_layers=1, center=np.array([0,0,0]), radius=10, triangulate=True, smoothing=False):
    topomesh = truncated_octahedron_topomesh(center=center,radius=radius)

    for layer in range(n_layers):
        for row in np.arange(-size,size+1):
            for col in (np.arange(2*size+1 - np.abs(row) - layer) - (size-layer/2.-(np.abs(row))/2.)):
                logging.debug(f"{layer} | {row} | {col}")
                if (row!=0) or (col!=0) or (layer!=0):
                    topomesh,_ = append_topomesh(topomesh, truncated_octahedron_topomesh(center=np.array([4.*col*radius*np.sqrt(2)/3,4.*row*radius/3,-4.*layer*radius/3]),radius=radius))
    fuse_topomesh_identical_vertices(topomesh)

    if triangulate:
        topomesh = star_interface_topomesh(topomesh)

    if triangulate and smoothing:
        topomesh = topomesh_triangle_split(topomesh)

        for cid in topomesh.wisps(3):
            for n_cid in topomesh.border_neighbors(3,cid):
                if (n_cid<cid) and (not (n_cid,cid) in topomesh._interface[3].values()):
                    iid = topomesh._interface[3].add((n_cid,cid),None)

    compute_topomesh_property(topomesh,'vertices',degree=1)
    compute_topomesh_property(topomesh,'vertices',degree=2)
    compute_topomesh_property(topomesh,'vertices',degree=3)

    if triangulate and smoothing:
        compute_topomesh_triangle_properties(topomesh)
        property_topomesh_vertices_deformation(topomesh,omega_forces=dict(taubin_smoothing=0.65,planarization=1),iterations=100)
        # property_topomesh_vertices_deformation(topomesh,omega_forces=dict(taubin_smoothing=0.65,regularization=0.01,planarization=1),iterations=50)
        # topomesh = property_topomesh_isotropic_remeshing(topomesh,maximal_length=3.,iterations=10)

    return topomesh
