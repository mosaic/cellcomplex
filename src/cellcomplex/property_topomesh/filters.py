import logging
from time import time as current_time

import numpy as np

from cellcomplex.utils import array_dict
from cellcomplex.property_topomesh.utils.array_tools import make_array

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces, compute_topomesh_vertex_property_gradient, is_triangular
from cellcomplex.property_topomesh.utils.matrix_tools import adjacency_matrix, vertex_distance_matrix, topological_distance_matrix


def topomesh_property_filtering(topomesh, property_name, degree, neighborhood=3, adjacency_sigma=1.0, iterations=1):
    """Filter an existing property by a topology-based operator.

    A new value of an existing property is computed for each element of a given
    degree as a linear combination of all the property values. The coefficient
    associated with each other element is defined relatively to their topological
    distance using a gaussian weighting function.

    Parameters
    --------
        topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
            The structure on which to compute the property.
        property_name : str
            The name of the property to compute (must be already computed on faces).
        degree : int
            The degree of the elements on which to compute the property.
        neighborhood : int
            The maximal ring-distance up to which elements have a non-zero ceofficient.
        adjacency_sigma : float
            The standard deviation of the gaussian function based on the topological distance.
        iterations : int
            The number of repeated applications of the filter.

    Returns
    --------
        None

    Note
    --------
        The PropertyTopomesh passed as argument is updated.

    """

    assert topomesh.has_wisp_property(property_name, degree=degree, is_computed=True)

    if degree == 0:
        adjacency_matrix = topological_distance_matrix(topomesh, degree)
    else:
        adjacency_matrix = topomesh.nb_wisps(degree)*np.ones((topomesh.nb_wisps(degree),topomesh.nb_wisps(degree)),int)
        adjacency_matrix[tuple([np.arange(topomesh.nb_wisps(degree)),np.arange(topomesh.nb_wisps(degree))])] = 0

        wisp_index = array_dict(np.arange(topomesh.nb_wisps(degree)),list(topomesh.wisps(degree)))
        for i,wid in enumerate(topomesh.wisps(degree)):
            if degree>1:
                adjacency_matrix[tuple([i*np.ones(topomesh.nb_border_neighbors(degree,wid),int),wisp_index.values(list(topomesh.border_neighbors(degree,wid)))])] = 1
            else:
                adjacency_matrix[tuple([i*np.ones(topomesh.nb_region_neighbors(degree,wid),int),wisp_index.values(list(topomesh.region_neighbors(degree,wid)))])] = 1

        for dist in (np.arange(neighborhood)+1):
            for i,wid in enumerate(topomesh.wisps(degree)):
                neighbors = np.where(adjacency_matrix[i] <= dist)[0]
                neighbor_neighbors = np.where(adjacency_matrix[neighbors] <= dist)
                neighbor_neighbor_successor = neighbor_neighbors[1]
                neighbor_neighbor_predecessor = neighbors[neighbor_neighbors[0]]

                current_adjacency = adjacency_matrix[i][neighbor_neighbor_successor]
                neighbor_adjacency = adjacency_matrix[i][neighbor_neighbor_predecessor] + adjacency_matrix[neighbors][neighbor_neighbors]

                current_adjacency = current_adjacency[np.argsort(-neighbor_adjacency)]
                neighbor_neighbor_successor = neighbor_neighbor_successor[np.argsort(-neighbor_adjacency)]
                neighbor_adjacency = neighbor_adjacency[np.argsort(-neighbor_adjacency)]

                adjacency_matrix[i][neighbor_neighbor_successor] = np.minimum(current_adjacency,neighbor_adjacency)


    vertex_adjacency_gaussian = np.exp(-np.power(adjacency_matrix,2.0)/(2*np.power(adjacency_sigma,2.)))
    # vertex_distance_gaussian = np.exp(-np.power(distance_matrix,2.0)/(2*np.power(distance_sigma,2.)))
    vertex_gaussian = vertex_adjacency_gaussian

    for iteration in range(iterations):
        properties = topomesh.wisp_property(property_name, degree=degree).values(list(topomesh.wisps(degree)))
        if properties.ndim == 1:
            filtered_properties = np.nansum(vertex_gaussian*properties[:, np.newaxis], axis=0) / np.nansum(vertex_gaussian, axis=0)
        else:
            filtered_properties = np.nansum(vertex_gaussian[..., np.newaxis]*properties[:, np.newaxis], axis=0) / np.nansum(vertex_gaussian, axis=0)[..., np.newaxis]

        topomesh.update_wisp_property(property_name, degree=degree,values=filtered_properties,keys=np.array(list(topomesh.wisps(degree))))


def topomesh_vertex_property_geodesic_gaussian_filtering(topomesh, property_name, gaussian_sigma, filtered_property_name=None):
    """

    Parameters
    ----------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The structure on which to compute the property.
    property_name : str
        The name of the property to compute (must be already computed on faces).
    gaussian_sigma : float
        The standard deviation of the gaussian function based on the geodesic distance.
    filtered_property_name: str, optional
        The name to assign to the filtered property.

    Returns
    -------

    """
    assert topomesh.has_wisp_property(property_name, 0, is_computed=True)

    vertex_properties = topomesh.wisp_property(property_name, 0).values(list(topomesh.wisps(0)))

    distance_matrix = vertex_distance_matrix(topomesh, cutoff=3*gaussian_sigma)
    weight_matrix = np.exp(-np.power(distance_matrix, 2) / np.power(gaussian_sigma, 2))
    weight_matrix[np.isnan(weight_matrix)] = 0.

    if vertex_properties.ndim == 1:
        nan_mask = np.logical_not(np.isnan(vertex_properties))
        filtered_properties = np.nansum(weight_matrix[nan_mask] * vertex_properties[nan_mask][:, np.newaxis], axis=0) / \
                              np.nansum(weight_matrix[nan_mask], axis=0)
    elif vertex_properties.ndim == 2:
        nan_mask = np.logical_not(np.any(np.isnan(vertex_properties), axis=1))
        filtered_properties = np.nansum(weight_matrix[nan_mask][:, :, np.newaxis] * vertex_properties[nan_mask][:, np.newaxis], axis=0) / \
                              np.nansum(weight_matrix[nan_mask][:, :, np.newaxis], axis=0)
    elif vertex_properties.ndim == 3:
        nan_mask = np.logical_not(np.any(np.isnan(vertex_properties), axis=(1, 2)))
        filtered_properties = np.nansum(weight_matrix[nan_mask][:, :, np.newaxis, np.newaxis] * vertex_properties[nan_mask][:, np.newaxis], axis=0) / \
                              np.nansum(weight_matrix[nan_mask][:, :, np.newaxis, np.newaxis], axis=0)

    if filtered_property_name is None:
        filtered_property_name = property_name

    topomesh.update_wisp_property(filtered_property_name, 0, dict(zip(topomesh.wisps(0), filtered_properties)))


def topomesh_vertex_property_landscape_analysis(topomesh, property_name, gaussian_sigma=None):
    """Perform a landscape analysis on a scalar vertex property

    Parameters
    ----------
    topomesh
    property_name
    gaussian_sigma

    Returns
    -------

    """

    assert is_triangular(topomesh)

    if gaussian_sigma is not None:
        topomesh_vertex_property_geodesic_gaussian_filtering(topomesh, property_name,
                                                             gaussian_sigma=gaussian_sigma,
                                                             filtered_property_name=property_name + "_filtered")
    else:
        topomesh.update_wisp_property(property_name + "_filtered", 0, topomesh.wisp_property(property_name, 0).to_dict())

    compute_topomesh_vertex_property_gradient(topomesh, property_name + "_filtered",
                                              gradient_property_name=property_name + "_gradient")

    triangle_vertices = topomesh.wisp_property('vertices', 2).values(list(topomesh.wisps(2)))
    triangle_centers = topomesh.wisp_property('barycenter', 2).values(list(topomesh.wisps(2)))

    edge_vertex_index = [[2, 0], [0, 1]]
    triangle_edge_vertices = triangle_vertices[:, edge_vertex_index]

    triangle_edge_vertex_points = topomesh.wisp_property('barycenter', 0).values(triangle_edge_vertices)

    triangle_edge_vectors = triangle_edge_vertex_points[:, :, 1] - triangle_edge_vertex_points[:, :, 0]

    triangle_normals = np.cross(triangle_edge_vectors[:, 0], triangle_edge_vectors[:, 1])
    triangle_normals = triangle_normals / np.linalg.norm(triangle_normals, axis=1)[:, np.newaxis]

    triangle_base_vectors = triangle_edge_vectors[:, 0] / np.linalg.norm(triangle_edge_vectors[:, 0], axis=1)[:, np.newaxis]
    triangle_orthogonal_vectors = np.cross(triangle_normals, triangle_base_vectors)

    vector_property_name = property_name + '_gradient'

    triangle_vertex_vector_properties = topomesh.wisp_property(vector_property_name, 0).values(triangle_vertices)

    triangle_vertex_points = topomesh.wisp_property('barycenter', 0).values(triangle_vertices)
    triangle_vertex_vectors = triangle_vertex_points - triangle_centers[:, np.newaxis]

    triangle_valleys = np.zeros(topomesh.nb_wisps(2), float)
    triangle_ridges = np.zeros(topomesh.nb_wisps(2), float)
    for angle in np.arange(360):
        triangle_angle_vectors = np.cos(np.radians(angle)) * triangle_base_vectors
        triangle_angle_vectors += np.sin(np.radians(angle)) * triangle_orthogonal_vectors

        triangle_vertex_dot_products = np.einsum("...ij,...ij->...i",
                                                 triangle_angle_vectors[:, np.newaxis],
                                                 triangle_vertex_vectors)
        triangle_vertex_distances = np.linalg.norm(triangle_vertex_vectors, axis=-1)
        triangle_vertex_cosines = triangle_vertex_dot_products / triangle_vertex_distances

        triangle_positive_vector_properties = (np.maximum(0, triangle_vertex_cosines)[:, :, np.newaxis] * triangle_vertex_vector_properties).sum(axis=1)
        triangle_negative_vector_properties = (np.maximum(0, -triangle_vertex_cosines)[:, :, np.newaxis] * triangle_vertex_vector_properties).sum(axis=1)

        triangle_positive_property_dot_products = np.einsum("...ij,...ij->...i",
                                                            triangle_angle_vectors,
                                                            triangle_positive_vector_properties)
        triangle_negative_property_dot_products = np.einsum("...ij,...ij->...i",
                                                            triangle_angle_vectors,
                                                            triangle_negative_vector_properties)

        triangle_valleys += (triangle_positive_property_dot_products > 0) & (triangle_negative_property_dot_products < 0)
        triangle_ridges += (triangle_positive_property_dot_products < 0) & (triangle_negative_property_dot_products > 0)

    triangle_valleys /= 360.
    triangle_ridges /= 360.

    topomesh.update_wisp_property(property_name + '_valley', 2, dict(zip(topomesh.wisps(2), triangle_valleys)))
    topomesh.update_wisp_property(property_name + '_ridge', 2, dict(zip(topomesh.wisps(2), triangle_ridges)))
    topomesh.update_wisp_property(property_name + '_saddle', 2, dict(zip(topomesh.wisps(2), np.sqrt(triangle_valleys * triangle_ridges))))

    for landscape_property_name in [property_name + '_valley', property_name + '_ridge']:
        compute_topomesh_vertex_property_from_faces(topomesh, landscape_property_name, neighborhood=1, weighting='area')

    vertex_valleys = topomesh.wisp_property(property_name + '_valley', 0).values(list(topomesh.wisps(0)))
    vertex_ridges = topomesh.wisp_property(property_name + '_ridge', 0).values(list(topomesh.wisps(0)))
    topomesh.update_wisp_property(property_name + '_saddle', 0, dict(zip(topomesh.wisps(0), np.sqrt(vertex_valleys * vertex_ridges))))


def topomesh_vertex_property_local_optimality(topomesh, property_name, optimality_radius=1., geodesic_distance=False, exclude_contour=True, gaussian_sigma=None):

    start_time = current_time()
    if gaussian_sigma is not None:
        topomesh_vertex_property_geodesic_gaussian_filtering(topomesh, property_name,
                                                             gaussian_sigma=gaussian_sigma,
                                                             filtered_property_name=property_name + "_filtered")
    else:
        topomesh.update_wisp_property(property_name + "_filtered", 0, topomesh.wisp_property(property_name, 0).to_dict())
    logging.info(f"  --> Filtering property {property_name} [{current_time()-start_time}s]")

    start_time = current_time()
    if geodesic_distance:
        distance_matrix = vertex_distance_matrix(topomesh, split_faces=False)
    else:
        distance_matrix = topological_distance_matrix(topomesh, 0, cutoff=optimality_radius)
    logging.info(f"  --> Computing vertex distance matrix [{current_time()-start_time}s]")

    start_time = current_time()
    vertices = np.array(list(topomesh.wisps(0)))
    vertex_neighbors, _ = make_array([vertices[d <= optimality_radius] for d in distance_matrix])

    vertex_properties = topomesh.wisp_property(property_name + "_filtered", 0).values(list(topomesh.wisps(0)))

    vertex_neighbor_properties = topomesh.wisp_property(property_name + "_filtered", 0).values(vertex_neighbors)

    vertex_neighbor_min_properties = np.array(list(map(np.nanmin, vertex_neighbor_properties)))
    vertex_neighbor_max_properties = np.array(list(map(np.nanmax, vertex_neighbor_properties)))

    if exclude_contour:
        compute_topomesh_property(topomesh, 'faces', 1)
        compute_topomesh_property(topomesh, 'contour', 1)
        compute_topomesh_property(topomesh, 'contour', 0)
        vertex_contour = topomesh.wisp_property('contour', 0)

    minimum_vertices = vertices[(vertex_properties <= vertex_neighbor_min_properties)]
    if exclude_contour:
        minimum_vertices = minimum_vertices[np.logical_not(vertex_contour.values(minimum_vertices))]
    topomesh.update_wisp_property(property_name + '_minimum', 0, {v:v in minimum_vertices for v in topomesh.wisps(0)})

    maximum_vertices = vertices[(vertex_properties >= vertex_neighbor_max_properties)]
    if exclude_contour:
        maximum_vertices = maximum_vertices[np.logical_not(vertex_contour.values(maximum_vertices))]
    topomesh.update_wisp_property(property_name + '_maximum', 0, {v:v in maximum_vertices for v in topomesh.wisps(0)})
    logging.info(f"  --> Extracting optimal vertices [{current_time()-start_time}s]")

    return minimum_vertices, maximum_vertices
