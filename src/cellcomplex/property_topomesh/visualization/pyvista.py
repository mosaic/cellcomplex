import logging

import numpy as np

import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy
import pyvista as pv

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property


def topomesh_unstructured_grid(
        topomesh: PropertyTopomesh,
        position_property: str='barycenter',
        save_edges: bool=True,
        save_faces: bool=True,
        save_cells: bool=True
):
    """Generate a UnstructuredGrid representing the elements of the complex.

    The function creates a cell in the grid for each edge, face and volume
    element in the PropertyTopomesh (if the corresponding saving option is set
    to True). The scalar, vector and tensor properties computed on the elements
    are passed on as cell data fields on the unstructured grid, except for
    vertex elements whose properties are passed on as point data fields.

    Parameters
    ----------
    topomesh
        The cell complex structure to visualize.
    position_name
        The property to use to define the position of the vertices.
    save_edges
        Whether to export the edge elements as line cells.
    save_faces
        Whether to export the face elements as polygon cells.
    save_cells
        Whether to export the volume elements as polyhedron cells.

    Returns
    -------
    grid: pv.UnstructuredGrid
        The mesh structure representing the elements of the complex

    Examples
    --------
    >>> import pyvista as pv
    >>> from pyvista.DataSetFilters import extract_cells_by_type

    >>> from cellcomplex.property_topomesh.example_topomesh import hexagonal_prism_layered_grid_topomesh
    >>> from cellcomplex.property_topomesh.analysis import compute_topomesh_property
    >>> from cellcomplex.property_topomesh.visualization.pyvista import topomesh_unstructured_grid

    >>> topomesh = hexagonal_prism_layered_grid_topomesh(size=3, n_layers=2)
    >>> compute_topomesh_property(topomesh, 'area', 2)

    >>> grid = topomesh_unstructured_grid(topomesh)

    >>> plotter = pv.Plotter()
    >>> plotter.add_mesh(extract_cells_by_type(grid, pv.CellType.LINE), scalars='length', cmap='Reds', render_lines_as_tubes=True, line_width=10)
    >>> plotter.add_mesh(extract_cells_by_type(grid, pv.CellType.POLYGON), scalars='area', cmap='Blues')
    >>> plotter.show()

    """

    valid_positions = topomesh.has_wisp_property(position_property, 0, is_computed=True)
    valid_positions &= topomesh.wisp_property(position_property, 0).values().ndim == 2
    valid_positions &= topomesh.wisp_property(position_property, 0).values().shape[1] == 3
    if not valid_positions:
        position_property = 'barycenter'
    points = topomesh.wisp_property(position_property, 0).values(list(topomesh.wisps(0)))
    vertex_point_ids = dict(zip(list(topomesh.wisps(0)), range(topomesh.nb_wisps(0))))

    if topomesh.nb_wisps(1) == 0:
        save_edges = False
        save_faces = False
        save_cells = False
    if topomesh.nb_wisps(2) == 0:
        save_faces = False
        save_cells = False
    if topomesh.nb_wisps(3) == 0:
        save_cells = False

    if save_edges:
        if not topomesh.has_wisp_property('vertices', 1, is_computed=True):
            compute_topomesh_property(topomesh, 'vertices', 1)
        edge_vertices = topomesh.wisp_property('vertices', 1).values(list(topomesh.wisps(1)))
    if save_faces or save_cells:
        if not topomesh.has_wisp_property('oriented_vertices', 2, is_computed=True):
            compute_topomesh_property(topomesh, 'oriented_vertices', 2)
        face_vertices = topomesh.wisp_property('oriented_vertices', 2).values(list(topomesh.wisps(2)))
        face_vertices_dict = topomesh.wisp_property('oriented_vertices', 2).to_dict()
    if save_cells:
        if not topomesh.has_wisp_property('oriented_borders', 3, is_computed=True):
            compute_topomesh_property(topomesh, 'oriented_borders', 3)
        cell_oriented_faces = topomesh.wisp_property('oriented_borders', 3).values(list(topomesh.wisps(3)))

    cells = []
    cell_types = []
    if save_edges:
        cells += [[vertex_point_ids[vid] for vid in v] for v in edge_vertices]
        cell_types += [pv.CellType.LINE for v in edge_vertices]
    if save_faces:
        cells += [[vertex_point_ids[vid] for vid in v] for v in face_vertices]
        cell_types += [pv.CellType.POLYGON for v in face_vertices]
    if save_cells:
        cells += [[len(c_f)]+list(np.concatenate([[len(face_vertices_dict[f])] + [vertex_point_ids[vid] for vid in face_vertices_dict[f][::o]] for f, o in zip(c_f, c_o)])) for c_f, c_o in cell_oriented_faces]
        cell_types += [pv.CellType.POLYHEDRON for v in cell_oriented_faces]

    cells = np.concatenate([[len(c)] + c for c in cells])
    cell_types = np.array(cell_types)
    grid = pv.UnstructuredGrid(cells, cell_types, points)

    logging.info("  --> Exporting vertex properties")
    for property_name in topomesh.wisp_property_names(0):
        if topomesh.has_wisp_property(property_name, 0, is_computed=True):
            logging.debug(f"    --> {property_name}")
            vertex_property = topomesh.wisp_property(property_name, 0).values(list(topomesh.wisps(0)))
            if vertex_property.ndim == 1 and vertex_property.dtype != np.dtype(object): # scalar
                grid.point_data[property_name] = list(vertex_property)
            elif vertex_property.ndim == 2 and vertex_property.shape[1]==3: # vector
                grid.point_data[property_name] = list(vertex_property)
            elif vertex_property.ndim == 3 and vertex_property.shape[1]==3 and vertex_property.shape[2]==3: # tensor
                grid.point_data[property_name] = list(vertex_property)

    logging.info("  --> Exporting edge properties")
    if save_edges:
        for property_name in topomesh.wisp_property_names(1):
            if topomesh.has_wisp_property(property_name, 1, is_computed=True):
                logging.debug(f"    --> {property_name}")
                edge_property = topomesh.wisp_property(property_name, 1).values(list(topomesh.wisps(1)))
                cell_data = []
                if edge_property.ndim == 1 and edge_property.dtype != np.dtype(object): # scalar
                    cell_data += list(edge_property)
                    if save_faces:
                        cell_data += [np.nan for _ in topomesh.wisps(2)]
                    if save_cells:
                        cell_data += [np.nan for _ in topomesh.wisps(3)]
                    grid.cell_data[property_name] = cell_data
                elif edge_property.ndim == 2 and edge_property.shape[1]==3: # vector
                    cell_data += list(edge_property)
                    if save_faces:
                        cell_data += [np.tile(np.nan, 3) for _ in topomesh.wisps(2)]
                    if save_cells:
                        cell_data += [np.tile(np.nan, 3) for _ in topomesh.wisps(3)]
                    grid.cell_data[property_name] = cell_data
                elif edge_property.ndim == 3 and edge_property.shape[1]==3 and edge_property.shape[2]==3: # tensor
                    cell_data += list(edge_property)
                    if save_faces:
                        cell_data += [np.tile(np.nan, (3, 3)) for _ in topomesh.wisps(2)]
                    if save_cells:
                        cell_data += [np.tile(np.nan, (3, 3)) for _ in topomesh.wisps(3)]
                    grid.cell_data[property_name] = cell_data

    logging.info("  --> Exporting face properties")
    if save_faces:
        for property_name in topomesh.wisp_property_names(2):
            if topomesh.has_wisp_property(property_name, 2, is_computed=True):
                logging.debug(f"    --> {property_name}")
                face_property = topomesh.wisp_property(property_name, 2).values(list(topomesh.wisps(2)))
                cell_data = []
                if face_property.ndim == 1 and face_property.dtype != np.dtype(object): # scalar
                    if save_edges:
                        cell_data += [np.nan for _ in topomesh.wisps(1)]
                    cell_data += list(face_property)
                    if save_cells:
                        cell_data += [np.nan for _ in topomesh.wisps(3)]
                    grid.cell_data[property_name] = cell_data
                elif face_property.ndim == 2 and face_property.shape[1]==3: # vector
                    if save_edges:
                        cell_data += [np.tile(np.nan, 3) for _ in topomesh.wisps(1)]
                    cell_data += list(face_property)
                    if save_cells:
                        cell_data += [np.tile(np.nan, 3) for _ in topomesh.wisps(3)]
                    grid.cell_data[property_name] = cell_data
                elif face_property.ndim == 3 and face_property.shape[1]==3 and face_property.shape[2]==3: # tensor
                    if save_edges:
                        cell_data += [np.tile(np.nan, (3, 3)) for _ in topomesh.wisps(1)]
                    cell_data += list(face_property)
                    if save_cells:
                        cell_data += [np.tile(np.nan, (3, 3)) for _ in topomesh.wisps(3)]
                    grid.cell_data[property_name] = cell_data

    logging.info("  --> Exporting cell properties")
    if save_cells:
        for property_name in topomesh.wisp_property_names(3):
            if topomesh.has_wisp_property(property_name, 3, is_computed=True):
                logging.debug(f"    --> {property_name}")
                cell_property = topomesh.wisp_property(property_name, 3).values(list(topomesh.wisps(3)))
                cell_data = []
                if cell_property.ndim == 1 and cell_property.dtype != np.dtype(object): # scalar
                    if save_edges:
                        cell_data += [np.nan for _ in topomesh.wisps(1)]
                    if save_faces:
                        cell_data += [np.nan for _ in topomesh.wisps(2)]
                    cell_data += list(cell_property)
                    grid.cell_data[property_name] = cell_data
                elif cell_property.ndim == 2 and cell_property.shape[1]==3: # vector
                    if save_edges:
                        cell_data += [np.tile(np.nan, 3) for _ in topomesh.wisps(1)]
                    if save_faces:
                        cell_data += [np.tile(np.nan, 3) for _ in topomesh.wisps(2)]
                    cell_data += list(cell_property)
                    grid.cell_data[property_name] = cell_data
                elif cell_property.ndim == 3 and cell_property.shape[1]==3 and cell_property.shape[2]==3: # tensor
                    if save_edges:
                        cell_data += [np.tile(np.nan, (3, 3)) for _ in topomesh.wisps(1)]
                    if save_faces:
                        cell_data += [np.tile(np.nan, (3, 3)) for _ in topomesh.wisps(2)]
                    cell_data += list(cell_property)
                    grid.cell_data[property_name] = cell_data

    return grid