import numpy as np
from cellcomplex.utils import array_dict

from matplotlib.colors import to_hex, Normalize
from matplotlib.cm import ScalarMappable

from cellcomplex.property_topomesh.example_topomesh import circle_voronoi_topomesh, sphere_topomesh, square_topomesh
from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.io import read_ply_property_topomesh
from cellcomplex.property_topomesh.extraction import star_interface_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from visu_core.matplotlib import glasbey


def _svg_filled_poly(points, color='k', opacity=1., id=None):
    text = ""

    text += "<path "
    if id is not None:
        text += "id=\"" + str(id) + "\" "
    text += "fill=\"" + to_hex(color) + "\" "
    text += "fill-opacity=\"" + str(opacity) + "\" "
    text += "stroke-width=\"0\" "
    text += "d=\"M "
    for i, p in enumerate(points):
        if i > 0:
            text += "L "
        text += str(p[0]) + ", "
        text += str(p[1]) + " "
    text += "Z "
    text += "\" "
    text += "/>\n"

    return text


def _svg_gradient_poly(points, colors, center, extent, opacity=1, id=None):
    text = ""

    text += "<g "
    if id is not None:
        text += "id=\"" + str(id) + "\" "
    else:
        id = ""
    text += ">\n"
    text += "    <defs>\n"
    for v, (p, color) in enumerate(zip(points,colors)):
        text += "      <linearGradient "
        text += "id=\"" + str(id)
        text += "_vertex" + str(v) + "_gradient\" "
        text += "gradientUnits=\"userSpaceOnUse\" "
        text += "x1=\"" + str(p[0]) + "\" "
        text += "y1=\"" + str(p[1]) + "\" "
        text += "x2=\"" + str((center + (center - p) / 2.)[0]) + "\" "
        text += "y2=\"" + str((center + (center - p) / 2.)[1]) + "\" "
        text += ">\n"
        text += "        <stop offset=\"0%\" "
        text += "stop-color=\"" + to_hex(color) + "\" "
        text += "stop-opacity=\"" + str(opacity) + "\" "
        text += "/>\n"
        text += "        <stop offset=\"100%\" "
        # text += "stop-color=\"#000000\"/ "
        text += "stop-color=\"" + to_hex(color) + "\" "
        text += "stop-opacity=\"0\" "
        text += "/>\n"
        text += "      </linearGradient>\n"
    for v, p in enumerate(points):
        text += "      <path id=\"" + str(id)
        text += "_vertex" + str(v) + "_path\" "
        text += "d=\"M "
        for i, pp in enumerate(points):
            if i > 0:
                text += "L "
            text += str(pp[0]) + ", "
            text += str(pp[1]) + " "
        text += "Z "
        text += "\" "
        text += "fill=\"url(#"
        text += str(id) + "_vertex" + str(v) + "_gradient)\" "
        text += "/>\n"
    text += "      <filter id=\"" + str(id) + "_gradient\">\n"
    for v,_ in enumerate(points):
        text += "        <feImage "
        text += "xlink:href=\"#" + str(id)
        text += "_vertex" + str(v) + "_path\" "
        text += "result=\"" + str(id)
        text += "_vertex" + str(v) + "_layer\" "
        text += "x=\"0\" y=\"0\" "
        text += "width=\"" + str(int(np.ceil(extent[0]))) + "\" "
        text += "height=\"" + str(int(np.ceil(extent[1]))) + "\" "
        text += "/>\n"
    for v,_ in enumerate(points[1:]):
        text += "        <feComposite "
        text += "in=\"" + str(id)
        text += "_vertex" + str(v-1)
        if v == 0:
            text += "_layer\" "
        else:
            text += "_composite\" "
        text += "in2=\"" + str(id)
        text += "_vertex" + str(v)
        text += "_layer\" "
        text += "operator=\"arithmetic\" k1=\"0\" k2=\"1.0\" k3=\"1.0\" k4=\"0\" "
        text += "result=\"" + str(id)
        text += "_vertex" + str(v)
        text += "_composite\"/>\n"

    text += "        <feComposite "
    text += "in=\"" + str(id)
    text += "_vertex" + str(len(points)-1)
    text += "_composite\" "
    text += "in2=\"SourceGraphic\" "
    text += "operator=\"arithmetic\" k1=\"0\" k2=\"1.0\" k3=\"1.0\" k4=\"0\" "
    text += "/>\n"
    text += "      </filter>\n"
    text += "    </defs>\n"

    text += "    <path "
    # text += "fill-opacity=\"" + str(self.opacity) + "\" "
    # text += "stroke-width=\"0\" "
    text += "d=\"M "
    for v, p in enumerate(points):
        if v > 0:
            text += "L "
        text += str(p[0]) + ", "
        text += str(p[1]) + " "
    text += "Z "
    text += "\" "
    text += "fill=\"#000000\" "
    text += "filter=\"url(#" + str(id) + "_gradient)\" "
    text += "/>\n"
    text += "  </g>\n"

    return text


def _svg_circle(x, y, r=2., color='k', edgecolor=None, linewidth=1, id=None):
    text = ""

    text += "<circle "
    if id is not None:
        text += "id=\"" + str(id) + "\" "
    text += "fill=\"" + to_hex(color) + "\" "
    if edgecolor is not None:
        text += "stroke=\"" + to_hex(edgecolor) + "\" "
    text += "stroke-width=\"" + str(linewidth) + "\" "
    text += "cx=\"" + str(x) + "\" "
    text += "cy=\"" + str(y) + "\" "
    text += "r=\"" + str(r) + "\" "
    text += "/>\n"

    return text


def _svg_line(points, color='k', dotted=False, linewidth=1, id=None):
    text = ""

    text += "<path "
    if id is not None:
        text += "id=\"" + str(id) + "\" "
        text += "stroke=\"" + to_hex(color) + "\" "
    if dotted:
        text += "stroke-dasharray=\"" + str(linewidth) + " " + str(2*linewidth) + "\" "
    text += "stroke-width=\"" + str(linewidth) + "\" "
    # text += "stroke-linejoin=\"null\" "
    # text += "stroke-linecap=\"null\" "
    text += "fill-opacity=\"0.0\" "
    text += "d=\"M "
    for i, p in enumerate(np.random.permutation(points)):
        if i > 0:
            text += "L "
        text += str(p[0]) + ", "
        text += str(p[1]) + " "
    text += "\" "
    text += "/>\n"

    return text


def _svg_text(x, y, s, color='k', text_size=10):
    text = ""

    text += "<text "
    text += "x=\"" + str(x) + "\" "
    text += "y=\"" + str(y) + "\" "
    text += "font-weight=\"bold\" "
    text += "xml:space=\"preserve\" "
    text += "text-anchor=\"middle\" "
    text += "baseline-shift=\"-0.5em\" "
    text += "font-family=\"Avenir\" "
    text += "font-size=\"" + str(text_size) + "\" "
    text += "fill=\"" + to_hex(color) +"\" "
    text += "stroke=\"#ffffff\" "
    text += "stroke-width=\"" + str(text_size / 24.) + "\" "
    text += ">" + str(s) + "</text>\n"

    return text


class SvgLayer(object):
    def __init__(self, topomesh, slice_wisps, origin, extent, scale, reversed_y=True, **kwargs):

        self.topomesh = topomesh
        self.slice_wisps = slice_wisps
        self.origin = origin
        self.extent = extent
        self.scale = scale
        self.reversed_y = reversed_y

        self.property_name = kwargs.get("property_name",None)
        self.colormap = kwargs.get("colormap",None)
        self.value_range = kwargs.get("value_range",None)
        self.use_ids = kwargs.get("use_ids",False)
        self.size = kwargs.get("size",None)
        self.linewidth = kwargs.get("linewidth",None)
        self.facecolor = kwargs.get("facecolor",'k')
        self.edgecolor = kwargs.get("edgecolor",'k')
        self.cell_edges = kwargs.get("cell_edges",False)
        self.opacity = kwargs.get("opacity",1.)
        self.draw_ids = kwargs.get("draw_ids",False)
        self.text_size = kwargs.get("text_size",None)

        self.property_values = None

        self.update()

    def update(self):
        if self.size is None:
            self.size = np.max(self.extent) / (20 * np.sqrt(self.topomesh.nb_wisps(0)))
        if self.linewidth is None:
            self.linewidth = np.max(self.extent) / (20 * np.sqrt(self.topomesh.nb_wisps(0)))
        if self.text_size is None:
            #self.text_size = 24 * self.linewidth / np.sqrt(self.topomesh.nb_wisps(0))
            self.text_size = 12*self.linewidth

        if self.property_values is not None and self.value_range is None:
            if self.colormap == 'glasbey':
                self.value_range = (0,255)
            else:
                self.value_range = (np.nanmin(self.property_values), np.nanmax(self.property_values))

        if self.property_values is not None and self.colormap=='glasbey' and self.value_range==(0,255):
            self.property_values = array_dict(dict(zip(self.property_values.keys(), self.property_values.values()%256)))


class SvgVerticesLayer(SvgLayer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update(self):
        if self.property_name is not None and self.topomesh.has_wisp_property(self.property_name, 0, is_computed=True):
            self.property_values = self.topomesh.wisp_property(self.property_name, 0)
        elif self.use_ids:
            self.property_values = array_dict(dict(zip(self.topomesh.wisps(0), self.topomesh.wisps(0))))
        else:
            self.property_values = None
        super().update()

    def text(self, s):
        positions = self.topomesh.wisp_property('barycenter', 0)

        text = ""
        if self.property_values is not None:
            norm = Normalize(vmin=self.value_range[0], vmax=self.value_range[1])
            cmap = ScalarMappable(cmap=self.colormap, norm=norm)
        text += "<g class=\"layer\">\n"
        text += "  <title>vertices" + str(s) + "</title>\n"
        for v in self.slice_wisps[s][0]:
            point = positions[v]
            projected_point = self.scale * (point - self.origin)
            if self.reversed_y:
                projected_point[1] = self.extent[1] - projected_point[1]
            color = self.facecolor if self.property_values is None else cmap.to_rgba(self.property_values[v])
            text += "  " + _svg_circle(projected_point[0], projected_point[1], r=self.size,
                                       color=color, edgecolor=self.edgecolor, linewidth=self.linewidth/2,
                                       id="vertex "+str(v))
        if self.draw_ids:
            for v in self.slice_wisps[s][0]:
                center = self.scale * (self.topomesh.wisp_property('barycenter', 0)[v] - self.origin)
                if self.reversed_y:
                    center[1] = self.extent[1] - center[1]
                text += "  " + _svg_text(center[0], center[1], v, text_size=self.text_size)
        text += "</g>\n"

        return text


class SvgEdgesLayer(SvgLayer):

    def __init__(self, *args, **kwargs):
        self.boundary_edges = []
        self.cell_boundary_edges = []

        super().__init__(*args, **kwargs)

    def update(self):
        if self.property_name is not None and self.topomesh.has_wisp_property(self.property_name, 1, is_computed=True):
            self.property_values = self.topomesh.wisp_property(self.property_name, 1)
        elif self.use_ids:
            self.property_values = array_dict(dict(zip(self.topomesh.wisps(1), self.topomesh.wisps(1))))
        else:
            self.property_values = None

        self.boundary_edges = [e for e in self.topomesh.wisps(1) if self.topomesh.nb_regions(1, e) == 1]
        self.cell_boundary_edges = [e for e in self.topomesh.wisps(1) if len(list(self.topomesh.regions(1, e, 2))) > 1]

        super().update()

    def text(self, s):
        positions = self.topomesh.wisp_property('barycenter', 0)

        text = ""
        if self.property_values is not None:
            norm = Normalize(vmin=self.value_range[0], vmax=self.value_range[1])
            cmap = ScalarMappable(cmap=self.colormap, norm=norm)
        text += "<g class=\"layer\">\n"
        text += "  <title>edges" + str(s) + "</title>\n"
        for e in self.slice_wisps[s][1]:
            vertices = list(self.topomesh.borders(1, e))
            points = positions.values(vertices)
            projected_points = self.scale * (points - self.origin)
            if self.reversed_y:
                projected_points[:,1] = self.extent[1] - projected_points[:,1]
            color = self.edgecolor if self.property_values is None else cmap.to_rgba(self.property_values[e])
            dotted = self.cell_edges and not e in self.boundary_edges+self.cell_boundary_edges
            linewidth = self.linewidth/2 if dotted else self.linewidth
            text += "  " + _svg_line(projected_points, color=color, dotted=dotted, linewidth=linewidth, id="edge" + str(e))

        if self.draw_ids:
            for e in self.slice_wisps[s][1]:
                center = self.scale * (self.topomesh.wisp_property('barycenter', 1)[e] - self.origin)
                if self.reversed_y:
                    center[1] = self.extent[1] - center[1]
                text += "  " + _svg_text(center[0], center[1], e, text_size=self.text_size)
        text += "</g>\n"

        return text


class SvgFacesLayer(SvgLayer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update(self):
        if self.property_name is not None and self.topomesh.has_wisp_property(self.property_name, 2, is_computed=True):
            self.property_values = self.topomesh.wisp_property(self.property_name, 2)
        elif self.use_ids:
            self.property_values = array_dict(dict(zip(self.topomesh.wisps(2), self.topomesh.wisps(2))))
        else:
            self.property_values = None

        super().update()

    def text(self, s):
        positions = self.topomesh.wisp_property('barycenter', 0)

        text = ""
        if self.property_values is not None:
            norm = Normalize(vmin=self.value_range[0], vmax=self.value_range[1])
            cmap = ScalarMappable(cmap=self.colormap, norm=norm)
        text += "<g class=\"layer\">\n"
        text += "  <title>faces " + str(s) + "</title>\n"
        for f in self.slice_wisps[s][2]:
            vertices = self.topomesh.wisp_property('oriented_vertices', 2)[f]
            points = positions.values(vertices)
            projected_points = self.scale * (points - self.origin)
            if self.reversed_y:
                projected_points[:,1] = self.extent[1] - projected_points[:,1]
            color = self.facecolor if self.property_values is None else cmap.to_rgba(self.property_values[f])
            text += "  " + _svg_filled_poly(projected_points, color=color, opacity=self.opacity, id="face "+str(f))

        if self.draw_ids:
            for f in self.slice_wisps[s][2]:
                center = self.scale * (self.topomesh.wisp_property('barycenter', 2)[f] - self.origin)
                if self.reversed_y:
                    center[1] = self.extent[1] - center[1]
                text += "  " + _svg_text(center[0], center[1], f, text_size=self.text_size)
        text += "</g>\n"

        return text


class SvgVertexPropertyFacesLayer(SvgLayer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update(self):
        if self.property_name is not None and self.topomesh.has_wisp_property(self.property_name, 0, is_computed=True):
            self.property_values = self.topomesh.wisp_property(self.property_name, 0)
        elif self.use_ids:
            self.property_values = array_dict(dict(zip(self.topomesh.wisps(0), self.topomesh.wisps(0))))
        else:
            self.property_values = None

        super().update()

    def text(self, s):
        positions = self.topomesh.wisp_property('barycenter', 0)

        text = ""
        if self.property_values is not None:
            norm = Normalize(vmin=self.value_range[0], vmax=self.value_range[1])
            cmap = ScalarMappable(cmap=self.colormap, norm=norm)
        text += "<g class=\"layer\">\n"
        text += "  <title>faces " + str(s) + "</title>\n"
        for f in self.slice_wisps[s][2]:
            vertices = self.topomesh.wisp_property('oriented_vertices', 2)[f]
            points = positions.values(vertices)
            projected_points = self.scale * (points - self.origin)
            if self.reversed_y:
                projected_points[:,1] = self.extent[1] - projected_points[:,1]
            center = self.scale * (self.topomesh.wisp_property('barycenter', 2)[f] - self.origin)
            if self.reversed_y:
                center[1] = self.extent[1] - center[1]
            colors = [cmap.to_rgba(self.property_values[v]) for v in vertices]
            text += "  " + _svg_gradient_poly(projected_points,colors,center,
                                              extent=self.extent,opacity=self.opacity,id="face" + str(f))

        if self.draw_ids:
            for f in self.slice_wisps[s][2]:
                center = self.scale * (self.topomesh.wisp_property('barycenter', 2)[f] - self.origin)
                if self.reversed_y:
                    center[1] = self.extent[1] - center[1]
                text += "  " + _svg_text(center[0], center[1], f, text_size=self.text_size)
        text += "</g>\n"

        return text


class SvgCellsLayer(SvgLayer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update(self):
        if self.property_name is not None and self.topomesh.has_wisp_property(self.property_name, 3, is_computed=True):
            self.property_values = self.topomesh.wisp_property(self.property_name, 3)
        elif self.use_ids:
            self.property_values = array_dict(dict(zip(self.topomesh.wisps(3), self.topomesh.wisps(3))))
        else:
            self.property_values = None

        super().update()

    def text(self, s):
        positions = self.topomesh.wisp_property('barycenter', 0)

        text = ""
        if self.property_values is not None:
            norm = Normalize(vmin=self.value_range[0], vmax=self.value_range[1])
            cmap = ScalarMappable(cmap=self.colormap, norm=norm)
        text += "<g class=\"layer\">\n"
        text += "  <title>cells " + str(s) + "</title>\n"
        for c in self.slice_wisps[s][3]:
            for f in self.topomesh.borders(3,c):
                vertices = self.topomesh.wisp_property('oriented_vertices', 2)[f]
                points = positions.values(vertices)
                projected_points = self.scale * (points - self.origin)
                if self.reversed_y:
                    projected_points[:,1] = self.extent[1] - projected_points[:,1]

                color = self.facecolor if self.property_values is None else cmap.to_rgba(self.property_values[c])
                text += "  " + _svg_filled_poly(projected_points, color=color, opacity=self.opacity, id="face " + str(f))

        if self.draw_ids:
            for c in self.slice_wisps[s][3]:
                center = self.scale * (self.topomesh.wisp_property('barycenter', 3)[c] - self.origin)
                if self.reversed_y:
                    center[1] = self.extent[1] - center[1]
                text += "  " + _svg_text(center[0], center[1], c, text_size=self.text_size)
        text += "</g>\n"

        return text


class SvgTopomesh(object):

    def __init__(self, topomesh, svg_filename, z_slicing=True, z_slices=100, scale=1.):
        self.topomesh = topomesh
        self.extent = None
        self.origin = None

        self.svg_filename = svg_filename

        self.z_slicing = z_slicing
        self.z_slices = z_slices
        self.slice_wisps = {}

        self.scale = scale

        self.layers = []

        self.update()

    def update(self):
        positions = self.topomesh.wisp_property('barycenter', 0)
        compute_topomesh_property(self.topomesh, 'oriented_vertices', 2)
        compute_topomesh_property(self.topomesh, 'barycenter', 1)
        compute_topomesh_property(self.topomesh, 'barycenter', 2)
        compute_topomesh_property(self.topomesh, 'barycenter', 3)

        self.origin = np.min(positions.values(), axis=0).astype(float)
        self.extent = np.max(positions.values(), axis=0).astype(float) - self.origin
        # origin = np.zeros(3)
        self.origin -= (1 / np.sqrt(self.topomesh.nb_wisps(0))) * self.extent
        self.extent += (2 / np.sqrt(self.topomesh.nb_wisps(0))) * self.extent

        self.extent = self.scale * self.extent

        if self.z_slicing:
            wisp_z = np.concatenate([self.topomesh.wisp_property('barycenter', d).values()[:, 2] for d in range(4)])
            wisp_ids = np.concatenate([list(self.topomesh.wisps(d)) for d in range(4)])
            wisp_degrees = np.concatenate([d * np.ones(self.topomesh.nb_wisps(d)) for d in range(4)])

            z_sorted_wisp_ids = wisp_ids[np.argsort(wisp_z)]
            z_sorted_wisp_degrees = wisp_degrees[np.argsort(wisp_z)]
            z_sorted_wisp_slices = np.floor(np.arange(len(z_sorted_wisp_ids)) / (len(z_sorted_wisp_ids) / (self.z_slices))).astype(int)

            self.slice_wisps = {}
            for slice in range(self.z_slices):
                self.slice_wisps[slice] = {d: [] for d in range(4)}
            for w, d, s in zip(z_sorted_wisp_ids, z_sorted_wisp_degrees, z_sorted_wisp_slices):
                self.slice_wisps[s][d] += [w]
        else:
            self.slice_wisps = {0: {d: list(self.topomesh.wisps(d)) for d in range(4)}}

    def add_layer(self, degree, property_degree=None, **kwargs):
        if property_degree is None:
            property_degree = degree
        if degree == 0:
            self.layers += [SvgVerticesLayer(self.topomesh, self.slice_wisps, self.origin, self.extent, self.scale, **kwargs)]
        elif degree == 1:
            self.layers += [SvgEdgesLayer(self.topomesh, self.slice_wisps, self.origin, self.extent, self.scale, **kwargs)]
        elif degree == 2:
            if property_degree == 2:
                self.layers += [SvgFacesLayer(self.topomesh, self.slice_wisps, self.origin, self.extent, self.scale, **kwargs)]
            elif property_degree == 0:
                self.layers += [SvgVertexPropertyFacesLayer(self.topomesh, self.slice_wisps, self.origin, self.extent, self.scale, **kwargs)]
        elif degree == 3:
            self.layers += [SvgCellsLayer(self.topomesh, self.slice_wisps, self.origin, self.extent, self.scale, **kwargs)]

    def write(self):
        svg_file = open(self.svg_filename, "w+")

        svg_file.write("<svg ")
        svg_file.write("width=\"" + str(int(np.ceil(self.extent[0]))) + "\" ")
        svg_file.write("height=\"" + str(int(np.ceil(self.extent[1]))) + "\" ")
        svg_file.write("xmlns=\"http://www.w3.org/2000/svg\" ")
        svg_file.write("version=\"1.2\" ")
        svg_file.write("xmlns:xlink=\"http://www.w3.org/1999/xlink\" ")
        svg_file.write(">\n")

        for s in self.slice_wisps.keys():
            for layer in self.layers:
                svg_file.write(layer.text(s))

        svg_file.write("</svg>\n")
        svg_file.flush()
        svg_file.close()


if __name__ == '__main__':
    # topomesh = read_ply_property_topomesh("/Volumes/[MoSAiC]/Data/nuclei_images/PI-Ler-della-LD_180616_sam10/PI-Ler-della-LD_180616_sam10_t00/PI-Ler-della-LD_180616_sam10_t00_L1_grifone_topomesh.ply")
    #topomesh = read_ply_property_topomesh("/Users/gcerutti/Projects/RDP/GA_Sensor_BiHai/nuclei_images/qRGA-CLV3-PI-LD_TEST_CROP_200703_sam04/qRGA-CLV3-PI-LD_TEST_CROP_200703_sam04_t00/qRGA-CLV3-PI-LD_TEST_CROP_200703_sam04_t00_l1_cell_topomesh.ply")
    #topomesh = read_ply_property_topomesh("/Users/gcerutti/Data/nuclei_images/dome_organ_sam00/dome_organ_sam00_t00/dome_organ_sam00_t00_L1_grifone_topomesh.ply")
    #topomesh = read_ply_property_topomesh("/Users/gcerutti/Data/nuclei_images/sphere_tissue_sam00/sphere_tissue_sam00_t00/sphere_tissue_sam00_t00_L1_grifone_topomesh.ply")
    #topomesh = read_ply_property_topomesh("/Users/gcerutti/Projects/RDP/LayerHeight_Marketa/test/E_set_blue_SAM02/E_set_blue_SAM02_PI_surface.ply")
    topomesh = star_interface_topomesh(circle_voronoi_topomesh(size=3,cell_size=10,circle_size=50))
    #topomesh = circle_voronoi_topomesh(size=3,cell_size=10,circle_size=50)
    # #topomesh.update_wisp_property('label',2,{f:list(topomesh.regions(2,f))[0]%256 for f in topomesh.wisps(2)})
    #topomesh = star_interface_topomesh(sphere_topomesh())

    #topomesh = square_topomesh(side_length=400)
    #topomesh = triangle_topomesh([[0,1,2]],{0:[10.,10.,0.],1:[90.,10.,0.],2:[50.,70.,0.]})

    compute_topomesh_property(topomesh,'valency',0)

    svg = SvgTopomesh(topomesh,"/Users/gcerutti/Desktop/test_gradient.svg", z_slicing=False, scale=10)
    #svg.add_layer(3, use_ids=True, colormap='glasbey', value_range=(0,255), opacity=0.05)
    #svg.add_layer(2, use_ids=True, colormap='glasbey', value_range=(0,255), draw_ids=True)
    svg.add_layer(2, property_degree=0, property_name='valency', colormap='viridis', draw_ids=True)
    svg.add_layer(1, cell_edges=True)
    #svg.add_layer(0, property_name='dimension', colormap='jet',value_range=(-1,3))
    #svg.add_layer(3, opacity=0, draw_ids=True)
    svg.write()
