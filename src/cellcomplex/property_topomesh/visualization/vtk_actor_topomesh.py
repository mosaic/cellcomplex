# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import logging
from time import time as current_time

import numpy as np

try:
    import visu_core
except:
    logging.error("Please install visu_core to use these functionalities: conda install -c mosaic visu_core")
else:
    import vtk
    from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

    from cellcomplex.property_topomesh.analysis import compute_topomesh_property
    from cellcomplex.utils import array_dict

    from visu_core.vtk.polydata import vertex_scalar_property_polydata, vertex_vector_property_polydata, vertex_tensor_property_polydata
    from visu_core.vtk.polydata import edge_scalar_property_polydata
    from visu_core.vtk.polydata import face_scalar_property_polydata, face_scalar_vertex_property_polydata

    # from visu_core.matplotlib import glasbey
    from visu_core.matplotlib.colormap import to_rgb

    from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas, vtk_glyph_polydata
    from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap


    class VtkActorTopomesh(vtk.vtkActor):
        def __init__(self, topomesh=None, degree=None, property_name=None, property_degree=None, **kwargs):
            """

            Parameters
            ----------
            topomesh
            degree
            property_name
            kwargs
            """

            super().__init__()

            self.topomesh = topomesh
            self._degree = degree
            self._property_name = property_name

            self._property_degree = property_degree if property_degree is not None else self._degree

            self._modified = True

            self.scale_factor = kwargs.get('scale_factor',1.)
            self.cell_edges = kwargs.get('cell_edges',False)
            self.tensor_glyph = kwargs.get('tensor_glyph', 'ellipsoid')
            self.vector_glyph = kwargs.get('vector_glyph','arrow')
            self.point_glyph= kwargs.get('point_glyph','sphere')
            self.line_glyph = kwargs.get('line_glyph','line')
            self.glyph_scale = kwargs.get('glyph_scale',1.)

            self.polydata = None
            self.display_polydata = None
            self.scalar_mode = None

        @property
        def degree(self):
            return self._degree

        @degree.setter
        def degree(self, value):
            logging.info("Setting degree :"+str(self._degree)+" --> "+str(value))
            if self._degree != value:
                self._modified = True
            self._degree = value
            if self._property_degree is None:
                self._property_degree = self.degree

        @property
        def property_name(self):
            return self._property_name

        @property_name.setter
        def property_name(self, value):
            logging.info("Setting property name :"+str(self._property_name)+" --> "+str(value))
            if self.property_name != value:
                if self.topomesh is not None:
                    if self.topomesh.has_wisp_property(self._property_name, self.degree, is_computed=True):
                        property_data = self.topomesh.wisp_property(self._property_name, self.degree).values()
                        ndim = property_data.ndim
                    else:
                        ndim = 1

                    if self.topomesh.has_wisp_property(value, self.degree, is_computed=True):
                        property_data = self.topomesh.wisp_property(value, self.degree).values()
                        new_ndim = property_data.ndim
                    else:
                        new_ndim = 1
                    self._modified = ndim != new_ndim
            self._property_name = value

        def set_topomesh(self, topomesh, degree, property_name=None):
            self.topomesh = topomesh
            self._degree = degree
            if self._property_degree is None:
                self._property_degree = degree
            self.property_name = property_name

        def update_polydata(self):
            logging.info("--> Update polydata (modified="+str(self._modified)+")")

            self.display_polydata = None
            self._modified = True

            if self.degree == 3:
                positions = self.topomesh.wisp_property("barycenter", 0)
                compute_topomesh_property(self.topomesh,"oriented_vertices",2)

                if self.topomesh.has_wisp_property(self.property_name,3,is_computed=True):
                    cell_property = self.topomesh.wisp_property(self.property_name,3)
                else:
                    cell_property = array_dict(dict(zip(self.topomesh.wisps(3),[cid%256 for cid in self.topomesh.wisps(3)])))

                if cell_property.values().ndim == 1:
                    compute_topomesh_property(self.topomesh,"barycenter",3)

                    cell_polydatas = []
                    for cid in self.topomesh.wisps(3):
                            face_vertices = self.topomesh.wisp_property("oriented_vertices", 2).values(list(self.topomesh.borders(3,cid)))
                            property_data = np.array([cell_property[cid] for fid in self.topomesh.borders(3,cid)])
                            cell_center = self.topomesh.wisp_property('barycenter',3)[cid]
                            cell_positions = array_dict(dict(zip(positions.keys(),cell_center + self.scale_factor*(positions.values()-cell_center))))
                            cell_polydata = face_scalar_property_polydata(cell_positions, face_vertices, property_data)
                            wisp_ids = numpy_to_vtk(np.array([cid for fid in self.topomesh.borders(3,cid)]), deep=True, array_type=vtk.VTK_LONG)
                            wisp_ids.SetName("WispId")
                            cell_polydata.GetCellData().AddArray(wisp_ids)
                            cell_polydatas += [cell_polydata]

                    self.polydata = vtk_combine_polydatas(cell_polydatas, clean=False)
                    self.scalar_mode = 'cell_data'
                else:
                    compute_topomesh_property(self.topomesh,"barycenter",3)
                    positions = self.topomesh.wisp_property("barycenter", 3)
                    property_data = cell_property.values(list(self.topomesh.wisps(3)))

                    wisp_ids = numpy_to_vtk(np.array([cid for cid in self.topomesh.wisps(3)]), deep=True, array_type=vtk.VTK_LONG)
                    wisp_ids.SetName("WispId")

                    if property_data.ndim == 3:
                        if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                            self.polydata = vertex_tensor_property_polydata(positions, property_data, self.polydata)
                            self.scalar_mode = 'point_data'
                            self.polydata.GetPointData().AddArray(wisp_ids)
                            self.display_polydata = vtk_glyph_polydata(self.polydata,tensor_glyph=self.tensor_glyph, glyph_scale=self.glyph_scale)

                    elif property_data.ndim == 2:
                        self.polydata = vertex_vector_property_polydata(positions, property_data, self.polydata, update_polydata=self._modified)
                        self.scalar_mode = 'point_data'
                        self.polydata.GetPointData().AddArray(wisp_ids)
                        self.display_polydata = vtk_glyph_polydata(self.polydata,vector_glyph=self.vector_glyph, glyph_scale=self.glyph_scale)

                    if self.scalar_mode == 'point_data':
                        self.polydata.GetPointData().AddArray(wisp_ids)
                    elif self.scalar_mode == 'cell_data':
                        self.polydata.GetCellData().AddArray(wisp_ids)

            elif self.degree == 2:
                wisp_ids = numpy_to_vtk(np.array([fid for fid in self.topomesh.wisps(2)]), deep=True, array_type=vtk.VTK_LONG)
                wisp_ids.SetName("WispId")

                positions = self.topomesh.wisp_property("barycenter", 0)
                compute_topomesh_property(self.topomesh,"oriented_vertices",2)
                face_vertices = self.topomesh.wisp_property("oriented_vertices", 2).values()

                if self._property_degree == self.degree:
                    if self.topomesh.has_wisp_property(self.property_name,2,is_computed=True):
                        property_data = self.topomesh.wisp_property(self.property_name,2).values()
                    else:
                        # property_data = np.ones(self.topomesh.nb_wisps(2))
                        property_data = np.array(list(self.topomesh.wisps(2)))

                    if property_data.ndim == 3:
                        compute_topomesh_property(self.topomesh, 'barycenter', 2)
                        positions = self.topomesh.wisp_property("barycenter", 2)

                        if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                            self.polydata = vertex_tensor_property_polydata(positions, property_data, self.polydata)
                            self.polydata.GetPointData().AddArray(wisp_ids)
                            self.display_polydata = vtk_glyph_polydata(self.polydata, tensor_glyph=self.tensor_glyph, glyph_scale=self.glyph_scale)
                            self.scalar_mode = 'point_data'

                    elif property_data.ndim == 2:
                        print("VERTEX VECTOR PROP")
                        compute_topomesh_property(self.topomesh, 'barycenter', 2)
                        positions = self.topomesh.wisp_property("barycenter", 2)
                        self.polydata = vertex_vector_property_polydata(positions, property_data, self.polydata, update_polydata=self._modified)
                        self.polydata.GetPointData().AddArray(wisp_ids)
                        self.display_polydata = vtk_glyph_polydata(self.polydata, vector_glyph=self.vector_glyph, glyph_scale=self.glyph_scale)
                        self.scalar_mode = 'point_data'

                    elif property_data.ndim == 1:
                        self.polydata = face_scalar_property_polydata(positions, face_vertices, property_data, self.polydata, update_polydata=self._modified)
                        self.polydata.GetCellData().AddArray(wisp_ids)
                        self.scalar_mode = 'cell_data'
                else:
                    if self.topomesh.has_wisp_property(self.property_name,self._property_degree,is_computed=True):
                        property_data = self.topomesh.wisp_property(self.property_name, self._property_degree).values(list(self.topomesh.wisps(self._property_degree)))

                        if property_data.ndim == 1:
                            self.polydata = face_scalar_vertex_property_polydata(positions, face_vertices, property_data, self.polydata, update_polydata=self._modified)
                            self.polydata.GetPointData().AddArray(wisp_ids)
                            self.display_polydata = vtk_glyph_polydata(self.polydata, vector_glyph=self.vector_glyph, glyph_scale=self.glyph_scale)
                            self.scalar_mode = 'point_data'

            elif self.degree == 1:
                if self.cell_edges:
                    boundary_edges = [e for e in self.topomesh.wisps(1) if self.topomesh.nb_regions(1, e) == 1]
                    cell_boundary_edges = [e for e in self.topomesh.wisps(1) if len(list(self.topomesh.regions(1, e, 2))) > 1]
                    edges = np.unique(cell_boundary_edges + boundary_edges)
                else:
                    edges = np.array([eid for eid in self.topomesh.wisps(1)])

                wisp_ids = numpy_to_vtk(np.array(edges), deep=True, array_type=vtk.VTK_LONG)
                positions = self.topomesh.wisp_property("barycenter", 0)

                compute_topomesh_property(self.topomesh,"vertices",1)
                edge_vertices = self.topomesh.wisp_property("vertices", 1).values(edges)

                if self.topomesh.has_wisp_property(self.property_name, 1, is_computed=True):
                    property_data = self.topomesh.wisp_property(self.property_name,1).values(edges)
                else:
                    property_data = np.ones(len(edges))

                self.polydata = edge_scalar_property_polydata(positions, edge_vertices, property_data, self.polydata, line_glyph=self.line_glyph, glyph_scale=self.glyph_scale)
                self.scalar_mode = 'point_data'

                wisp_ids.SetName("WispId")
                if self.scalar_mode == 'point_data':
                    self.polydata.GetPointData().AddArray(wisp_ids)
                elif self.scalar_mode == 'cell_data':
                    self.polydata.GetCellData().AddArray(wisp_ids)

            elif self.degree == 0:
                wisp_ids = numpy_to_vtk(np.array([pid for pid in self.topomesh.wisps(0)]), deep=True, array_type=vtk.VTK_LONG)
                wisp_ids.SetName("WispId")
                positions = array_dict(dict(zip(self.topomesh.wisps(0),self.topomesh.wisp_property("barycenter", 0).values(list(self.topomesh.wisps(0))))))

                if self.topomesh.has_wisp_property(self.property_name,0,is_computed=True):
                    property_data = self.topomesh.wisp_property(self.property_name,0).values(positions.keys())
                    print(len(property_data),property_data.ndim)

                    if property_data.ndim == 3:
                        if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                            self.polydata = vertex_tensor_property_polydata(positions, property_data)
                            self.scalar_mode = 'point_data'
                            self.polydata.GetPointData().AddArray(wisp_ids)
                            self.display_polydata = vtk_glyph_polydata(self.polydata,tensor_glyph=self.tensor_glyph,glyph_scale=self.glyph_scale)

                    elif property_data.ndim == 2:
                        self.polydata = vertex_vector_property_polydata(positions, property_data)
                        self.scalar_mode = 'point_data'
                        self.polydata.GetPointData().AddArray(wisp_ids)
                        self.display_polydata = vtk_glyph_polydata(self.polydata,vector_glyph=self.vector_glyph, glyph_scale=self.glyph_scale)

                    elif property_data.ndim == 1:
                        self.polydata = vertex_scalar_property_polydata(positions, property_data)
                        self.scalar_mode = 'point_data'
                        self.polydata.GetPointData().AddArray(wisp_ids)
                        self.display_polydata = vtk_glyph_polydata(self.polydata, point_glyph=self.point_glyph, glyph_scale=self.glyph_scale)

                else:
                    property_data = np.ones(self.topomesh.nb_wisps(0))
                    self.polydata = vertex_scalar_property_polydata(positions, property_data)
                    self.scalar_mode = 'point_data'
                    self.polydata.GetPointData().AddArray(wisp_ids)
                    self.display_polydata = vtk_glyph_polydata(self.polydata, point_glyph=self.point_glyph, glyph_scale=self.glyph_scale)


        def update_actor(self, colormap='viridis', value_range=None, opacity=1, linewidth=1):

            if value_range is None:
                value_range = (0,1)
                if self.polydata.GetCellData().GetNumberOfArrays()>0:
                    scalars = vtk_to_numpy(self.polydata.GetCellData().GetArray(0))
                    if len(scalars)>0:
                        value_range = (np.nanmin(scalars),np.nanmax(scalars))
                elif self.polydata.GetPointData().GetNumberOfArrays()>0:
                    scalars = vtk_to_numpy(self.polydata.GetPointData().GetArray(0))
                    if len(scalars)>0:
                        value_range = (np.nanmin(scalars),np.nanmax(scalars))
            logging.info("--> Value range : "+str(value_range))

            lut = vtk_lookuptable_from_mpl_cmap(colormap, value_range)

            mapper = vtk.vtkPolyDataMapper()

            if self.scalar_mode == 'cell_data':
                mapper.SetScalarModeToUseCellData()
            elif self.scalar_mode == 'point_data':
                mapper.SetScalarModeToUsePointData()

            if self.display_polydata is not None:
                mapper.SetInputData(self.display_polydata)
            else:
                mapper.SetInputData(self.polydata)
            mapper.SetLookupTable(lut)

            self.SetMapper(mapper)

            self.GetProperty().SetOpacity(opacity)
            self.GetProperty().SetLineWidth(linewidth)

        def update(self, colormap='viridis',value_range=None, opacity=1, linewidth=1):
            self.update_polydata()
            self.update_actor(colormap=colormap, value_range=value_range, opacity=opacity, linewidth=linewidth)
            self._modified = False

        def clean(self):
            self.polydata.Initialize()


    class VtkLabelActorTopomesh(vtk.vtkActor2D):
        def __init__(self, topomesh, degree, color='k', font_size=40, wisp_ids=None):
            super().__init__()

            self.topomesh = topomesh
            self.degree = degree
            
            self.label_mapper = vtk.vtkLabeledDataMapper()
            self.SetMapper(self.label_mapper)

            self.update(color=color, font_size=font_size, wisp_ids=wisp_ids)
            
        def update(self, color='k', font_size=40, wisp_ids=None):
            if wisp_ids is None:
                wisp_ids = list(self.topomesh.wisps(self.degree))
            if not self.topomesh.has_wisp_property('barycenter', self.degree, is_computed=True):
                compute_topomesh_property(self.topomesh, 'barycenter', self.degree)

            centers = self.topomesh.wisp_property('barycenter', self.degree).to_dict()
            center_polydata = vertex_scalar_property_polydata({w: centers[w] for w in wisp_ids},
                                                              np.array([w for w in wisp_ids]))

            self.label_mapper.SetInputData(center_polydata)
            self.label_mapper.SetLabelModeToLabelScalars()
            self.label_mapper.SetLabelFormat("%g")
            self.label_mapper.GetLabelTextProperty().SetColor(*to_rgb(color))
            self.label_mapper.GetLabelTextProperty().SetFontSize(font_size)
            self.label_mapper.GetLabelTextProperty().ItalicOff()
            self.label_mapper.GetLabelTextProperty().SetJustificationToCentered()
            self.label_mapper.GetLabelTextProperty().SetVerticalJustificationToCentered()
