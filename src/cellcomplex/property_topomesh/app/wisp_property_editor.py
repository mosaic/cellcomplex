from PyQt5.QtWidgets import QWidget, QLineEdit, QCheckBox, QHBoxLayout
from PyQt5.Qt import QDoubleValidator, QIntValidator
from PyQt5.QtCore import pyqtSignal


class WispPropertyEditor(QWidget):
    editingFinished = pyqtSignal()

    def __init__(self, property_value, property_type, parent=None):
        super().__init__(parent)

        self.global_properties = {}
        self.global_name = ""

        self.layout = QHBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.property_type = property_type

        if self.property_type == 'bool':
            self.value_edit = QCheckBox()
            self.value_edit.setChecked(property_value)
            self.value_edit.stateChanged.connect(self.editingFinished)
        else:
            value_text = str(property_value) if property_value is not None else ""
            self.value_edit = QLineEdit(value_text)
            if self.property_type == 'int':
                self.value_edit.setValidator(QIntValidator())
            elif self.property_type == 'float':
                self.value_edit.setValidator(QDoubleValidator())
            self.value_edit.editingFinished.connect(self.editingFinished)
        self.layout.addWidget(self.value_edit)

        # self.global_value_edit = QComboBox()
        # self.global_value_edit.currentTextChanged.connect(self.set_global_value)
        # self.layout.addWidget(self.global_value_edit)

        # self.set_global_properties({})

    def value(self):
        if self.property_type == 'bool':
            return self.value_edit.isChecked()
        else:
            if self.property_type == 'int':
                return int(self.value_edit.text())
            elif self.property_type == 'float':
                return float(self.value_edit.text())
            elif self.property_type == 'str':
                return self.value_edit.text()
        return

    # def has_global_value(self):
    #     return self.global_name == "" or self.global_name not in self.global_properties
    #
    # def set_global_value(self, global_name):
    #     self.global_name = global_name
    #     if global_name in self.global_properties:
    #         self.global_value_edit.setCurrentText(self.global_name)
    #     self.update_value_edit()

    def update_value_edit(self):
        # if self.global_name == "" or self.global_name not in self.global_properties:
        #     self.value_edit.setEnabled(True)
        # else:
        self.value_edit.setText(str(self.global_properties[self.global_name]))
        self.editingFinished.emit()
        self.value_edit.setEnabled(False)
    #
    # def set_global_properties(self, global_properties):
    #     self.global_properties = global_properties
    #
    #     self.global_value_edit.clear()
    #     self.global_value_edit.addItems([""]+list(self.global_properties.keys()))
    #
    #     if self.global_name in [""]+list(self.global_properties.keys()):
    #         self.global_value_edit.setCurrentText(self.global_name)
    #     else:
    #         self.global_value_edit.setCurrentText("")
    #     if len(self.global_properties) == 0:
    #         self.global_value_edit.setVisible(False)
    #     else:
    #         self.global_value_edit.setVisible(True)
