import os
import argparse
import json
import logging
import time

from PyQt5 import Qt, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal

import numpy as np
import matplotlib.pyplot as plt

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_cell_property_from_faces
from cellcomplex.property_topomesh.io import save_ply_property_topomesh, read_ply_property_topomesh
from cellcomplex.property_topomesh.visualization.matplotlib import mpl_draw_topomesh

import cellcomplex.property_topomesh.app
from cellcomplex.property_topomesh.app.interactor_styles.cellcomplex_interactor_style import CellComplexWispPickerInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.zoom_drag import FigureZoomDragInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.edge_picker import CellComplexEdgeTopologyEditorInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.vertex_property import CellComplexVertexPropertyEditorInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.edge_property import CellComplexEdgePropertyEditorInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.face_property import CellComplexFacePropertyEditorInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.cell_property import CellComplexCellPropertyEditorInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.vertex_hover import CellComplexVertexHoverInformationInteractorStyle
from cellcomplex.property_topomesh.app.interactor_styles.vertex_move import CellComplexVertexMovePositionInteractorStyle

from visu_core.matplotlib.figure import figure_widgets

class SwitchSlider(QtWidgets.QWidget):
    
    valueChanged = pyqtSignal()
    
    def __init__(self, parent=None, off_value=0, on_value=1):
        super().__init__(parent)

        self.on = True
        
        self.slider = QtWidgets.QSlider()
        self.slider.setOrientation(Qt.Qt.Horizontal)
        self.slider.setFixedWidth(40)
        self.slider.setMinimum(0)
        self.slider.setMaximum(1)
        self.slider.setValue(self.on)
        self.slider.valueChanged.connect(self.update)
        
        self.off_value = off_value
        self.on_value = on_value
        
        self.off_label = QtWidgets.QLabel(str(self.off_value))
        self.on_label = QtWidgets.QLabel(str(self.on_value))
        
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.off_label)
        self.layout.addWidget(self.slider)
        self.layout.addWidget(self.on_label)

        self.setLayout(self.layout)
    
    def setValue(self, value):
        if value == self.off_value:
            self.slider.setValue(0)
        elif value == self.on_value:
            self.slider.setValue(1)
    
    def update(self):
        self.on = (self.slider.value() == 1)
        self.valueChanged.emit()
    
    def value(self):
        return self.on_value if self.on else self.off_value

    def mouseReleaseEvent(self, event):
        self.setValue(self.on_value if not self.on else self.off_value)

        super().mouseReleaseEvent(event)
    


class CellComplexEditor(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.topomesh = None

        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.plot_vertices = True
        # self.direction = 'down'
        # self.nodescale = 100.
        # self.linewidth = 1.
        # self.highlight_color = 'k'

        self.face_plot_degree = 2
        self.face_property = None

        # self.variable_type = 'attribute'
        # self.nodescale_attribute = ""
        # self.color_attribute = ""
        # self.linewidth_attribute = ""

        self.plot_vertices_edit = QtWidgets.QCheckBox()
        # self.direction_edit = QtWidgets.QComboBox()
        # self.nodescale_edit = QtWidgets.QLineEdit(str(self.nodescale))
        # self.linewidth_edit = QtWidgets.QLineEdit(str(self.linewidth))
        #
        self.face_plot_degree_edit = SwitchSlider(off_value=2,on_value=3)

        self.face_property_edit = QtWidgets.QComboBox()

        # self.nodescale_attribute_edit = QtWidgets.QComboBox()
        # self.color_attribute_edit = QtWidgets.QComboBox()
        # self.linewidth_attribute_edit = QtWidgets.QComboBox()
        #
        self.visualization_layout = QtWidgets.QVBoxLayout()
        self.visualization_pane = QtWidgets.QWidget(self)

        self.visualization_parameters_label = QtWidgets.QLabel("Visualization Parameters")
        self.visualization_parameters_layout = QtWidgets.QFormLayout()
        self.visualization_parameters_pane = QtWidgets.QWidget()

        # self.global_attribute_values = {}
        # self.global_widgets = {}
        # self.global_types = {}
        # self.node_global_attributes = {}
        #
        # self.global_label = QLabel("Global Attribute Values")
        # self.global_layout = QFormLayout()
        # self.global_pane = QWidget()
        # self.new_global_button = QPushButton('New Global Attribute')
        #
        self.layout.addWidget(self.visualization_pane)

        self.figure = plt.figure(0)
        self.figure_widget = figure_widgets[0]
        self.figure_widget.setToolTip(None)
        self.figure_widget.setSizePolicy(Qt.QSizePolicy.Expanding, Qt.QSizePolicy.Expanding)
        self.layout.addWidget(self.figure_widget)

        # self.hidden_attributes = []
        # self.hidden_attributes_for_plots = []
        # self.attribute_widgets = {}
        # self.attribute_types = {}
        # self.pick_connect = None
        # self.unselect_connect = None
        # self.select = False

        # self.load_button = QPushButton("Load Tree")
        # self.save_button = QPushButton("Save Tree")
        # self.export_button = QPushButton("Export Tree Data")
        # self.latex_button = QPushButton("Export to LaTeX")
        # self.screenshot_button = QPushButton("Save Screenshot")
        # self.new_attribute_button = QPushButton('New Attribute')
        # self.add_button = QPushButton("Add Child")
        # self.add_select_parent = QCheckBox("Select Parent Node After Adding Child")
        # self.remove_button = QPushButton("Remove Subtree")
        #
        # self.node_property_label = QLabel("Current Node Properties")
        # self.property_layout = QFormLayout()
        # self.property_pane = QWidget()
        #
        # self.node_attribute_label = QLabel("Current Node Attributes")
        # self.attribute_layout = QFormLayout()
        # self.attribute_pane = QWidget()
        #
        self.edition_layout = QtWidgets.QVBoxLayout()
        self.edition_pane = QtWidgets.QWidget(self)
        self.layout.addWidget(self.edition_pane)

        self.interactor_styles = {}
        self.actions = {}
        self.action_buttons = {}

        self.interactor_style = None

        self.initialize_interactor_toolbar()
        self.initialize_visualization_pane()
        self.initialize_edition_pane()

        self.refresh_plot()

    def set_topomesh(self, topomesh):
        assert isinstance(topomesh, PropertyTopomesh)
        self.topomesh = topomesh

        for name, style in self.interactor_styles.items():
            if isinstance(style,dict):
                for sub_name, sub_style in style.items():
                    sub_style.set_topomesh(self.topomesh)
            else:
                style.set_topomesh(self.topomesh)
        self.update_property_editors()
        self.refresh_plot()

    def initialize_interactor_toolbar(self):

        self.interactor_styles['figure'] = FigureZoomDragInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['vertex'] = {}
        self.interactor_styles['vertex']['vertex_hover'] = CellComplexVertexHoverInformationInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['vertex']['vertex_move'] = CellComplexVertexMovePositionInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['edge_picker'] = CellComplexEdgeTopologyEditorInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['wisp_property'] = {}
        self.interactor_styles['wisp_property']['vertex_property'] = CellComplexVertexPropertyEditorInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['wisp_property']['edge_property'] = CellComplexEdgePropertyEditorInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['wisp_property']['face_property'] = CellComplexFacePropertyEditorInteractorStyle(self.topomesh, self.figure)
        self.interactor_styles['wisp_property']['cell_property'] = CellComplexCellPropertyEditorInteractorStyle(self.topomesh, self.figure)

        for name, style in self.interactor_styles.items():
            if isinstance(style,dict):
                for sub_name, sub_style in style.items():
                    sub_style.editingFinished.connect(self.refresh_plot)
                    sub_style.setParent(self)
                    self.actions[sub_name] = Qt.QAction(sub_style.name(), self)
                    self.actions[sub_name].setCheckable(True)
                    self.actions[sub_name].setIcon(sub_style.icon())
                    self.actions[sub_name].triggered.connect(self.update_interactor_style)
            else:
                style.editingFinished.connect(self.refresh_plot)
                style.setParent(self)
                self.actions[name] = Qt.QAction(style.name(), self)
                self.actions[name].setCheckable(True)
                self.actions[name].setIcon(style.icon())
                self.actions[name].triggered.connect(self.update_interactor_style)

        interactor_toolbar = QtWidgets.QToolBar(self)
        #interactor_toolbar.setFixedHeight(60)
        interactor_toolbar.setIconSize(Qt.QSize(60,60))
        interactor_toolbar.setSizePolicy(Qt.QSizePolicy.Expanding, Qt.QSizePolicy.Expanding)

        app_directory = cellcomplex.property_topomesh.app.__path__[0]

        new_action = Qt.QAction("New CellComplex", self)
        new_action.triggered.connect(self.new_topomesh)
        new_action.setIcon(QtGui.QIcon(app_directory+"/new.svg"))
        new_action_button = QtWidgets.QToolButton(self)
        new_action_button.setDefaultAction(new_action)
        interactor_toolbar.addWidget(new_action_button)

        open_action = Qt.QAction("Open CellComplex", self)
        open_action.triggered.connect(self.open_topomesh)
        open_action.setIcon(QtGui.QIcon(app_directory + "/open.svg"))
        open_action_button = QtWidgets.QToolButton(self)
        open_action_button.setDefaultAction(open_action)
        interactor_toolbar.addWidget(open_action_button)

        save_action = Qt.QAction("Save CellComplex", self)
        save_action.triggered.connect(self.save_topomesh)
        save_action.setIcon(QtGui.QIcon(app_directory+"/save.svg"))
        save_action_button = QtWidgets.QToolButton(self)
        save_action_button.setDefaultAction(save_action)
        interactor_toolbar.addWidget(save_action_button)

        interactor_toolbar.addSeparator()

        action_group = Qt.QActionGroup(self)
        action_group.setExclusive(True)
        for name in self.interactor_styles.keys():
            self.action_buttons[name] = QtWidgets.QToolButton(self)
            if isinstance(self.interactor_styles[name],dict):
                # sub_action_group = Qt.QActionGroup(self)
                sub_action_menu = Qt.QMenu(self)
                # sub_action_group.setExclusive(True)
                for i,(sub_name, sub_style) in enumerate(self.interactor_styles[name].items()):
                    action_group.addAction(self.actions[sub_name])
                    # sub_action_group.addAction(self.actions[sub_name])
                    sub_action_menu.addAction(self.actions[sub_name])
                    if i == 0:
                        self.action_buttons[name].setDefaultAction(self.actions[sub_name])
                self.action_buttons[name].setMenu(sub_action_menu)
                sub_action_menu.triggered.connect(self.action_buttons[name].setDefaultAction)
                self.action_buttons[name].setPopupMode(QtWidgets.QToolButton.MenuButtonPopup)
            else:
                action_group.addAction(self.actions[name])
                self.action_buttons[name].setDefaultAction(self.actions[name])
            interactor_toolbar.addWidget(self.action_buttons[name])
        self.layout.setMenuBar(interactor_toolbar)

    def get_interactor_style(self, name):
        if name in self.interactor_styles.keys():
            return self.interactor_styles[name]
        else:
            for group in [s for s in self.interactor_styles.keys() if isinstance(self.interactor_styles[s], dict)]:
                if name in self.interactor_styles[group].keys():
                    return self.interactor_styles[group][name]

    def disable_interactor_style(self):
        if self.interactor_style is not None:
            self.interactor_style.clear()
        self.interactor_style = None
        while not self.edition_layout.isEmpty():
            item = self.edition_layout.takeAt(0)
            item.widget().setParent(self)
            del item

    def enable_interactor_style(self, name):
        self.interactor_style = self.get_interactor_style(name)
        if self.interactor_style is not None:
            self.edition_layout.addWidget(self.interactor_style.pane())

    def update_interactor_style(self):
        self.disable_interactor_style()
        for name in self.actions.keys():
            if self.actions[name].isChecked():
                self.enable_interactor_style(name)
        self.refresh_plot()

    def initialize_edition_pane(self):
        self.edition_layout.setContentsMargins(0, 0, 0, 0)
        if self.interactor_style is not None:
            self.edition_layout.addWidget(self.interactor_style.pane())

        self.edition_pane.setFixedWidth(300)
        self.edition_pane.setLayout(self.edition_layout)

    def initialize_visualization_pane(self):
        self.visualization_layout.setContentsMargins(0, 10, 0, 0)
        self.visualization_layout.setAlignment(Qt.Qt.AlignCenter)

        self.visualization_pane.setFixedWidth(300)
        self.visualization_pane.setLayout(self.visualization_layout)

        font = QtGui.QFont()
        self.visualization_parameters_label.setFont(QtGui.QFont(font.defaultFamily(), 16, QtGui.QFont.Bold))
        self.visualization_parameters_label.setAlignment(Qt.Qt.AlignCenter)
        self.visualization_layout.addWidget(self.visualization_parameters_label)

        self.visualization_parameters_layout.setContentsMargins(0, 0, 0, 0)

        self.visualization_parameters_pane.setSizePolicy(Qt.QSizePolicy.Expanding, Qt.QSizePolicy.Expanding)
        self.visualization_parameters_pane.setLayout(self.visualization_parameters_layout)
        self.visualization_layout.addWidget(self.visualization_parameters_pane)

        self.plot_vertices_edit.setChecked(self.plot_vertices)
        self.plot_vertices_edit.stateChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Plot Vertices", self.plot_vertices_edit)

        self.face_plot_degree_edit.setValue(self.face_plot_degree)
        self.face_plot_degree_edit.valueChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Face Plot Degree", self.face_plot_degree_edit)

        self.update_property_editors()
        self.face_property_edit.currentTextChanged.connect(self.refresh_plot)
        self.visualization_parameters_layout.addRow("Face Property", self.face_property_edit)


        # self.direction_edit.clear()
        # self.direction_edit.addItems(['down', 'up', 'left', 'right'])
        # self.direction_edit.setCurrentText(self.direction)
        # self.direction_edit.currentIndexChanged.connect(self.refresh_plot)
        # self.visualization_parameters_layout.addRow("Direction", self.direction_edit)
        #
        # self.nodescale_edit.setValidator(QDoubleValidator())
        # self.nodescale_edit.editingFinished.connect(self.refresh_plot)
        # self.visualization_parameters_layout.addRow("Scale factor", self.nodescale_edit)
        #
        # self.linewidth_edit.setValidator(QDoubleValidator())
        # self.linewidth_edit.editingFinished.connect(self.refresh_plot)
        # self.visualization_parameters_layout.addRow("Width factor", self.linewidth_edit)
        #
        # self.nodescale_attribute_edit.setFixedWidth(150)
        # self.nodescale_attribute_edit.clear()
        # self.nodescale_attribute_edit.addItems([""])
        # self.nodescale_attribute_edit.setCurrentText(self.nodescale_attribute)
        # self.nodescale_attribute_edit.currentIndexChanged.connect(self.refresh_plot)
        # self.visualization_parameters_layout.addRow("Node Scale Attribute", self.nodescale_attribute_edit)
        #
        # self.color_attribute_edit.setFixedWidth(150)
        # self.color_attribute_edit.clear()
        # self.color_attribute_edit.addItems([""])
        # self.color_attribute_edit.setCurrentText(self.color_attribute)
        # self.color_attribute_edit.currentIndexChanged.connect(self.refresh_plot)
        # self.visualization_parameters_layout.addRow("Node Color Attribute", self.color_attribute_edit)
        #
        # self.linewidth_attribute_edit.setFixedWidth(150)
        # self.linewidth_attribute_edit.clear()
        # self.linewidth_attribute_edit.addItems([""])
        # self.linewidth_attribute_edit.setCurrentText(self.linewidth_attribute)
        # self.linewidth_attribute_edit.currentIndexChanged.connect(self.refresh_plot)
        # self.visualization_parameters_layout.addRow("Line Width Attribute", self.linewidth_attribute_edit)
        #
        # font = QFont()
        # self.global_label.setFont(QFont(font.defaultFamily(), 16, QFont.Bold))
        # self.global_label.setAlignment(Qt.AlignCenter)
        # self.global_label.setVisible(False)
        # self.visualization_layout.addWidget(self.global_label)
        #
        # self.global_layout.setContentsMargins(0, 0, 0, 0)
        #
        # self.global_pane.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        # self.global_pane.setLayout(self.global_layout)
        # self.visualization_layout.addWidget(self.global_pane)
        #
        # self.new_global_button.clicked.connect(self.add_new_global)

    def update_property_editors(self):
        self.face_property_edit.blockSignals(True)
        self.face_property_edit.clear()
        if self.topomesh is not None:
            property_names = [p for p in self.topomesh.wisp_property_names(self.face_plot_degree)]
            property_names = [p for p in property_names if self.topomesh.wisp_property(p,self.face_plot_degree).values().ndim == 1]
            property_names = [p for p in property_names if self.topomesh.wisp_property(p,self.face_plot_degree).values().dtype in [np.dtype(t) for t in 'OS']]
            self.face_property_edit.addItems([""] + property_names)
            if self.face_property in property_names:
                self.face_property_edit.setCurrentText(self.face_property)
            else:
                self.face_property_edit.setCurrentText("")
        self.face_property_edit.blockSignals(False)

    def update_visualization_parameters(self):
        self.plot_vertices = self.plot_vertices_edit.isChecked()
        self.face_plot_degree = self.face_plot_degree_edit.value()
        self.face_property = self.face_property_edit.currentText()
        self.update_property_editors()
        self.face_property = self.face_property_edit.currentText()
        if self.face_property == "":
            self.face_property = None

    def refresh_plot(self):
        self.figure.clf()
        self.update_visualization_parameters()

        if self.topomesh is not None:

            compute_topomesh_property(self.topomesh, 'length', 1)
            compute_topomesh_property(self.topomesh, 'barycenter', 1)
            compute_topomesh_property(self.topomesh, 'area', 2)
            compute_topomesh_property(self.topomesh, 'barycenter', 2)
            compute_topomesh_cell_property_from_faces(self.topomesh, 'area', weighting='uniform', reduce='sum')
            compute_topomesh_property(self.topomesh, 'barycenter', 3)

            face_plot = mpl_draw_topomesh(self.topomesh, self.figure, degree=self.face_plot_degree,
                                          property_name=self.face_property, coef=0.8,
                                          color='k', alpha=0.33)

            mpl_draw_topomesh(self.topomesh, self.figure, degree=1, cell_edges=True, color='k', linewidth=5)
            edge_plot = mpl_draw_topomesh(self.topomesh, self.figure, degree=1, color='k', alpha=0.5, linewidth=3)

            if self.plot_vertices:
                vertex_plot = mpl_draw_topomesh(self.topomesh, self.figure, degree=0, color='k', size=100)
            else:
                vertex_plot = None

            if self.interactor_style is not None:
                self.interactor_style.refresh()

                if self.interactor_style in [self.get_interactor_style('edge_picker'),
                                             self.get_interactor_style('edge_property')]:
                    self.interactor_style.set_artist(edge_plot)
                elif self.interactor_style in [self.get_interactor_style('face_property'),
                                               self.get_interactor_style('cell_property')]:
                    self.interactor_style.set_artist(face_plot)
                elif self.interactor_style in [self.get_interactor_style('vertex_hover'),
                                               self.get_interactor_style('vertex_move'),
                                               self.get_interactor_style('vertex_property')]:
                    self.interactor_style.set_artist(vertex_plot)
                else:
                    self.interactor_style.connect()

        self.figure.gca().axis('equal')
        self.figure.gca().axis('off')

    def new_topomesh(self):
        if self.topomesh is not None:
            reply = QtWidgets.QMessageBox.question(self,"New CellComplex","Are you sure? Your current CellComplex will be overwritten!",
                                                   QtWidgets.QMessageBox.Ok|QtWidgets.QMessageBox.Cancel)
        else:
            reply = QtWidgets.QMessageBox.Ok

        if reply == QtWidgets.QMessageBox.Ok:
            positions = {}
            positions[0] = [-np.sqrt(3) / 2, -1 / 2, 0]
            positions[1] = [np.sqrt(3) / 2, -1 / 2, 0]
            positions[2] = [0, 1, 0]
            topomesh = triangle_topomesh([[0, 1, 2]], positions)
            self.set_topomesh(topomesh)

    def open_topomesh(self):
        file_dialog = QtWidgets.QFileDialog(self,"Load CellComplex",".","PLY CellComplex files (*.ply)")
        file_dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            try:
                t = read_ply_property_topomesh(filename)
            except:
                logging.error("Could not load CellComplex from file "+filename+"!")
            else:
                self.set_topomesh(t)
                self.refresh_plot()

    def save_topomesh(self):
        file_dialog = QtWidgets.QFileDialog(self,"Save CellComplex",".","PLY CellComplex files (*.ply)")
        file_dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        # file_dialog.setWindowFlags(Qt.Sheet)
        # file_dialog.setWindowModality(Qt.Qt.WindowModal)
        accept = file_dialog.exec_()
        if accept:
            filename = file_dialog.selectedFiles()[0]
            properties_to_save = {d:self.topomesh.wisp_property_names(d) for d in range(4)}
            save_ply_property_topomesh(self.topomesh, filename, properties_to_save=properties_to_save)


def main():
    """
    Returns
    -------
    """
    parser = argparse.ArgumentParser()
    # parser.add_argument('-f', '--filename', help='Path to file containing a pickled treex Tree object', default="")
    # parser.add_argument('-H', '--hidden-attributes', default=['color_plotgraph','nodescale_plotgraph','linewidth_plotgraph'], nargs='+', help='List of attribute names to hide from the edition panel')
    # parser.add_argument('-V', '--variable-type', help='Whether to use node attributes or properties as plot parameters', default='attribute')
    # parser.add_argument('-c', '--highlight-color', help='Color used to highlight the current node', default='chartreuse')

    args = parser.parse_args()

    Qt.QLocale.setDefault(Qt.QLocale.c())

    app = Qt.QApplication([])

    window = Qt.QMainWindow()
    window.setWindowTitle("CellComplex Editor")

    editor = CellComplexEditor()
    editor.new_topomesh()
    editor.actions['figure'].activate(Qt.QAction.Trigger)
    # editor.set_variable_type(args.variable_type)
    # editor.set_hidden_attributes(args.hidden_attributes)
    # editor.highlight_color = args.highlight_color
    window.setCentralWidget(editor)

    window.show()

    app.exec_()


if __name__ == '__main__':
    main()
