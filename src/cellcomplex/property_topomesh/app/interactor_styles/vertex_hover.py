from PyQt5 import Qt, QtGui, QtWidgets

import cellcomplex.property_topomesh.app.interactor_styles
from cellcomplex.property_topomesh.app.interactor_styles.cellcomplex_interactor_style import CellComplexWispPickerInteractorStyle


class CellComplexVertexHoverInformationInteractorStyle(CellComplexWispPickerInteractorStyle):

    def __init__(self, topomesh, figure):
        super().__init__(topomesh, figure, degree=0, event_type='hover')

        self.annotation = None

        self.initialize_pane()

    def name(self):
        return "Vertex Hover"

    def icon(self):
        style_directory = cellcomplex.property_topomesh.app.interactor_styles.__path__[0]
        return QtGui.QIcon(style_directory + "/vertex_hover.svg")

    def clear(self):
        super().clear()

        if self.annotation is not None:
            self.annotation.remove()
            self.annotation = None

        self.refresh()

    def refresh(self):
        super().refresh()
        if self.annotation is None:
            self.annotation = self.figure.gca().annotate("Vertex", xy=(0,0),
                                                         xytext=(20, 10), textcoords="offset points",
                                                         bbox=dict(boxstyle="round", fc="k", ec='none', alpha=0.1),
                                                         arrowprops=dict(arrowstyle="-"))
            self.annotation.set_visible(False)

        if self.wisp_id is not None:
            pos = self.topomesh.wisp_property('barycenter',0)[self.wisp_id][:2]
            self.annotation.xy = pos
            self.annotation.set_text("Vertex "+str(self.wisp_id))
            self.annotation.set_visible(True)
        else:
            self.annotation.set_visible(False)
        self.figure.canvas.draw()

