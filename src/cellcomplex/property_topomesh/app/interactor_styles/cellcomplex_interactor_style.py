import logging

from PyQt5 import Qt, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal

from matplotlib.collections import LineCollection, PolyCollection, PathCollection


class CellComplexInteractorStyle(Qt.QObject):
    editingFinished = pyqtSignal()

    def __init__(self, topomesh, figure):

        super().__init__()

        self.topomesh = topomesh
        self.figure = figure

        self.editor_layout = QtWidgets.QVBoxLayout()
        self.editor_pane = QtWidgets.QWidget()

        self.connects = {}

    def set_topomesh(self, topomesh):
        self.clear()
        self.topomesh = topomesh

    def initialize_pane(self):
        self.editor_layout.setContentsMargins(0, 0, 0, 0)
        self.editor_pane.setLayout(self.editor_layout)

    def name(self):
        return ""

    def icon(self):
        return QtGui.QIcon()

    def pane(self):
        return self.editor_pane

    def clear(self):
        for c in self.connects.keys():
            self.figure.canvas.mpl_disconnect(self.connects[c])
        self.connects = {}

        self.refresh()

    def refresh(self):
        pass


class CellComplexWispPickerInteractorStyle(CellComplexInteractorStyle):

    def __init__(self, topomesh, figure, degree, event_type='pick'):

        super().__init__(topomesh, figure)

        self.degree = degree

        self.event_type = event_type
        self.picked = False
        self.artist = None

        self.editing = False

        self.wisp_id = None

        self.highlight_artist = None
        self.highlight_color = 'y'
        self.highlight_alpha = 0.33

    def name(self):
        element_names = {0:"Vertex", 1:"Edge", 2:"Face", 3:"Cell"}
        return element_names[self.degree] +" Picker"

    def clear(self):
        super().clear()

        if self.artist is not None:
            self.artist.set_picker(None)

        if not self.editing:
            self.wisp_id = None
        self.refresh()

    def set_artist(self, artist):
        self.clear()

        self.artist = artist

        wisp_artist_types = {0:PathCollection, 1:LineCollection, 2:PolyCollection, 3:PolyCollection}
        if isinstance(artist, wisp_artist_types[self.degree]):
            if self.event_type == "pick":
                self.artist.set_picker(True)
                self.connects['wisp_picker'] = self.figure.canvas.mpl_connect('pick_event', self.pick)
                self.connects['background_picker'] = self.figure.canvas.mpl_connect('button_release_event', self.unpick)
            elif self.event_type == "hover":
                self.connects['wisp_hover'] = self.figure.canvas.mpl_connect('motion_notify_event', self.hover)
        else:
            logging.warning("Not the right type of artist! "+str(artist))

    def clear(self):
        self.wisp_id = None
        super().clear()

    def refresh(self):
        super().refresh()

        if self.highlight_artist is not None:
            for a in self.highlight_artist:
                a.remove()
                del a
        if self.wisp_id is not None:
            if self.degree == 0:
                vertex_point = self.topomesh.wisp_property('barycenter', 0)[self.wisp_id]

                self.highlight_artist = [self.figure.gca().scatter(vertex_point[0], vertex_point[1],
                                                                   color=self.highlight_color, s=240,
                                                                   alpha=self.highlight_alpha)]

            elif self.degree == 1:
                edge_vertices = list(self.topomesh.borders(1, self.wisp_id))
                edge_points = self.topomesh.wisp_property('barycenter', 0).values(edge_vertices)

                self.highlight_artist = self.figure.gca().plot(edge_points[:, 0], edge_points[:, 1],
                                                               color=self.highlight_color, linewidth=9,
                                                               alpha=self.highlight_alpha)
            elif self.degree == 2:
                face_vertices = list(self.topomesh.borders(2, self.wisp_id, 2))
                face_center = self.topomesh.wisp_property('barycenter', 2)[self.wisp_id]
                face_points = self.topomesh.wisp_property('barycenter', 0).values(face_vertices)
                face_points = face_center + 0.9 * (face_points - face_center)
                self.highlight_artist = [PolyCollection([face_points[:, :2]],
                                                        color=self.highlight_color, linewidth=0,
                                                        alpha=self.highlight_alpha)]
                for a in self.highlight_artist:
                    self.figure.gca().add_collection(a)

            elif self.degree == 3:
                cell_faces = list(self.topomesh.borders(3, self.wisp_id))
                cell_face_vertices = [list(self.topomesh.borders(2, fid, 2)) for fid in cell_faces]
                cell_center = self.topomesh.wisp_property('barycenter', 3)[self.wisp_id]
                cell_face_points = self.topomesh.wisp_property('barycenter', 0).values(cell_face_vertices)
                cell_face_points = cell_center + 0.9 * (cell_face_points - cell_center)
                self.highlight_artist = [PolyCollection([p[:, :2]],
                                                        color=self.highlight_color, linewidth=0,
                                                        alpha=self.highlight_alpha) for p in cell_face_points]
                for a in self.highlight_artist:
                    self.figure.gca().add_collection(a)
        else:
            self.highlight_artist = None

    def pick(self, event):
        self.picked = True

        if self.degree < 3:
            wid = list(self.topomesh.wisps(self.degree))[event.ind[0]]
        else:
            fid = list(self.topomesh.wisps(2))[event.ind[0]]
            wid = list(self.topomesh.regions(2,fid))[0]

        self.wisp_id = wid
        self.refresh()

    def unpick(self, event):
        if not self.picked:
            self.wisp_id = None
            self.refresh()
        self.picked = False

    def hover(self, event):
        if self.artist is not None:
            picked, ind = self.artist.contains(event)
            if picked:
                wid = list(self.topomesh.wisps(self.degree))[ind["ind"][0]]
                self.wisp_id = wid
            else:
                self.wisp_id = None
        self.refresh()