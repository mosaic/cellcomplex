import logging

from PyQt5 import Qt, QtGui, QtWidgets

import numpy as np

import cellcomplex.property_topomesh.app.interactor_styles
from cellcomplex.property_topomesh.app.interactor_styles.cellcomplex_interactor_style import CellComplexWispPickerInteractorStyle

from cellcomplex.property_topomesh.composition import fuse_topomesh_identical_vertices


class CellComplexEdgeTopologyEditorInteractorStyle(CellComplexWispPickerInteractorStyle):

    def __init__(self, topomesh, figure):

        super().__init__(topomesh, figure, degree=1)

        self.add_face_button = QtWidgets.QPushButton("Add Face")

        self.initialize_pane()

    def initialize_pane(self):
        super().initialize_pane()

        self.add_face_button.setEnabled(True)
        self.add_face_button.clicked.connect(self.add_face_to_edge)
        self.editor_layout.addWidget(self.add_face_button)

    def name(self):
        return "Edge Topology Editor"

    def icon(self):
        style_directory = cellcomplex.property_topomesh.app.interactor_styles.__path__[0]
        return QtGui.QIcon(style_directory + "/edge_picker.svg")

    def refresh(self):
        super().refresh()

        if self.wisp_id is not None:
            if self.topomesh.nb_regions(1, self.wisp_id) == 1:
                self.add_face_button.setEnabled(True)
            else:
                self.add_face_button.setEnabled(False)
        else:
            self.add_face_button.setEnabled(False)

    def add_face_to_edge(self):
        if self.wisp_id is not None:
            if self.topomesh.nb_regions(1, self.wisp_id) == 1:
                edge_vertices = list(self.topomesh.borders(1, self.wisp_id))
                edge_center = np.mean(self.topomesh.wisp_property('barycenter', 0).values(edge_vertices), axis=0)

                edge_face = list(self.topomesh.regions(1, self.wisp_id))[0]
                face_opposite_vertices = [v for v in self.topomesh.borders(2, edge_face, 2) if not v in edge_vertices]
                face_opposite_point = np.mean(self.topomesh.wisp_property('barycenter', 0).values(face_opposite_vertices), axis=0)

                fid = self.topomesh.add_wisp(2)
                self.topomesh.link(2, fid, self.wisp_id)
                face_vid = self.topomesh.add_wisp(0)
                self.topomesh.wisp_property('barycenter', 0)[face_vid] = edge_center - (face_opposite_point - edge_center)
                for vid in edge_vertices:
                    eid = self.topomesh.add_wisp(1)
                    self.topomesh.link(1, eid, vid)
                    self.topomesh.link(1, eid, face_vid)
                    self.topomesh.link(2, fid, eid)
                for cid in self.topomesh.regions(2, edge_face):
                    self.topomesh.link(3, cid, fid)

                fuse_topomesh_identical_vertices(self.topomesh)

                self.wisp_id = None
                self.editingFinished.emit()