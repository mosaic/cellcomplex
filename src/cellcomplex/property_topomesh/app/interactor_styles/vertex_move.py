from PyQt5 import Qt, QtGui, QtWidgets

import numpy as np

import cellcomplex.property_topomesh.app.interactor_styles
from cellcomplex.property_topomesh.app.interactor_styles.cellcomplex_interactor_style import CellComplexWispPickerInteractorStyle
from cellcomplex.property_topomesh.composition import fuse_topomesh_identical_vertices


class CellComplexVertexMovePositionInteractorStyle(CellComplexWispPickerInteractorStyle):

    def __init__(self, topomesh, figure):
        super().__init__(topomesh, figure, degree=0)

        self.initialize_pane()

    def name(self):
        return "Vertex Move"

    def icon(self):
        style_directory = cellcomplex.property_topomesh.app.interactor_styles.__path__[0]
        return QtGui.QIcon(style_directory + "/vertex_move.svg")

    def set_artist(self, artist):
        super().set_artist(artist)

        if self.artist is not None:
            self.connects['wisp_drag'] = self.figure.canvas.mpl_connect('motion_notify_event', self.drag)

    def clear(self):
        super().clear()
        self.refresh()

    def refresh(self):
        super().refresh()
        self.figure.canvas.draw()

    def drag(self, event):
        if self.wisp_id is not None:
            self.editing = True
            drag_point = np.array([event.xdata, event.ydata, 0])
            if not np.any(drag_point==None):
                self.topomesh.wisp_property('barycenter',0)[self.wisp_id] = drag_point

                self.editingFinished.emit()
        self.refresh()

    def unpick(self, event):
        super().unpick(event)

        # fuse_topomesh_identical_vertices(self.topomesh, precision=1)
        # self.editingFinished.emit()

        self.wisp_id = None
        self.picked = False
        self.editing = False

