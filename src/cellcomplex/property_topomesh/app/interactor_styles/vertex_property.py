from PyQt5 import Qt, QtGui, QtWidgets

import numpy as np

import cellcomplex.property_topomesh.app.interactor_styles

from cellcomplex.property_topomesh.app.wisp_property_editor import WispPropertyEditor
from cellcomplex.property_topomesh.app.interactor_styles.cellcomplex_interactor_style import CellComplexWispPickerInteractorStyle


class CellComplexVertexPropertyEditorInteractorStyle(CellComplexWispPickerInteractorStyle):

    def __init__(self, topomesh, figure):

        super().__init__(topomesh, figure, degree=0)

        self.vertex_property_label = QtWidgets.QLabel("Vertex Properties")
        self.property_layout = QtWidgets.QFormLayout()
        self.property_pane = QtWidgets.QWidget()

        self.property_widgets = {}
        self.property_types = {}

        self.new_property_button = QtWidgets.QPushButton('New Property')

        self.initialize_pane()

    def initialize_pane(self):
        super().initialize_pane()

        font = QtGui.QFont()
        self.vertex_property_label.setFont(QtGui.QFont(font.defaultFamily(), 16, QtGui.QFont.Bold))
        self.vertex_property_label.setAlignment(Qt.Qt.AlignCenter)
        self.editor_layout.addWidget(self.vertex_property_label)

        self.property_layout.setContentsMargins(0, 0, 0, 0)

        self.property_pane.setSizePolicy(Qt.QSizePolicy.Expanding, Qt.QSizePolicy.Expanding)
        self.property_pane.setLayout(self.property_layout)
        self.editor_layout.addWidget(self.property_pane)

        self.new_property_button.clicked.connect(self.add_new_property)

    def name(self):
        return "Vertex Property"

    def icon(self):
        style_directory = cellcomplex.property_topomesh.app.interactor_styles.__path__[0]
        return QtGui.QIcon(style_directory + "/vertex_property.svg")

    def edit_vertex_property(self):
        names = [n for n in self.property_widgets.keys() if self.property_widgets[n] == self.sender()]
        if len(names) > 0:
            name = names[0]
            property_value = self.sender().value()
            self.topomesh.wisp_property(name, self.degree)[self.wisp_id] = property_value
            self.editingFinished.emit()

    def add_new_property(self):
        if self.wisp_id is not None:

            dialog = QtWidgets.QDialog(self.parent())
            dialog.setWindowModality(Qt.Qt.WindowModal)

            new_property_layout = QtWidgets.QGridLayout()
            new_property_layout.setContentsMargins(10, 10, 10, 10)

            name_label = QtWidgets.QLabel("New property Name")
            new_property_layout.addWidget(name_label, 0, 0, 1, 1)

            name_edit = QtWidgets.QLineEdit()
            new_property_layout.addWidget(name_edit, 0, 1, 1, 1)

            value_label = QtWidgets.QLabel("New property Value")
            new_property_layout.addWidget(value_label, 1, 0, 1, 1)

            value_edit = QtWidgets.QLineEdit()
            new_property_layout.addWidget(value_edit, 1, 1, 1, 1)

            ok_button = QtWidgets.QPushButton("Ok")
            ok_button.clicked.connect(dialog.accept)
            new_property_layout.addWidget(ok_button, 2, 0, 1, 2)

            dialog.setLayout(new_property_layout)

            accept = dialog.exec_()
            if accept:
                property_name = name_edit.text()
                if property_name not in self.topomesh.wisp_property_names(1):
                    property_value = value_edit.text()

                    if len(property_name) > 0 and len(property_value) > 0:
                        try:
                            property_as_int = int(property_value)
                        except ValueError:
                            try:
                                property_as_float = float(property_value)
                            except ValueError:
                                pass
                            else:
                                property_value = property_as_float
                        else:
                            property_value = property_as_int

                        property_dict = {f: np.nan for f in self.topomesh.wisps(self.degree)}
                        property_dict[self.wisp_id] = property_value
                        self.topomesh.update_wisp_property(property_name, self.degree, property_dict)

                        self.display_properties()

    def clear_properties(self):
        for r in reversed(range(self.property_layout.count())):
            widget = self.property_layout.itemAt(r).widget()
            if widget != self.new_property_button:
                widget.deleteLater()
        self.new_property_button.setVisible(False)
        self.property_widgets = {}
        self.property_types = {}
        # self.vertex_property_label.setText("Vertex Properties")
        self.vertex_property_label.setVisible(False)

    def display_properties(self):
        self.clear_properties()

        for name in self.topomesh.wisp_property_names(self.degree):
            value = [v for v in self.topomesh.wisp_property(name, self.degree).values()
                     if v is not None][0]
            if isinstance(value, bool):
                self.property_types[name] = 'bool'
            elif isinstance(value, int):
                self.property_types[name] = 'int'
            elif isinstance(value, float):
                self.property_types[name] = 'float'
            else:
                self.property_types[name] = 'str'

        valid_properties = [n for n in self.topomesh.wisp_property_names(self.degree)
                            if self.topomesh.wisp_property(n, self.degree).values().ndim == 1]

        for name in valid_properties:
            if self.wisp_id in self.topomesh.wisp_property(name, self.degree).to_dict().keys():
                value = self.topomesh.wisp_property(name, self.degree).to_dict()[self.wisp_id]
            else:
                value = None
            self.property_widgets[name] = WispPropertyEditor(value, self.property_types[name])
            self.property_widgets[name].editingFinished.connect(self.edit_vertex_property)

        for name in valid_properties:
            if name in self.property_widgets:
                self.property_layout.addRow(name, self.property_widgets[name])

        self.property_layout.addRow("", self.new_property_button)
        self.new_property_button.setVisible(True)

        self.property_layout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.property_layout.setFormAlignment(Qt.Qt.AlignHCenter | Qt.Qt.AlignTop)

        self.vertex_property_label.setText("Vertex " + str(self.wisp_id) + " Properties")
        self.vertex_property_label.setVisible(True)

    def refresh(self):
        super().refresh()

        if self.wisp_id is not None:
            self.display_properties()
        else:
            self.clear_properties()

    def unpick(self, event):
        super().unpick(event)
        self.editing = False