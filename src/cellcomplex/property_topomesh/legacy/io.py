import numpy as np

import pickle

from cellcomplex.property_topomesh import PropertyTopomesh

from cellcomplex.utils import array_dict


def save_property_topomesh(topomesh, path, cells_to_save=None, properties_to_save=dict([(0,['barycenter']),(1,[]),(2,[]),(3,[])]),**kwargs):
    if cells_to_save is None:
        cells_to_save = np.array(list(topomesh.wisps(3)))
    triangles_to_save = np.array(np.unique(np.concatenate([np.array(list(topomesh.borders(3,c))) for c in cells_to_save])),int)
    edges_to_save = np.array(np.unique(np.concatenate([np.array(list(topomesh.borders(2,t))) for t in triangles_to_save])),int)
    vertices_to_save = np.array(np.unique(np.concatenate([np.array(list(topomesh.borders(1,e))) for e in edges_to_save])),int)

    original_pids = kwargs.get('original_pids',False)
    original_eids = kwargs.get('original_eids',False)
    original_fids = kwargs.get('original_fids',False)
    original_cids = kwargs.get('original_cids',True)

    sub_topomesh = PropertyTopomesh(3)

    vertices_to_pids = {}
    for v in vertices_to_save:
        if original_pids:
            pid = sub_topomesh.add_wisp(0,v)
        else:
            pid = sub_topomesh.add_wisp(0)
        vertices_to_pids[v] = pid

    edges_to_eids = {}
    for e in edges_to_save:
        if original_eids:
            eid = sub_topomesh.add_wisp(1,e)
        else:
            eid = sub_topomesh.add_wisp(1)
        edges_to_eids[e] = eid
        for v in topomesh.borders(1,e):
            sub_topomesh.link(1,eid,vertices_to_pids[v])

    triangles_to_fids = {}
    for t in triangles_to_save:
        if original_fids:
            fid = sub_topomesh.add_wisp(2,t)
        else:
            fid = sub_topomesh.add_wisp(2)
        triangles_to_fids[t] = fid
        for e in topomesh.borders(2,t):
            sub_topomesh.link(2,fid,edges_to_eids[e])

    cells_to_cids = {}
    for c in cells_to_save:
        if original_cids:
            cid = sub_topomesh.add_wisp(3,c)
        else:
            cid = sub_topomesh.add_wisp(3,c)
        cells_to_cids[c] = cid
        for t in topomesh.borders(3,c):
            sub_topomesh.link(3,cid,triangles_to_fids[t])

    wisps_to_save = {}
    wisps_to_save[0] = vertices_to_save
    wisps_to_save[1] = edges_to_save
    wisps_to_save[2] = triangles_to_save
    wisps_to_save[3] = cells_to_save

    wisps_to_wids = {}
    wisps_to_wids[0] = vertices_to_pids
    wisps_to_wids[1] = edges_to_eids
    wisps_to_wids[2] = triangles_to_fids
    wisps_to_wids[3] = cells_to_cids

    if not 0 in properties_to_save.keys():
        properties_to_save[0] = []
    if not 'barycenter' in properties_to_save[0]:
        properties_to_save[0].append('barycenter')

    for degree in properties_to_save.keys():
        for property_name in properties_to_save[degree]:
            print("Property ",property_name,'(',degree,')')
            if topomesh.has_wisp_property(property_name,degree=degree,is_computed=True):
                wids_to_save = array_dict(wisps_to_wids[degree]).values(wisps_to_save[degree])
                sub_topomesh.update_wisp_property(property_name,degree,array_dict(topomesh.wisp_property(property_name,degree).values(wisps_to_save[degree]),wids_to_save))

    pickle.dump(sub_topomesh,open(path,"wb"))


def save_tissue_property_topomesh(topomesh,tissue_filename='vplants.zip'):
    from openalea.celltissue import Tissue, TissueDB, topen, Config, ConfigItem, ConfigFormat

    tissue = Tissue()

    ptyp = tissue.add_type("point")
    etyp = tissue.add_type("edge")
    ftyp = tissue.add_type("face")
    ctyp = tissue.add_type("cell")

    mesh_id = tissue.add_relation("mesh",(ptyp,etyp,ftyp,ctyp))
    mesh = tissue.relation(mesh_id)

    cells = {}
    for c in topomesh.wisps(3):
        if len(list(topomesh.borders(3,c)))>0:
            cid = tissue.add_element(ctyp)
            mesh.add_wisp(3,cid)
            cells[c] = cid

    faces = {}
    for t in topomesh.wisps(2):
        fid = tissue.add_element(ftyp)
        mesh.add_wisp(2,fid)
        faces[t] = fid

    edges = {}
    for e in topomesh.wisps(1):
        eid = tissue.add_element(etyp)
        mesh.add_wisp(1,eid)
        edges[e] = eid

    points = {}
    for v in topomesh.wisps(0):
        pid = tissue.add_element(ptyp)
        mesh.add_wisp(0,pid)
        points[v] = pid

    for e in topomesh.wisps(1):
        for v in topomesh.borders(1,e):
            mesh.link(1,edges[e],points[v])

    for t in topomesh.wisps(2):
        for e in topomesh.borders(2,t):
            mesh.link(2,faces[t],edges[e])

    for c in topomesh.wisps(3):
        if len(list(topomesh.borders(3,c)))>0:
            for t in topomesh.borders(3,c):
                mesh.link(3,cells[c],faces[t])

    positions = {}
    for v in topomesh.wisps(0):
        positions[points[v]] = tuple(topomesh.wisp_property('barycenter',0)[v])

    cfg = ConfigFormat("config")
    cfg.add_section("topology")
    cfg.add_item(ConfigItem("cell",ctyp) )
    cfg.add_item(ConfigItem("face",ftyp) )
    cfg.add_item(ConfigItem("edge",etyp) )
    cfg.add_item(ConfigItem("point",ptyp) )
    cfg.add_item(ConfigItem("mesh_id",mesh_id))

    tissue_db = TissueDB()
    tissue_db.set_tissue(tissue)
    tissue_db.set_property("position",positions)
    tissue_db.set_description("position","position of points")
    tissue_db.set_config("config",cfg.config())

    tissue_db.write(tissue_filename)

