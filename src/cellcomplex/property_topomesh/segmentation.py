import logging
from copy import deepcopy
from time import time as current_time
from tqdm import tqdm

import numpy as np
import scipy.ndimage as nd

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.extraction import sub_topomesh
from cellcomplex.property_topomesh.filters import topomesh_vertex_property_local_optimality


def topomesh_vertex_property_watershed_segmentation(topomesh, property_name, minima=True, gaussian_sigma=0., labelchoice='most', weighting='area'):
    """

    Parameters
    ----------
    topomesh
    property_name
    minima
    gaussian_sigma
    labelchoice
    weighting

    Returns
    -------

    """

    assert topomesh.has_wisp_property(property_name, 0, is_computed=True)

    compute_topomesh_property(topomesh, 'neighbors', 0)
    if labelchoice == 'most' and weighting == 'area':
        compute_topomesh_property(topomesh, 'area', 2)
        compute_topomesh_vertex_property_from_faces(topomesh, 'area', weighting='uniform', neighborhood=1)

    start_time = current_time()
    property_minimum_vertices, property_maximum_vertices = topomesh_vertex_property_local_optimality(topomesh, property_name, gaussian_sigma=gaussian_sigma, exclude_contour=False)

    vertex_regions = {v: np.nan for v in topomesh.wisps(0)}
    if minima:
        region_labels = np.arange(len(property_minimum_vertices))
        vertex_regions.update(dict(zip(property_minimum_vertices, region_labels)))
    else:
        region_labels = np.arange(len(property_maximum_vertices))
        vertex_regions.update(dict(zip(property_maximum_vertices, region_labels)))
    topomesh.update_wisp_property(f"{property_name}_seeds", 0, vertex_regions)
    logging.info(f"--> Found {len(region_labels)} seeds for watershed [{current_time()-start_time}s]")

    start_time = current_time()
    vertices = np.array(list(topomesh.wisps(0)))
    vertex_properties = topomesh.wisp_property(f'{property_name}_filtered', 0).values(vertices)
    vertex_neighbors = topomesh.wisp_property('neighbors', 0).values(vertices)

    if minima:
        property_sorting = np.argsort(vertex_properties)
    else:
        property_sorting = np.argsort(-vertex_properties)
    sorted_vertices = vertices[property_sorting]
    sorted_vertex_neighbors = vertex_neighbors[property_sorting]

    for v, v_n in tqdm(zip(sorted_vertices, sorted_vertex_neighbors), total=len(sorted_vertices), unit='vertex'):
        if np.isnan(vertex_regions[v]):
            neighbor_regions = [vertex_regions[n] for n in v_n]
            if labelchoice == 'min':
                vertex_regions[v] = np.nanmin(neighbor_regions)
            else:
                region_neighbors = v_n[np.logical_not(np.isnan(neighbor_regions))]
                region_neighbor_regions = [vertex_regions[n] for n in region_neighbors]
                if len(region_neighbors) == 0:
                    vertex_regions[v] = np.nan
                elif len(region_neighbors) == 1:
                    vertex_regions[v] = region_neighbor_regions[0]
                else:
                    if labelchoice == 'first':
                        region_neighbor_properties = topomesh.wisp_property(f'{property_name}_filtered', 0).values(region_neighbors)
                        if minima:
                            assert np.all(region_neighbor_properties <= topomesh.wisp_property(f'{property_name}_filtered', 0)[v])
                            vertex_regions[v] = region_neighbor_regions[np.argmax(region_neighbor_properties)]
                        else:
                            assert np.all(region_neighbor_properties >= topomesh.wisp_property(f'{property_name}_filtered', 0)[v])
                            vertex_regions[v] = region_neighbor_regions[np.argmin(region_neighbor_properties)]
                    elif labelchoice == 'most':
                        candidate_regions = np.unique(region_neighbor_regions)
                        if weighting == 'area':
                            region_neighbor_weights = topomesh.wisp_property('area', 0).values(region_neighbors)
                        elif weighting == 'uniform':
                            region_neighbor_weights = np.ones_like(region_neighbor_regions)
                        candidate_region_weights = nd.sum(region_neighbor_weights, region_neighbor_regions, index=candidate_regions)
                        vertex_regions[v] = candidate_regions[np.argmax(candidate_region_weights)]

    logging.info(f"--> Watershed : {np.sum(np.isnan(list(vertex_regions.values())))} unlabelled vertice [{current_time()-start_time}s]")

    start_time = current_time()
    topomesh.update_wisp_property(f"{property_name}_regions", 0, vertex_regions)
    region_vertices = {r: [v for v in topomesh.wisps(0) if vertex_regions[v]==r] for r in region_labels}
    logging.info(f"--> Listing region vertices [{current_time()-start_time}s]")


    return region_vertices


def topomesh_merge_vertex_watershed_regions(topomesh, property_name, minima=True, depth_threshold=0.01, engulfed_fraction=0.95):
    """

    Parameters
    ----------
    topomesh
    property_name
    minima
    depth_threshold
    engulfed_fraction

    Returns
    -------

    """

    compute_topomesh_property(topomesh, 'contour', 0)

    vertex_regions = topomesh.wisp_property(f'{property_name}_regions', 0).to_dict()
    region_labels = np.unique(list(vertex_regions.values()))

    region_vertices = {r: [v for v in topomesh.wisps(0) if vertex_regions[v]==r] for r in region_labels}
    region_sizes = {r: len(region_vertices[r]) for r in region_vertices.keys()}

    merge = True

    while merge:
        region_neighbors = {}
        region_neighbor_vertices = {}
        region_neighbor_vertex_regions = {}
        for r in region_vertices.keys():
            r_vertices = region_vertices[r]
            r_neighbor_vertices = np.unique(np.concatenate(topomesh.wisp_property('neighbors', 0).values(r_vertices)))
            r_neighbor_regions = topomesh.wisp_property(f'{property_name}_regions', 0).values(r_neighbor_vertices)
            r_neighbor_vertices = r_neighbor_vertices[r_neighbor_regions != r]
            r_neighbor_vertex_regions = r_neighbor_regions[r_neighbor_regions != r]
            r_contour_vertices = np.array(r_vertices)[topomesh.wisp_property('contour', 0).values(r_vertices).astype(bool)]

            region_neighbors[r] = np.unique(r_neighbor_vertex_regions)

            r_neighbor_vertices = np.array(list(r_neighbor_vertices) + list(r_contour_vertices))
            r_neighbor_vertex_regions = np.array(list(r_neighbor_vertex_regions) + [r for v in r_contour_vertices])
            region_neighbor_vertices[r] = r_neighbor_vertices
            region_neighbor_vertex_regions[r] = r_neighbor_vertex_regions

        regions_to_merge = []
        merged_region_size = []
        for r in region_vertices.keys():
            r_vertices = region_vertices[r]

            # Merge, depth
            if minima:
                r_property_opt = topomesh.wisp_property(f'{property_name}_filtered', 0).values(r_vertices).min()
            else:
                r_property_opt = topomesh.wisp_property(f'{property_name}_filtered', 0).values(r_vertices).max()
            for n_r in region_neighbors[r]:
                n_r_vertices = region_neighbor_vertices[r][region_neighbor_vertex_regions[r] == n_r]

                if minima:
                    n_r_property_opt = topomesh.wisp_property(f'{property_name}_filtered', 0).values(n_r_vertices).min()
                else:
                    n_r_property_opt = topomesh.wisp_property(f'{property_name}_filtered', 0).values(n_r_vertices).max()
                if np.abs(r_property_opt - n_r_property_opt) < depth_threshold:
                    if region_sizes[r] >= region_sizes[n_r]:
                        regions_to_merge += [(n_r, r)]
                        merged_region_size += [region_sizes[n_r]]
                    else:
                        regions_to_merge += [(r, n_r)]
                        merged_region_size += [region_sizes[r]]

            # Merge, engulfed
            region_neighbor_count = nd.sum(np.ones_like(region_neighbor_vertex_regions[r]),
                                           region_neighbor_vertex_regions[r],
                                           index=region_neighbors[r])
            for n_r, n_c in zip(region_neighbors[r], region_neighbor_count):
                if n_c/len(region_neighbor_vertices[r]) > engulfed_fraction:
                    if region_sizes[r] >= region_sizes[n_r]:
                        regions_to_merge += [(n_r, r)]
                        merged_region_size += [region_sizes[n_r]]
                    else:
                        regions_to_merge += [(r, n_r)]
                        merged_region_size += [region_sizes[r]]

        if len(regions_to_merge) > 0:
            region_to_merge, region_to_keep = regions_to_merge[np.argmin(merged_region_size)]
            region_vertices[region_to_keep] += region_vertices[region_to_merge]
            del region_vertices[region_to_merge]
            region_sizes[region_to_keep] += region_sizes[region_to_merge]
            del region_sizes[region_to_merge]
            logging.info(f"  --> Merged {region_to_merge} into {region_to_keep}!")
            merge = True
        else:
            merge = False

        vertex_regions = {v: r for r in region_vertices for v in region_vertices[r]}
        topomesh.update_wisp_property(f'{property_name}_regions', 0, vertex_regions)

    return region_vertices
