# -*- coding: utf-8 -*-
# -*- python -*-
#
#       PropertyTopomesh
#
#       Copyright 2014-2016 INRIA - CIRAD - INRA
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the LGPL License.
#       See accompanying LICENSE file
#
#       OpenaleaLab Website : http://virtualplants.github.io/
#
###############################################################################

import logging

import numpy as np

from copy import deepcopy
from time import time

from cellcomplex.utils import array_dict
from cellcomplex.utils import IdDict

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces, is_triangular
from cellcomplex.property_topomesh.composition import append_topomesh

from cellcomplex.property_topomesh.utils.geometry_tools import triangle_geometric_features
from cellcomplex.property_topomesh.utils.delaunay_tools import delaunay_triangulation


def property_filtering_sub_topomesh(input_topomesh, property_name, degree, filter_range, clean=True):
    """
    """

    assert np.all([w in input_topomesh.wisp_property(property_name, degree).keys() for w in input_topomesh.wisps(degree)])

    element_property = input_topomesh.wisp_property(property_name, degree)

    elements_to_keep = [w for w in input_topomesh.wisps(degree) if (element_property[w]>=filter_range[0]) and (element_property[w]<=filter_range[1])]

    return sub_topomesh(input_topomesh, degree, elements_to_keep, clean=clean)


def sub_topomesh(input_topomesh, degree, elements_to_keep, clean=True):
    """


    Parameters
    ----------
    input_topomesh
    degree
    elements_to_keep
    clean

    Returns
    -------

    """

    topomesh = deepcopy(input_topomesh)

    elements_to_remove = [w for w in topomesh.wisps(degree) if not w in elements_to_keep]
    for w in elements_to_remove:
        topomesh.remove_wisp(degree,w)

    if degree<1 and input_topomesh.nb_wisps(1)>0:
        edges_to_remove = [e for e in topomesh.wisps(1) if topomesh.nb_borders(1,e) < 2]
        for e in edges_to_remove:
            topomesh.remove_wisp(1,e)

    if degree<2 and input_topomesh.nb_wisps(2)>0:
        faces_to_remove = [f for f in topomesh.wisps(2) if topomesh.nb_borders(2,f) < 3]
        for f in faces_to_remove:
            topomesh.remove_wisp(2,f)

    if clean:
        topomesh = clean_topomesh(topomesh, clean_properties=True, degree=np.max([d for d in range(degree+1) if input_topomesh.nb_wisps(d)>0]))
    return topomesh


def epidermis_topomesh(topomesh,cells=None):

    compute_topomesh_property(topomesh,'epidermis',3)
    compute_topomesh_property(topomesh,'epidermis',2)
    compute_topomesh_property(topomesh,'epidermis',1)
    compute_topomesh_property(topomesh,'epidermis',0)

    if cells is None:
        epidermis_topomesh = deepcopy(topomesh)
    else:
        faces = np.array(np.unique(np.concatenate([np.array(list(topomesh.borders(3,c))) for c in cells])),int)
        edges = np.array(np.unique(np.concatenate([np.array(list(topomesh.borders(2,t))) for t in faces])),int)
        vertices = np.array(np.unique(np.concatenate([np.array(list(topomesh.borders(1,e))) for e in edges])),int)
        epidermis_topomesh = PropertyTopomesh(3)
        vertices_to_pids = {}
        for v in vertices:
            pid = epidermis_topomesh.add_wisp(0,v)
            vertices_to_pids[v] = pid
        edges_to_eids = {}
        for e in edges:
            eid = epidermis_topomesh.add_wisp(1,e)
            edges_to_eids[e] = eid
            for v in topomesh.borders(1,e):
                epidermis_topomesh.link(1,eid,vertices_to_pids[v])
        faces_to_fids = {}
        for f in faces:
            fid = epidermis_topomesh.add_wisp(2,f)
            faces_to_fids[f] = fid
            for e in topomesh.borders(2,f):
                epidermis_topomesh.link(2,fid,edges_to_eids[e])
        for c in cells:
            cid = epidermis_topomesh.add_wisp(3,c)
            for f in topomesh.borders(3,c):
                epidermis_topomesh.link(3,cid,faces_to_fids[f])

    vertices_to_remove = []
    for v in epidermis_topomesh.wisps(0):
        if not topomesh.wisp_property('epidermis',0)[v]:
            vertices_to_remove.append(v)
    for v in vertices_to_remove:
        epidermis_topomesh.remove_wisp(0,v)
    edges_to_remove = []
    for e in epidermis_topomesh.wisps(1):
        if not topomesh.wisp_property('epidermis',1)[e]:
            edges_to_remove.append(e)
    for e in edges_to_remove:
        epidermis_topomesh.remove_wisp(1,e)
    faces_to_remove = []
    for f in epidermis_topomesh.wisps(2):
        if not topomesh.wisp_property('epidermis',2)[f]:
            faces_to_remove.append(f)
    for f in faces_to_remove:
        epidermis_topomesh.remove_wisp(2,f)
    cells_to_remove = []
    for c in epidermis_topomesh.wisps(3):
        if not topomesh.wisp_property('epidermis',3)[c]:
            cells_to_remove.append(c)
    for c in cells_to_remove:
        epidermis_topomesh.remove_wisp(3,c)
    epidermis_topomesh.update_wisp_property('barycenter',0,topomesh.wisp_property('barycenter',0).values(list(epidermis_topomesh.wisps(0))),keys=np.array(list(epidermis_topomesh.wisps(0))))

    return epidermis_topomesh


def cut_surface_topomesh(input_topomesh, z_cut=0, z_offset=None, below=True):

    topomesh = deepcopy(input_topomesh)

    compute_topomesh_property(topomesh,'vertices',2)

    if below:
        triangle_below = array_dict(np.all(topomesh.wisp_property('barycenter',0).values(topomesh.wisp_property('vertices',2).values())[...,2] <= z_cut,axis=1),list(topomesh.wisps(2)))
    else:
        triangle_below = array_dict(np.all(topomesh.wisp_property('barycenter',0).values(topomesh.wisp_property('vertices',2).values())[...,2] >= z_cut,axis=1),list(topomesh.wisps(2)))
    topomesh.update_wisp_property('below',2,triangle_below)

    triangles_to_remove = [t for t in topomesh.wisps(2) if triangle_below[t]]
    for t in triangles_to_remove:
        topomesh.remove_wisp(2,t)

    topomesh = clean_topomesh(topomesh)

    compute_topomesh_property(topomesh,'faces',1)
    compute_topomesh_property(topomesh,'vertices',1)
    compute_topomesh_property(topomesh,'length',1)

    topomesh.update_wisp_property('boundary',1,array_dict((np.array(list(map(len,topomesh.wisp_property('faces',1).values())))==1).astype(int),list(topomesh.wisps(1))))

    boundary_edges = np.array(list(topomesh.wisps(1)))[topomesh.wisp_property('boundary',1).values()==1]
    boundary_vertices = np.unique(topomesh.wisp_property('vertices',1).values(boundary_edges))

    if z_offset is None:
        # z_offset = topomesh.wisp_property('barycenter',0).values()[:,2].std()/8.
        z_offset = np.percentile(topomesh.wisp_property('length',1).values(),10)
    iso_z_positions = np.array([np.concatenate([topomesh.wisp_property('barycenter',0)[v][:2],[z_cut+(1-2*below)*z_offset]]) if v in boundary_vertices else  topomesh.wisp_property('barycenter',0)[v] for v in topomesh.wisps(0)])
    topomesh.update_wisp_property('barycenter',0,array_dict(iso_z_positions,list(topomesh.wisps(0))))

    clean_topomesh_properties(topomesh)

    return topomesh


def clean_topomesh(input_topomesh, clean_properties=False, degree=2):

    topomesh = deepcopy(input_topomesh)

    cells_to_remove = [w for w in topomesh.wisps(3) if topomesh.nb_borders(3,w)==0]
    for w in cells_to_remove:
        topomesh.remove_wisp(3,w)

    if degree > 2:
        triangles_to_remove = [w for w in topomesh.wisps(2) if topomesh.nb_regions(2,w)==0]
        for w in triangles_to_remove:
            topomesh.remove_wisp(2,w)

    if degree > 1:
        edges_to_remove = [w for w in topomesh.wisps(1) if topomesh.nb_regions(1,w)==0]
        for w in edges_to_remove:
            topomesh.remove_wisp(1,w)

    if degree > 0:
        vertices_to_remove = [w for w in topomesh.wisps(0) if topomesh.nb_regions(0,w)==0]
        for w in vertices_to_remove:
            topomesh.remove_wisp(0,w)

    if clean_properties:
        clean_topomesh_properties(topomesh)

    return topomesh


def topomesh_element_coloring(topomesh, degree=2, color_property_name='color'):
    """Compute an optimal coloring on the adjacency graph of a given degree.

    The function assigns a color property to each element of a given degree
    by performing a LexBFS-Color algorithm on the adjacency graph of the
    elements of the considered degree.

    Parameters
    ----------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The structure on which to compute the coloring.
    degree : int
        The degree of the elements on which to compute the property.
    color_property_name : str
        The name to give to the color property

    Returns
    -------
        None

    Note
    -------
    The PropertyTopomesh passed as argument is updated.

    """
    compute_topomesh_property(topomesh, 'neighbors', degree)

    lex_bfs_ordered_elements = []
    element_tags = {w:[] for w in topomesh.wisps(degree)}
    while len(lex_bfs_ordered_elements) < topomesh.nb_wisps(degree):
        available_elements = [w for w in topomesh.wisps(degree) if w not in lex_bfs_ordered_elements]
        element_labels = ["".join([chr(t) for t in element_tags[w]]) for w in available_elements]
        max_tag_element = available_elements[np.argmax(element_labels)]
        lex_bfs_ordered_elements.append(max_tag_element)
        element_neighbors = topomesh.wisp_property('neighbors', degree)[max_tag_element]
        for n in element_neighbors:
            if not n in lex_bfs_ordered_elements:
                element_tags[n] += [topomesh.nb_wisps(degree) - len(lex_bfs_ordered_elements)]

    element_colors = {}
    colors = np.arange(10)
    
    for w in lex_bfs_ordered_elements:
        element_neighbors = topomesh.wisp_property('neighbors', degree)[w]
        element_neighbor_colors = [element_colors[n] for n in element_neighbors if n in element_colors]
        available_colors = list(set(colors).difference(set(element_neighbor_colors)))
        element_colors[w] = np.min(available_colors)
    element_colors = {w: element_colors[w] for w in topomesh.wisps(degree)}

    topomesh.update_wisp_property(color_property_name, degree, element_colors)


def extruded_2d_topomesh(input_topomesh, thickness=None, direction=None):
    """Make a 3D mesh from a 2D mesh by thickening faces in a given direction.

    The function computes a 3D PropertyTopomesh in which the cells are the
    faces of the original 2D mesh thickened with a given height.

    Parameters
    ----------
    input_topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The 2D mesh to extrude.
    thickness : float
        The thickness of the resulting cells in the 3D mesh.
    direction : list or np.ndarray
        The direction in which to extrude (vertex normals by default)

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The extruded 3D mesh.
    """

    topomesh = deepcopy(input_topomesh)

    if thickness is None:
        compute_topomesh_property(topomesh,'length',1)
        thickness = np.nanmean(topomesh.wisp_property('length',1).values())

    if direction is None:
        if not topomesh.has_wisp_property('normal', 2, is_computed=True):
            compute_topomesh_property(topomesh,'normal',2,normal_method='orientation')
        compute_topomesh_vertex_property_from_faces(topomesh,'normal',neighborhood=3,adjacency_sigma=1.2)
    compute_topomesh_property(topomesh,'cells',1)

    bottom_vertex = {}
    new_positions = topomesh.wisp_property('barycenter',0).to_dict()

    for v in input_topomesh.wisps(0):
        bottom_vertex[v] = topomesh.add_wisp(0)
        if direction is None:
            new_positions[bottom_vertex[v]] = new_positions[v] - thickness*topomesh.wisp_property('normal',0)[v]
        else:
            new_positions[bottom_vertex[v]] = new_positions[v] - thickness*direction
    topomesh.update_wisp_property('barycenter',0,new_positions)

    bottom_edge = {}
    for e in input_topomesh.wisps(1):
        side_edges = []
        for v in topomesh.borders(1,e):
            s = topomesh.add_wisp(1)
            topomesh.link(1,s,v)
            topomesh.link(1,s,bottom_vertex[v])
            side_edges += [s]
        bottom_edge[e] = topomesh.add_wisp(1)
        for v in topomesh.borders(1, e):
            topomesh.link(1, bottom_edge[e], bottom_vertex[v])

        side_face = topomesh.add_wisp(2)
        topomesh.link(2,side_face,e)
        for s in side_edges:
            topomesh.link(2,side_face,s)
        topomesh.link(2,side_face,bottom_edge[e])

        for c in topomesh.wisp_property('cells',1)[e]:
            topomesh.link(3,c,side_face)

    for f in input_topomesh.wisps(2):
        bottom_face = topomesh.add_wisp(2)
        for e in topomesh.borders(2,f):
            topomesh.link(2,bottom_face,bottom_edge[e])
        for c in topomesh.regions(2,f):
            topomesh.link(3,c,bottom_face)

    return topomesh


def staggered_extruded_2d_topomesh(input_topomesh, thickness=None, direction=None):
    """Make a 3D mesh from a 2D mesh with staggered adjacent cells.

    The function computes a 3D PropertyTopomesh in which the cells are the
    faces of the original 2D mesh thickened with a given height. Cells are
    staggered in such a way that adjacent cells have different levels.

    Parameters
    ----------
    input_topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The 2D mesh to extrude.
    thickness : float
        The thickness of the resulting cells in the 3D mesh.
    direction : list or np.ndarray
        The direction in which to extrude (vertex normals by default)

    Returns
    -------
    topomesh : :class:`cellcomplex.property_topomesh.PropertyTopomesh`
        The extruded 3D mesh.

    """

    topomesh = contiguous_wisps_topomesh(input_topomesh)

    if thickness is None:
        compute_topomesh_property(topomesh,'length',1)
        thickness = np.nanmean(topomesh.wisp_property('length',1).values())

    if direction is None:
        if not topomesh.has_wisp_property('normal', 0, is_computed=True):
            direction = np.array([0, 0, 1], float)

    topomesh_element_coloring(topomesh, 2, 'color')

    n_colors = len(np.unique(topomesh.wisp_property('color', 2).values()))
    stack_height = thickness/n_colors

    stacked_topomesh = deepcopy(topomesh)
    for h in range(1, 2*n_colors):
        stack_topomesh = deepcopy(topomesh)
        positions = stack_topomesh.wisp_property('barycenter', 0)
        if direction is None:
            stack_positions = {v: p+ h*stack_height*topomesh.wisp_property('normal',0)[v] for v,p in positions.items()}
        else:
            stack_positions = {v: p+ h*stack_height*direction for v,p in positions.items()}
        stack_topomesh.update_wisp_property('barycenter', 0, stack_positions)
        append_topomesh(stacked_topomesh, stack_topomesh)
    for c in list(stacked_topomesh.wisps(3)):
        stacked_topomesh.remove_wisp(3, c)

    vertex_stacked_edges = {}
    edge_stacked_faces = {}
    for h in range(1, 2*n_colors):
        vertex_stacked_edges[h] = {}
        for v in topomesh.wisps(0):
            e = stacked_topomesh.add_wisp(1)
            vertex_stacked_edges[h][v] = e
            stacked_topomesh.link(1, e, v+h*topomesh.nb_wisps(0))
            stacked_topomesh.link(1, e, v+(h-1)*topomesh.nb_wisps(0))

        edge_stacked_faces[h] = {}
        for e in topomesh.wisps(1):
            f = stacked_topomesh.add_wisp(2)
            edge_stacked_faces[h][e] = f
            stacked_topomesh.link(2, f, e+h*topomesh.nb_wisps(1))
            stacked_topomesh.link(2, f, e+(h-1)*topomesh.nb_wisps(1))
            for v in topomesh.borders(1, e):
                stacked_topomesh.link(2, f, vertex_stacked_edges[h][v])

    stacked_face_cells = {}
    for f, col in topomesh.wisp_property('color', 2).items():
        stacked_topomesh.add_wisp(3, f)
        heights_to_keep = [col, (col+n_colors)]
        for h in range(2*n_colors):
            if h in heights_to_keep:
                stacked_topomesh.link(3, f, f + h*topomesh.nb_wisps(2))
            else:
                stacked_topomesh.remove_wisp(2, f + h*topomesh.nb_wisps(2))

    for e in topomesh.wisps(1):
        edge_faces = list(topomesh.regions(1, e))
        edge_colors = topomesh.wisp_property('color', 2).values(edge_faces)
        heights_to_keep = np.unique([[col+i+1 for i in range(n_colors)] for col in edge_colors])
        for h in range(1, 2*n_colors):
            if h in heights_to_keep:
                for f, col in zip(edge_faces, edge_colors):
                    if h in [col+i+1 for i in range(n_colors)]:
                        stacked_topomesh.link(3, f, edge_stacked_faces[h][e])
            else:
                stacked_topomesh.remove_wisp(2, edge_stacked_faces[h][e])

    edges_to_remove = [e for e in stacked_topomesh.wisps(1) if stacked_topomesh.nb_regions(1,e) < 1]
    for e in edges_to_remove:
        stacked_topomesh.remove_wisp(1, e)

    vertices_to_remove = [v for v in stacked_topomesh.wisps(0) if stacked_topomesh.nb_regions(0,v) < 1]
    for v in vertices_to_remove:
        stacked_topomesh.remove_wisp(0, v)

    clean_topomesh_properties(stacked_topomesh)

    stacked_topomesh.update_wisp_property('color', 3, topomesh.wisp_property('color', 2).to_dict())

    return stacked_topomesh


def contiguous_wisps_topomesh(input_topomesh):

    topomesh = deepcopy(input_topomesh)

    wisp_conversion = {}
    for degree in range(topomesh.degree()+1):
        old_wisps = np.array(list(topomesh.wisps(degree)))
        new_wisps = np.arange(topomesh.nb_wisps(degree))
        wisp_conversion[degree] = array_dict(dict(zip(old_wisps, new_wisps)))

    for degree in range(topomesh.degree()+1):
        old_wisps = np.array(list(topomesh.wisps(degree)))
        new_wisps = np.arange(topomesh.nb_wisps(degree))
        if degree>0:
            new_borders = dict(zip(
                new_wisps,
                [list(wisp_conversion[degree-1].values(topomesh._borders[degree][o_w])) for o_w in old_wisps]
            ))
            topomesh._borders[degree] = IdDict(new_borders)
        if degree<topomesh.degree():
            new_regions = dict(zip(
                new_wisps,
                [list(wisp_conversion[degree+1].values(topomesh._regions[degree][o_w])) for o_w in old_wisps]
            ))
            if degree == 0:
                topomesh._regions[degree] = IdDict(new_regions)
            else:
                topomesh._regions[degree] = new_regions

        for property_name in topomesh._wisp_properties[degree].keys():
            if all([w in topomesh._wisp_properties[degree][property_name].keys() for w in old_wisps]):
                topomesh._wisp_properties[degree][property_name] = array_dict(dict(zip(
                    new_wisps,
                    topomesh._wisp_properties[degree][property_name].values(old_wisps)
                )))

    return topomesh


def clean_topomesh_properties(topomesh):
    for degree in range(topomesh.degree()+1):
        property_names = list(topomesh.wisp_property_names(degree))
        for property_name in property_names:
            if topomesh.nb_wisps(degree) > 0:
                if np.all([w in topomesh.wisp_property(property_name, degree).keys() for w in topomesh.wisps(degree)]):
                    topomesh.update_wisp_property(property_name, degree,
                                                  dict(zip(list(topomesh.wisps(degree)),
                                                           topomesh.wisp_property(property_name,degree).values(list(topomesh.wisps(degree))))))
            else:
                del topomesh._wisp_properties[degree][property_name]


def cell_topomesh(input_topomesh, cells=None, copy_properties=False, preserve_ids=True):

    # start_time = time()

    #topomesh = PropertyTopomesh(topomesh=input_topomesh)
    topomesh = PropertyTopomesh(3)

    if cells is None:
        cells = set(input_topomesh.wisps(3))
    else:
        cells = set(cells).intersection(set(input_topomesh.wisps(3)))

    wisp_index = {}
    wisp_index[3] = dict(zip(cells,range(len(cells))))

    faces = set()
    for c in cells:
        faces = faces.union(set(input_topomesh._borders[3][c]))
    wisp_index[2] = dict(zip(faces,range(len(faces))))

    edges = set()
    for f in faces:
        edges = edges.union(set(input_topomesh._borders[2][f]))
    wisp_index[1] = dict(zip(edges,range(len(edges))))

    vertices = set()
    for e in edges:
        vertices = vertices.union(set(input_topomesh._borders[1][e]))
    wisp_index[0] = dict(zip(vertices,range(len(vertices))))

    for c in cells:
        if preserve_ids:
            topomesh._borders[3][c] = deepcopy(input_topomesh._borders[3][c])
        else:
            topomesh._borders[3][wisp_index[3][c]] = [wisp_index[2][f] for f in input_topomesh._borders[3][c]]

    for f in faces:
        if preserve_ids:
            topomesh._borders[2][f] = deepcopy(input_topomesh._borders[2][f])
            topomesh._regions[2][f] = deepcopy(list(set(input_topomesh._regions[2][f]).intersection(cells)))
        else:
            topomesh._borders[2][wisp_index[2][f]] = [wisp_index[1][e] for e in input_topomesh._borders[2][f]]
            topomesh._regions[2][wisp_index[2][f]] = [wisp_index[3][c] for c in input_topomesh._regions[2][f] if c in cells]

    for e in edges:
        if preserve_ids:
            topomesh._borders[1][e] = deepcopy(input_topomesh._borders[1][e])
            topomesh._regions[1][e] = deepcopy(list(set(input_topomesh._regions[1][e]).intersection(faces)))
        else:
            topomesh._borders[1][wisp_index[1][e]] = [wisp_index[0][v] for v in input_topomesh._borders[1][e]]
            topomesh._regions[1][wisp_index[1][e]] = [wisp_index[2][f] for f in input_topomesh._regions[1][e] if f in faces]

    for v in vertices:
        if preserve_ids:
            topomesh._regions[0][v] = deepcopy(list(set(input_topomesh._regions[0][v]).intersection(edges)))
        else:
            topomesh._regions[0][wisp_index[0][v]] = [wisp_index[1][e] for e in input_topomesh._regions[0][v] if e in edges]

    if copy_properties:
        for degree in range(4):
            for property_name in input_topomesh.wisp_property_names(degree):
                input_property = input_topomesh.wisp_property(property_name,degree)
                property_keys = np.intersect1d(input_property.keys(),list(wisp_index[degree].keys()))
                if len(property_keys)>0:
                    if preserve_ids:
                        topomesh.update_wisp_property(property_name,degree,array_dict(input_topomesh.wisp_property(property_name,degree).values(property_keys),property_keys))
                    else:
                        property_key_indices = [wisp_index[degree][w] for w in property_keys]
                        topomesh.update_wisp_property(property_name,degree,array_dict(input_topomesh.wisp_property(property_name,degree).values(property_keys),property_key_indices))

    if preserve_ids:
        topomesh.update_wisp_property('barycenter',0,array_dict(input_topomesh.wisp_property('barycenter',0).values(list(vertices)),list(vertices)))
    else:
        topomesh.update_wisp_property('barycenter', 0,array_dict(input_topomesh.wisp_property('barycenter', 0).values(list(vertices)), [wisp_index[0][v] for v in vertices]))

    # cells_to_remove = [c for c in topomesh.wisps(3) if not c in cells]
    # cells_to_remove = list(set(topomesh.wisps(3)).difference(set(cells)))

    # for c in cells_to_remove:
    #     topomesh.remove_wisp(3,c)

    # faces_to_remove = [w for w in topomesh.wisps(2) if topomesh.nb_regions(2,w)==0]
    # for w in faces_to_remove:
    #     topomesh.remove_wisp(2,w)

    # edges_to_remove = [w for w in topomesh.wisps(1) if topomesh.nb_regions(1,w)==0]
    # for w in edges_to_remove:
    #     topomesh.remove_wisp(1,w)

    # vertices_to_remove = [w for w in topomesh.wisps(0) if topomesh.nb_regions(0,w)==0]
    # for w in vertices_to_remove:
    #     topomesh.remove_wisp(0,w)

    # end_time = time()
    # print("<-- Extracting cell topomesh     [",end_time-start_time,"s]")

    return topomesh


def star_interface_topomesh(topomesh, inner_faces=True, verbose=False):
    from time import time

    triangle_edge_list  = np.array([[1, 2],[0, 2],[0, 1]])

    triangular_topomesh = PropertyTopomesh(3)
    triangle_vertex_positions = {}

    for v in topomesh.wisps(0):
        triangular_topomesh.add_wisp(0,v)
        triangle_vertex_positions[v] = topomesh.wisp_property('barycenter',0)[v]

    for e in topomesh.wisps(1):
        triangular_topomesh.add_wisp(1,e)
        for v in topomesh.borders(1,e):
            triangular_topomesh.link(1,e,v)

    for c in topomesh.wisps(3):
        triangular_topomesh.add_wisp(3,c)

    compute_topomesh_property(topomesh,'regions',2)
    compute_topomesh_property(topomesh,'edges',2)
    compute_topomesh_property(topomesh,'vertices',1)
    compute_topomesh_property(topomesh,'vertices',2)

    face_centers = {}
    face_triangles = {}

    start_time = time()
    logging.debug("--> Triangulating Faces")
    for face in topomesh.wisps(2):
        if face%1 == 0:
            face_start_time = time()

        if topomesh.nb_borders(2,face)>0:
            face_cells = topomesh.wisp_property('regions',2)[face]
            face_edges = topomesh.wisp_property('vertices',1).values(topomesh.wisp_property('edges',2)[face])
            face_vertices = np.unique(face_edges)

            if (len(face_vertices)>3) and (inner_faces or (len(face_cells) == 1)):

                face_positions = array_dict(topomesh.wisp_property('barycenter',0).values(face_vertices),face_vertices)
                face_center = face_positions.values().mean(axis=0)

                center_pid = triangular_topomesh.add_wisp(0)
                triangle_vertex_positions[center_pid] = face_center

                face_centers[face] = center_pid
                face_triangles[face] = []

                vertex_center_edges = {}
                for v in face_vertices:
                    eid = triangular_topomesh.add_wisp(1)
                    triangular_topomesh.link(1,eid,v)
                    triangular_topomesh.link(1,eid,center_pid)
                    vertex_center_edges[v] = eid

                for e in topomesh.borders(2,face):
                    fid = triangular_topomesh.add_wisp(2)
                    face_triangles[face] += [fid]
                    triangular_topomesh.link(2,fid,e)
                    for v in topomesh.borders(1,e):
                        triangular_topomesh.link(2,fid,vertex_center_edges[v])
                    for cid in face_cells:
                        triangular_topomesh.link(3,cid,fid)

            elif (len(face_vertices)>2) and (inner_faces or (len(face_cells) == 1)):
                fid = triangular_topomesh.add_wisp(2)
                for e in topomesh.borders(2,face):
                    face_triangles[face] = [fid]
                    triangular_topomesh.link(2,fid,e)
                for cid in face_cells:
                    triangular_topomesh.link(3,cid,fid)

            if face%1 == 0:
                face_end_time = time()
                logging.debug("  --> Face "+str(face)+" / "+str(topomesh.nb_wisps(2))+'     ['+str(face_end_time-face_start_time)+'s]')

    end_time = time()

    # for property_name in topomesh.wisp_property_names(0):
    #     try:
    #         center_property = [topomesh.wisp_property(property_name,0).values(topomesh.wisp_property('vertices',2)[f]).mean(axis=0) for f in face_centers.keys()]
    #     except:
    #         center_property = [topomesh.wisp_property(property_name,0).values(topomesh.wisp_property('vertices',2)[f])[0] for f in face_centers.keys()]
    #     vertex_property = array_dict(list(topomesh.wisp_property(property_name,0).values(list(topomesh.wisps(0))))+center_property,list(topomesh.wisps(0))+[face_centers[f] for f in face_centers.keys()])
    #     triangular_topomesh.update_wisp_property(property_name,0,vertex_property)

    for property_name in topomesh.wisp_property_names(2):
        triangle_faces = np.concatenate([[f for t in face_triangles[f]] for f in face_triangles.keys()])
        triangle_keys = np.concatenate([face_triangles[f] for f in face_triangles.keys()])
        triangle_property = array_dict(topomesh.wisp_property(property_name,2).values(triangle_faces),triangle_keys)
        triangular_topomesh.update_wisp_property(property_name,2,triangle_property)

    for property_name in topomesh.wisp_property_names(3):
        triangular_topomesh.update_wisp_property(property_name,3,topomesh.wisp_property(property_name,3))

    logging.debug("--> Triangulating Faces  ["+str(end_time-start_time)+"s]")
    triangular_topomesh.update_wisp_property('barycenter',degree=0,values=triangle_vertex_positions)
    return triangular_topomesh


def delaunay_interface_topomesh(topomesh, ignore_properties=False, verbose=False):
    from time import time

    triangle_edge_list  = np.array([[1, 2],[0, 2],[0, 1]])

    triangular_topomesh = PropertyTopomesh(3)
    triangle_vertex_positions = {}

    for v in topomesh.wisps(0):
        triangular_topomesh.add_wisp(0, v)
        triangle_vertex_positions[v] = topomesh.wisp_property('barycenter', 0)[v]

    for e in topomesh.wisps(1):
        triangular_topomesh.add_wisp(1, e)
        for v in topomesh.borders(1, e):
            triangular_topomesh.link(1, e, v)

    for c in topomesh.wisps(3):
        triangular_topomesh.add_wisp(3, c)

    compute_topomesh_property(topomesh, 'cells', 2)
    compute_topomesh_property(topomesh, 'edges', 2)
    compute_topomesh_property(topomesh, 'vertices', 1)
    compute_topomesh_property(topomesh, 'vertices', 2)

    def project(points, center, normal_vector):
        vectors = points - center
        plane_vectors = {}
        plane_vectors[0] = np.cross(normal_vector, [1, 0, 0])
        if np.linalg.norm(plane_vectors[0]) < 1e-1:
            plane_vectors[0] = np.cross(normal_vector, [0, 1, 0])
        plane_vectors[1] = np.cross(normal_vector, plane_vectors[0])
        projected_points = np.transpose([np.einsum('ij,ij->i', vectors, plane_vectors[0][np.newaxis, :]), np.einsum('ij,ij->i', vectors, plane_vectors[1][np.newaxis, :]), np.zeros_like(points[:, 2])])
        return projected_points

    face_triangles = {}

    start_time = time()
    logging.debug("--> Triangulating Faces")
    for face in topomesh.wisps(2):
        if face%1 == 0:
            face_start_time = time()
            
        if topomesh.nb_borders(2, face) > 0:
            face_cells = topomesh.wisp_property('cells', 2)[face]
            face_edges = topomesh.wisp_property('vertices', 1).values(topomesh.wisp_property('edges', 2)[face])
            face_vertices = np.unique(face_edges)

            if len(face_vertices) > 3:

                face_positions = array_dict(topomesh.wisp_property('barycenter', 0).values(face_vertices), face_vertices)
                face_center = face_positions.values().mean(axis=0)

                face_covariance = np.cov(face_positions.values(),rowvar=False)
                face_eval, face_evec = np.linalg.eigh(face_covariance)
                face_normal = face_evec[:, np.argmin(np.abs(face_eval))]

                face_projected_points = project(face_positions.values(face_vertices),face_center,face_normal)
                face_delaunay_triangles = face_vertices[delaunay_triangulation(face_projected_points)]

                face_delaunay_edges = np.unique(np.sort(np.concatenate(face_delaunay_triangles[:,triangle_edge_list])),axis=0)

                face_edge_ids = {}
                for v1, v2 in face_delaunay_edges:
                    if v1 in triangular_topomesh.region_neighbors(0,v2):
                        eid = list(set(triangular_topomesh.regions(0,v1))&set(triangular_topomesh.regions(0,v2)))[0]
                    else:
                        eid = triangular_topomesh.add_wisp(1)
                        triangular_topomesh.link(1,eid,v1)
                        triangular_topomesh.link(1,eid,v2)
                    face_edge_ids[(v1, v2)] = eid

                face_triangles[face] = []
                for t in face_delaunay_triangles:
                    fid = triangular_topomesh.add_wisp(2)
                    for v1, v2 in np.sort(t[triangle_edge_list]):
                        if (v1,v2) in face_edge_ids:
                            triangular_topomesh.link(2,fid,face_edge_ids[(v1,v2)])
                    for cid in face_cells:
                        triangular_topomesh.link(3, cid, fid)
                    face_triangles[face] += [fid]
            else:
                fid = triangular_topomesh.add_wisp(2)
                for e in topomesh.borders(2, face):
                    face_triangles[face] = [fid]
                    triangular_topomesh.link(2, fid, e)
                for cid in face_cells:
                    triangular_topomesh.link(3, cid, fid)
                face_triangles[face] = [fid]

            if face % 1 == 0:
                face_end_time = time()
                logging.debug("  --> Face "+str(face)+" / "+str(topomesh.nb_wisps(2))+'     ['+str(face_end_time - face_start_time)+'s]')

    end_time = time()

    if not ignore_properties:
        for property_name in topomesh.wisp_property_names(2):
            triangle_faces = np.concatenate([[f for t in face_triangles[f]] for f in face_triangles.keys()])
            triangle_keys = np.concatenate([face_triangles[f] for f in face_triangles.keys()])
            triangle_property = array_dict(topomesh.wisp_property(property_name, 2).values(triangle_faces), triangle_keys)
            triangular_topomesh.update_wisp_property(property_name, 2, triangle_property)

        for property_name in topomesh.wisp_property_names(3):
            triangular_topomesh.update_wisp_property(property_name, 3, topomesh.wisp_property(property_name, 3))

    logging.debug("--> Triangulating Faces  ["+str(end_time - start_time)+"s]")
    triangular_topomesh.update_wisp_property('barycenter', degree=0, values=triangle_vertex_positions)
    return triangular_topomesh


def surface_dual_topomesh(topomesh, vertex_placement='center', exterior_vertex=None, exterior_distance=0, face_positions=None, vertices_as_cells=None):
    if vertices_as_cells is None:
        vertices_as_cells = topomesh.nb_wisps(3) != 1

    dual_topomesh = PropertyTopomesh(3)

    for f in topomesh.wisps(2):
        dual_topomesh.add_wisp(0,f)

    for e in topomesh.wisps(1):
        dual_topomesh.add_wisp(1,e)
        for f in topomesh.regions(1,e):
            dual_topomesh.link(1,e,f)

    for v in topomesh.wisps(0):
        if v != exterior_vertex:
            dual_topomesh.add_wisp(2,v)
            for e in topomesh.regions(0,v):
                dual_topomesh.link(2,v,e)

            if vertices_as_cells:
                dual_topomesh.add_wisp(3,v)
                dual_topomesh.link(3,v,v)
            else:
                for c in topomesh.regions(0,v,3):
                    if not dual_topomesh.has_wisp(3,c):
                        dual_topomesh.add_wisp(3,c)
                    dual_topomesh.link(3,c,v)

    for degree in [0,1,2]:
        for property_name in topomesh.wisp_property_names(degree):
            dual_topomesh.update_wisp_property(property_name,2-degree,topomesh.wisp_property(property_name,degree))

    if vertices_as_cells:
        for property_name in topomesh.wisp_property_names(0):
            dual_topomesh.update_wisp_property(property_name,3,topomesh.wisp_property(property_name,0))
    else:
        for property_name in topomesh.wisp_property_names(3):
            dual_topomesh.update_wisp_property(property_name,3,topomesh.wisp_property(property_name,3))


    compute_topomesh_property(topomesh,'barycenter',2)
    face_centers = topomesh.wisp_property('barycenter',2)
    positions = topomesh.wisp_property('barycenter',0)

    if is_triangular(topomesh):
        compute_topomesh_property(topomesh,'vertices',2)
        triangle_ids = np.array(list(topomesh.wisps(2)))
        triangles = topomesh.wisp_property('vertices',2).values(triangle_ids)

        if exterior_vertex is not None:
            exterior_triangle_ids = triangle_ids[np.where(np.any(triangles==exterior_vertex,axis=1))]
            exterior_triangles = triangles[np.where(np.any(triangles==exterior_vertex,axis=1))]
            exterior_edges = [t[t!=exterior_vertex] for t in exterior_triangles]
            exterior_edge_ids = np.concatenate([[e for e in topomesh.borders(2,t) if not exterior_vertex in topomesh.borders(1,e)] for t in exterior_triangle_ids])
            exterior_edge_triangle_ids = np.concatenate([[t for t in topomesh.regions(1,e) if not exterior_vertex in topomesh.borders(2,t,2)] for e in exterior_edge_ids])

            triangle_ids = triangle_ids[np.where(np.all(triangles!=exterior_vertex,axis=1))]
            triangles = triangles[np.where(np.all(triangles!=exterior_vertex,axis=1))]

        else:
            exterior_triangles = []
            exterior_triangle_ids = []
            exterior_edges = []
            exterior_edge_triangle_ids = []
            exterior_centers = []

        if vertex_placement == 'voronoi':
            centers = triangle_geometric_features(triangles,positions,['circumscribed_circle_center'])[:,0]
        elif vertex_placement == 'projected_voronoi':
            centers = triangle_geometric_features(triangles,positions,['projected_circumscribed_circle_center'])[:,0]
        elif vertex_placement == 'center':
            centers = triangle_geometric_features(triangles,positions,['barycenter'])[:,0]

        if exterior_vertex is not None:
            exterior_edge_centers = positions.values(exterior_edges).mean(axis=1)
            exterior_face_centers = face_centers.values(exterior_edge_triangle_ids.astype(int))
            exterior_face_dual_centers = array_dict(centers,triangle_ids).values(exterior_edge_triangle_ids.astype(int))

            exterior_edge_vectors = np.sign(np.einsum("...ij,...ij->...i",exterior_face_centers-exterior_edge_centers,exterior_face_dual_centers-exterior_edge_centers))[:,np.newaxis]*(exterior_edge_centers-exterior_face_dual_centers)
            logging.debug(str(exterior_edge_vectors))
            exterior_edge_vectors = exterior_edge_vectors/np.linalg.norm(exterior_edge_vectors,axis=1)[:,np.newaxis]
            exterior_dual_distance = np.linalg.norm(exterior_face_dual_centers-exterior_edge_centers,axis=1)

            #exterior_centers = face_centers.values(exterior_edge_triangle_ids) + exterior_coef*(positions.values(exterior_edges).mean(axis=1)-face_centers.values(exterior_edge_triangle_ids))
            exterior_centers = exterior_face_dual_centers + np.maximum(exterior_distance-exterior_dual_distance,0)[:,np.newaxis]*exterior_edge_vectors


        dual_topomesh.update_wisp_property('barycenter',0,array_dict(list(centers)+list(exterior_centers),list(triangle_ids)+list(exterior_triangle_ids)))
    else:
        dual_topomesh.update_wisp_property('barycenter',0,face_centers)

    edges_to_remove = [e for e in dual_topomesh.wisps(1) if dual_topomesh.nb_borders(1,e) != 2]
    for e in edges_to_remove:
        dual_topomesh.remove_wisp(1,e)

    return clean_topomesh(dual_topomesh)


def triangulation_add_exterior(triangulation_topomesh):
    assert is_triangular(triangulation_topomesh)

    # print np.min(list(triangulation_topomesh.wisps(0)))

    positions = triangulation_topomesh.wisp_property('barycenter',0)

    if not triangulation_topomesh.has_wisp(0,1):
        exterior_vertex = triangulation_topomesh.add_wisp(0,1)
    else:
        exterior_vertex = triangulation_topomesh.add_wisp(0,np.max(list(triangulation_topomesh.wisps(0)))+1)

    positions[exterior_vertex] = np.zeros(3)

    boundary_edges = [e for e in triangulation_topomesh.wisps(1) if triangulation_topomesh.nb_regions(1,e)==1]
    boundary_vertices = np.unique([list(triangulation_topomesh.borders(1,e)) for e in boundary_edges])

    exterior_edges = {}
    for v in boundary_vertices:
        e = triangulation_topomesh.add_wisp(1)
        triangulation_topomesh.link(1,e,v)
        triangulation_topomesh.link(1,e,exterior_vertex)
        exterior_edges[v] = e

    for e in boundary_edges:
        t = triangulation_topomesh.add_wisp(2)
        for v in triangulation_topomesh.borders(1,e):
            triangulation_topomesh.link(2,t,exterior_edges[v])
        triangulation_topomesh.link(2,t,e)

    triangulation_topomesh.update_wisp_property('barycenter',0,positions)

    compute_topomesh_property(triangulation_topomesh,'faces',0)
    compute_topomesh_property(triangulation_topomesh,'vertices',1)
    compute_topomesh_property(triangulation_topomesh,'regions',1)
    compute_topomesh_property(triangulation_topomesh,'faces',1)
    compute_topomesh_property(triangulation_topomesh,'cells',1)
    compute_topomesh_property(triangulation_topomesh,'vertices',2)
    compute_topomesh_property(triangulation_topomesh,'cells',2)
    compute_topomesh_property(triangulation_topomesh,'regions',2)
    compute_topomesh_property(triangulation_topomesh,'vertices',3)
    compute_topomesh_property(triangulation_topomesh,'edges',3)
    compute_topomesh_property(triangulation_topomesh,'faces',3)
    compute_topomesh_property(triangulation_topomesh,'vertices',3)
    compute_topomesh_property(triangulation_topomesh,'epidermis',2)

    return exterior_vertex


def triangulation_remove_exterior(triangulation_topomesh, exterior_vertex=1):
    assert is_triangular(triangulation_topomesh)

    triangles_to_remove = []
    for t in triangulation_topomesh.wisps(2):
        if exterior_vertex in triangulation_topomesh.borders(2,t,2):
            triangles_to_remove.append(t)
    edges_to_remove = []
    for e in triangulation_topomesh.wisps(1):
        if exterior_vertex in triangulation_topomesh.borders(1,e):
            edges_to_remove.append(e)
    triangulation_topomesh.remove_wisp(0,exterior_vertex)
    for e in edges_to_remove:
        triangulation_topomesh.remove_wisp(1,e)
    for t in triangles_to_remove:
        triangulation_topomesh.remove_wisp(2,t)

    triangulation_topomesh.update_wisp_property('barycenter',0,triangulation_topomesh.wisp_property('barycenter',0).values(list(triangulation_topomesh.wisps(0))),list(triangulation_topomesh.wisps(0)))

    compute_topomesh_property(triangulation_topomesh,'faces',0)
    compute_topomesh_property(triangulation_topomesh,'vertices',1)
    compute_topomesh_property(triangulation_topomesh,'regions',1)
    compute_topomesh_property(triangulation_topomesh,'faces',1)
    compute_topomesh_property(triangulation_topomesh,'cells',1)
    compute_topomesh_property(triangulation_topomesh,'vertices',2)
    compute_topomesh_property(triangulation_topomesh,'cells',2)
    compute_topomesh_property(triangulation_topomesh,'regions',2)
    compute_topomesh_property(triangulation_topomesh,'vertices',3)
    compute_topomesh_property(triangulation_topomesh,'edges',3)
    compute_topomesh_property(triangulation_topomesh,'faces',3)
    compute_topomesh_property(triangulation_topomesh,'vertices',3)
    compute_topomesh_property(triangulation_topomesh,'epidermis',2)


def topomesh_connected_wisps(topomesh, degree=2):
    # if topomesh.nb_wisps(3)>0:
    #     new_component = np.max(list(topomesh.wisps(3)))+1
    # else:
    #     new_component = 0
    # component_cells = []

    if degree == 2:
        considered_fids = set()
        component_fids = []
        for fid in topomesh.wisps(2):
            if not fid in considered_fids:
                component_fids += [[fid]]
                #c = new_component
                #topomesh.add_wisp(3,c)
                #topomesh.link(3,c,fid)
                considered_fids |= {fid}
                neighbor_fids = list(topomesh.border_neighbors(2,fid))
                while(len(neighbor_fids) > 0):
                    n_fid = neighbor_fids.pop()
                    if not n_fid in considered_fids:
                        #topomesh.link(3,c,n_fid)
                        component_fids[-1] += [n_fid]
                        considered_fids |= {n_fid}
                        neighbor_fids += list(set(list(topomesh.border_neighbors(2,n_fid))).difference(considered_fids))
                #component_cells += [c]
                #new_component += 1
        return component_fids

    elif degree == 0:
        considered_vids = set()
        component_vids = []
        vertex_components = {}
        for vid in topomesh.wisps(0):
            if not vid in considered_vids:
                component_vids += [[vid]]
                # c = new_component
                # vertex_components[vid] = c
                considered_vids |= {vid}
                neighbor_vids = list(topomesh.region_neighbors(0,vid))
                while(len(neighbor_vids) > 0):
                    n_vid = neighbor_vids.pop()
                    if not n_vid in considered_vids:
                        component_vids[-1] += [n_vid]
                        # vertex_components[n_vid] = c
                        considered_vids |= {n_vid}
                        neighbor_vids += list(set(list(topomesh.region_neighbors(0,n_vid))).difference(considered_vids))
                # component_cells += [c]
                # new_component += 1
        # topomesh.update_wisp_property('component',0,vertex_components)
        return component_vids


def topomesh_connected_components(topomesh, degree=2):
    component_wisps = topomesh_connected_wisps(topomesh, degree=degree)

    component_meshes = [sub_topomesh(topomesh, degree, elements_to_keep=w) for w in component_wisps]
    component_size = [component.nb_wisps(degree) for component in component_meshes]

    return [component_meshes[i] for i in np.argsort(component_size)][::-1]
