{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Computing Geometrical Properties on a PropertyTopomesh\n",
    "\n",
    "In CellComplex, cell tissues are represented as meshes (more exactly cellular complexes implemented as incidence graphs) in an object called **PropertyTopomesh**. This data structure contains the geometry and topology of the cells, and can also bear other properties defined on each of its elements (vertices, edges, faces or cells).\n",
    "\n",
    "The CellComplex.PropertyTopomesh library comes with tools to automatically compute a whole range of geometrical properties on the tissue mesh (*compute_topomesh_property*) and to propagate properties between elements of different dimensions (*compute_topomesh_vertex_property_from_faces* for instance)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import os\n",
    "\n",
    "from cellcomplex.property_topomesh.analysis import compute_topomesh_property\n",
    "from cellcomplex.property_topomesh.analysis import compute_topomesh_vertex_property_from_faces\n",
    "from cellcomplex.property_topomesh.analysis import compute_topomesh_cell_property_from_faces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to construct a **PropertyTopomesh** from standard mesh file formats, such as PLY. Here an example of a meristematic L1 tissue saved as a PLY file, provided as an example in the *share/data* directory of the package, is loaded."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cellcomplex.property_topomesh\n",
    "dirname = os.path.abspath(cellcomplex.property_topomesh.__path__[0]+\"/../../../share/data\")\n",
    "print(dirname)\n",
    "filename = os.path.join(dirname,\"p194-t4_L1_topomesh.ply\")\n",
    "\n",
    "from cellcomplex.property_topomesh.io import read_ply_property_topomesh\n",
    "topomesh = read_ply_property_topomesh(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tissue is a surfacic triangular mesh where triangles are linked to a labelled cell. Each label can be mapped to an independent color for visualization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specific function for 3D visualization in notebooks\n",
    "#from cellcomplex.property_topomesh.triangular_mesh import topomesh_to_triangular_mesh\n",
    "\n",
    "#mesh,_ = topomesh_to_triangular_mesh(topomesh,3,coef=0.95)\n",
    "#mesh._repr_vtk_()\n",
    "\n",
    "%matplotlib notebook\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh\n",
    "\n",
    "figure = plt.figure(0)\n",
    "figure.clf()\n",
    "\n",
    "mpl_draw_topomesh(topomesh,figure,3,colormap='jet')\n",
    "mpl_draw_topomesh(topomesh,figure,1,cell_edges=True,color='w')\n",
    "\n",
    "figure.gca().axis('equal')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "compute_topomesh_property?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on this initial mesh, we would like to compute the curvature of the surface. To do so, we will use the function *compute_topomesh_property* to compute the geometrical properties allowing to obtain this information.\n",
    "\n",
    "The first property to compute is the **normal** of the faces (elements of degree = 2). Given the implementation as an incidence graph, the order of vertices for each face is not unambiguous, hence the orientation of faces is not contained in the surface. It is necessary to have a way of re-orienting consistently the normals. Here we choose the *orientation* mode that propagates the orientation infomration in a topologically accurate way on a connected surfacic mesh.\n",
    "\n",
    "Next, we compute the **area** of all faces, and use it to compute the **normal** property on the vertices (elements of degree = 0). This is done by averaging the normals of faces neighboring each vertex using the function *compute_topomesh_vertex_property_from_faces*.\n",
    "\n",
    "Once each vertex bears a consistent normal, it is possible to compute the **mean curvature** of the each face (actually the **3D curvature tensor** containing all the information on the principal curvatures, in particular mean, gaussian, min and max curvatures). This property can be propagated to the vertices using the same method as for the normals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "compute_topomesh_property(topomesh,'normal',2,normal_method='orientation')\n",
    "compute_topomesh_property(topomesh,'area',2)\n",
    "compute_topomesh_vertex_property_from_faces(topomesh,'normal',weighting='area',adjacency_sigma=1.2,neighborhood=3)\n",
    "compute_topomesh_property(topomesh,'mean_curvature',2)\n",
    "compute_topomesh_vertex_property_from_faces(topomesh,'mean_curvature',weighting='area',adjacency_sigma=1.2,neighborhood=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This numerical property can be visualized on the triangles of the mesh using the property on the vertices, creating a smooth interpolation of curvature values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "figure = plt.figure(1)\n",
    "figure.clf()\n",
    "\n",
    "mpl_draw_topomesh(topomesh,figure,2,property_name='mean_curvature',intensity_range=(-0.05,0.05),colormap='RdBu_r')\n",
    "figure.gca().axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, the mean curvature could be propagated to the cells (elements of degree = 3) to have unique value for each cell of the tissue. This value can then be conveniently visualized on a representation of the mesh that specifically isolates the cells."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(compute_topomesh_cell_property_from_faces.__doc__)\n",
    "\n",
    "compute_topomesh_cell_property_from_faces(topomesh,'mean_curvature')\n",
    "\n",
    "figure = plt.figure(2)\n",
    "figure.clf()\n",
    "\n",
    "mpl_draw_topomesh(topomesh,figure,3,property_name='mean_curvature',intensity_range=(-0.05,0.05),colormap='RdBu_r')\n",
    "mpl_draw_topomesh(topomesh,figure,1,cell_edges=True,color='w')\n",
    "\n",
    "figure.gca().axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All the properties are stored inside the topomesh that can then be saved with only the desired properties in a standard PLY file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "save_filename = os.path.join(dirname,\"p194-t4_L1_topomesh_curvature.ply\")\n",
    "\n",
    "from cellcomplex.property_topomesh.io import save_ply_property_topomesh\n",
    "save_ply_property_topomesh(topomesh,save_filename,\n",
    "                           properties_to_save=dict([(0,[]),(1,[]),(2,[]),(3,['mean_curvature'])]))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
