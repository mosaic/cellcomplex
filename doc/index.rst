.. include:: ../README.rst

Examples
--------

.. toctree::

    examples/property_topomesh
    examples/creation
    examples/topology
    examples/analysis


Contents
--------

* Main modules

.. autosummary::

   cellcomplex.property_topomesh.io
   cellcomplex.property_topomesh.analysis
   cellcomplex.property_topomesh.topological_operations
   cellcomplex.property_topomesh.optimization

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`