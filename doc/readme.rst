Install
=======

Download sources and use setup::

    $ python setup.py install
    or
    $ python setup.py develop


Use
===

Simple usage:

.. code-block:: python

    from cellcomplex.property_topomesh import PropertyTopomesh


Contribute
==========

Fork this project on gitlab_

.. _gitlab: https://gitlab.inria.fr/mosaic/cellcomplex



Acknowledgments
===============
