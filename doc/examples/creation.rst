.. toctree::
   :maxdepth: 2

.. _examples-creation:

################################################################################
Creation
################################################################################

Though cellcomplex objects can be constructed manually by enumerating elements at each dimension and explicitly providing the boundary relationships between them (see :ref:`examples-property-topomesh-link` for more details) the most common way to create a :class:`PropertyTopomesh` is through pre-built functions that can be found in the :mod:`property_topomesh_creation <cellcomplex.property_topomesh.creation>` module.

************************
Basic creation functions
************************

The most basic functions to create a :class:`PropertyTopomesh` are the ones that create a simplicial complex from arrays of elelments. These complexes may consist of:
    * only vertices (:func:`vertex_topomesh <cellcomplex.property_topomesh.creation.vertex_topomesh>`)
    * edges and vertices (:func:`edge_topomesh <cellcomplex.property_topomesh.creation.edge_topomesh>`)
    * triangular faces, edges and vertices (:func:`triangle_topomesh <cellcomplex.property_topomesh.creation.triangle_topomesh>`)
    * tetrahedral cells, triangular faces, edges and vertices (:func:`tetrahedra_topomesh <cellcomplex.property_topomesh.creation.tetrahedra_topomesh>`)

The :func:`vertex_topomesh <cellcomplex.property_topomesh.creation.vertex_topomesh>` function only inputs an array of 3d **vertex points** of shape :math:`(V,3)` and creates a :class:`PropertyTopomesh` containing :math:`V` ``wisps`` of dimension 0 indexed by the array rows, and associated to the corresponding array point.

.. code-block:: python
    :linenos:

    import numpy as np

    from cellcomplex.property_topomesh.creation import vertex_topomesh

    n_rows = 4
    n_cols = 4

    point_x = [c + (r%2)/2.      for r in range(n_rows) for c in range(n_cols)]
    point_y = [r * np.sqrt(3)/2. for r in range(n_rows) for c in range(n_cols)]
    point_z = [0.                for r in range(n_rows) for c in range(n_cols)]
    points = np.transpose([point_x,point_y,point_z])

    topomesh = vertex_topomesh(points)

.. plot::

    import numpy as np
    import matplotlib.pyplot as plt

    from cellcomplex.property_topomesh.creation import vertex_topomesh

    from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh

    n_rows = 4
    n_cols = 4

    point_x = [c + (r%2)/2.      for r in range(n_rows) for c in range(n_cols)]
    point_y = [r * np.sqrt(3)/2. for r in range(n_rows) for c in range(n_cols)]
    point_z = [0.                for r in range(n_rows) for c in range(n_cols)]
    points = np.transpose([point_x,point_y,point_z])

    topomesh = vertex_topomesh(points)

    figure = plt.figure(0)
    figure.clf()

    figure.gca().axis('equal')

    #mpl_draw_topomesh(topomesh,figure,2,color='g')
    #mpl_draw_topomesh(topomesh,figure,1,color='b')
    mpl_draw_topomesh(topomesh,figure,0,color='m',plot_ids=True)

    figure.gca().axis('off')
    figure.tight_layout()

In :func:`edge_topomesh <cellcomplex.property_topomesh.creation.edge_topomesh>`, we still need to input an array of points that will define the ``wisps`` of dimension 0 as previously, but this time the function requires an array of vertex IDs of shape :math:`(E,2)` that represents the edges between vertices of the array. The IDs contained in this **edge array** correspond to the line indices of the vertex point array, and therefore to the ``wisp`` IDs of the :class:`PropertyTopomesh`. The resulting complex contains :math:`E` ``wisps`` of dimension 1 with IDs corresponding to the indices in the edge array.

.. code-block:: python
    :linenos:

    from cellcomplex.property_topomesh.creation import edge_topomesh

    edges = []
    edges += [[c + (r+1)*n_cols      , c + r*n_cols          ] for r in range(n_rows-1) for c in range(n_cols)  ]
    edges += [[c + (r+1)*n_cols + r%2, c + r*n_cols + (r+1)%2] for r in range(n_rows-1) for c in range(n_cols-1)]
    edges += [[c + r*n_cols          , c + r*n_cols + 1      ] for r in range(n_rows)   for c in range(n_cols-1)]

    topomesh = edge_topomesh(edges,points)

.. plot::

    import numpy as np
    import matplotlib.pyplot as plt

    from cellcomplex.property_topomesh.creation import edge_topomesh

    from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh

    n_rows = 4
    n_cols = 4

    point_x = [c + (r%2)/2.      for r in range(n_rows) for c in range(n_cols)]
    point_y = [r * np.sqrt(3)/2. for r in range(n_rows) for c in range(n_cols)]
    point_z = np.zeros_like(point_x)
    points = np.transpose([point_x,point_y,point_z])

    edges = []
    edges += [[c + (r+1)*n_cols      , c + r*n_cols          ] for r in range(n_rows-1) for c in range(n_cols)  ]
    edges += [[c + (r+1)*n_cols + r%2, c + r*n_cols + (r+1)%2] for r in range(n_rows-1) for c in range(n_cols-1)]
    edges += [[c + r*n_cols          , c + r*n_cols + 1      ] for r in range(n_rows)   for c in range(n_cols-1)]

    topomesh = edge_topomesh(edges,points)

    figure = plt.figure(0)
    figure.clf()

    figure.gca().axis('equal')

    #mpl_draw_topomesh(topomesh,figure,2,color='g')
    mpl_draw_topomesh(topomesh,figure,1,color='b',plot_ids=True)
    mpl_draw_topomesh(topomesh,figure,0,color='m')

    figure.gca().axis('off')
    figure.tight_layout()

.. note::

    Those two functions require the user to provide the list(s) of all the elements that will finally be part of the complex. One may think that they have little added-value compared to actually performing the :func:`link <cellcomplex.property_topomesh.topomesh.Topomesh.link>` operations one by one, even if they are convenient ways to use results of other computations. However, in the following examples, the link between user-provided elements is not direct in the resulting complex, there is a real need for an automated creation procedure.

In most standard mesh representations (OpenGL to start with) and file formats (`.obj <https://www.fileformat.info/format/wavefrontobj/egff.htm>`_, `.ply <http://paulbourke.net/dataformats/ply>`_, `.msh <http://www.manpagez.com/info/gmsh/gmsh-2.2.6/gmsh_63.php>`_) faces are defined by an ordered list of vertices. Typically, triangle meshes often come as a list of vertex points and a list a 3-uples of vertex IDs defining the triangles. In a :class:`PropertyTopomesh` however, faces are defined only by the set of their boundary edges. The :func:`triangle_topomesh <cellcomplex.property_topomesh.creation.triangle_topomesh>` function is essentially here to create easily a :class:`PropertyTopomesh` from a vertex list type of representation that is omnipresent when we manipulate mesh data. It inputs an array of vertex points and a array of shape :math:`(T,3)` defining triangles using vertex indices.

.. code-block:: python
    :linenos:

    from cellcomplex.property_topomesh.creation import triangle_topomesh

    triangles = []
    triangles += [[c + (r+1)*n_cols, c + (r+1)*n_cols + 1, c + r*n_cols + (r+1)%2] for r in range(n_rows-1) for c in range(n_cols-1)]
    triangles += [[c + r*n_cols    , c + r*n_cols + 1    , c + (r+1)*n_cols + r%2] for r in range(n_rows-1) for c in range(n_cols-1)]

    topomesh = triangle_topomesh(triangles,points)

.. plot::

    import numpy as np
    import matplotlib.pyplot as plt

    from cellcomplex.property_topomesh.creation import triangle_topomesh

    from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh

    n_rows = 4
    n_cols = 4

    point_x = [c + (r%2)/2.      for r in range(n_rows) for c in range(n_cols)]
    point_y = [r * np.sqrt(3)/2. for r in range(n_rows) for c in range(n_cols)]
    point_z = np.zeros_like(point_x)
    points = np.transpose([point_x,point_y,point_z])

    triangles = []
    triangles += [[c + (r+1)*n_cols, c + (r+1)*n_cols + 1, c + r*n_cols + (r+1)%2] for r in range(n_rows-1) for c in range(n_cols-1)]
    triangles += [[c + r*n_cols    , c + r*n_cols + 1    , c + (r+1)*n_cols + r%2] for r in range(n_rows-1) for c in range(n_cols-1)]

    topomesh = triangle_topomesh(triangles,points)

    figure = plt.figure(0)
    figure.clf()

    figure.gca().axis('equal')

    mpl_draw_topomesh(topomesh,figure,2,color='g',plot_ids=True)
    mpl_draw_topomesh(topomesh,figure,1,color='b')
    mpl_draw_topomesh(topomesh,figure,0,color='m')

    figure.gca().axis('off')
    figure.tight_layout()

The same idea is used for 3D tetrahedral mesh in the function :func:`tetrahedra_topomesh <cellcomplex.property_topomesh.creation.tetrahedra_topomesh>` that inputs an array of vertex points and an array of tetrahedras fo shape :math:`(T,4)` that will be used to define :math:`T` ``wisps`` of dimension 3, as well as all the ``wisps`` of dimension 2 and 1 needed to make the links down to the vertices.

.. note::

    As quadrilateral meshes are also a very common representation, there is also a :func:`quad_topomesh <cellcomplex.property_topomesh.creation.quad_topomesh>` method that inputs an array of shape :math:`(Q,4)` to generate a :class:`PropertyTopomesh` where the quads are the ``wisps`` of dimension 2.

********************************
Creating a not-so-simple complex
********************************

Sometimes, it is more convenient to define a complex by hand, defining the vertex points one by one and faces as lists of vertex of arbitrary length. The function :func:`poly_topomesh <cellcomplex.property_topomesh.creation.poly_topomesh>` is a versatile tool that allows just that, and makes possible to design custom shapes at a high level.

.. code-block:: python
    :linenos:

    import numpy as np

    from cellcomplex.property_topomesh.creation import poly_topomesh

    points = {}
    points[0]  = np.array([ -2, -2,  0])
    points[1]  = np.array([ -2,  2,  0])
    points[2]  = np.array([  2,  2,  0])
    points[3]  = np.array([  2, -2,  0])
    points[4]  = np.array([  0,  0,  0])
    points[5]  = np.array([ -1,  1,  0])
    points[6]  = np.array([  1, -1,  0])
    points[7]  = np.array([  1,  1,  0])
    points[8]  = np.array([ -1, -1,  0])
    points[9]  = np.array([  0,  2,  0])
    points[10] = np.array([  2,  0,  0])
    points[11] = np.array([  0, -2,  0])
    points[12] = np.array([ -2,  0,  0])

    polys = []
    polys += [[ 1, 5, 4, 8, 0,12]]
    polys += [[ 0, 8 ,4, 6, 3,11]]
    polys += [[ 4, 5, 7]]
    polys += [[ 1, 9, 7, 5]]
    polys += [[ 3, 6,10]]
    polys += [[ 2,10, 7, 9]]
    polys += [[ 4, 7,10, 6]]

    topomesh = poly_topomesh(polys,points)

.. plot::

    import numpy as np
    import matplotlib.pyplot as plt

    from cellcomplex.property_topomesh.creation import poly_topomesh

    from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh

    points = {}
    points[0]  = np.array([ -2, -2,  0])
    points[1]  = np.array([ -2,  2,  0])
    points[2]  = np.array([  2,  2,  0])
    points[3]  = np.array([  2, -2,  0])
    points[4]  = np.array([  0,  0,  0])
    points[5]  = np.array([ -1,  1,  0])
    points[6]  = np.array([  1, -1,  0])
    points[7]  = np.array([  1,  1,  0])
    points[8]  = np.array([ -1, -1,  0])
    points[9]  = np.array([  0,  2,  0])
    points[10] = np.array([  2,  0,  0])
    points[11] = np.array([  0, -2,  0])
    points[12] = np.array([ -2,  0,  0])

    polys = []
    polys += [[ 1, 5, 4, 8, 0,12]]
    polys += [[ 0, 8 ,4, 6, 3,11]]
    polys += [[ 4, 5, 7]]
    polys += [[ 1, 9, 7, 5]]
    polys += [[ 3, 6,10]]
    polys += [[ 2,10, 7, 9]]
    polys += [[ 4, 7,10, 6]]

    topomesh = poly_topomesh(polys,points)

    figure = plt.figure(0)
    figure.clf()

    figure.gca().axis('equal')

    mpl_draw_topomesh(topomesh,figure,2,color='g',plot_ids=True)
    mpl_draw_topomesh(topomesh,figure,1,color='b',plot_ids=True)
    mpl_draw_topomesh(topomesh,figure,0,color='m',plot_ids=True)

    figure.gca().axis('off')

    figure.tight_layout()

***********
Dualization
***********

The dualization of a cellular complex is an apparently non trivial operation where lowest dimension elements of the initial complex correspond the highest dimension elements in its dual, and vice versa. For instance faces in the original complex will be converted into points in the dual. Interfaces between faces (edges) will be converted into adjacency edges between the two dual vertices. With the incidence graph representation, the dualization becomes direct, since it simply consists in reversing element dimensions and boundary relationships. Borders become regions, and regions become borders. The function :func:`dual_topomesh <cellcomplex.property_topomesh.creation.dual_topomesh>` takes a :class:`PropertyTopomesh` instance and returns its corresponding dual cellular complex, with either faces or cells considered as highest dimension elements. The only constraint is that the resulting complex should be valid (edges with 2 vertices, faces with at least 3 edges). Therefore only the elements of the original complex that satisfy those topological constraints will be kept in the dual cellular complex.

.. code-block:: python
    :linenos:

    from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh
    from cellcomplex.property_topomesh.creation import dual_topomesh

    topomesh = hexagonal_grid_topomesh()
    dual = dual_topomesh(topomesh,degree=2)


.. plot::

    import numpy as np
    import matplotlib.pyplot as plt

    from cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh
    from cellcomplex.property_topomesh.creation import dual_topomesh

    from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh, mpl_draw_incidence_graph

    topomesh = hexagonal_grid_topomesh()
    dual = dual_topomesh(topomesh,degree=2)

    figure = plt.figure(0)
    figure.clf()

    figure.add_subplot(2,2,1)
    figure.gca().axis('equal')

    mpl_draw_topomesh(topomesh,figure,2,color='g',plot_ids=True)
    mpl_draw_topomesh(topomesh,figure,1,color='b',plot_ids=True)
    mpl_draw_topomesh(topomesh,figure,0,color='m',plot_ids=True)

    figure.gca().axis('off')

    figure.add_subplot(2,2,2)
    figure.gca().axis('equal')
    mpl_draw_incidence_graph(topomesh,figure,degree=2,plot_ids=True)
    figure.gca().axis('off')

    figure.add_subplot(2,2,3)
    figure.gca().axis('equal')

    mpl_draw_topomesh(topomesh,figure,1,color='k',alpha=0.2)
    mpl_draw_topomesh(dual,figure,2,color='g',plot_ids=True)
    mpl_draw_topomesh(dual,figure,1,color='b',plot_ids=True)
    mpl_draw_topomesh(dual,figure,0,color='m',plot_ids=True)

    figure.gca().axis('off')

    figure.add_subplot(2,2,4)
    figure.gca().axis('equal')
    mpl_draw_incidence_graph(dual,figure,degree=2,plot_ids=True)
    figure.gca().axis('off')

    figure.set_size_inches(15,15)
    figure.tight_layout()