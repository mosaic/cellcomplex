============================
CellComplex Python Library
============================

.. image:: https://anaconda.org/mosaic/cellcomplex/badges/version.svg
   :target: https://anaconda.org/mosaic/cellcomplex

.. image:: https://anaconda.org/mosaic/cellcomplex/badges/platforms.svg
   :target: https://anaconda.org/mosaic/cellcomplex

.. image:: https://anaconda.org/mosaic/cellcomplex/badges/license.svg
   :target: https://anaconda.org/mosaic/cellcomplex

.. image:: https://gitlab.inria.fr/mosaic/cellcomplex/badges/develop/pipeline.svg
   :target: https://gitlab.inria.fr/mosaic/cellcomplex/commits/develop

.. image:: https://gitlab.inria.fr/mosaic/cellcomplex/badges/develop/coverage.svg
   :target: https://gitlab.inria.fr/mosaic/cellcomplex/commits/develop


.. {# pkglts, doc


.. image:: https://travis-ci.org/mosaic/cellcomplex.svg?branch=master
    :alt: Travis build status
    :target: https://travis-ci.org/mosaic/cellcomplex


.. image:: https://readthedocs.org/projects/cellcomplex/badge/?version=latest
    :alt: Documentation status
    :target: https://cellcomplex.readthedocs.io/en/latest/?badge=latest
.. #}

The Topological Plant Tissue datastructure

:Coordination: Guillaume Cerutti, Christophe Godin
:Contributors: Frederic Boudon, Jerome Chopard, André-Claude Clapson, Hadrien Oliveri, Florian Gacon

:Active team: Inria project team `Mosaic <https://team.inria.fr/mosaic/>`_

:Former team: Inria-Cirad-Inra `Virtual Plants <https://team.inria.fr/virtualplants/>`_

:Institutes: `Inria <http://www.inria.fr>`_, `INRA <https://inra.fr>`_, `Cirad <http://www.cirad.fr>`_

:Language: Python

:Supported OS: Linux, MacOS

:Licence: `LGPL-3.0-or-later`


Description
-----------

CellComplex is a library providing data structures and algorithms to represent and analyze plant cell tissues in 3D. It offers an implementation of the topological structure of cellular complex as an Incidence Graph in a class named :class:`PropertyTopomesh <cellcomplex.property_topomesh.PropertyTopomesh>`.

.. image:: https://gitlab.inria.fr/uploads/-/system/project/avatar/6011/LogoCellComplex.png?width=64
    :width: 200px


The structure comes with algorithms allowing to
	* Create a structure from more basic representations
	* Compute geometrical and topological properties and store them within the structure
	* Edit the structure by local topological operations
	* Read and export the structure from/to a `standard PLY format <http://sainsburyworkshop2015.wikispaces.com/file/view/PlyFormat.pdf/>`_


Requirements
------------

* Python 3.6+
* NumPy
* SciPy
* *Matplotlib* (optional)
* *VTK* (optional)


Installation
------------

* From sources:

.. code-block:: bash

	python setup.py develop


* On `Anaconda Cloud <https://anaconda.org/mosaic/cellcomplex>`_:

.. code-block:: bash

	conda install -c mosaic cellcomplex



