# {# pkglts, pysetup
# requirements are managed by pkglts, do not edit this file at all
# edit .pkglts/pkg_cfg instead
# section pysetup

# doc
sphinx


# dvlpt


# install
matplotlib
numpy
pandas
scipy


# test
coverage
pytest
pytest-cov
pytest-mock


# #}
